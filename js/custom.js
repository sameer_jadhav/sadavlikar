var itemWidth = $(".mobile .articles-list .item").outerWidth();
var artListWidth = 0;
var autoSlide = 0;

//Mobile menu btn
function mobMenu() {
    $(".menu-btn").click(function() {
        $(this).toggleClass("active");
        if ($(this).hasClass("active")) {
            $(".nav-bar").addClass("active");
        } else {
            $(".nav-bar").removeClass("active");
        }
    })
}

//Track Shipment mob
function mobTrackShipment() {
    $(".track-link").click(function() {
        $(".mobile .mod-track-form").addClass("active");
    });
    $(".mod-track-form .close-btn").click(function() {
        $(".mobile .mod-track-form").removeClass("active");
    })
}

// Article section mobile slider - auto slide function

function autoSlider() {
    var currentPos = $(".articles-list .item").position().left;
    var currentHlght = $(".article-slider .slide-btn span.active").index();
    if (currentPos > (itemWidth - artListWidth) && currentHlght < $(".article-slider .slide-btn span").length) {
        $(".mobile .articles-list .item").css({ "left": -(itemWidth - currentPos), "transition": "all 0.8s" });
        $(".article-slider .slide-btn span").removeClass("active");
        $(".article-slider .slide-btn span").eq(currentHlght + 1).addClass("active");
    } else {
        $(".mobile .articles-list .item").css({ "left": 0, "transition": "none" });
        $(".article-slider .slide-btn span").removeClass("active");
        $(".article-slider .slide-btn span").eq(0).addClass("active");
    }
}

// Article section mobile slider
function articleSlider() {
    $(".mobile .articles-list .item").each(function() {
        artListWidth += $(this).outerWidth();
    });
    $(".mobile .articles-list").css("width", artListWidth);
    $(".mobile .article-slider").css("width", itemWidth);
    $(".article-slider .slide-btn span").click(function() {
        clearInterval(autoSlide);
        $(".article-slider .slide-btn span").removeClass("active");
        $(".mobile .articles-list .item").css({ "left": -(itemWidth * $(this).index()) });
        $(this).addClass("active");
        autoSlide = setInterval("autoSlider()", 5000);
    })
}

// Advertise section show hide

(function adSection() {
    $(".lyt-advertise-with-us .cont").slideUp();
    $(".btn-ad").on("click", function() {
        var obj = this;
        $(obj).parent().find(".cont").slideToggle();
        var swiper = new Swiper('.lyt-advertise-with-us .swiper-container', {
            pagination: '.swiper-pagination',
            slidesPerView: 1,
            paginationClickable: true,
            autoplay: 3000
                // breakpoints: {
                //     768: {
                //         slidesPerView: 1
                //     }
                // }
        });
    });
    $(".js-close").on("click", function() {
        var obj = this;
        $(obj).parent().removeClass("active");
    })
})();

// Submenu show hide

function submenuToggle() {
    $(".js-submenu-active").on("click", function() {
        var obj = this;
        $(obj).parent().find(".sub-menu-wrap").addClass("active");
    });
    $(".sub-menu-wrap .close-btn").on("click", function() {
        var obj = this;
        $(obj).parent().removeClass("active");
    })
}

// Map function

(function mapShow() {
    $(".bs-tab.typ-map .nav-tabs li a").on("click", function(e) {
        var obj = $(this);
        var currIndx = obj.parent().index();
        $(".bs-tab.typ-map .tab-content .address-map .location-points .list").removeClass("active");
        $(".bs-tab.typ-map .tab-content .address-map .location-points .list").eq(currIndx).addClass("active");
    });
    $(".location-points .point").on("click", function() {
        var obj = $(this);
        // console.log(obj.parent());
        if (!(obj.parent().hasClass("active"))) {
            obj.parents(".location-points").find(".list.active .item").removeClass("active");
            $(obj.data("target")).click();
            obj.parent().addClass("active");
        }
    });
    $(".mod-address .title-wrap").on("click", function() {
        var obj = $(this);
        var currId = "#" + obj.attr("id");
        // console.log(currId);
        $(".location-points .list.active .item").removeClass("active");
        $(".location-points .list.active .point").each(function() {
            if ($(this).attr("data-target") == currId) {
                $(this).parent().addClass("active");
            }
            // console.log($(this));
        })
    })
})();

/* Document Ready */
$(function() {
    $("a").each(function() {
        if ($(this).attr('href') == '#' || $(this).attr('href') == ' ') {
            $(this).attr('href', 'javascript:void(0)');
        }
    });

    setTimeout(function() {
        $('.desktop .articles-list').masonry({
            itemSelector: '.item',
        });
        $('.ipad .articles-list').masonry({
            itemSelector: '.item',
        });
    }, 1000);


    var winHeight = $(window).height();
    var totalHeight = winHeight - 85;
    $(".home .mod-banner.banner1").css("height", totalHeight);
    $(".ipad .mod-banner.banner1").css("height", totalHeight);
    $(".mobile .mod-banner.banner1").css("height", "inherit");

    //Mobile menu btn
    mobMenu();

    // Article section mobile slider - auto slide

    autoSlide = setInterval("autoSlider()", 5000);

    // Article section mobile slider
    articleSlider();

    //Track Shipment mob
    if ($(".mobile .mod-track-form").length != 0) {
        mobTrackShipment();
    }

    if ($(".mobile").length != 0) {
        submenuToggle();
    }

    var swiper = new Swiper('.mobile .swiper-container', {
        slidesPerView: 'auto',
        simulateTouch: true
    });

    var swiper = new Swiper('.mod-partners .swiper-container', {
        pagination: '.swiper-pagination',
        slidesPerView: 'auto',
        paginationClickable: true,
        autoplay: 2000,
        simulateTouch: true,
        breakpoints: {
            768: {
                autoplay: 2000,
                slidesPerView: 3
            }
        }
    });

    var swiper = new Swiper('.lyt-advertise-with-us .swiper-container', {
        pagination: '.swiper-pagination',
        slidesPerView: 1,
        paginationClickable: true,
        autoplay: 20000,
        simulateTouch: true,
        breakpoints: {
            768: {
                slidesPerView: 1
            }
        }
    });

    $(".typ-track .act-wrap .btn").click(function() {
        $(this).parents(".typ-track").find(".details").toggleClass("active");
    });

})
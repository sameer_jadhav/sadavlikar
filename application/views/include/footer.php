<footer>
        <!-- footer start -->
        <div class="lyt-footer">
    <div class="container">
        <ul class="footer-nav">
            <li class="col-md-3 col-xs-6">
                <h3 class="title">Overview</h3>
                <ul class="quick-links">
                    <li class="list-item">
                        <a href="<?php echo base_url();?>home">home</a>
                    </li>
                    <li class="list-item">
                        <a href="<?php echo base_url();?>aboutus">company</a>
                    </li>
                    <li class="list-item">
                        <a href="#">Tracking</a>
                    </li>
                    <li class="list-item">
                        <a href="investors.shtml">Investors</a>
                    </li>
                    <li class="list-item">
                        <a href="<?php echo base_url();?>careers">career</a>
                    </li>
                    <li class="list-item">
                        <a href="<?php echo base_url();?>contactus">contact</a>
                    </li>
                </ul>
            </li>
            <li class="col-md-3 col-xs-6">
                <h3 class="title">services</h3>
                <ul class="quick-links">
                    <li class="list-item">
                        <a href="template_solutions.shtml">full load</a>
                    </li>
                    <li class="list-item">
                        <a href="template_solutions.shtml">parcel/part load</a>
                    </li>
                    <li class="list-item">
                        <a href="template_solutions.shtml">bonded trucking</a>
                    </li>
                    <li class="list-item">
                        <a href="template_solutions.shtml">3PL & Warehousing</a>
                    </li>
                    <li class="list-item">
                        <a href="template_solutions.shtml">shipping line container movement</a>
                    </li>
                    <li class="list-item">
                        <a href="template_solutions.shtml">value added services</a>
                    </li>
                </ul>
            </li>
            <li class="col-md-3 col-xs-12">
                <h3 class="title">Social media</h3>
                <ul class="quick-links typ-app">
                    <li class="list-item">
                        <a class="icon icon-fb" href="https://www.facebook.com/Shreejitranslogistics/" target="_blank"></a>
                    </li>
                    <li class="list-item">
                        <a class="icon icon-tube" href="https://www.youtube.com/watch?v=6RQPSCC_GUE" target="_blank"></a>
                    </li>
                    <li class="list-item">
                        <a class="icon icon-in" href="#"></a>
                    </li>
                </ul>
            </li>
            <li class="col-md-3 col-xs-12 pull-right">
                <h3 class="title">newsletter</h3>
                <form id="newsletter-form" method="post">
                <span id="result" style="color:red"></span>
                <div class="input-group">
                    <input type="text" class="form-control" data-rule-required="true" placeholder="email" name="email_newsletter" id="email_newsletter">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit">subscribe</button>
                    </span>
                </div>
                </form>
            </li>
        </ul>
        <label class="copyright text-center cm-line-break">Copyright © 2017 <span class="cm-bold">Shreeji Translogistics Ltd.</span> All Rights Reserved.</label>
    </div>
</div>
<div class="lyt-advertise-with-us">
    <button class="btn-ad"><span class="cm-bold">Advertise</span><span> with us</span></button>
    <div class="cont">
        <div class="container">
            <h4 class="title">Advertise with us</h4>
            <div class="swiper-container">
                <ul class="swiper-wrapper">
                    <li class="swiper-slide mod-advertise-with-us">
                        <div class="img-wrap">
                            <h5 class="title">Lorem Ipsum</h5>
                            <img src="images/ab.jpg" alt="ab">
                        </div>
                        <div class="dics">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
                                a type specimen book.</p>
                            <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and
                                more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                            <button class="btn btn-orange" type="button" data-toggle="modal" data-target="#subscribe">subscribe</button>
                        </div>
                    </li>
                    <li class="swiper-slide mod-advertise-with-us">
                        <div class="img-wrap">
                            <h5 class="title">Lorem Ipsum</h5>
                            <img src="images/ab.jpg" alt="ab">
                        </div>
                        <div class="dics">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
                                a type specimen book.</p>
                            <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and
                                more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                            <button class="btn btn-orange" type="button" data-toggle="modal" data-target="#subscribe">subscribe</button>
                        </div>
                    </li>
                    <li class="swiper-slide mod-advertise-with-us">
                        <div class="img-wrap">
                            <h5 class="title">Lorem Ipsum</h5>
                            <img src="images/ab.jpg" alt="ab">
                        </div>
                        <div class="dics">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
                                a type specimen book.</p>
                            <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and
                                more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                            <button class="btn btn-orange" type="button" data-toggle="modal" data-target="#subscribe">subscribe</button>
                        </div>
                    </li>
                    <li class="swiper-slide mod-advertise-with-us">
                        <div class="img-wrap">
                            <h5 class="title">Lorem Ipsum</h5>
                            <img src="images/ab.jpg" alt="ab">
                        </div>
                        <div class="dics">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
                                a type specimen book.</p>
                            <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and
                                more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                            <button class="btn btn-orange" type="button" data-toggle="modal" data-target="#subscribe">subscribe</button>
                        </div>
                    </li>
                </ul>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </div>
    <a href="#" class="close-btn js-close">
        <img src="images/close.png" alt="close" />
    </a>
</div>
        <!-- footer end -->
    </footer>
    <script type="text/javascript" src="<?php echo base_url();?>js/plugins/libraries.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/custom.js"></script>
    <!-- js group end -->
    <script>
        function init() {
            var vidDefer = document.getElementsByTagName('iframe');
            for (var i = 0; i < vidDefer.length; i++) {
                if (vidDefer[i].getAttribute('data-src')) {
                    vidDefer[i].setAttribute('src', vidDefer[i].getAttribute('data-src'));
                }
            }
        }
        window.onload = init;
    </script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/jquery-validation/dist/jquery.validate.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/jquery-validation/dist/additional-methods.min.js"></script>

  <script type="text/javascript">
      $("#newsletter-form").validate({
        rules: {
            email_newsletter : {
                 required: true,
                email: true
            }
        },
        submitHandler: function(form) {
                 $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>newsletter/subscribe",
                    data: $(form).serialize(),
                    timeout: 3000,
                    success: function(response) {
                        $("#result").html(response);
                        $("#email_newsletter").val("")

                    },
                    error: function(response) {
                        $("#result").html(response);
                        $("#email_newsletter").val("")
                    }
                  });
                  return false;
            }
         });
  </script>
    <!-- js group end -->            
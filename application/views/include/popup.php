<!-- popup start -->
<?php 
//print_r($pdf_data);
 $file_count = count($pdf_data);
 //echo $file_count;exit;
 $investor_count = count($investor_data);
for($p=0;$p<$investor_count;$p++){ ?>
    <div class="modal fade" id="<?php echo $investor_data[$p]['slug'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="images/close.png" alt="close" /></button>
                <h4 class="modal-title" id="myModalLabel">Download PDF - <?php echo $investor_data[$p]['name'];?></h4>
            </div>
            <div class="modal-body">
                <div class="download-list">
                    <ul>
                    <?php 

                    for($w=0;$w<$file_count;$w++){
                        if($pdf_data[$w]->id ==$investor_data[$p]['id']){ 
                     ?>
                        <li>
                            <a href="<?php echo base_url();?>uploads/<?php echo $pdf_data[$w]->file_name;?>" target="_blank">
                                <span class="icon pdf"></span>
                                <span class="text"><?php echo $pdf_data[$w]->file_name;?></span>
                            </a>
                        </li>
                    <?php } 
                    }
                    ?>    
                        <!--li>
                            <a href="downloads/NOTICE17-18.pdf" target="_blank">
                                <span class="icon pdf"></span>
                                <span class="text">Notice 17-18</span>
                            </a>
                        </li-->
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<div class="modal fade" id="track" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="images/close.png" alt="close" /></button>
                <h4 class="modal-title" id="header-track">Tracking for : 9898789898</h4>
            </div>
            <div class="modal-body typ-track">
                <div class="statu-bar">
                    <h3 class="title">Tracking status</h3>
                    <ul>
                        <!-- <li class="start done"></li> -->
                        <li class="done process">shipment movement</li>
                        <!-- <li class="complete done"></li> -->
                    </ul>
                </div>
               <!--  <div class="act-wrap">
                    <button type="button" class="btn">view details</button>
                </div> -->
                <div class="details active">
                    <ul>
                        <li class="title">LRCODE</li>
                        <li class="decs">A2050155</li>

                        <li class="title">LRD</li>
                        <li class="decs">02-OCT-17</li>

                        <li class="title">AIRWAY BILLNO</li>
                        <li class="decs">065 2780 3661</li>

                        <li class="title">CONSIGNEE</li>
                        <li class="decs">EXPO FREIGHT (P) LTD</li>

                        <li class="title">CONSIGNOR</li>
                        <li class="decs">EXPO FREIGHT (P) LTD</li>

                        <li class="title">PACKAGES</li>
                        <li class="decs">266</li>

                        <li class="title">AIR PACKAGES</li>
                        <li class="decs">266</li>
                        <li class="title">AIR PACKAGES</li>
                        <li class="decs">266</li>
                        <li class="title">AIR WIGHT</li>
                        <li class="decs">2994</li>
                        <li class="title">AIR NAME</li>
                        <li class="decs">EXPO FREIGHT (P) LTD</li>
                        <li class="title">BOOKING BRANCH</li>
                        <li class="decs">BONDED TRUCKING</li>
                        <li class="title">B FROM</li>
                        <li class="decs">BANGALORE</li>
                        <li class="title">B TO</li>
                        <li class="decs">TRIVANDRUM</li>
                        <li class="title">LR STATUS</li>
                        <li class="decs">Material Delivered</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- popup end -->

<!-- <div class="modal fade" id="governance" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="images/close.png" alt="close" /></button>
                <h4 class="modal-title" id="myModalLabel">Download PDF - Corporate Governance</h4>
            </div>
            <div class="modal-body">
                <div class="download-list">
                    <ul>
                        <li>
                            <a href="#" target="_blank">
                                <span class="icon pdf"></span>
                                <span class="text">Corporate Governance</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="pattern" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="images/close.png" alt="close" /></button>
                <h4 class="modal-title" id="myModalLabel">Download PDF - Shareholding Pattern</h4>
            </div>
            <div class="modal-body">
                <div class="download-list">
                    <ul>
                        <li>
                            <a href="downloads/SHAREHOLDINGPATTERN.pdf" target="_blank">
                                <span class="icon pdf"></span>
                                <span class="text">Shareholding Pattern</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="public-issue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="images/close.png" alt="close" /></button>
                <h4 class="modal-title" id="myModalLabel">Download PDF - Public Issue</h4>
            </div>
            <div class="modal-body">
                <div class="download-list">
                    <ul>
                        <li>
                            <a href="downloads/PUBLICISSUE.pdf" target="_blank">
                                <span class="icon pdf"></span>
                                <span class="text">Public Issue</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="investors-contact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="images/close.png" alt="close" /></button>
                <h4 class="modal-title" id="myModalLabel">Download PDF - Investors Contact</h4>
            </div>
            <div class="modal-body">
                <div class="download-list">
                    <ul>
                        <li>
                            <a href="#" target="_blank">
                                <span class="icon pdf"></span>
                                <span class="text">Investors Contact</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="annual-report" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="images/close.png" alt="close" /></button>
                <h4 class="modal-title" id="myModalLabel">Download PDF - Annual Report</h4>
            </div>
            <div class="modal-body">
                <div class="download-list">
                    <ul>
                        <li>
                            <a href="downloads/ANNUALREPORTFY14-15.pdf" target="_blank">
                                <span class="icon pdf"></span>
                                <span class="text">Annual Report 14-15</span>
                            </a>
                        </li>
                        <li>
                            <a href="downloads/ANNUALREPORTFY15-16.pdf" target="_blank">
                                <span class="icon pdf"></span>
                                <span class="text">Annual Report 15-16</span>
                            </a>
                        </li>
                        <li>
                            <a href="downloads/ANNUALREPORTFY16-17.pdf" target="_blank">
                                <span class="icon pdf"></span>
                                <span class="text">Annual Report 16-17</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="policies" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="images/close.png" alt="close" /></button>
                <h4 class="modal-title" id="myModalLabel">Download PDF - Policies</h4>
            </div>
            <div class="modal-body">
                <div class="download-list">
                    <ul>
                        <li>
                            <a href="#" target="_blank">
                                <span class="icon pdf"></span>
                                <span class="text">Policies</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="results" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="images/close.png" alt="close" /></button>
                <h4 class="modal-title" id="myModalLabel">Download PDF - Results</h4>
            </div>
            <div class="modal-body">
                <div class="download-list">
                    <ul>
                        <li>
                            <a href="downloads/RESULTFY14-15.pdf" target="_blank">
                                <span class="icon pdf"></span>
                                <span class="text">Results 14-15</span>
                            </a>
                        </li>
                        <li>
                            <a href="downloads/RESULTSFY15-16.pdf" target="_blank">
                                <span class="icon pdf"></span>
                                <span class="text">Results 15-16</span>
                            </a>
                        </li>
                        <li>
                            <a href="downloads/RESULTFY16-17.pdf" target="_blank">
                                <span class="icon pdf"></span>
                                <span class="text">Results 16-17</span>
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="stock" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="images/close.png" alt="close" /></button>
                <h4 class="modal-title" id="myModalLabel">Download PDF - Stock Exchange Intimation</h4>
            </div>
            <div class="modal-body">
                <div class="download-list">
                    <ul>
                        <li>
                            <a href="#" target="_blank">
                                <span class="icon pdf"></span>
                                <span class="text">Stock Exchange Intimation</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="board" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="images/close.png" alt="close" /></button>
                <h4 class="modal-title" id="myModalLabel">Download PDF - Board of Committees</h4>
            </div>
            <div class="modal-body">
                <div class="download-list">
                    <ul>
                        <li>
                            <a href="#" target="_blank">
                                <span class="icon pdf"></span>
                                <span class="text">Board of Committees</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="unpaid" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="images/close.png" alt="close" /></button>
                <h4 class="modal-title" id="myModalLabel">Download PDF - Unpaid / Unclaimed Dividend</h4>
            </div>
            <div class="modal-body">
                <div class="download-list">
                    <ul>
                        <li>
                            <a href="#" target="_blank">
                                <span class="icon pdf"></span>
                                <span class="text">Unpaid / Unclaimed Dividend</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="investors" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="images/close.png" alt="close" /></button>
                <h4 class="modal-title" id="myModalLabel">Download PDF - Investors Complaints</h4>
            </div>
            <div class="modal-body">
                <div class="download-list">
                    <ul>
                        <li>
                            <a href="#" target="_blank">
                                <span class="icon pdf"></span>
                                <span class="text">Investors Complaints</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="subscribe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="images/close.png" alt="close" /></button>
                <h4 class="modal-title" id="myModalLabel">ENQUIRY</h4>
            </div>
            <div class="modal-body typ-form">
                <div class="mod-inquiry-form">
                    <form>
                        <div class="form-group typ-comp">
                            <input type="text" placeholder="Name" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="Email" class="form-control">
                        </div>
                        <div class="form-group typ-comp">
                            <input type="text" placeholder="Phone" class="form-control">
                        </div>
                        <div class="form-group typ-comp">
                            <textarea class="form-control" placeholder="Subject"></textarea>
                        </div>
                        <button class="btn btn-orange" type="button">send</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->

<!-- <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="images/close.png" alt="close" /></button>
                <h4 class="modal-title" id="myModalLabel">Download PDF</h4>
            </div>
            <div class="modal-body">
                <div class="download-list">
                    <ul>
                        <li>
                            <a href="#">
                                <span class="icon pdf"></span>
                                <span class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="icon pdf"></span>
                                <span class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="icon pdf"></span>
                                <span class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="icon pdf"></span>
                                <span class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="icon pdf"></span>
                                <span class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="icon pdf"></span>
                                <span class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="icon pdf"></span>
                                <span class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="icon pdf"></span>
                                <span class="text">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div> -->
    <!-- popup end -->
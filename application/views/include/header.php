<?php //echo $_SERVER['DOCUMENT_ROOT'];
/*$directoryURI = $_SERVER['REQUEST_URI'];
$path = parse_url($directoryURI, PHP_URL_PATH);
$components = explode('/', $path);
$first_part = $components[1];
$second_part = $components[2];
$third_part = $components[3];
$fourth_part = $components[4];
if($fourth_part!="")
{
    $str_file = "../";
}
else if($third_part!="")
{
    $str_file = "";
}else if($second_part!="")
{
    $str_file = "";

}else
{
    $str_file = "";
}*/
$activePage = $this->uri->segment(1);
//var_dump($activePage);

?>
<header>
        <!-- header start -->
        <div class="lyt-header">
            <a href="#" class="menu-btn visible-xs visible-sm">
                <span></span>
                <span></span>
                <span></span>
            </a>
            <a class="logo" href="<?php echo base_url();?>">
                <!--img src="<?php //echo base_url();?>images/logo.png" alt="logo"-->
            </a>
            <div class="rhs">
                <nav class="nav-bar">
                    <ul class="nav-list">
                        <li class="nav-item <?php if( $activePage== "home"){ echo "active"; }?>">
                            <a href="<?php echo base_url();?>home">HOME</a>
                        </li>
                        <li class="nav-item <?php if($activePage == "aboutus"){ echo "active";}?>">
                            <a href="<?php echo base_url();?>aboutus">company</a>
                        </li>
                        <li class="nav-item <?php if($activePage == "services"){ echo "active";}?>">
                            <a href="<?php echo base_url();?>services">services</a>
                        </li>
                        <li class="nav-item">
                            <a href="#">Tracking</a>
                        </li>
                        <!-- <li class="nav-item">
                            <a href="#" class="js-submenu-active">INVESTORS</a>
                            <div class="sub-menu-wrap">
                                <ul class="sub-menu">
                                    <li>
                                        <a data-toggle="modal" data-target="#notice" href="#">Notice</a>
                                    </li>
                                    <li>
                                        <a data-toggle="modal" data-target="#governance" href="#">Corporate Governance</a>
                                    </li>
                                    <li>
                                        <a data-toggle="modal" data-target="#pattern" href="#">Shareholding Pattern</a>
                                    </li>
                                    <li>
                                        <a data-toggle="modal" data-target="#public-issue" href="#">Public Issue</a>
                                    </li>
                                    <li>
                                        <a data-toggle="modal" data-target="#investors-contact" href="#">Investors contact</a>
                                    </li>
                                    <li>
                                        <a data-toggle="modal" data-target="#annual-report" href="#">Annual Report</a>
                                    </li>
                                    <li>
                                        <a data-toggle="modal" data-target="#policies" href="#">Policies</a>
                                    </li>
                                    <li>
                                        <a data-toggle="modal" data-target="#results" href="#">Results</a>
                                    </li>
                                    <li>
                                        <a data-toggle="modal" data-target="#stock" href="#">Stock Exchange Intimation</a>
                                    </li>
                                    <li>
                                        <a data-toggle="modal" data-target="#board" href="#">Board of Committees</a>
                                    </li>
                                    <li>
                                        <a data-toggle="modal" data-target="#unpaid" href="#">Unpaid / Unclaimed Dividend</a>
                                    </li>
                                    <li>
                                        <a data-toggle="modal" data-target="#investors" href="#">Investors Complaints</a>
                                    </li>
                                </ul>
                                <a href="#" class="close-btn visible-sm visible-xs"><img src="images/close.png" alt="close" /></a>
                            </div>
                        </li> -->

                        <li class="nav-item">
                            <a href="#" class="js-submenu-active">INVESTORS</a>
                            <div class="sub-menu-wrap">
                                <ul class="sub-menu">
                                <?php for($m=0;$m<count($investor_data);$m++){?>
                                    <li>
                                        <a data-toggle="modal" data-target="#<?php echo $investor_data[$m]['slug'];?>" href="#"><?php echo $investor_data[$m]['name'];?></a>
                                    </li>
                                <?php } ?>    
                                </ul>
                                <a href="#" class="close-btn visible-sm visible-xs"><img src="images/close.png" alt="close" /></a>
                            </div>
                        </li>
                        <!-- <li class="nav-item">
                            <a href="investors.shtml">INVESTORS</a>
                        </li> -->
                        <li class="nav-item <?php if($activePage == "careers"){ echo "active";}?>">
                            <a href="<?php echo base_url();?>careers">career</a>
                        </li>
                        <li class="nav-item <?php if($activePage == "contactus"){ echo "active";}?>">
                            <a href="<?php echo base_url();?>contactus">contact</a>
                        </li>
                    </ul>
                </nav>
                <a class="btn-language" href="#"></a>
            </div>
            <!-- ticker start -->
            <div class="mod-ticker hidden-xs hidden-sm">
    <marquee>Shreeji Translogistics Ltd. (Formerly known as Shreeji Transport Services Pvt. Ltd.).&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Shreeji expands it's network to Noida and Haridwar.</marquee>
</div>
            <!-- ticker end -->
        </div>
        <!-- header end -->
    </header>
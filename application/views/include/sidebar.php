<?php $this->load->helper('menu_helper');
$expertise_data = get_expertise();
?>

<nav>
    <div class="lyt-menu">
    <div class="menu-wrap">
         <div class="main-nav">
              <ul>
                   <li>
                        <a class="item" href="#">Who are we</a>
                        <div class="sub-nav">
                            <ul>
                                 <li>
                                    <a href="<?php echo base_url();?>india" class="item">India</a>
                                    <a href="<?php echo base_url();?>oman" class="item">Oman</a>
                                    <a href="<?php echo base_url();?>core-team" class="item">core team</a>
                                    <a href="<?php echo base_url();?>csr-and-values" class="item">CSR and Values</a>
                                    <a href="<?php echo base_url();?>certifications" class="item">Cetification</a>
                                    
                                 </li>
                            </ul>
                        </div>
                   </li>
                   <li>
                        <a class="item" href="<?php echo base_url();?>expertise">Our expertise</a>
                        <div class="sub-nav">
                            <ul>
                                 <li>
                                 <?php for($p=0;$p<count($expertise_data);$p++){ ?>
                                    <a href="<?php echo base_url();?>expertise/<?php echo $expertise_data[$p]['cat_slug'];?>" class="item"><?php echo $expertise_data[$p]['cat_name'];?></a>
                                 <?php } ?>   
                                 </li>
                            </ul>
                        </div>
                   </li>
                   <li>
                        <a class="item" href="<?php echo base_url();?>people-first">People first</a>
                        
                   </li>
                   <li>
                        <a class="item" href="<?php echo base_url();?>project-archives">Projects Archive</a>
                        
                   </li>
                   

              </ul>
         </div>
         <!-- <div class="secondary-nav">
            <ul>
                 <li>
                     <a class="item" href="#">About us</a>
                 </li>
                 <li>
                     <a class="item" href="#">Contact us</a>
                 </li>
                 <li>
                     <a class="item" href="#">Terms and Conditions</a>
                 </li>
                 <li>
                     <a class="item" href="#">Legal</a>
                 </li>
            </ul>
         </div> -->
    </div>


    <a href="#" class="btn-menu-close">
       <i class="fa fa-times" aria-hidden="true"></i>
    </a>

</div>
            </nav>
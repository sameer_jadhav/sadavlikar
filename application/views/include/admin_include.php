<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

  <link href="<?php echo base_url(); ?>assets/fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/icheck/flat/green.css" rel="stylesheet">


  <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
  <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
        
		<script src="<?php echo base_url(); ?>assets/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/jquery-validation/dist/jquery.validate.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/jquery-validation/dist/additional-methods.min.js"></script>
  	<script src="<?php echo base_url(); ?>assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/jquery-cookie/jquery.cookie.js"></script>
    <!--script src="<?php echo base_url(); ?>js/flaty.js"></script-->
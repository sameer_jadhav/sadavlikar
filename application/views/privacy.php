When you use our services, you’re trusting us with your information. We understand this is a big responsibility and work hard to protect your information and put you in control. This Privacy Policy is meant to help you understand what information we collect, why we collect it your information.

Information we collect as you use our services If you use our Application services to make and receive calls or send and receive messages, we may collect telephony log information like your phone number, calling-party number, receiving-party number, forwarding numbers, time and date of calls and messages, duration of calls, routing information, and types of calls.

Communicate with you we use information we collect, like your email address, to interact with you directly. For example, we may let you know about upcoming changes or improvements to our services. And if you contact, we’ll keep a record of your request in order to help solve any issues you might be facing.

We build security into our services to protect your information
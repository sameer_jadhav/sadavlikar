<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/admin-head.php'); ?> 
<body class="page-body  page-left-in" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<?php $this->load->view('admin/admin-sidebar.php'); ?> 

	<div class="main-content">
		<?php $this->load->view('admin/admin_top_nav.php'); ?> 		
		
		
		<hr />
		

		
		
		<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title">
							<?php echo $pagetitle;?>
						</div>
						
						
					</div>
					
					<div class="panel-body">
					<?php
                          if($error!=''){  ?>
        <div class="alert alert-danger"><?php echo $error; ?></div>
        <?php }
        ///var_dump($this->session->flashdata('success'));
        if($this->session->flashdata('success')!=''){?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
        <?php } ?>
						
						<form role="form" novalidate='novalidate' method="post" id="validation-form" enctype="multipart/form-data" action="<?php echo base_url('superadmin/media/addPhoto'); ?>" class="form-horizontal form-groups-bordered">
			
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Title*</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" data-rule-required="true" id="media_name" name="media_name" placeholder="Title">
								</div>
							</div>


							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Category*</label>
								
								<div class="col-sm-5">
									<!-- <input type=""  data-rule-required="true"  placeholder="Icon Class"> -->
									<select class="form-control" id="media_category" name="media_category">
										 <!--  <option value="press-coverage">Press coverage</option>
										 
										  <option value="media-updates">Media Updates</option> -->
										  <option value="photo-gallery">Image Gallery</option>
										  
										</select>
								</div>
							</div>


							


							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload Thumbnail Image*</label>
								
								<div class="col-sm-5">
									<input type="file" class="form-control" data-rule-required="true"  id="media_banner_img" name="media_banner_img" placeholder="Banner Image>
									
									<label for="field-1" class="col-sm-10 control-label">Upload Image size 768X500 * and less than 2MB</label>
								</div>
							</div>


							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload JPG/JPEG Image</label>
								
								<div class="col-sm-5">
									<input type="file" class="form-control"  id="press_jpg_img" name="press_jpg_img" placeholder="JPG?JPEG Image">
									
									<!-- <label for="field-1" class="col-sm-10 control-label">Upload Image size 1920x1200 * and less than 2MB</label> -->
								</div>
							</div>


							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload TIFF Image</label>
								
								<div class="col-sm-5">
									<input type="file" class="form-control"   id="press_tiff_img" name="press_tiff_img" placeholder="TIFF Image">
									
									<!-- <label for="field-1" class="col-sm-10 control-label">Upload Image size 1920x1200* and less than 2MB</label> -->
								</div>
							</div>


					

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload PDF</label>
								
								<div class="col-sm-5">
									<input type="file" class="form-control"   id="media_pdf" name="media_pdf" placeholder="PDF File">
									
									<label for="field-1" class="col-sm-10 control-label">Upload PDF file less than 2MB</label>
								</div>
							</div>
							
							
							
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-5">
									<button type="submit"  name="btn_submit" class="btn btn-success">Submit</button>
								</div>
							</div>
						</form>
						
					</div>
				
				</div>
			
			</div>
		</div>
		
		
		
		
		
		<!-- Footer -->
		
	</div>

	
	
	
	
	

	
</div>

<?php $this->load->view('admin/admin-footer.php'); ?> 
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>	
<script>

jQuery("#validation-form").validate();
jQuery(document).ready(function() {
     jQuery('#media_pdf').filer({
     	limit: 1,
		maxSize: 5,
		extensions: ["pdf"],
		showThumbs: true,
		addMore: true
     });  

     jQuery('#press_jpg_img').filer({
     	limit: 1,
		maxSize: 5,
		extensions: ["jpg","jpeg"],
		showThumbs: true,
		addMore: true
     });  

     jQuery('#press_tiff_img').filer({
     	limit: 1,
		maxSize: 5,
		extensions: ["tif"],
		showThumbs: true,
		addMore: true
     });  

     jQuery('#media_banner_img').filer({
     	limit: 1,
		maxSize: 5,
		extensions: ["jpg", "png", "gif","jpeg"],
		showThumbs: true,
		addMore: true
     });

    /* jQuery('#press_banner_desktop_img').filer({
     	limit: 1,
		maxSize: 5,
		extensions: ["jpg", "png", "gif","jpeg"],
		showThumbs: true,
		addMore: true
     });

     jQuery('#press_banner_mobile_img').filer({
     	limit: 1,
		maxSize: 5,
		extensions: ["jpg", "png", "gif","jpeg"],
		showThumbs: true,
		addMore: true
     });    */
     

   
     /*jQuery('#project_icon_img').filer({
     	limit: 1,
		maxSize: 5,
		extensions: ["jpg", "png", "gif","jpeg"],
		showThumbs: true,
		addMore: true
     }); */     
});


/*jQuery("#select_expertise").change(function(){
	var expertise = jQuery("#select_expertise option:selected").text();
	alert(expertise)
	jQuery("#tags").val(expertise)
})
*/


	</script>  

</body>
</html>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/admin-head.php'); ?> 
<body class="page-body login-page login-form-fall" data-url="http://neon.dev">


<!-- This is needed when you send requests via Ajax -->
<script type="text/javascript">
var baseurl = '';
</script>

<div class="login-container">
	
	<div class="login-header login-caret">
		
		<div class="login-content">
			
			<a href="index.html" class="logo">
				<img src="<?php echo base_url();?>images/logo.png" width="100%" alt="" >
			</a>
			
			<p class="description">Dear user, log in to access the admin area!</p>
			
			<!-- progress bar indicator -->
			<div class="login-progressbar-indicator">
				<h3>43%</h3>
				<span>logging in...</span>
			</div>
		</div>
		
	</div>
	
	<div class="login-progressbar">
		<div></div>
	</div>
	
	<div class="login-form">
		
		<div class="login-content">
		<?php
		//var_dump($error);
          if($this->session->flashdata('error')!=''){  ?>
       			<div class="form-login-error visible">
					<h3>Invalid login</h3>
					<p><?php echo $this->session->flashdata('error'); ?></p>
				</div>
        <?php }
         if($this->session->flashdata('success')!=''){?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
        <?php } ?>
			
			
			<form novalidate='novalidate' id="validation-form" action="<?php echo base_url('superadmin/admin/login'); ?>"  method="post">
				
				<div class="form-group">
					
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-user"></i>
						</div>
						
						<input type="text" class="form-control" name="username" id="username" placeholder="Username"  data-rule-required="true" autocomplete="off" />
						<?php if(form_error('username')!=''){ ?><span class="help-block" for="password">This field is required.</span> <?php } ?>
					</div>
					
				</div>
				
				<div class="form-group">
					
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-key"></i>
						</div>
						
						<input type="password" class="form-control" data-rule-required="true" name="password" id="password" placeholder="Password" autocomplete="off" />
						<?php if(form_error('password')!=''){ ?><span class="help-block" for="password">This field is required.</span> <?php } ?>
					</div>
				
				</div>
				
				<div class="form-group">
					<button type="submit" id="btn_login" name="btn_login" class="btn btn-primary btn-block btn-login">
						<i class="entypo-login"></i>
						Login In
					</button>
				</div>
				
				
				
			</form>
			
			
			
		</div>
		
	</div>
	
</div>


	<?php $this->load->view("admin/admin-footer.php");?>
<script type="text/javascript">
	
	jQuery("#validation-form").validate();
</script>
</body>
</html>
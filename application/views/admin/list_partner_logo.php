<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/admin-head.php'); ?> 
<body class="page-body  page-left-in" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<?php $this->load->view('admin/admin-sidebar.php'); ?> 

	<div class="main-content">
		<?php $this->load->view('admin/admin_top_nav.php'); ?> 		
		
		
		<hr />
		

		
		
		<div class="row">
			<div class="col-sm-12">
			<div class="panel-heading">
						<div class="panel-title">
							<?php echo $pagetitle;?>
						</div>

						
					</div>
					<?php
			          if($error!=''){  ?>
			        <div class="alert alert-danger"><?php echo $error; ?></div>
			        <?php }
			        if($this->session->flashdata('success')!=''){?>
			        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
			        <?php } ?>					
				<table class="table table-bordered datatable" id="table-1">
			<thead>
				<tr>
					<!-- <th data-hide="phone">Title</th> -->
					<th data-hide="phone">Logo Image</th>
					<th data-hide="phone">Url</th>
					<th data-hide="phone">Alt Text</th>
					
					
					<th>Actions</th>
					
				</tr>
			</thead>
			<tbody>
			<?php 
                for($i=0;$i<count($spaces_data);$i++){ 
            ?>
				<tr class="odd gradeX">

					<td>
						 <img width="125"  src="<?php echo base_url();?>uploads/<?php echo $spaces_data[$i]['img_name'];?>" alt="image">
					</td>
					<td><?php echo $spaces_data[$i]['title'];?></td>
					<td><?php echo $spaces_data[$i]['alt_text'];?></td>
					
					<td>
						<a href="<?php echo base_url(); ?>superadmin/admin/editPartnerLogo/<?php echo base64_encode($spaces_data[$i]['id']); ?>" class="btn btn-default btn-sm btn-icon icon-left">
							<i class="entypo-pencil"></i>
							Edit
						</a>
						
						<a  href="javascript:void(0)" onclick="delete_logo('<?php echo base64_encode($spaces_data[$i]['id']);  ?>');" class="btn btn-danger btn-sm btn-icon icon-left">
							<i class="entypo-cancel"></i>
							Delete
						</a>
					</td>	
						
				</tr>
				<?php } ?>
			</tbody>
		</table>
			</div>
		</div>
		
		
		
		
		
		<!-- Footer -->
		
	</div>

	
	
	
	
	

	
</div>

<?php $this->load->view('admin/admin-footer.php'); ?> 	
<script type="text/javascript">
		var responsiveHelper;
		var breakpointDefinition = {
		    tablet: 1024,
		    phone : 480
		};
		var tableContainer;
		
			jQuery(document).ready(function($)
			{
				tableContainer = $("#table-1");
				
				tableContainer.dataTable({
					"sPaginationType": "bootstrap",
					"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
					"bStateSave": true,
					
		
				    // Responsive Settings
				    bAutoWidth     : false,
				    fnPreDrawCallback: function () {
				        // Initialize the responsive datatables helper once.
				        if (!responsiveHelper) {
				            responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
				        }
				    },
				    fnRowCallback  : function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				        responsiveHelper.createExpandIcon(nRow);
				    },
				    fnDrawCallback : function (oSettings) {
				        responsiveHelper.respond();
				    }
				});
				
				$(".dataTables_wrapper select").select2({
					minimumResultsForSearch: -1
				});
			});

				function delete_logo(logo_id)
          {
            var x = window.confirm("Do you really want to delete this Logo?");
            if (x == true) {
                jQuery.post("<?php echo base_url();?>superadmin/admin/deleteLogo","logo_id="+logo_id,function(res){
                window.location.href="<?php echo base_url();?>superadmin/admin/listPartnerLogo"
           })
            } 
           
          }

           function sort_val(sort_value,artist_id)
          {
           // alert(sort_value);

            jQuery.post("<?php echo base_url();?>superadmin/admin/sortArtist","order="+sort_value+"&artist_id="+artist_id,function(res){
            })
          } 
		</script>

</body>
</html>
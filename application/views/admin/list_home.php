<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/admin-head.php'); ?> 
<body class="page-body  page-left-in" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<?php $this->load->view('admin/admin-sidebar.php'); ?> 

	<div class="main-content">
		<?php $this->load->view('admin/admin_top_nav.php'); ?> 		
		
		
		<hr />
		

		
		
		<div class="row">
			<div class="col-sm-12">
			<div class="panel-heading">
						<div class="panel-title">
							<?php echo $pagetitle;?>
						</div>

						
					</div>
					<?php
			          if($this->session->flashdata('error')!=''){  ?>
			        <div class="alert alert-danger"><?php echo $error; ?></div>
			        <?php }
			        if($this->session->flashdata('success')!=''){?>
			        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
			        <?php } ?>					
				<table class="table table-bordered datatable" id="table-1">
			<thead>
				<tr>
					
                    <th>Image</th>
                    <th>Sort Order</th>
                    <th>Actions</th>
				</tr>
			</thead>
			<tbody>
			<?php 
            	for($i=0;$i<count($image_data);$i++){ 
            ?>
				<tr class="odd gradeX">
					
                    <td>    
            			 <img width="125" src="<?php echo base_url().'uploads/'.$image_data[$i]['image_name_desktop'];?>" alt="image" />         
                     </td>
					<td>
						<input type="text" class="form-control"  value="<?php echo $image_data[$i]['sort_order'];?>" name="sort_order"  id="sort_order"  onKeyup="sort_val(this.value,<?php echo $image_data[$i]['image_id'];?>)">
					</td>
					
					<td>
						
						
						<a  href="javascript:void(0)" onclick="delete_image('<?php echo base64_encode($image_data[$i]['image_id']); ?>')" class="btn btn-danger btn-sm btn-icon icon-left">
							<i class="entypo-cancel"></i>
							Delete
						</a>
					</td>	
				</tr>
				<?php } ?>
			</tbody>
		</table>
			</div>
		</div>
		
		
		
		
		
		<!-- Footer -->
		
	</div>

	
	
	
	
	

	
</div>

<?php $this->load->view('admin/admin-footer.php'); ?> 	
<script type="text/javascript">
		var responsiveHelper;
		var breakpointDefinition = {
		    tablet: 1024,
		    phone : 480
		};
		var tableContainer;
		
			jQuery(document).ready(function($)
			{
				tableContainer = $("#table-1");
				
				tableContainer.dataTable({
					"sPaginationType": "bootstrap",
					"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
					"bStateSave": true,
					
		
				    // Responsive Settings
				    bAutoWidth     : false,
				    fnPreDrawCallback: function () {
				        // Initialize the responsive datatables helper once.
				        if (!responsiveHelper) {
				            responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
				        }
				    },
				    fnRowCallback  : function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				        responsiveHelper.createExpandIcon(nRow);
				    },
				    fnDrawCallback : function (oSettings) {
				        responsiveHelper.respond();
				    }
				});
				
				$(".dataTables_wrapper select").select2({
					minimumResultsForSearch: -1
				});
			});

			function delete_image(image_id)
          {
            var x = window.confirm("Do you really want to delete this Image?");
            if (x == true) {
                jQuery.post("<?php echo base_url();?>superadmin/admin/deleteImage","image_id="+image_id,function(res){
                window.location.href="<?php echo base_url();?>superadmin/admin/listHome"
           })
            } 
           
          }

          function sort_val(sort_value,image_id)
          {
           // alert(sort_value);

            jQuery.post("<?php echo base_url();?>superadmin/admin/sortImage","order="+sort_value+"&image_id="+image_id,function(res){
            })
          } 
		</script>

</body>
</html>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/admin-head.php'); ?> 
<body class="page-body  page-left-in" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<?php $this->load->view('admin/admin-sidebar.php'); ?> 

	<div class="main-content">
		<?php $this->load->view('admin/admin_top_nav.php'); ?> 		
		
		
		<hr />
		

		
		
		<div class="row">
			<div class="col-sm-12">
			<div class="panel-heading">
						<div class="panel-title">
							<?php echo $pagetitle;?>
						</div>

						
					</div>
					<?php
			          if($error!=''){  ?>
			        <div class="alert alert-danger"><?php echo $error; ?></div>
			        <?php }
			        if($this->session->flashdata('success')!=''){?>
			        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
			        <?php } ?>	

				<table class="table table-bordered datatable" id="table-1">
			<thead>
				<tr>
					<th data-hide="phone">Name</th>
					<th data-hide="phone">Email Address</th>
					<th data-hide="phone">Phone Number</th>
					<th data-hide="phone">Subject</th>
					<th data-hide="phone">Message</th>
					<th data-hide="phone">Do you know a member of The A?</th>
					<th data-hide="phone">How did you hear about us?</th> 
					<th data-hide="phone">Received On</th>
					<th>Actions</th>
					
				</tr>
			</thead>
			<tbody>
			<?php 
                for($i=0;$i<count($country_data);$i++){ 
            ?>
				<tr class="odd gradeX">
					<td><?php echo $country_data[$i]['name'];?></td>
					
					<td><?php echo $country_data[$i]['email'];?></td>
					<td><?php echo $country_data[$i]['phone']?></td>
					<td><?php echo $country_data[$i]['subject'];?></td>
					<td><?php echo $country_data[$i]['message']?></td>
					<td><?php echo $country_data[$i]['know_member']?></td>
					<td><?php echo $country_data[$i]['heard_from']?></td>
					
					<td><?php echo $country_data[$i]['date_created']?></td>
					<td>
						
						
						<a  href="javascript:void(0)" onclick="delete_enquiry('<?php echo base64_encode($country_data[$i]['id']); ?>');" class="btn btn-danger btn-sm btn-icon icon-left">
							<i class="entypo-cancel"></i>
							Delete
						</a>
					</td>
						
				</tr>
				<?php } ?>
			</tbody>
		</table>
			</div>
		</div>
		
		
		
		
		
		<!-- Footer -->
		
	</div>

	
	
	
	
	

	
</div>

<?php $this->load->view('admin/admin-footer.php'); ?> 	
<script type="text/javascript">
		/*var responsiveHelper;
		var breakpointDefinition = {
		    tablet: 1024,
		    phone : 480
		};
		var tableContainer;
		
			jQuery(document).ready(function($)
			{
				tableContainer = $("#table-1");
				
				tableContainer.dataTable({
					"sPaginationType": "bootstrap",
					"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
					"bStateSave": true,
					
		
				    // Responsive Settings
				    bAutoWidth     : false,
				    fnPreDrawCallback: function () {
				        // Initialize the responsive datatables helper once.
				        if (!responsiveHelper) {
				            responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
				        }
				    },
				    fnRowCallback  : function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				        responsiveHelper.createExpandIcon(nRow);
				    },
				    fnDrawCallback : function (oSettings) {
				        responsiveHelper.respond();
				    }
				});
				
				$(".dataTables_wrapper select").select2({
					minimumResultsForSearch: -1
				});
			});

*/			function delete_enquiry(enquiry_id)
          {
            var x = window.confirm("Do you really want to delete this Enquiry?");
            if (x == true) {
                jQuery.post("<?php echo base_url();?>superadmin/admin/deleteEnquiry","enquiry_id="+enquiry_id,function(res){
                window.location.href="<?php echo base_url();?>superadmin/admin/listEnquiry"
           })
            } 
           
          }

           function sort_val(sort_value,artist_id)
          {
           // alert(sort_value);

            jQuery.post("<?php echo base_url();?>superadmin/admin/sortArtist","order="+sort_value+"&artist_id="+artist_id,function(res){
            })
          } 

          jQuery(document).ready(function($) {
			var $table4 = $("#table-1");
				$table4.DataTable({
					dom: 'Bfrtip',
					buttons: [
					'excelHtml5',
					'csvHtml5'
					]
				});

			$(".dataTables_wrapper select").select2({
					minimumResultsForSearch: -1
				});
			} );
		</script>
		<style type="text/css">
			/*div.dt-buttons{
				display: none;
			}*/
		</style>

</body>
</html>
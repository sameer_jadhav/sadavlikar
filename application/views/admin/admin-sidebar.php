<div class="sidebar-menu">

		<div class="sidebar-menu-inner">
			
			<header class="logo-env">

				<!-- logo -->
				<div class="logo">
					<a href="<?php echo base_url();?>superadmin/admin/dashboard/">
						<!--img src="<?php //echo base_url();?>images/logo.png" width="60" alt="Shreeji" /--> 
					</a>
				</div>

				<!-- logo collapse icon -->
				<div class="sidebar-collapse">
					<a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
						<i class="entypo-menu"></i>
					</a>
				</div>

								
				<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
				<div class="sidebar-mobile-menu visible-xs">
					<a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
						<i class="entypo-menu"></i>
					</a>
				</div>

			</header>
			
						<div class="sidebar-user-info">

				<div class="sui-normal">
					<a href="#" class="user-link">
						<img src="<?php echo base_url();?>assets/images/thumb-1.png" alt="" class="img-circle" />

						<span>Welcome,</span>
						<strong>Admin</strong>
					</a>
				</div>


			</div>


			
			
									
			<ul id="main-menu" class="main-menu">
				<!-- add class "multiple-expanded" to allow multiple submenus to open -->
				<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
				<li class="">
					<a href="#">
						<i class="entypo-picture"></i>
						<span class="title">Banner</span>
					</a>
					<ul>


						<li>
							<a href="<?php echo base_url();?>superadmin/home/listBanner">
								<span class="title">List Banner</span>
							</a>
						</li>



						<li class="">
							<a href="<?php echo base_url();?>superadmin/home/addBanner">
								<span class="title">Add Banner</span>
							</a>
						</li>

						
					</ul>
				
				</li>

				<li class="">
					<a href="#">
						<i class="entypo-picture"></i>
						<span class="title">माझे गाव</span>
					</a>
					<ul>
						<li>
							<a href="<?php echo base_url();?>superadmin/villege/history">
								<span class="title">इतिहास </span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url();?>superadmin/villege/listSites">
								<span class="title">पर्यटन स्थळे  </span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url();?>superadmin/villege/listSchool">
								<span class="title">गावची शाळा </span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url();?>superadmin/villege/listTemple">
								<span class="title">गावातील मंदिरे </span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url();?>superadmin/villege/listFestival">
								<span class="title">मुख्य सण </span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url();?>superadmin/admin/listAboutVillege">
								<span class="title">यादी </span>
							</a>
						</li>
						<li class="">
							<a href="<?php echo base_url();?>superadmin/admin/addAboutVillege">
								<span class="title">यादी जोडा</span>
							</a>
						</li>

						
					</ul>
				
				</li>


				<li class="">
					<a href="#">
						<i class="entypo-picture"></i>
						<span class="title">कार्यक्रम</span>
					</a>
					<ul>


						<li>
							<a href="<?php echo base_url();?>superadmin/events/listEvents">
								<span class="title">कार्यक्रम यादी </span>
							</a>
						</li>



						<li class="">
							<a href="<?php echo base_url();?>superadmin/events/addEvent">
								<span class="title">कार्यक्रम  Add करा </span>
							</a>
						</li>

						
					</ul>
				
				</li>


				<li class="">
					<a href="#">
						<i class="entypo-picture"></i>
						<span class="title">ग्रामपंचायत  </span>
					</a>
					<ul>

						<li>
							<a href="<?php echo base_url();?>superadmin/civic/history">
								<span class="title">इतिहास</span>
							</a>
						</li>


						<li>
							<a href="<?php echo base_url();?>superadmin/civic/listHead">
								<span class="title">सरपंच </span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url();?>superadmin/civic/listEmployee">
								<span class="title">कर्मचारी </span>
							</a>
						</li>


						<li>
							<a href="<?php echo base_url();?>superadmin/civic/listPlan">
								<span class="title">योजना यादी  </span>
							</a>
						</li>
						<li class="">
							<a href="<?php echo base_url();?>superadmin/civic/addPlan">
								<span class="title">योजना Add करा </span>
							</a>
						</li>

						
					</ul>
				
				</li>



				<li class="">
					<a href="#">
						<i class="entypo-picture"></i>
						<span class="title">गावची मंडळे</span>
					</a>
					<ul>
						<li>
							<a href="<?php echo base_url();?>superadmin/mandal/listMandal">
								<span class="title">मंडळे यादी </span>
							</a>
						</li>

						

						<li class="">
							<a href="<?php echo base_url();?>superadmin/mandal/addMandal">
								<span class="title">मंडळ Add करा </span>
							</a>
						</li>

						
					</ul>
				
				</li>


				<li class="">
					<a href="#">
						<i class="entypo-picture"></i>
						<span class="title">सूचना </span>
					</a>
					<ul>
						<li>
							<a href="<?php echo base_url();?>superadmin/notification/listNotification">
								<span class="title">सूचना यादी  </span>
							</a>
						</li>
						<li class="">
							<a href="<?php echo base_url();?>superadmin/notification/addNotification">
								<span class="title">सूचना Add करा </span>
							</a>
						</li>

						
					</ul>
				
				</li>


				<li class="">
					<a href="#">
						<i class="entypo-picture"></i>
						<span class="title">ई-पेमेंट </span>
					</a>
					<ul>
						<li>
							<a href="<?php echo base_url();?>superadmin/payment/listPayment">
								<span class="title">ई-पेमेंट यादी  </span>
							</a>
						</li>
						<li class="">
							<a href="<?php echo base_url();?>superadmin/payment/addPayment">
								<span class="title">ई-पेमेंट Add करा </span>
							</a>
						</li>

						
					</ul>
				
				</li>

				<li class="">
					<a href="#">
						<i class="entypo-picture"></i>
						<span class="title">सडवली संघटन </span>
					</a>
					<ul>
						<li>
							<a href="<?php echo base_url();?>superadmin/union/info">
								<span class="title">माहिती </span>
							</a>
						</li>

						<li class="">
							<a href="<?php echo base_url();?>superadmin/union/listProgram">
								<span class="title">कार्यक्रम यादी पहा  </span>
							</a>
						</li>

						<li class="">
							<a href="<?php echo base_url();?>superadmin/union/addProgram">
								<span class="title">कार्यक्रम add करा  </span>
							</a>
						</li>

						
					</ul>
				
				</li>
				
				<li class="">
					<a href="#">
						<i class="entypo-picture"></i>
						<span class="title">वंशावळ </span>
					</a>
					<ul>
						<li>
							<a href="<?php echo base_url();?>superadmin/generation/history">
								<span class="title">इतिहास</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url();?>superadmin/generation/listArea">
								<span class="title">वाडी यादी  </span>
							</a>
						</li>
						<li class="">
							<a href="<?php echo base_url();?>superadmin/generation/addArea">
								<span class="title">वाडी Add करा </span>
							</a>
						</li>

						<li>
							<a href="<?php echo base_url();?>superadmin/generation/listFamily">
								<span class="title">भावकी यादी   </span>
							</a>
						</li>
						<li class="">
							<a href="<?php echo base_url();?>superadmin/generation/addFamily">
								<span class="title">भावकी Add करा </span>
							</a>
						</li>

						
					</ul>
				
				</li>

				

				<li class="">
					<a href="#">
						<i class="entypo-picture"></i>
						<span class="title">महत्वाचे नंबर  </span>
					</a>
					<ul>
						<li>
							<a href="<?php echo base_url();?>superadmin/contact/listContact">
								<span class="title">नंबर यादी</span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url();?>superadmin/contact/addContact">
								<span class="title">नंबर add करा </span>
							</a>
						</li>
						

						
					</ul>
				
				</li>

				<li class="">
					<a href="<?php echo base_url();?>superadmin/users/listUsers">
						<i class="entypo-mail"></i>
						<span class="title">युसर्स यादी</span>
					</a>
					
				
				</li>

				 
				<!-- <li class="">
					<a href="<?php echo base_url();?>superadmin/admin/listEnquiry">
						<i class="entypo-mail"></i>
						<span class="title">Enquiries</span>
					</a>
					
				
				</li> -->



				<li class="">
					<a href="<?php echo base_url();?>superadmin/admin/change_password">
						<i class="entypo-key"></i>
						<span class="title">Change Password</span>
					</a>
					
				
				</li>					
			</ul>
			
		</div>

	</div>
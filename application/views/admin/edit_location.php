<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/admin-head.php'); ?> 
<body class="page-body  page-left-in" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<?php $this->load->view('admin/admin-sidebar.php'); ?> 

	<div class="main-content">
		<?php $this->load->view('admin/admin_top_nav.php'); ?> 		
		
		
		<hr />
		

		
		
		<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title">
							<?php echo $pagetitle;?>
						</div>
						
						
					</div>
					
					<div class="panel-body">
					<?php
                          if($error!=''){  ?>
        <div class="alert alert-danger"><?php echo $error; ?></div>
        <?php }
        ///var_dump($this->session->flashdata('success'));
        if($this->session->flashdata('success')!=''){?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
        <?php } ?>
						
						<form role="form" novalidate='novalidate' method="post" id="validation-form" enctype="multipart/form-data" action="<?php echo base_url('superadmin/admin/editLocation'); ?>" class="form-horizontal form-groups-bordered">
			
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Title*</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" data-rule-required="true" id="title" name="title" placeholder="Title" value="<?php echo $spaces_data[0]['title'];?>">
								</div>
							</div>

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Distance*</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" data-rule-required="true" id="distance" name="distance" placeholder="Distance" value="<?php echo $spaces_data[0]['distance'];?>">
								</div>
							</div>

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Type*</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" data-rule-required="true" id="loc_type" name="loc_type" placeholder="Type" value="<?php echo $spaces_data[0]['type'];?>">
								</div>
							</div>

							
							


							

							
							<input type="hidden" value="<?php echo $spaces_data[0]['id'];?>" class="form-control" id="location_id" name="location_id"  />

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload Location Image*</label>
								
								<div class="col-sm-5">
									<input type="file" class="form-control"   id="project_banner_img" name="project_banner_img" placeholder="Placeholder">
									
									<label for="field-1" class="col-sm-10 control-label">Upload Image size 1366 X 768 * and less than 2MB</label>
								</div>
							</div>
							

							
							
							
							
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-5">
									<button type="submit"  name="btn_submit" class="btn btn-success">Submit</button>
								</div>
							</div>
						</form>
						
					</div>
				
				</div>
			
			</div>
		</div>
		
		
		
		
		
		<!-- Footer -->
		
	</div>

	
	
	
	
	

	
</div>

<?php $this->load->view('admin/admin-footer.php'); ?> 
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>	
<script>

jQuery("#validation-form").validate({
	rules:{
		v_tour_link : {
			url: true,
			required:true
		}
	}
});
filer_default_opts = {
            changeInput2: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><div class="jFiler-input-text"><h3>Drag&Drop files here</h3> <span style="display:inline-block; margin: 15px 0">or</span></div><a class="jFiler-input-choose-btn btn-custom blue-light">Browse Files</a></div></div>',
            templates: {
                box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
                item: '<li class="jFiler-item" style="width:49%">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-thumb-overlay">\
    										<div class="jFiler-item-info">\
    											<div style="display:table-cell;vertical-align: middle;">\
    												<span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
    												<span class="jFiler-item-others">{{fi-size2}}</span>\
    											</div>\
    										</div>\
    									</div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <li>{{fi-progressBar}}</li>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                            <li><a class="icon-jfi-trash jFiler-item-trash-action" id="" ></a></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                        </li>',
                itemAppend: '<li class="jFiler-item" style="width:49%">\
                                <div class="jFiler-item-container">\
                                    <div class="jFiler-item-inner">\
                                        <div class="jFiler-item-thumb">\
                                            <div class="jFiler-item-status"></div>\
                                            <div class="jFiler-item-thumb-overlay">\
        										<div class="jFiler-item-info">\
        											<div style="display:table-cell;vertical-align: middle;">\
        												<span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
        												<span class="jFiler-item-others">{{fi-size2}}</span>\
        											</div>\
        										</div>\
        									</div>\
                                            {{fi-image}}\
                                        </div>\
                                        <div class="jFiler-item-assets jFiler-row">\
                                            <ul class="list-inline pull-left">\
                                                <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                            </ul>\
                                            <ul class="list-inline pull-right">\
                                            </ul>\
                                        </div>\
                                    </div>\
                                </div>\
                            </li>',
                progressBar: '<div class="bar"></div>',
                itemAppendToEnd: false,
                removeConfirmation: true,
                _selectors: {
                    list: '.jFiler-items-list',
                    item: '.jFiler-item',
                    progressBar: '.bar',
                    remove: '.jFiler-item-trash-action'
                }
            }
            
        };

jQuery(document).ready(function() {
     
     //jQuery('#publish_date').datepicker("setValue", '10/10/2017' );


     

      jQuery('#project_banner_img').filer({
     	limit: 1,
		maxSize: 1,
		extensions: ["jpg", "png", "gif","jpeg"],
		showThumbs: true,
		addMore: false,
		showThumbs: true,
		templates: filer_default_opts.templates,
		files : [
					<?php 
						$finfo = finfo_open(FILEINFO_MIME_TYPE);
						$mime = finfo_file($finfo, 'uploads/'.$spaces_data[0]['img_name']);
						finfo_close($finfo);
						//$mime = "jpeg";
						?>
				    {
				        name: "<?php echo $spaces_data[0]['id']; ?>",
				        size: <?php echo filesize('uploads/'.$spaces_data[0]['img_name']);?>,
				        type: "<?php echo $mime;?>",
				        file: "<?php echo base_url();?>uploads/<?php echo $spaces_data[0]['img_name'];?>",
				        url: "<?php echo base_url().'uploads/'.$spaces_data[0]['img_name'];?>"
				    } ,
				    
				]
     }); 

    


     
});



/*jQuery("#select_expertise").change(function(){
	var expertise = jQuery("#select_expertise option:selected").text();
	alert(expertise)
	jQuery("#tags").val(expertise)
})
*/


	</script>  

</body>
</html>
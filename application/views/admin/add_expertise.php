<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/admin-head.php'); ?> 
<body class="page-body  page-left-in" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<?php $this->load->view('admin/admin-sidebar.php'); ?> 

	<div class="main-content">
		<?php $this->load->view('admin/admin_top_nav.php'); ?> 		
		
		
		<hr />
		

		
		
		<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title">
							<?php echo $pagetitle;?>
						</div>
						
						
					</div>
					
					<div class="panel-body">
					<?php
						//var_dump($error);
					if($error!="")
						{
							echo '<div class="alert alert-danger">'.$error.'</div>';
						}
				          if($this->session->flashdata('error')!=''){  ?>
				       			<div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
				        <?php }
				         if($this->session->flashdata('success')!=''){?>
				        		<div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
				        <?php } ?>
						
						<form role="form" novalidate='novalidate' method="post" id="validation-form" action="<?php echo base_url('superadmin/admin/addExpertise'); ?>" enctype="multipart/form-data" class="form-horizontal form-groups-bordered">
			
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Expertise Name *</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" data-rule-required="true" id="cat_name" name="cat_name" placeholder="Name">
								</div>
							</div>

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload Featured Image*</label>
								
								<div class="col-sm-5">
									<input type="file" data-rule-required="true"  class="form-control" data-rule-required="true" id="cat_featured_img" name="cat_featured_img" placeholder="Placeholder">
								</div>
								<label for="field-1" class="col-sm-7 control-label">Upload Image size 370px X 370px  and less than 2MB</label>
							</div>

							
							
							
							
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-5">
									<button type="submit" name="btn_submit" class="btn btn-success">Submit</button>
								</div>
							</div>
						</form>
						
					</div>
				
				</div>
			
			</div>
		</div>
		
		
		
		
		
		<!-- Footer -->
		
	</div>

	
	
	
	
	

	
</div>

<?php $this->load->view('admin/admin-footer.php'); ?> 	
<script>
jQuery("#validation-form").validate();
	</script>  

</body>
</html>
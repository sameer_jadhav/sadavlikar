<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/admin-head.php'); ?> 
<body class="page-body  page-left-in" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<?php $this->load->view('admin/admin-sidebar.php'); ?> 

	<div class="main-content">
		<?php $this->load->view('admin/admin_top_nav.php'); ?> 		
		
		
		<hr />
		

		
		
		<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title">
							<?php echo $pagetitle;?>
						</div>
						
						
					</div>
					
					<div class="panel-body">
					<?php
                          if($error!=''){  ?>
        <div class="alert alert-danger"><?php echo $error; ?></div>
        <?php }
        ///var_dump($this->session->flashdata('success'));
        if($this->session->flashdata('success')!=''){?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
        <?php } ?>
						
						<form role="form" novalidate='novalidate' method="post" id="validation-form" enctype="multipart/form-data" action="<?php echo base_url('superadmin/blog/editBlog'); ?>" class="form-horizontal form-groups-bordered">
			
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Title*</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" data-rule-required="true" id="blog_name" name="blog_name" placeholder="Title" value="<?php echo $blog_data[0]['title'];?>">
								</div>
							</div>

							<?php 
										   $featured = "";
										   $trending = "";
										  
										   if($blog_data[0]['category']=="featured"){
											$featured = "selected=selected";
											}
											if($blog_data[0]['category']=="trending"){
												$trending = "selected=selected";
											}
											
								?>
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Category*</label>
								
								<div class="col-sm-5">
									<!-- <input type=""  data-rule-required="true"  placeholder="Icon Class"> -->
									<select class="form-control" data-rule-required="true" id="blog_category" name="blog_category">
										  <option value="featured" <?php echo $featured;?> >Featured</option>
										  <option value="trending" <?php echo $trending;?> >Trending</option>
										  <!-- <option value="image-gallery">Image Gallery</option>
										  <option value="media-updates">Media Updates</option> -->
										  
										</select>
								</div>
							</div>


							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Sub Category*</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" data-rule-required="true" value="<?php echo $blog_data[0]['sub_category'];?>"  id="sub_category" name="sub_category" placeholder="Sub Category">
								</div>
							</div>


							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Author *</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" data-rule-required="true" value="<?php echo $blog_data[0]['author'];?>"  id="author" name="author" placeholder="Author">
								</div>
							</div>


							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Published Date *</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control datepicker" data-rule-required="true" value="<?php echo $blog_data[0]['publish_date'];?>"  id="publish_date" name="publish_date" placeholder="Published Date">
								</div>
							</div>

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label"> Short Description*</label>
								
								<div class="col-sm-7">
								
									<textarea class="form-control" rows="7" id="short_desc" data-rule-required="true"  name="short_desc" placeholder="Short Description"><?php echo $blog_data[0]['short_desc'];?></textarea>
									
								</div>
							</div>

							

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Description *</label>
								
								<div class="col-sm-7">
								
									<textarea class="form-control summernote" rows="7" cols="10" id="blog_desc" data-rule-required="true" name="blog_desc" placeholder="Description"><?php echo $blog_data[0]['description'];?></textarea>
									
								</div>
							</div>

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label"></label>
								<div class="col-sm-7">
									<img src="<?php echo base_url();?>images/ref.jpg" width="95%">
								</div>
								
								
							</div>
							
							


							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload Thumbnail Image</label>
								
								<div class="col-sm-5">
									<input type="file" class="form-control"  id="media_banner_img" name="media_banner_img" placeholder="Banner Image>
									
									<label for="field-1" class="col-sm-10 control-label">Upload Image size 768X500 * and less than 2MB</label>
								</div>
							</div>


							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload Banner Desktop Image</label>
								
								<div class="col-sm-5">
									<input type="file" class="form-control"   id="press_banner_desktop_img" name="press_banner_desktop_img" placeholder="Banner Image>
									
									<label for="field-1" class="col-sm-10 control-label">Upload Image size 1920x1200 * and less than 2MB</label>
								</div>
							</div>


							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload Banner Mobile Image</label>
								
								<div class="col-sm-5">
									<input type="file" class="form-control"   id="press_banner_mobile_img" name="press_banner_mobile_img" placeholder="Banner Image>
									
									<label for="field-1" class="col-sm-10 control-label">Upload Image size 768X500 * and less than 2MB</label>
								</div>
							</div>
							<input type="hidden" value="<?php echo $blog_data[0]['id'];?>" class="form-control" id="blog_id" name="blog_id"  />
							

							
							
							
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-5">
									<button type="submit"  name="btn_submit" class="btn btn-success">Submit</button>
								</div>
							</div>
						</form>
						
					</div>
				
				</div>
			
			</div>
		</div>
		
		
		
		
		
		<!-- Footer -->
		
	</div>

	
	
	
	
	

	
</div>

<?php $this->load->view('admin/admin-footer.php'); ?> 
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>	
  <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
  <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<script>

jQuery("#validation-form").validate();

filer_default_opts = {
            changeInput2: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><div class="jFiler-input-text"><h3>Drag&Drop files here</h3> <span style="display:inline-block; margin: 15px 0">or</span></div><a class="jFiler-input-choose-btn btn-custom blue-light">Browse Files</a></div></div>',
            templates: {
                box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
                item: '<li class="jFiler-item" style="width:49%">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-thumb-overlay">\
    										<div class="jFiler-item-info">\
    											<div style="display:table-cell;vertical-align: middle;">\
    												<span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
    												<span class="jFiler-item-others">{{fi-size2}}</span>\
    											</div>\
    										</div>\
    									</div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <li>{{fi-progressBar}}</li>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                            <li><a class="icon-jfi-trash jFiler-item-trash-action" id="" ></a></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                        </li>',
                itemAppend: '<li class="jFiler-item" style="width:49%">\
                                <div class="jFiler-item-container">\
                                    <div class="jFiler-item-inner">\
                                        <div class="jFiler-item-thumb">\
                                            <div class="jFiler-item-status"></div>\
                                            <div class="jFiler-item-thumb-overlay">\
        										<div class="jFiler-item-info">\
        											<div style="display:table-cell;vertical-align: middle;">\
        												<span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
        												<span class="jFiler-item-others">{{fi-size2}}</span>\
        											</div>\
        										</div>\
        									</div>\
                                            {{fi-image}}\
                                        </div>\
                                        <div class="jFiler-item-assets jFiler-row">\
                                            <ul class="list-inline pull-left">\
                                                <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                            </ul>\
                                            <ul class="list-inline pull-right">\
                                            </ul>\
                                        </div>\
                                    </div>\
                                </div>\
                            </li>',
                progressBar: '<div class="bar"></div>',
                itemAppendToEnd: false,
                removeConfirmation: true,
                _selectors: {
                    list: '.jFiler-items-list',
                    item: '.jFiler-item',
                    progressBar: '.bar',
                    remove: '.jFiler-item-trash-action'
                }
            }
            
        };
jQuery('.datepicker').datepicker({
    dateFormat: 'dd M yy'
 });
jQuery(document).ready(function() {
     
	 jQuery('#media_banner_img').filer({
     	limit: 1,
		maxSize: 1,
		extensions: ["jpg", "png", "gif","jpeg"],
		showThumbs: true,
		addMore: false,
		showThumbs: true,
		templates: filer_default_opts.templates,
		files : [
					<?php 
						$finfo = finfo_open(FILEINFO_MIME_TYPE);
						$mime = finfo_file($finfo, 'uploads/'.$blog_data[0]['thumbnail_img']);
						finfo_close($finfo);
						//$mime = "jpeg";
						?>
				    {
				        name: "<?php echo $blog_data[0]['id']; ?>",
				        size: <?php echo filesize('uploads/'.$blog_data[0]['thumbnail_img']);?>,
				        type: "<?php echo $mime;?>",
				        file: "<?php echo base_url();?>uploads/<?php echo $blog_data[0]['thumbnail_img'];?>",
				        url: "<?php echo base_url().'uploads/'.$blog_data[0]['thumbnail_img'];?>"
				    } ,
				    
				]
     }); 


	 jQuery('#press_banner_desktop_img').filer({
     	limit: 1,
		maxSize: 1,
		extensions: ["jpg", "png", "gif","jpeg"],
		showThumbs: true,
		addMore: false,
		showThumbs: true,
		templates: filer_default_opts.templates,
		files : [
					<?php 
						$finfo = finfo_open(FILEINFO_MIME_TYPE);
						$mime = finfo_file($finfo, 'uploads/'.$blog_data[0]['banner_desktop_img']);
						finfo_close($finfo);
						//$mime = "jpeg";
						?>
				    {
				        name: "<?php echo $blog_data[0]['id']; ?>",
				        size: <?php echo filesize('uploads/'.$blog_data[0]['banner_desktop_img']);?>,
				        type: "<?php echo $mime;?>",
				        file: "<?php echo base_url();?>uploads/<?php echo $blog_data[0]['banner_desktop_img'];?>",
				        url: "<?php echo base_url().'uploads/'.$blog_data[0]['banner_desktop_img'];?>"
				    } ,
				    
				]
     }); 


      jQuery('#press_banner_mobile_img').filer({
     	limit: 1,
		maxSize: 1,
		extensions: ["jpg", "png", "gif","jpeg"],
		showThumbs: true,
		addMore: false,
		showThumbs: true,
		templates: filer_default_opts.templates,
		files : [
					<?php 
						$finfo = finfo_open(FILEINFO_MIME_TYPE);
						$mime = finfo_file($finfo, 'uploads/'.$blog_data[0]['banner_mobile_img']);
						finfo_close($finfo);
						//$mime = "jpeg";
						?>
				    {
				        name: "<?php echo $blog_data[0]['id']; ?>",
				        size: <?php echo filesize('uploads/'.$blog_data[0]['banner_mobile_img']);?>,
				        type: "<?php echo $mime;?>",
				        file: "<?php echo base_url();?>uploads/<?php echo $blog_data[0]['banner_mobile_img'];?>",
				        url: "<?php echo base_url().'uploads/'.$blog_data[0]['banner_mobile_img'];?>"
				    } ,
				    
				]
     });    

       
     
      jQuery('.summernote').summernote({
      	 	height: 300,
            callbacks: {
            	onImageUpload: function(files, editor, welEditable) {
                sendFile(files[0], editor, welEditable);
            	}
            }
            
        });

       function sendFile(file, editor, welEditable) {
            data = new FormData();
            data.append("file", file);
            jQuery.ajax({
                data: data,
                type: "POST",
                url: "<?php echo base_url();?>superadmin/blog/uploadImage",
                cache: false,
                contentType: false,
                processData: false,
                success: function(url) {
                    //editor.insertImage(url);
                    jQuery('.summernote').summernote('insertImage', url);
                }
            });
        }


   
      
});




	</script>  

</body>
</html>
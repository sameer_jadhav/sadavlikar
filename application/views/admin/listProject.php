<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/admin-head.php'); ?> 
<body class="page-body  page-left-in" data-url="http://neon.dev">
<style type="text/css">
/*ul .table_ul { display: table; width: 600px; font-size:0;}
li .ui-state-default { display: inline-table; width: 33%;background:red; font-size:15px;}*/
.table_ul{
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color:#ccc;
    
}
.table_data_ul
{
	 list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;

}

.ui-state-default{
    float: left;
    
}
.ui-state-default 
{
	display: block;
    text-align: center;
    text-decoration: none;
    font-weight:bold;
 
    color :#a6a7aa;
    

}
.main_col li{min-height:100px; width:100%;}
.title_span{  border: 1px solid #f5f5f6;
    min-height: 104px;
    float: left;
    padding: 15px;
    min-width: 322px;
    text-align: center;

}
.table_ul li{padding: 15px 142.9px;}





</style>
<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<?php $this->load->view('admin/admin-sidebar.php'); ?> 

	<div class="main-content">
		<?php $this->load->view('admin/admin_top_nav.php'); ?> 		
		
		
		<hr />
		

		
		
		<div class="row">
			<div class="col-sm-12">
			<div class="panel-heading">
						<div class="panel-title">
							<?php echo $pagetitle;?>
						</div>

						
					</div>
					<?php
			          if($this->session->flashdata('error')!=''){  ?>
			        <div class="alert alert-danger"><?php echo $error; ?></div>
			        <?php }
			        if($this->session->flashdata('success')!=''){?>
			        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
			        <?php } ?>	
			        
			        

			        <!-- <span class="form-control" style="font-weight:bold;">Drag and Drop List</span>
			        <ul  class="table_ul ">
					  <li class="ui-state-default"><span>Title</span></li>
					  <li class="ui-state-default"><span>Image</span></li>
					  <li class="ui-state-default"><span>Actions</span></li>
					</ul>

					<ul id="sortable" class="table_data_ul main_col">
					<?php 
             		//for($i=0;$i<count($work_data);$i++){ 
            		?>
					
					  <li  id="<?php //echo $work_data[$i]->work_id;?>" class="ui-state-default" >
					  <span class="title_span"><?php //echo $work_data[$i]->work_title;?></span>
					 

					<span class="title_span">  <img width="125"  src="<?php //echo base_url();?>uploads/<?php //echo $work_data[$i]->work_image;?>" alt="image">

</span>
					  <span class="title_span"><a href="<?php //echo base_url(); ?>superadmin/admin/editWork/<?php //echo base64_encode($work_data[$i]->work_id); ?>" class="btn btn-default btn-sm btn-icon icon-left">
							<i class="entypo-pencil"></i>
							Edit
						</a>
						<a  href="javascript:void(0)" onclick="delete_work('<?php //echo base64_encode($work_data[$i]->work_id); ?>')" class="btn btn-danger btn-sm btn-icon icon-left">
							<i class="entypo-cancel"></i>
							Delete
						</a></span>
						</li>
					 
					
					<?php //} ?>
					</ul> -->
					<span class="form-control" style="font-weight:bold;">Project Listing</span>
				<table class="table table-bordered datatable childgrid" id="table1">
			<thead>
				<tr>
					<th>Project Name</th>
                    <th>Short Description</th>
                    <th>Featured Image</th>
                    <th>Tags</th>
                    <th>Actions</th>
				</tr>
			</thead>
			<tbody>
			<?php 
             for($i=0;$i<count($project_data);$i++){ 
            ?>
				<tr class="odd gradeX sortable">
					<td><?php echo $project_data[$i]->project_name;?></td>
                    <td><?php echo $project_data[$i]->project_short_desc;?></td>
                    
                    <td>
                                
                                  <img width="125"  src="<?php echo base_url();?>uploads/<?php echo $project_data[$i]->project_featured_img;?>" alt="image">
                                  
                                
                     </td>
					
					<td><?php echo $project_data[$i]->tag_ids;?></td>
					<td>
						<a href="<?php echo base_url(); ?>superadmin/admin/editProject/<?php echo base64_encode($project_data[$i]->project_id); ?>" class="btn btn-default btn-sm btn-icon icon-left">
							<i class="entypo-pencil"></i>
							Edit
						</a>
						
						<a  href="javascript:void(0)" onclick="delete_work('<?php echo base64_encode($project_data[$i]->project_id); ?>')" class="btn btn-danger btn-sm btn-icon icon-left">
							<i class="entypo-cancel"></i>
							Delete
						</a>
					</td>	
				</tr>
				<?php } ?>
			</tbody>
		</table>
		
			</div>
		</div>
		
		
		
		
		
		<!-- Footer -->
		
	</div>

	
	
	
	
	

	
</div>

<?php $this->load->view('admin/admin-footer.php'); ?> 	
<script type="text/javascript">
		var responsiveHelper;
		var breakpointDefinition = {
		    tablet: 1024,
		    phone : 480
		};
		var tableContainer;
		
			jQuery(document).ready(function($)
			{
				tableContainer = $("#table-1");
				
				tableContainer.dataTable({
					"sPaginationType": "bootstrap",
					"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
					"bStateSave": true,
					
		
				    // Responsive Settings
				    bAutoWidth     : false,
				    fnPreDrawCallback: function () {
				        // Initialize the responsive datatables helper once.
				        if (!responsiveHelper) {
				            responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
				        }
				    },
				    fnRowCallback  : function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				        responsiveHelper.createExpandIcon(nRow);
				    },
				    fnDrawCallback : function (oSettings) {
				        responsiveHelper.respond();
				    }
				});
				
				$(".dataTables_wrapper select").select2({
					minimumResultsForSearch: -1
				});
			});

			function delete_work(work_id)
          {
            var x = window.confirm("Do you really want to delete this work?");
            if (x == true) {
                jQuery.post("<?php echo base_url();?>superadmin/admin/deletework","work_id="+work_id,function(res){
                window.location.href="<?php echo base_url();?>superadmin/admin/listProject/<?php echo $country_id;?>"
           })
            } 
           
          }
          function sort_val(sort_value,work_id)
          {
           // alert(sort_value);

            jQuery.post("<?php echo base_url();?>superadmin/admin/sortWork","order="+sort_value+"&work_id="+work_id,function(res){
            })
          } 
            /*jQuery("#table1 .childgrid tr").draggable({
      helper: function(){
          var selected = jQuery('.childgrid tr.selectedRow');
        if (selected.length === 0) {
          selected = jQuery(this).addClass('selectedRow');
        }
        var container = jQuery('<div/>').attr('id', 'draggingContainer');
    container.append(selected.clone().removeClass("selectedRow"));
    return container;
      }
 });

jQuery("#table1 .childgrid").droppable({
    drop: function (event, ui) {
    jQuery(this).append(ui.helper.children());
    jQuery('.selectedRow').remove();
    }
});

jQuery(document).on("click", ".childgrid tr", function () {
	
    jQuery(this).toggleClass("selectedRow");
});*/
jQuery( function() {
    jQuery( "#sortable" ).sortable({
      revert: true,
      stop: function(e, ui) {
      	var data = new Array();
            jQuery.map(jQuery(this).find('li'), function(el) {
            	 data.push([el.id,jQuery(el).index()])
                //return el.id + ' = ' + jQuery(el).index();
            });
          
            var json_str = JSON.stringify(data);
            jQuery.post("<?php echo base_url();?>superadmin/admin/sortWorkorder","data="+json_str,function(res){

            })
        }
    });
 });   
		</script>

</body>
</html>
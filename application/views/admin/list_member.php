<?php 
$page=$this->uri->segment('3');

?>

<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/admin-head.php'); ?> 
<body class="page-body  page-left-in" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<?php $this->load->view('admin/admin-sidebar.php'); ?> 

	<div class="main-content">
		<?php $this->load->view('admin/admin_top_nav.php'); ?> 		
		
		
		<hr />
		

		
		
		<div class="row">
			<div class="col-sm-12">
			<div class="panel-heading">
						<div class="panel-title">
							<?php echo $pagetitle;?>
						</div>

						
					</div>
					<?php if($page=="listPresi"){ ?>
					<a href="<?php echo base_url();?>superadmin/mandal/addPresi/<?Php echo $master_id;?>">+ Add अध्यक्ष </a>
					<?php }else if($page=="listHead"){ ?>
					<a href="<?php echo base_url();?>superadmin/civic/addHead/<?Php echo $master_id;?>">+ Add सरपंच  </a>
					<?php } if($page=="listEmployee"){ ?>
					<a href="<?php echo base_url();?>superadmin/civic/addEmployee/<?Php echo $master_id;?>">+ Add कर्मचारी   </a>
					<?php }else{?>
					<a href="<?php echo base_url();?>superadmin/mandal/addMember/<?Php echo $master_id;?>">+ Add Member </a>
					<?php } ?>
					<?php
			          if($error!=''){  ?>
			        <div class="alert alert-danger"><?php echo $error; ?></div>
			        <?php }
			        if($this->session->flashdata('success')!=''){?>
			        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
			        <?php } ?>					
				<table class="table table-bordered datatable" id="table-1">
			<thead>
				<tr>
					<th data-hide="phone">Name</th>
					<th data-hide="phone">Image</th>
					<th data-hide="phone">Designation</th>
					<th data-hide="phone">Contact</th>
					<th>Sort Order</th>
					
					
					<th>Actions</th>
					
				</tr>
			</thead>
			<tbody>
			<?php 
			//print_r($villege[0]->title);
                for($i=0;$i<count($villege);$i++){ 
            ?>
				<tr class="odd gradeX">
					<td><?php echo $villege[$i]['name'];?></td>
					<td>
						 <img width="125"  src="<?php echo base_url();?>uploads/<?php echo $villege[$i]['img_name'];?>" alt="image">
					</td>
					<td><?php echo $villege[$i]['designation'];?></td>
					<td><?php echo $villege[$i]['contact'];?></td>
					<td><input type="text" class="form-control"  size="3" name="sort_order" id="sort_order" value="<?php echo $villege[$i]['sort_order'];?>" onKeyup="sort_val(this.value,<?php echo $villege[$i]['id'];?>)"></td>
					
					<td>
						<?php if($page=="listPresi"){ ?>
							<a href="<?php echo base_url(); ?>superadmin/mandal/editPresi/<?php echo base64_encode($master_id);?>/<?php echo base64_encode($villege[$i]['id']); ?>" class="btn btn-default btn-sm btn-icon icon-left">
								<i class="entypo-pencil"></i>
								Edit
							</a>
							
							<a  href="javascript:void(0)" onclick="delete_presi('<?php echo base64_encode($villege[$i]['id']);  ?>','<?php echo base64_encode($master_id);?>');" class="btn btn-danger btn-sm btn-icon icon-left">
								<i class="entypo-cancel"></i>
								Delete
							</a>
						<?Php } else { ?>
							<a href="<?php echo base_url(); ?>superadmin/mandal/editMember/<?php echo base64_encode($master_id);?>/<?php echo base64_encode($villege[$i]['id']); ?>" class="btn btn-default btn-sm btn-icon icon-left">
								<i class="entypo-pencil"></i>
								Edit
							</a>
							
							<a  href="javascript:void(0)" onclick="delete_member('<?php echo base64_encode($villege[$i]['id']); ?>','<?php echo base64_encode($master_id);?>');" class="btn btn-danger btn-sm btn-icon icon-left">
								<i class="entypo-cancel"></i>
								Delete
							</a>
						<?php } ?>
					</td>	
						
				</tr>
				<?php } ?>
			</tbody>
		</table>
			</div>
		</div>
		
		
		
		
		
		<!-- Footer -->
		
	</div>

	
	
	
	
	

	
</div>

<?php $this->load->view('admin/admin-footer.php'); ?> 	
<script type="text/javascript">
		var responsiveHelper;
		var breakpointDefinition = {
		    tablet: 1024,
		    phone : 480
		};
		var tableContainer;
		
			jQuery(document).ready(function($)
			{
				tableContainer = $("#table-1");
				
				tableContainer.dataTable({
					"sPaginationType": "bootstrap",
					"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
					"bStateSave": true,
					
		
				    // Responsive Settings
				    bAutoWidth     : false,
				    fnPreDrawCallback: function () {
				        // Initialize the responsive datatables helper once.
				        if (!responsiveHelper) {
				            responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
				        }
				    },
				    fnRowCallback  : function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				        responsiveHelper.createExpandIcon(nRow);
				    },
				    fnDrawCallback : function (oSettings) {
				        responsiveHelper.respond();
				    }
				});
				
				$(".dataTables_wrapper select").select2({
					minimumResultsForSearch: -1
				});
			});

				function delete_mandal(org_id)
          {
            var x = window.confirm("Do you really want to delete this info?");
            if (x == true) {
                jQuery.post("<?php echo base_url();?>superadmin/mandal/deleteMandal","org_id="+org_id,function(res){
                window.location.href="<?php echo base_url();?>superadmin/mandal/listMandal"
           })
            } 
           
          }

          function delete_member(id,master_id){

            var x = window.confirm("Do you really want to delete this info?");
            if (x == true) {
                jQuery.post("<?php echo base_url();?>superadmin/mandal/deleteMember","id="+id,function(res){
                window.location.href="<?php echo base_url();?>superadmin/mandal/listMember/"+master_id
           })
            } 

          }

          function delete_presi(id,master_id){

            var x = window.confirm("Do you really want to delete this info?");
            if (x == true) {
                jQuery.post("<?php echo base_url();?>superadmin/mandal/deleteMember","id="+id,function(res){
                window.location.href="<?php echo base_url();?>superadmin/mandal/listPresi/"+master_id
           })
            } 

          }

           function sort_val(sort_value,member_id)
          {
           //alert(sort_value);

            jQuery.post("<?php echo base_url();?>superadmin/admin/sortMember","order="+sort_value+"&member_id="+member_id,function(res){
            })
          } 
		</script>

</body>
</html>
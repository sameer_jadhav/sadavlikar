<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/admin-head.php'); ?> 
<body class="page-body  page-left-in" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<?php $this->load->view('admin/admin-sidebar.php'); ?> 

	<div class="main-content">
		<?php $this->load->view('admin/admin_top_nav.php'); ?> 		
		
		
		<hr />
		

		
		
		<div class="row">
			<div class="col-sm-12">
			<div class="panel-heading">
						<div class="panel-title">
							<?php echo $pagetitle;?>
						</div>

						
					</div>
					<?php
			          if($error!=''){  ?>
			        <div class="alert alert-danger"><?php echo $error; ?></div>
			        <?php }
			        if($this->session->flashdata('success')!=''){?>
			        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
			        <?php } ?>					
				<table class="table table-bordered datatable" id="table-1">
			<thead>
				<tr>
					<th data-hide="phone">Name</th>
					<th data-hide="phone">Email Id</th>
					<th data-hide="phone">Image</th>
					<th data-hide="phone">Contact</th>
					<th data-hide="phone">Address</th>
					
					
					
				</tr>
			</thead>
			<tbody>
			<?php 
			//print_r($villege[0]->title);
                for($i=0;$i<count($villege);$i++){ 
            ?>
				<tr class="odd gradeX">
					<td><?php echo $villege[$i]['name'];?></td>
					<td><?php echo $villege[$i]['email_id'];?></td>
					<td>
						 <img width="125"  src="<?php echo $villege[$i]['img_url'];?>" alt="image">
					</td>
					<td><?php echo $villege[$i]['contact'];?></td>
					<td><?php echo $villege[$i]['address'];?></td>
					
					
				</tr>
				<?php } ?>
			</tbody>
		</table>
			</div>
		</div>
		
		
		
		
		
		<!-- Footer -->
		
	</div>

	
	
	
	
	

	
</div>

<?php $this->load->view('admin/admin-footer.php'); ?> 	
<script type="text/javascript">
		var responsiveHelper;
		var breakpointDefinition = {
		    tablet: 1024,
		    phone : 480
		};
		var tableContainer;
		
			jQuery(document).ready(function($)
			{
				tableContainer = $("#table-1");
				
				tableContainer.dataTable({
					"sPaginationType": "bootstrap",
					"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
					"bStateSave": true,
					
		
				    // Responsive Settings
				    bAutoWidth     : false,
				    fnPreDrawCallback: function () {
				        // Initialize the responsive datatables helper once.
				        if (!responsiveHelper) {
				            responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
				        }
				    },
				    fnRowCallback  : function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				        responsiveHelper.createExpandIcon(nRow);
				    },
				    fnDrawCallback : function (oSettings) {
				        responsiveHelper.respond();
				    }
				});
				
				$(".dataTables_wrapper select").select2({
					minimumResultsForSearch: -1
				});
			});

				function delete_notification(noti_id)
          {
            var x = window.confirm("Do you really want to delete this info?");
            if (x == true) {
                jQuery.post("<?php echo base_url();?>superadmin/notification/deleteNotification","noti_id="+noti_id,function(res){
                window.location.href="<?php echo base_url();?>superadmin/notification/listNotification"
           })
            } 
           
          }

           function sort_val(sort_value,artist_id)
          {
           // alert(sort_value);

            jQuery.post("<?php echo base_url();?>superadmin/admin/sortArtist","order="+sort_value+"&artist_id="+artist_id,function(res){
            })
          } 
		</script>

</body>
</html>

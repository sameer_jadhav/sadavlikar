<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/admin-head.php'); ?> 
<body class="page-body  page-left-in" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<?php $this->load->view('admin/admin-sidebar.php'); ?> 

	<div class="main-content">
		<?php $this->load->view('admin/admin_top_nav.php'); ?> 		
		
		
		<hr />
		

		
		
		<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title">
							<?php echo $pagetitle;?>
						</div>
						
						
					</div>
					
					<div class="panel-body">
						
						<form role="form" novalidate='novalidate' method="post" id="validation-form" enctype="multipart/form-data"  action="<?php echo base_url('superadmin/admin/editExpertise'); ?>" class="form-horizontal form-groups-bordered">
			
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Expertise Name *</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" value="<?php echo $category_data[0]['cat_name'];?>"  data-rule-required="true" id="cat_name" name="cat_name" placeholder="Name">
									<?php if(form_error('member_name')!=''){ ?><span class="help-block" for="password">This field is required.</span> <?php } ?>
								</div>

							</div>

				

							

							
							
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload Featured Image*</label>
								
								<div class="col-sm-5">
									<input type="file" class="form-control"  id="cat_featured_image" name="cat_featured_image" placeholder="Placeholder" >
									<?php if(form_error('artist_name')!=''){ ?><span class="help-block" for="password">This field is required.</span> <?php } ?>
								</div>
								<label class="control-label " for="first-name"> Upload Image size 370 X 370  <span class="required">*</span>
							</div>
							<input type="hidden" name="cat_id" value="<?php echo $category_data[0]['cat_id'];?>" >
                    		<input type="hidden" name="old_img" value="<?php echo $category_data[0]['cat_featured_image'];?>" >
							
							
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-5">
									<button type="submit"  name="btn_submit" class="btn btn-success">Submit</button>
								</div>
							</div>
						</form>
						
					</div>
				
				</div>
			
			</div>
		</div>
		
		
		
		
		
		<!-- Footer -->
		
	</div>

	
	
	
	
	

	
</div>

<?php $this->load->view('admin/admin-footer.php'); ?> 	
<script>
jQuery("#validation-form").validate();
	</script>  

</body>
</html>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/admin-head.php'); ?> 
<body class="page-body  page-left-in" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<?php $this->load->view('admin/admin-sidebar.php'); ?> 

	<div class="main-content">
		<?php $this->load->view('admin/admin_top_nav.php'); ?> 		
		
		
		<hr />
		

		
		
		<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title">
							<?php echo $pagetitle;?>
						</div>
						
						
					</div>
					
					<div class="panel-body">
					<?php
                          if($error!=''){  ?>
        <div class="alert alert-danger"><?php echo $error; ?></div>
        <?php }
        ///var_dump($this->session->flashdata('success'));
        if($this->session->flashdata('success')!=''){?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
        <?php } ?>
						
						<form role="form" novalidate='novalidate' method="post" id="validation-form" enctype="multipart/form-data" action="<?php echo base_url('superadmin/admin/editProject'); ?>" class="form-horizontal form-groups-bordered">
			
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Project Title *</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" data-rule-required="true" id="project_name" name="project_name" value="<?php echo $project_data[0]['project_name'];?>" placeholder="Project Name">
								</div>
							</div>

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Project Heading 1</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" value="<?php echo $project_data[0]['heading1'];?>"  id="project_heading1" name="project_heading1" placeholder="Project Heading 1">
								</div>
							</div>

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Project Heading 2</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" value="<?php echo $project_data[0]['heading2'];?>" id="project_heading2" name="project_heading2" placeholder="Project Heading 2">
								</div>
							</div>

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Project Short Description *</label>
								
								<div class="col-sm-5">
								
									<textarea class="form-control" rows="5" id="project_short_desc" data-rule-required="true" name="project_short_desc" placeholder="Project Short Description"><?php echo $project_data[0]['project_short_desc'];?></textarea>
									
								</div>
							</div>

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Project Description</label>
								<div class="col-sm-4">
								
									<textarea class="form-control" rows="10"  id="project_des"  name="project_des_left" placeholder="Project Left Description"><?php echo $project_data[0]['project_des'];?></textarea>
									
								</div>

								<div class="col-sm-4">
								
									<textarea class="form-control" rows="10" id="project_des"  name="project_des_right" placeholder="Project Right Description"><?php echo $project_data[0]['right_desc'];?></textarea>
									
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label">Select Expertise*</label>
								
								<div class="col-sm-5">
									<select class="form-control" data-rule-required="true" id="select_expertise" name="select_expertise" >
										<?Php for($m=0;$m<count($cat_data);$m++){
											$sel_exp = "";
											if( $project_data[0]['cat_id']==$cat_data[$m]['cat_id'])
											{
												$sel_exp = "selected='selected'";
											}
										 ?>
										<option <?php echo $sel_exp;?> value="<?Php echo $cat_data[$m]['cat_id'];?>"><?Php echo $cat_data[$m]['cat_name'];?></option>
                          				<?Php } ?>
									</select>
								</div>
							</div>

							
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Select Country*</label>
								
								<div class="col-sm-5">
									<select class="form-control" data-rule-required="true" id="select_country" name="select_country" >
										<?Php for($j=0;$j<count($country_data);$j++){

										$sel_country = "";
											if( $project_data[0]['country_id']==$country_data[$j]['country_id'])
											{
												$sel_country = "selected='selected'";
											}
											 ?>
										<option <?php echo $sel_country;?>value="<?Php echo $country_data[$j]['country_id'];?>"><?Php echo $country_data[$j]['country_name'];?></option>
                          				<?Php } ?>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label">Select Status*</label>
								<?Php 
									$check_ongoing_status = "";
									$check_archive_status = "";
											if( $project_data[0]['status']==1)
											{
												$check_ongoing_status = "checked='checked'";
											}else if( $project_data[0]['status']==2)
											{
												$check_archive_status = "checked='checked'";
											}
								?>
								<div class="col-sm-5">
									<ul class="icheck-list">
								    <li>
								        <input tabindex="7" class="icheck" type="radio" id="minimal-radio-1" value="1" name="status" <?php echo $check_ongoing_status;?>>
								        <label for="minimal-radio-1">Ongoing</label>
								    </li>
								    <li>
								        <input tabindex="8" class="icheck" type="radio" id="minimal-radio-2" value="2" name="status" <?php echo $check_archive_status;?> >
								        <label for="minimal-radio-2">Archive</label>
								    </li>
								    </ul>
								</div>
							</div>

							<div class="form-group">
							<?Php 
									$check_yes = "";
									$check_no = "";
											if( $project_data[0]['key_project']==1)
											{
												$check_yes = "checked='checked'";
											}else if( $project_data[0]['key_project']==2)
											{
												$check_no = "checked='checked'";
											}
								?>
								<label class="col-sm-3 control-label">Key Project*</label>
								
								<div class="col-sm-5">
									<ul class="icheck-list">
								    <li>
								        <input tabindex="7" class="icheck" type="radio" id="minimal-radio-1" value="1" name="key_project" <?php echo $check_yes;?>>
								        <label for="minimal-radio-1">Yes</label>
								    </li>
								    <li>
								        <input tabindex="8" class="icheck" type="radio" id="minimal-radio-2"  value="2" name="key_project" <?php echo $check_no;?> >
								        <label for="minimal-radio-2">No</label>
								    </li>
								    </ul>
								</div>
							</div>

							

							
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload Featured Image*</label>
								
								<div class="col-sm-6">
									<input type="file" class="form-control"  id="project_featured_img" name="project_featured_img" placeholder="Placeholder">
									
									<label for="field-1" class="col-sm-10 control-label">Upload Image size 300 X 300 * and less than 2MB</label>
								</div>
							</div>

							<!-- <div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Tags *</label>
								
								<div class="col-sm-5">

									<input type="text" value="" class="form-control" id="tags" name="tags" placeholder="Tags" />
									
									
								</div>
							</div> -->
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload Banner Image*</label>
								
								<div class="col-sm-5">
									<input type="file" class="form-control"  id="project_banner_img" name="project_banner_img" placeholder="Placeholder">
									
									<label for="field-1" class="col-sm-10 control-label">Upload Image size 1366 X 768 * and less than 2MB</label>
								</div>
							</div>


							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Banner Heading 1</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control"  id="banner_heading1" name="banner_heading1" placeholder="Banner Heading 1" value="<?php echo $project_data[0]['banner_heading1'];?>">
								</div>
							</div>

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Banner Heading 2</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" id="banner_heading2" name="banner_heading2" placeholder="Banner Heading 2" value="<?php echo $project_data[0]['banner_heading2'];?>">
								</div>
							</div>

							<input type="hidden" value="<?php echo $project_data[0]['project_id'];?>" class="form-control" id="project_id" name="project_id"  />
							
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload Gallery Image</label>
								
								<div class="col-sm-7">
									<input type="file" class="form-control"  id="project_gal_img" name="project_gal_img[]" placeholder="Placeholder"  multiple="multiple">
									
									<label for="field-1" class="col-sm-10 control-label">Upload Image size 300 X 300 * and less than 2MB</label>
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-5">
									<button type="submit"  name="btn_submit" class="btn btn-success">Submit</button>
								</div>
							</div>
						</form>
						
					</div>
				
				</div>
			
			</div>
		</div>
		
		
		
		
		
		<!-- Footer -->
		
	</div>

	
	
	
	
	

	
</div>

<?php $this->load->view('admin/admin-footer.php'); ?> 
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>	
<script>

jQuery("#validation-form").validate({
  rules: {
    work_url: {
      required: true,
      url: true
    }
  }
});
jQuery(document).ready(function() {

	var body = jQuery(document.body),
        filer_default_opts = {
            changeInput2: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><div class="jFiler-input-text"><h3>Drag&Drop files here</h3> <span style="display:inline-block; margin: 15px 0">or</span></div><a class="jFiler-input-choose-btn btn-custom blue-light">Browse Files</a></div></div>',
            templates: {
                box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
                item: '<li class="jFiler-item" style="width:49%">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-thumb-overlay">\
    										<div class="jFiler-item-info">\
    											<div style="display:table-cell;vertical-align: middle;">\
    												<span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
    												<span class="jFiler-item-others">{{fi-size2}}</span>\
    											</div>\
    										</div>\
    									</div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <li>{{fi-progressBar}}</li>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                            <li><a class="icon-jfi-trash jFiler-item-trash-action" id="" ></a></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                        </li>',
                itemAppend: '<li class="jFiler-item" style="width:49%">\
                                <div class="jFiler-item-container">\
                                    <div class="jFiler-item-inner">\
                                        <div class="jFiler-item-thumb">\
                                            <div class="jFiler-item-status"></div>\
                                            <div class="jFiler-item-thumb-overlay">\
        										<div class="jFiler-item-info">\
        											<div style="display:table-cell;vertical-align: middle;">\
        												<span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
        												<span class="jFiler-item-others">{{fi-size2}}</span>\
        											</div>\
        										</div>\
        									</div>\
                                            {{fi-image}}\
                                        </div>\
                                        <div class="jFiler-item-assets jFiler-row">\
                                            <ul class="list-inline pull-left">\
                                                <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                            </ul>\
                                            <ul class="list-inline pull-right">\
                                                <li><a class="icon-jfi-trash jFiler-item-trash-action" id="{{fi-name}}"></a></li>\
                                            </ul>\
                                        </div>\
                                    </div>\
                                </div>\
                            </li>',
                progressBar: '<div class="bar"></div>',
                itemAppendToEnd: false,
                removeConfirmation: true,
                _selectors: {
                    list: '.jFiler-items-list',
                    item: '.jFiler-item',
                    progressBar: '.bar',
                    remove: '.jFiler-item-trash-action'
                }
            }
            
        };

     jQuery('#project_gal_img').filer({
     	limit: 10,
		maxSize: 3,
		extensions: ["jpg", "png", "gif","jpeg"],
		showThumbs: true,
		addMore: true,
		templates: filer_default_opts.templates,
		files : [
					<?php 
					//print_r($project_gal_data);
					for($k=0;$k<count($project_gal_data);$k++){ 
						$finfo = finfo_open(FILEINFO_MIME_TYPE);
						$mime = finfo_file($finfo, 'uploads/'.$project_gal_data[$k]['img_name']);
						finfo_close($finfo);
						//echo $mime;
						//$mime = "jpeg";
						?>
				    {

				        name: "<?php echo $project_gal_data[$k]['img_name'];?>",
				        size: <?php echo filesize('uploads/'.$project_gal_data[$k]['img_name']);?>,
				        type: "<?php echo $mime;?>",
				        file: "<?php echo base_url();?>uploads/<?php echo $project_gal_data[$k]['img_name'];?>",
				        url: "<?php echo base_url();?>uploads/<?php echo $project_gal_data[$k]['img_name'];?>",
				        id : "<?php echo $project_gal_data[$k]['gallery_img_id'];?>"
				    } ,
				    <?php } ?>
				]
     });  

     jQuery('#project_featured_img').filer({
     	limit: 1,
		maxSize: 1,
		extensions: ["jpg", "png", "gif","jpeg"],
		showThumbs: true,
		addMore: false,
		showThumbs: true,
		templates: filer_default_opts.templates,
		files : [
					<?php 
						$finfo = finfo_open(FILEINFO_MIME_TYPE);
						$mime = finfo_file($finfo, 'uploads/'.$project_data[0]['project_featured_img']);
						finfo_close($finfo);
						//$mime = "jpeg";
						?>
				    {
				        name: "<?php echo $project_data[0]['project_featured_img'];?>",
				        size: <?php echo filesize('uploads/'.$project_data[0]['project_featured_img']);?>,
				        type: "<?php echo $mime;?>",
				        file: "<?php echo base_url();?>uploads/<?php echo $project_data[0]['project_featured_img'];?>",
				        url: "<?php echo base_url().'uploads/'.$project_data[0]['project_featured_img'];?>"
				    } ,
				    
				]
     });  

     jQuery('#project_banner_img').filer({
     	limit: 1,
		maxSize: 1,
		extensions: ["jpg", "png", "gif","jpeg"],
		showThumbs: true,
		addMore: false,
		showThumbs: true,
		templates: filer_default_opts.templates,
		files : [
					<?php 
						$finfo = finfo_open(FILEINFO_MIME_TYPE);
						$mime = finfo_file($finfo, 'uploads/'.$project_data[0]['project_banner_img']);
						finfo_close($finfo);
						//$mime = "jpeg";
						?>
				    {
				        name: "<?php echo $project_data[0]['project_banner_img'];?>",
				        size: <?php echo filesize('uploads/'.$project_data[0]['project_banner_img']);?>,
				        type: "<?php echo $mime;?>",
				        file: "<?php echo base_url();?>uploads/<?php echo $project_data[0]['project_banner_img'];?>",
				        url: "<?php echo base_url().'uploads/'.$project_data[0]['project_banner_img'];?>"
				    } ,
				    
				]
     });    
     jQuery(".icon-jfi-trash").click(function(){
     	alert(1)
     	var filename = jQuery(this).attr('id')
     	alert(filename);
     	if(filename!="")
     	{
     		jQuery.post("<?php echo base_url();?>superadmin/admin/delete_gal_images/","filename="+filename,function(data){

			})
     	}
     })    
});





         


jQuery(function($) {
    function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }
 
    $( "#tags" )
      // don't navigate away from the field on tab when selecting an item
      .on( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
          event.preventDefault();
        }
      })
      .autocomplete({
        source: function( request, response ) {
          $.getJSON( "<?php echo base_url();?>superadmin/admin/search_tag", {
            term: extractLast( request.term )
          }, response );
          //alert(response)
        },
        search: function() {
          // custom minLength
          var term = extractLast( this.value );
          if ( term.length < 1 ) {
            return false;
          }
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },

        select: function( event, ui ) {
          var terms = split( this.value );
          var ids = split( ui.item.id );
          ids.push("")

          terms.pop();
          ids.pop()
         
        // add the selected item
          terms.push( ui.item.value );
          ids.push(ui.item.id)	
          //ids.push(ui.item.id)
          ids.push("")
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          
          this.value = terms.join( ", " );
          document.getElementById("tag_ids").value = ids.join( ", " );

          return false;
        }
      });
  } );
/*,
  submitHandler: function(form) {
    if(jQuery("#work_url").val()=="" && jQuery("#work_url_youtube").val()==""  )
	{
		alert("Please enter atleast one work url")
		return false;
	}else
	{
		form.submit();
	}
  }*/
	</script>  

</body>
</html>
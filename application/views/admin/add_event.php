<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/admin-head.php'); ?> 
<body class="page-body  page-left-in" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<?php $this->load->view('admin/admin-sidebar.php'); ?> 

	<div class="main-content">
		<?php $this->load->view('admin/admin_top_nav.php'); ?> 		
		
		
		<hr />
		

		
		
		<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title">
							<?php echo $pagetitle;?>
						</div>
						
						
					</div>
					
					<div class="panel-body">
					<?php
                          if($error!=''){  ?>
        <div class="alert alert-danger"><?php echo $error; ?></div>
        <?php }
        ///var_dump($this->session->flashdata('success'));
        if($this->session->flashdata('success')!=''){?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
        <?php } ?>
						
						<form role="form" novalidate='novalidate' method="post" id="validation-form" enctype="multipart/form-data" action="<?php echo base_url('superadmin/events/addEvent'); ?>" class="form-horizontal form-groups-bordered">
			
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Title*</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" data-rule-required="true" id="event_name" name="event_name" placeholder="Title">
								</div>
							</div>


							


							
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload Media Image</label>
								
								<div class="col-sm-5">
									<input type="file" class="form-control"  id="media_images" name="media_images[]" placeholder="Placeholder"  multiple="multiple">
									
									<label for="field-1" class="col-sm-10 control-label">Upload Image size 1200 X anything* and less than 2MB</label>
								</div>
							</div>

							
							
							
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-5">
									<button type="submit"  name="btn_submit" class="btn btn-success">Submit</button>
								</div>
							</div>
						</form>
						
					</div>
				
				</div>
			
			</div>
		</div>
		
		
		
		
		
		<!-- Footer -->
		
	</div>

	
	
	
	
	

	
</div>

<?php $this->load->view('admin/admin-footer.php'); ?> 
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>	
  <!-- <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
  <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script> -->
<script>

jQuery("#validation-form").validate();
jQuery(document).ready(function() {
     

     jQuery('#media_banner_img').filer({
     	limit: 1,
		maxSize: 5,
		extensions: ["jpg", "png", "gif","jpeg"],
		showThumbs: true,
		addMore: true
     });

     jQuery('#press_banner_desktop_img').filer({
     	limit: 1,
		maxSize: 5,
		extensions: ["jpg", "png", "gif","jpeg"],
		showThumbs: true,
		addMore: true
     });

     jQuery('#press_banner_mobile_img').filer({
     	limit: 1,
		maxSize: 5,
		extensions: ["jpg", "png", "gif","jpeg"],
		showThumbs: true,
		addMore: true
     });


     
     /* jQuery('#media_images').filer({
     	limit: 10,
		maxSize: 10,
		extensions: ["jpg", "png", "gif","jpeg"],
		showThumbs: true,
		addMore: true
     });  */ 

      jQuery('#media_images').filer({
     	limit: 10,
		maxSize: 10,
		extensions:["jpg", "png", "gif","jpeg"],
		showThumbs: true,
		addMore: true
     });    
});

jQuery('.datepicker').datepicker({
    dateFormat: 'dd M yy'
 });
/*jQuery("#select_expertise").change(function(){
	var expertise = jQuery("#select_expertise option:selected").text();
	alert(expertise)
	jQuery("#tags").val(expertise)
})
*/


	</script>  

</body>
</html>
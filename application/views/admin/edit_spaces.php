<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/admin-head.php'); ?> 
<body class="page-body  page-left-in" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<?php $this->load->view('admin/admin-sidebar.php'); ?> 

	<div class="main-content">
		<?php $this->load->view('admin/admin_top_nav.php'); 
		//print_r();exit;
		?> 		
		
		
		<hr />
		

		
		
		<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title">
							<?php echo $pagetitle;?>
						</div>
						
						
					</div>
					
					<div class="panel-body">
					<?php
                          if($error!=''){  ?>
        <div class="alert alert-danger"><?php echo $error; ?></div>
        <?php }
        ///var_dump($this->session->flashdata('success'));
        if($this->session->flashdata('success')!=''){?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
        <?php } ?>
						
						<form role="form" novalidate='novalidate' method="post" id="validation-form" enctype="multipart/form-data" action="<?php echo base_url('superadmin/admin/editSpaces'); ?>" class="form-horizontal form-groups-bordered">
			
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Name*</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" data-rule-required="true" id="project_name" name="project_name" placeholder="Name" value="<?php echo $spaces_data[0]['project_title'];?>">
								</div>
							</div>


							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Icon Class*</label>
								
								<div class="col-sm-5">
									<!-- <input type="text" class="form-control" data-rule-required="true" id="icon_class" name="icon_class" placeholder="Icon Class"  value="<?php echo $spaces_data[0]['icon_class'];?>" > -->
									<?php 
										   $sel_alcove = "";
										   $sel_meeting = "";
										   $sel_sanctury = "";
										   $sel_brewlab = "";
										   $sel_bar = "";
										   $sel_gaming = "";
										   $sel_drawing = "";
										   $sel_health = "";
										   $sel_gallery = "";
										if($spaces_data[0]['icon_class']=="icon-alcove"){
											$sel_alcove = "selected=selected";
										}
										if($spaces_data[0]['icon_class']=="icon-meeting-room"){
											$sel_meeting = "selected=selected";
										}
										if($spaces_data[0]['icon_class']=="icon-sanctuary"){
											$sel_sanctury = "selected=selected";
										}
										if($spaces_data[0]['icon_class']=="icon-brew-lab"){
											$sel_brewlab = "selected=selected";
										}
										if($spaces_data[0]['icon_class']=="icon-bar"){
											$sel_bar = "selected=selected";
										}
										if($spaces_data[0]['icon_class']=="icon-reality-gaming"){
											$sel_gaming = "selected=selected";
										}
										if($spaces_data[0]['icon_class']=="icon-drawing-room"){
											$sel_drawing = "selected=selected";
										}
										if($spaces_data[0]['icon_class']=="icon-health-club"){
											$sel_health = "selected=selected";
										}
										if($spaces_data[0]['icon_class']=="icon-gallery"){
											$sel_gallery = "selected=selected";
										}
										?>
									<select class="form-control" id="icon_class" name="icon_class">
										  <option value="icon-alcove" <?php echo $sel_alcove;?> >icon-alcove</option>
										  <option value="icon-meeting-room" <?php echo $sel_meeting;?> >icon-meeting-room</option>
										  <option value="icon-sanctuary" <?php echo $sel_sanctury;?> >icon-sanctuary</option>
										  <option value="icon-brew-lab" <?php echo $sel_brewlab;?>>icon-brew-lab</option>
										  <option value="icon-bar" <?php echo $sel_bar;?>>icon-bar</option>
										  <option value="icon-reality-gaming" <?php echo $sel_gaming;?>>icon-reality-gaming</option>
										  <option value="icon-drawing-room" <?php echo $sel_drawing;?>>icon-drawing-room</option>
										  <option value="icon-health-club" <?php echo $sel_health;?>>icon-health-club</option>
										  <option value="icon-gallery" <?php echo $sel_gallery;?>>icon-gallery</option>
										</select>
								</div>
							</div>

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Image Testimonial</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control"  id="img_quote" name="img_quote" placeholder="Image Testimonial" value="<?php echo $spaces_data[0]['img_quote'];?>">
								</div>
							</div>

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Testimonial By</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control"  id="quote_by" name="quote_by" placeholder="Testimonial By" value="<?php echo $spaces_data[0]['quote_by'];?>" >
								</div>
							</div>


							<!-- <div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload Icon Image*</label>
								
								<div class="col-sm-5">
									<input type="file" class="form-control"   id="project_icon_img" name="project_icon_img" placeholder="Placeholder">
									
									<label for="field-1" class="col-sm-10 control-label">Upload Image size 1366 X 768 * and less than 2MB</label>
								</div>
							</div> -->


							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Title *</label>
								
								<div class="col-sm-5">
								
									<textarea class="form-control" rows="5" id="project_quote" data-rule-required="true" name="project_quote" placeholder="Title"><?php echo $spaces_data[0]['project_quote'];?></textarea>
									
								</div>
							</div>

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Description *</label>
								
								<div class="col-sm-5">
								
									<textarea class="form-control" rows="5" id="project_short_desc" data-rule-required="true" name="project_short_desc" placeholder="Description"><?php echo $spaces_data[0]['project_short_desc'];?></textarea>
									
								</div>
							</div>
							
							


							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload Desktop Banner Image*</label>
								
								<div class="col-sm-5">
									<input type="file" class="form-control"  id="project_banner_img" name="project_banner_img" placeholder="Placeholder">
									
									<label for="field-1" class="col-sm-10 control-label">Upload Image size 1920x1200 * and less than 2MB</label>
								</div>
							</div>
							

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload Mobile Banner Image*</label>
								
								<div class="col-sm-5">
									<input type="file" class="form-control"   id="project_mobile_banner_img" name="project_mobile_banner_img" placeholder="Placeholder">
									
									<label for="field-1" class="col-sm-10 control-label">Upload Image size 768X500 * and less than 2MB </label>
								</div>
							</div>
							

							<input type="hidden" value="<?php echo $spaces_data[0]['id'];?>" class="form-control" id="space_id" name="space_id"  />
							

							
							
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-5">
									<button type="submit"  name="btn_submit" class="btn btn-success">Submit</button>
								</div>
							</div>
						</form>
						
					</div>
				
				</div>
			
			</div>
		</div>
		
		
		
		
		
		<!-- Footer -->

		
	</div>

	
	
	
	
	

	
</div>

<?php $this->load->view('admin/admin-footer.php'); ?> 
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>	
<script>

jQuery("#validation-form").validate({
	rules:{
		site_url : {
			url: true
		}
	}
});

filer_default_opts = {
            changeInput2: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><div class="jFiler-input-text"><h3>Drag&Drop files here</h3> <span style="display:inline-block; margin: 15px 0">or</span></div><a class="jFiler-input-choose-btn btn-custom blue-light">Browse Files</a></div></div>',
            templates: {
                box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
                item: '<li class="jFiler-item" style="width:49%">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-thumb-overlay">\
    										<div class="jFiler-item-info">\
    											<div style="display:table-cell;vertical-align: middle;">\
    												<span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
    												<span class="jFiler-item-others">{{fi-size2}}</span>\
    											</div>\
    										</div>\
    									</div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <li>{{fi-progressBar}}</li>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                            <li><a class="icon-jfi-trash jFiler-item-trash-action" id="" ></a></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                        </li>',
                itemAppend: '<li class="jFiler-item" style="width:49%">\
                                <div class="jFiler-item-container">\
                                    <div class="jFiler-item-inner">\
                                        <div class="jFiler-item-thumb">\
                                            <div class="jFiler-item-status"></div>\
                                            <div class="jFiler-item-thumb-overlay">\
        										<div class="jFiler-item-info">\
        											<div style="display:table-cell;vertical-align: middle;">\
        												<span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
        												<span class="jFiler-item-others">{{fi-size2}}</span>\
        											</div>\
        										</div>\
        									</div>\
                                            {{fi-image}}\
                                        </div>\
                                        <div class="jFiler-item-assets jFiler-row">\
                                            <ul class="list-inline pull-left">\
                                                <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                            </ul>\
                                            <ul class="list-inline pull-right">\
                                            </ul>\
                                        </div>\
                                    </div>\
                                </div>\
                            </li>',
                progressBar: '<div class="bar"></div>',
                itemAppendToEnd: false,
                removeConfirmation: true,
                _selectors: {
                    list: '.jFiler-items-list',
                    item: '.jFiler-item',
                    progressBar: '.bar',
                    remove: '.jFiler-item-trash-action'
                }
            }
            
        };

jQuery(document).ready(function() {
     
     //jQuery('#publish_date').datepicker("setValue", '10/10/2017' );


     /*jQuery('#project_icon_img').filer({
     	limit: 1,
		maxSize: 1,
		extensions: ["jpg", "png", "gif","jpeg"],
		showThumbs: true,
		addMore: false,
		showThumbs: true,
		templates: filer_default_opts.templates,
		files : [
					<?php 
						$finfo = finfo_open(FILEINFO_MIME_TYPE);
						$mime = finfo_file($finfo, 'uploads/'.$spaces_data[0]['project_icon_img']);
						finfo_close($finfo);
						//$mime = "jpeg";
						?>
				    {
				        name: "<?php echo $spaces_data[0]['id']; ?>",
				        size: <?php echo filesize('uploads/'.$spaces_data[0]['project_icon_img']);?>,
				        type: "<?php echo $mime;?>",
				        file: "<?php echo base_url();?>uploads/<?php echo $spaces_data[0]['project_icon_img'];?>",
				        url: "<?php echo base_url().'uploads/'.$spaces_data[0]['project_icon_img'];?>"
				    } ,
				    
				]
     }); */

      jQuery('#project_banner_img').filer({
     	limit: 1,
		maxSize: 1,
		extensions: ["jpg", "png", "gif","jpeg"],
		showThumbs: true,
		addMore: false,
		showThumbs: true,
		templates: filer_default_opts.templates,
		files : [
					<?php 
						$finfo = finfo_open(FILEINFO_MIME_TYPE);
						$mime = finfo_file($finfo, 'uploads/'.$spaces_data[0]['project_banner_img']);
						finfo_close($finfo);
						//$mime = "jpeg";
						?>
				    {
				        name: "<?php echo $spaces_data[0]['id']; ?>",
				        size: <?php echo filesize('uploads/'.$spaces_data[0]['project_banner_img']);?>,
				        type: "<?php echo $mime;?>",
				        file: "<?php echo base_url();?>uploads/<?php echo $spaces_data[0]['project_banner_img'];?>",
				        url: "<?php echo base_url().'uploads/'.$spaces_data[0]['project_banner_img'];?>"
				    } ,
				    
				]
     }); 

     jQuery('#project_mobile_banner_img').filer({
     	limit: 1,
		maxSize: 1,
		extensions: ["jpg", "png", "gif","jpeg"],
		showThumbs: true,
		addMore: false,
		showThumbs: true,
		templates: filer_default_opts.templates,
		files : [
					<?php 
						$finfo = finfo_open(FILEINFO_MIME_TYPE);
						$mime = finfo_file($finfo, 'uploads/'.$spaces_data[0]['project_mobile_img']);
						finfo_close($finfo);
						//$mime = "jpeg";
						?>
				    {
				        name: "<?php echo $spaces_data[0]['id']; ?>",
				        size: <?php echo filesize('uploads/'.$spaces_data[0]['project_mobile_img']);?>,
				        type: "<?php echo $mime;?>",
				        file: "<?php echo base_url();?>uploads/<?php echo $spaces_data[0]['project_mobile_img'];?>",
				        url: "<?php echo base_url().'uploads/'.$spaces_data[0]['project_mobile_img'];?>"
				    } ,
				    
				]
     });    


     /*jQuery('#project_gal_img').filer({
     	limit: 1,
		maxSize: 5,
		extensions: ["pdf"],
		showThumbs: true,
		addMore: true,
		//templates: filer_default_opts.templates,
		files : [
					<?php 
					//print_r($project_gal_data);
					if($article_data[0]['pdf_file_name']!=""){ 
						$finfo = finfo_open(FILEINFO_MIME_TYPE);
						$mime = finfo_file($finfo, 'uploads/'.$spaces_data[0]['pdf_file_name']);
						finfo_close($finfo);
						//echo $mime;
						//$mime = "jpeg";
						?>
				    {

				        name: "<?php echo $spaces_data[0]['pdf_file_name'];?>",
				        size: <?php echo filesize('uploads/'.$spaces_data[0]['pdf_file_name']);?>,
				        type: "<?php echo $mime;?>",
				        file: "<?php echo base_url();?>uploads/<?php echo $spaces_data[0]['pdf_file_name'];?>",
				        url: "<?php echo base_url();?>uploads/<?php echo $spaces_data[0]['pdf_file_name'];?>",
				        id : "<?php echo $spaces_data[0]['id'];?>"
				    } ,
				    <?php } ?>
				],
		onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
		        var file_name;
		        var filerKit = inputEl.prop("jFiler");

		        if (typeof filerKit.files_list[id].name === 'undefined') {
		        	console.log(filerKit.files_list[id].file.id)
		            file_name = filerKit.files_list[id].file.id;
		        }
		        else {

		            file_name = filerKit.files_list[id].id;
		        }
		        //alert(file_name);
		       // $.post('./php/ajax_remove_file.php', {file: file_name});
		       jQuery.post("<?php echo base_url();?>superadmin/admin/delete_article_pdf/","article_id="+file_name,function(data){

				})
		    }		
     });  */
     



    /* jQuery(".icon-jfi-trash").click(function(){
     	
     	var filename = jQuery(this).attr('id')
     	//var filetype = jQuery(this).attr('type')
     	filename = filerKit.files_list[id].file.name;
     	alert(filename)
     	
     	if(filename!="")
     	{
     		jQuery.post("<?php echo base_url();?>superadmin/admin/delete_article_images/","filename="+filename,function(data){

			})
     	}
     }) */       
});

/*jQuery("#select_expertise").change(function(){
	var expertise = jQuery("#select_expertise option:selected").text();
	alert(expertise)
	jQuery("#tags").val(expertise)
})
*/


	</script>  

</body>
</html>
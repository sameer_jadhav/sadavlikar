<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/admin-head.php'); ?> 
<body class="page-body  page-left-in" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<?php $this->load->view('admin/admin-sidebar.php'); ?> 

	<div class="main-content">
		<?php $this->load->view('admin/admin_top_nav.php'); ?> 		
		
		
		<hr />
		

		
		
		<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title">
							<?php echo $pagetitle;?>
						</div>
						
						
					</div>
					
					<div class="panel-body">
					<?php
                          if($error!=''){  ?>
        <div class="alert alert-danger"><?php echo $error; ?></div>
        <?php }
        ///var_dump($this->session->flashdata('success'));
        if($this->session->flashdata('success')!=''){?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
        <?php } ?>
						
						<form role="form" novalidate='novalidate' method="post" id="validation-form" enctype="multipart/form-data" action="<?php echo base_url('superadmin/admin/addSpaces'); ?>" class="form-horizontal form-groups-bordered">
			
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Name*</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" data-rule-required="true" id="project_name" name="project_name" placeholder="Name">
								</div>
							</div>


							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Icon Class*</label>
								
								<div class="col-sm-5">
									<!-- <input type=""  data-rule-required="true"  placeholder="Icon Class"> -->
									<select class="form-control" id="icon_class" name="icon_class">
										  <option value="icon-alcove">icon-alcove</option>
										  <option value="icon-meeting-room">icon-meeting-room</option>
										  <option value="icon-sanctuary">icon-sanctuary</option>
										  <option value="icon-brew-lab">icon-brew-lab</option>
										  <option value="icon-bar">icon-bar</option>
										  <option value="icon-reality-gaming">icon-reality-gaming</option>
										  <option value="icon-drawing-room">icon-drawing-room</option>
										  <option value="icon-health-club">icon-health-club</option>
										  <option value="icon-gallery">icon-gallery</option>
										</select>
								</div>
							</div>


							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Image Testimonial</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control"  id="image_quote" name="image_quote" placeholder="Image Testimonial">
								</div>
							</div>

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Testimonial By</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control"  id="quote_by" name="quote_by" placeholder="Testimonial By">
								</div>
							</div>


							<!-- <div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload Icon Image*</label>
								
								<div class="col-sm-5">
									<input type="file" class="form-control" data-rule-required="true"  id="project_icon_img" name="project_icon_img" placeholder="Placeholder">
									
									<label for="field-1" class="col-sm-10 control-label">Upload Image size 1366 X 768 * and less than 2MB</label>
								</div>
							</div> -->


							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Title *</label>
								
								<div class="col-sm-5">
								
									<textarea class="form-control" rows="5" id="project_quote" data-rule-required="true" name="project_quote" placeholder="Title"><?php echo $project_data[0]['project_quote'];?></textarea>
									
								</div>
							</div>

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Description *</label>
								
								<div class="col-sm-5">
								
									<textarea class="form-control" rows="5" id="project_short_desc" data-rule-required="true" name="project_short_desc" placeholder="Description"><?php echo $project_data[0]['project_short_desc'];?></textarea>
									
								</div>
							</div>
							
							


							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload Desktop Banner Image*</label>
								
								<div class="col-sm-5">
									<input type="file" class="form-control" data-rule-required="true"  id="project_banner_img" name="project_banner_img" placeholder="Placeholder">
									
									<label for="field-1" class="col-sm-10 control-label">Upload Image size 1920x1200 * and less than 2MB</label>
								</div>
							</div>
							

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload Mobile Banner Image*</label>
								
								<div class="col-sm-5">
									<input type="file" class="form-control" data-rule-required="true"  id="project_mobile_banner_img" name="project_mobile_banner_img" placeholder="Placeholder">
									
									<label for="field-1" class="col-sm-10 control-label">Upload Image size 768X500 * and less than 2MB </label>
								</div>
							</div>
							
							
							
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-5">
									<button type="submit"  name="btn_submit" class="btn btn-success">Submit</button>
								</div>
							</div>
						</form>
						
					</div>
				
				</div>
			
			</div>
		</div>
		
		
		
		
		
		<!-- Footer -->
		
	</div>

	
	
	
	
	

	
</div>

<?php $this->load->view('admin/admin-footer.php'); ?> 
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>	
<script>

jQuery("#validation-form").validate({
	rules:{
		site_url : {
			url: true
		}
	}
});
jQuery(document).ready(function() {
     jQuery('#project_gal_img').filer({
     	limit: 1,
		maxSize: 5,
		extensions: ["pdf"],
		showThumbs: true,
		addMore: true
     });  

     jQuery('#project_banner_img').filer({
     	limit: 1,
		maxSize: 5,
		extensions: ["jpg", "png", "gif","jpeg"],
		showThumbs: true,
		addMore: true
     });    

     jQuery('#project_mobile_banner_img').filer({
     	limit: 1,
		maxSize: 5,
		extensions: ["jpg", "png", "gif","jpeg"],
		showThumbs: true,
		addMore: true
     });   
     /*jQuery('#project_icon_img').filer({
     	limit: 1,
		maxSize: 5,
		extensions: ["jpg", "png", "gif","jpeg"],
		showThumbs: true,
		addMore: true
     }); */     
});


/*jQuery("#select_expertise").change(function(){
	var expertise = jQuery("#select_expertise option:selected").text();
	alert(expertise)
	jQuery("#tags").val(expertise)
})
*/


	</script>  

</body>
</html>
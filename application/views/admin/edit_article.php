<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/admin-head.php'); ?> 
<body class="page-body  page-left-in" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<?php $this->load->view('admin/admin-sidebar.php'); ?> 

	<div class="main-content">
		<?php $this->load->view('admin/admin_top_nav.php'); 
		//print_r();exit;
		?> 		
		
		
		<hr />
		

		
		
		<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title">
							<?php echo $pagetitle;?>
						</div>
						
						
					</div>
					
					<div class="panel-body">
					<?php
                          if($error!=''){  ?>
        <div class="alert alert-danger"><?php echo $error; ?></div>
        <?php }
        ///var_dump($this->session->flashdata('success'));
        if($this->session->flashdata('success')!=''){?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
        <?php } ?>
						
						<form role="form" novalidate='novalidate' method="post" id="validation-form" enctype="multipart/form-data" action="<?php echo base_url('superadmin/admin/editArticle'); ?>" class="form-horizontal form-groups-bordered">
			
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Title*</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" data-rule-required="true" id="project_name" name="project_name" placeholder="Title" value="<?php echo $article_data[0]['title'];?>">
								</div>
							</div>

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Description *</label>
								
								<div class="col-sm-5">
								
									<textarea class="form-control" rows="5" id="project_short_desc" data-rule-required="true" name="project_short_desc" placeholder="Description"><?php echo $article_data[0]['description'];?></textarea>
									
								</div>
							</div>
							
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Date*</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" data-rule-required="true" id="publish_date" name="publish_date" placeholder="Date">
								</div>
							</div>

							
							<!--div class="form-group"> <label class="col-sm-3 control-label">Date* </label> <div class="col-sm-3"> <div class="input-group"> <input type="text" name="publish_date" id="publish_date" data-rule-required="true" class="form-control datepicker" data-format="D, dd MM yyyy"> <div class="input-group-addon"> <a href="#"><i class="entypo-calendar"></i></a> </div> </div> </div> </div-->

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload Banner Image*</label>
								
								<div class="col-sm-5">
									<input type="file" class="form-control"  id="project_banner_img" name="project_banner_img" placeholder="Placeholder">
									
									<label for="field-1" class="col-sm-10 control-label">Upload Image size 1366 X 768 * and less than 2MB</label>
								</div>
							</div>
							
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">External Url if Any</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control"  id="site_url" name="site_url"  placeholder="Please enter URL" value="<?php echo $article_data[0]['url'];?>">
								</div>
							</div>

							<input type="hidden" value="<?php echo $article_data[0]['id'];?>" class="form-control" id="article_id" name="article_id"  />
							

							
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload PDF</label>
								
								<div class="col-sm-5">
									<input type="file" class="form-control"   id="project_gal_img" name="project_gal_img[]" placeholder="Placeholder"  multiple="multiple">
									
									<label for="field-1" class="col-sm-10 control-label">Upload PDF file less than 2MB</label>
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-5">
									<button type="submit"  name="btn_submit" class="btn btn-success">Submit</button>
								</div>
							</div>
						</form>
						
					</div>
				
				</div>
			
			</div>
		</div>
		
		
		
		
		
		<!-- Footer -->

		
	</div>

	
	
	
	
	

	
</div>

<?php $this->load->view('admin/admin-footer.php'); ?> 
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>	
<script>

jQuery("#validation-form").validate({
	rules:{
		site_url : {
			url: true
		}
	}
});

filer_default_opts = {
            changeInput2: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><div class="jFiler-input-text"><h3>Drag&Drop files here</h3> <span style="display:inline-block; margin: 15px 0">or</span></div><a class="jFiler-input-choose-btn btn-custom blue-light">Browse Files</a></div></div>',
            templates: {
                box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
                item: '<li class="jFiler-item" style="width:49%">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-thumb-overlay">\
    										<div class="jFiler-item-info">\
    											<div style="display:table-cell;vertical-align: middle;">\
    												<span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
    												<span class="jFiler-item-others">{{fi-size2}}</span>\
    											</div>\
    										</div>\
    									</div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <li>{{fi-progressBar}}</li>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                            <li><a class="icon-jfi-trash jFiler-item-trash-action" id="" ></a></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                        </li>',
                itemAppend: '<li class="jFiler-item" style="width:49%">\
                                <div class="jFiler-item-container">\
                                    <div class="jFiler-item-inner">\
                                        <div class="jFiler-item-thumb">\
                                            <div class="jFiler-item-status"></div>\
                                            <div class="jFiler-item-thumb-overlay">\
        										<div class="jFiler-item-info">\
        											<div style="display:table-cell;vertical-align: middle;">\
        												<span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
        												<span class="jFiler-item-others">{{fi-size2}}</span>\
        											</div>\
        										</div>\
        									</div>\
                                            {{fi-image}}\
                                        </div>\
                                        <div class="jFiler-item-assets jFiler-row">\
                                            <ul class="list-inline pull-left">\
                                                <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                            </ul>\
                                            <ul class="list-inline pull-right">\
                                                <li><a class="icon-jfi-trash jFiler-item-trash-action" id="{{fi-name}}"></a></li>\
                                            </ul>\
                                        </div>\
                                    </div>\
                                </div>\
                            </li>',
                progressBar: '<div class="bar"></div>',
                itemAppendToEnd: false,
                removeConfirmation: true,
                _selectors: {
                    list: '.jFiler-items-list',
                    item: '.jFiler-item',
                    progressBar: '.bar',
                    remove: '.jFiler-item-trash-action'
                }
            }
            
        };

jQuery(document).ready(function() {
     
     //jQuery('#publish_date').datepicker("setValue", '10/10/2017' );

      jQuery('#project_banner_img').filer({
     	limit: 1,
		maxSize: 1,
		extensions: ["jpg", "png", "gif","jpeg"],
		showThumbs: true,
		addMore: false,
		showThumbs: true,
		templates: filer_default_opts.templates,
		files : [
					<?php 
						$finfo = finfo_open(FILEINFO_MIME_TYPE);
						$mime = finfo_file($finfo, 'uploads/'.$article_data[0]['img_name']);
						finfo_close($finfo);
						//$mime = "jpeg";
						?>
				    {
				        name: "<?php echo $article_data[0]['id']; ?>",
				        size: <?php echo filesize('uploads/'.$article_data[0]['img_name']);?>,
				        type: "<?php echo $mime;?>",
				        file: "<?php echo base_url();?>uploads/<?php echo $article_data[0]['img_name'];?>",
				        url: "<?php echo base_url().'uploads/'.$article_data[0]['img_name'];?>"
				    } ,
				    
				]
     });    


     jQuery('#project_gal_img').filer({
     	limit: 1,
		maxSize: 5,
		extensions: ["pdf"],
		showThumbs: true,
		addMore: true,
		//templates: filer_default_opts.templates,
		files : [
					<?php 
					//print_r($project_gal_data);
					if($article_data[0]['pdf_file_name']!=""){ 
						$finfo = finfo_open(FILEINFO_MIME_TYPE);
						$mime = finfo_file($finfo, 'uploads/'.$article_data[0]['pdf_file_name']);
						finfo_close($finfo);
						//echo $mime;
						//$mime = "jpeg";
						?>
				    {

				        name: "<?php echo $article_data[0]['pdf_file_name'];?>",
				        size: <?php echo filesize('uploads/'.$article_data[0]['pdf_file_name']);?>,
				        type: "<?php echo $mime;?>",
				        file: "<?php echo base_url();?>uploads/<?php echo $article_data[0]['pdf_file_name'];?>",
				        url: "<?php echo base_url();?>uploads/<?php echo $article_data[0]['pdf_file_name'];?>",
				        id : "<?php echo $article_data[0]['id'];?>"
				    } ,
				    <?php } ?>
				],
		onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
		        var file_name;
		        var filerKit = inputEl.prop("jFiler");

		        if (typeof filerKit.files_list[id].name === 'undefined') {
		        	console.log(filerKit.files_list[id].file.id)
		            file_name = filerKit.files_list[id].file.id;
		        }
		        else {

		            file_name = filerKit.files_list[id].id;
		        }
		        //alert(file_name);
		       // $.post('./php/ajax_remove_file.php', {file: file_name});
		       jQuery.post("<?php echo base_url();?>superadmin/admin/delete_article_pdf/","article_id="+file_name,function(data){

				})
		    }		
     });  
     



    /* jQuery(".icon-jfi-trash").click(function(){
     	
     	var filename = jQuery(this).attr('id')
     	//var filetype = jQuery(this).attr('type')
     	filename = filerKit.files_list[id].file.name;
     	alert(filename)
     	
     	if(filename!="")
     	{
     		jQuery.post("<?php echo base_url();?>superadmin/admin/delete_article_images/","filename="+filename,function(data){

			})
     	}
     }) */       
});

/*jQuery("#select_expertise").change(function(){
	var expertise = jQuery("#select_expertise option:selected").text();
	alert(expertise)
	jQuery("#tags").val(expertise)
})
*/


	</script>  

</body>
</html>
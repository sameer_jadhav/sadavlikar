<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/admin-head.php'); ?> 
<body class="page-body  page-left-in" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<?php $this->load->view('admin/admin-sidebar.php'); ?> 

	<div class="main-content">
		<?php $this->load->view('admin/admin_top_nav.php'); ?> 		
		
		
		<hr />
		

		
		
		<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title">
							<?php echo $pagetitle;?>
						</div>
						
						
					</div>
					
					<div class="panel-body">
					<?php
                          if($error!=''){  ?>
        <div class="alert alert-danger"><?php echo $error; ?></div>
        <?php }
        ///var_dump($this->session->flashdata('success'));
        if($this->session->flashdata('success')!=''){?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
        <?php } ?>
						
						<form role="form" novalidate='novalidate' method="post" id="validation-form" enctype="multipart/form-data" action="<?php echo base_url('superadmin/admin/addProject'); ?>" class="form-horizontal form-groups-bordered">
			
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Project Title *</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" data-rule-required="true" id="project_name" name="project_name" placeholder="Project Name">
								</div>
							</div>

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Project Heading 1</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control"  id="project_heading1" name="project_heading1" placeholder="Project Heading 1">
								</div>
							</div>

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Project Heading 2</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" id="project_heading2" name="project_heading2" placeholder="Project Heading 2">
								</div>
							</div>

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Project Short Description *</label>
								
								<div class="col-sm-5">
								
									<textarea class="form-control" rows="5" id="project_short_desc" data-rule-required="true" name="project_short_desc" placeholder="Project Short Description"></textarea>
									
								</div>
							</div>

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Project Description</label>
								
								<div class="col-sm-4">
								
									<textarea class="form-control" rows="10"  id="project_des"  name="project_des_left" placeholder="Project Left Description"></textarea>
									
								</div>

								<div class="col-sm-4">
								
									<textarea class="form-control" rows="10" id="project_des_right"  name="project_des_right" placeholder="Project Right Description"></textarea>
									
								</div>

							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label">Select Expertise*</label>
								
								<div class="col-sm-5">
									<select class="form-control" data-rule-required="true" id="select_expertise" name="select_expertise" >
										<?Php for($j=0;$j<count($cat_data);$j++){ ?>
										<option value="<?Php echo $cat_data[$j]['cat_id'];?>"><?Php echo $cat_data[$j]['cat_name'];?></option>
                          				<?Php } ?>
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Select Country*</label>
								
								<div class="col-sm-5">
									<select class="form-control" data-rule-required="true" id="select_country" name="select_country" >
										<?Php for($j=0;$j<count($country_data);$j++){ ?>
										<option value="<?Php echo $country_data[$j]['country_id'];?>"><?Php echo $country_data[$j]['country_name'];?></option>
                          				<?Php } ?>
									</select>
								</div>
							</div>


							<div class="form-group">
								<label class="col-sm-3 control-label">Select Status*</label>
								
								<div class="col-sm-5">
									<ul class="icheck-list">
								    <li>
								        <input tabindex="7" class="icheck" type="radio" id="minimal-radio-1" value="1" name="status" checked>
								        <label for="minimal-radio-1">Ongoing</label>
								    </li>
								    <li>
								        <input tabindex="8" class="icheck" type="radio" id="minimal-radio-2"  value="2" name="status" >
								        <label for="minimal-radio-2">Archive</label>
								    </li>
								    </ul>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label">Key Project*</label>
								
								<div class="col-sm-5">
									<ul class="icheck-list">
								    <li>
								        <input tabindex="7" class="icheck" type="radio" id="minimal-radio-1" value="1" name="key_project" checked>
								        <label for="minimal-radio-1">Yes</label>
								    </li>
								    <li>
								        <input tabindex="8" class="icheck" type="radio" id="minimal-radio-2"  value="2" name="key_project" >
								        <label for="minimal-radio-2">No</label>
								    </li>
								    </ul>
								</div>
							</div>


							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload Featured Image*</label>
								
								<div class="col-sm-5">
									<input type="file" class="form-control" data-rule-required="true" id="project_featured_img" name="project_featured_img" placeholder="Placeholder">
									
									<label for="field-1" class="col-sm-10 control-label">Upload Image size 300 X 300 * and less than 2MB</label>
								</div>
							</div>
							
							

							
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload Banner Image*</label>
								
								<div class="col-sm-5">
									<input type="file" class="form-control" data-rule-required="true" id="project_banner_img" name="project_banner_img" placeholder="Placeholder">
									
									<label for="field-1" class="col-sm-10 control-label">Upload Image size 1366 X 768 * and less than 2MB</label>
								</div>
							</div>


							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Banner Heading 1</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control"  id="banner_heading1" name="banner_heading1" placeholder="Banner Heading 1">
								</div>
							</div>

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Banner Heading 2</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" id="banner_heading2" name="banner_heading2" placeholder="Banner Heading 2">
								</div>
							</div>

							<!-- <div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Tags *</label>
								
								<div class="col-sm-5">

									<input type="text" value="" class="form-control tagsinput" id="tags" name="tags" placeholder="Tags" />
									
									
								</div>
							</div> -->
							
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload Gallery Image</label>
								
								<div class="col-sm-5">
									<input type="file" class="form-control"  id="project_gal_img" name="project_gal_img[]" placeholder="Placeholder"  multiple="multiple">
									
									<label for="field-1" class="col-sm-10 control-label">Upload Image size 300 X 300 * and less than 2MB</label>
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-5">
									<button type="submit"  name="btn_submit" class="btn btn-success">Submit</button>
								</div>
							</div>
						</form>
						
					</div>
				
				</div>
			
			</div>
		</div>
		
		
		
		
		
		<!-- Footer -->
		
	</div>

	
	
	
	
	

	
</div>

<?php $this->load->view('admin/admin-footer.php'); ?> 
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>	
<script>

jQuery("#validation-form").validate({
  rules: {
    work_url: {
      required: true,
      url: true
    }
  }
});
jQuery(document).ready(function() {
     jQuery('#project_gal_img').filer({
     	limit: 10,
		maxSize: 3,
		extensions: ["jpg", "png", "gif","jpeg"],
		showThumbs: true,
		addMore: true
     });       
});

/*jQuery("#select_expertise").change(function(){
	var expertise = jQuery("#select_expertise option:selected").text();
	alert(expertise)
	jQuery("#tags").val(expertise)
})
*/


	</script>  

</body>
</html>
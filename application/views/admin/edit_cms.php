<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/admin-head.php'); ?> 
<body class="page-body  page-left-in" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<?php $this->load->view('admin/admin-sidebar.php'); ?> 

	<div class="main-content">
		<?php $this->load->view('admin/admin_top_nav.php'); ?> 		
		
		
		<hr />
		

		
		
		<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title">
							<?php echo $pagetitle;?>
						</div>
						
						
					</div>
					
					<div class="panel-body">
						
						<form role="form" novalidate='novalidate' method="post" id="validation-form" enctype="multipart/form-data" action="<?php echo base_url('superadmin/admin/editCms'); ?>" class="form-horizontal form-groups-bordered">
			
							
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Description *</label>
								<div class="col-sm-7">
								<textarea data-rule-required="true"  class="form-control wysihtml5" data-stylesheet-url="<?php echo base_url();?>assets/css/wysihtml5-color.css" name="content"><?php echo stripslashes($cms_data[0]['content'])?></textarea>
								</div>
							</div>
							<input type="hidden" name="cms_id" value="<?php echo $cms_data[0]['cms_id'];?>"> 
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload Logo*</label>
								
								<div class="col-sm-5">
									<input type="file"  class="form-control"  id="work_image" name="work_image" placeholder="Placeholder">
								</div>
							</div>
							<input type="hidden" value="<?php echo $cms_data[0]['logo_img'];?>" name="old_image" >

							
							
							
							
							
							
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-5">
									<button type="submit"  name="btn_submit" class="btn btn-success">Submit</button>
								</div>
							</div>
						</form>
						
					</div>
				
				</div>
			
			</div>
		</div>
		
		
		
		
		
		<!-- Footer -->
		
	</div>

	
	
	
	
	

	
</div>

<?php $this->load->view('admin/admin-footer.php'); ?> 	


</body>
</html>
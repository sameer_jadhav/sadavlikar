<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/admin-head.php'); ?> 
<body class="page-body  page-left-in" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<?php $this->load->view('admin/admin-sidebar.php'); ?> 

	<div class="main-content">
		<?php $this->load->view('admin/admin_top_nav.php'); ?> 		
		
		
		<hr />
		

		
		
		<div class="row">
			<div class="col-sm-12">
			<div class="panel-heading">
						<div class="panel-title">
							<?php echo $pagetitle;?>
						</div>

						
					</div>
					<?php
			          if($error!=''){  ?>
			        <div class="alert alert-danger"><?php echo $error; ?></div>
			        <?php }
			        if($this->session->flashdata('success')!=''){?>
			        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
			        <?php } ?>					
				<table class="table table-bordered datatable" id="table-1">
			<thead>
				<tr>
					<!-- <th data-hide="phone">Title</th> -->
					<th data-hide="phone">Image</th>
					
					<!-- <th data-hide="phone">Redirect Url</th> -->
					<th data-hide="phone">Valid till</th>
					<th data-hide="phone">Enabled?</th>
					<th data-hide="phone">Total Views</th>
					<th data-hide="phone">Unique Views</th>
					<th data-hide="phone">Total Hits</th>
					<th data-hide="phone">Unique Hits</th>
					
					
					<th>Actions</th>
					
				</tr>
			</thead>
			<tbody>
			<?php 
                for($i=0;$i<count($spaces_data);$i++){ 
            ?>
				<tr class="odd gradeX">

					<td>
						 <img width="125"  src="<?php echo base_url();?>uploads/<?php echo $spaces_data[$i]['img_name'];?>" alt="image">
					</td>
					<!--td><?php //echo $spaces_data[$i]['redirect_url'];?></td-->
					<td><?php
						echo date('d-m-Y', strtotime(' + '.$spaces_data[$i]['valid_days'].' days'));

					 //echo $spaces_data[$i]['valid_days'];?></td>
					<?php 
					$check = "";
					$disable = "disabled='disabled'";
					if($spaces_data[$i]['is_active']==1){
						$check = "checked='checked'";
						$disable = "";
					}else if(count($spaces_data)==1){
						$disable = "";
					}

					?>
					<td><input type="checkbox" <?php echo $check;?>  <?php echo $disable;?> class="is_enabled" id="<?php echo $spaces_data[$i]['id'];?>"><?php //echo $spaces_data[$i]['is_enabled'];?></td>
					
					
					<td><?php echo $spaces_data[$i]['total_views'];?></td>
					<td style="cursor:pointer" data-toggle="modal" data-target="#totalViewsModal" ><?php echo $spaces_data[$i]['unique_views'];?></td>
					<td><?php echo $spaces_data[$i]['total_hits'];?></td>
					<td style="cursor:pointer" adddata-toggle="modal" data-target="#totalHitsModal" ><?php echo $spaces_data[$i]['unique_hits'];?></td>
					<td>

						
						
						<a  href="javascript:void(0)" onclick="delete_ads('<?php echo base64_encode($spaces_data[$i]['id']);  ?>');" class="btn btn-danger btn-sm btn-icon icon-left">
							<i class="entypo-cancel"></i>
							Delete
						</a>
					</td>	
						
				</tr>
				<?php } ?>
			</tbody>
		</table>
			</div>
		</div>
		
		
		
		
		
		<!-- Footer -->
		
	</div>

	
	
	
	
	

	
</div>
<div id="totalViewsModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Ubique Views</h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered">
        	<thead>
				<tr>
        			<th>IP Address</th>
				</tr>
			</thead>
			<tbody>
				<?php for($j=0;$j<count($unique_views_data);$j++){ ?> 
				<tr>
					<td><?php  echo $unique_views_data[$j]['ip_address'];?></td>
				</tr>
			    <?php } ?>		
			</tbody>	        			
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div id="totalHitsModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Unique Hits</h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered">
        	<thead>
				<tr>
        			<th>IP Address</th>
				</tr>
			</thead>
			<tbody>
				<?php for($j=0;$j<count($unique_hits_data);$j++){ ?> 
				<tr>
					<td><?php  echo $unique_hits_data[$j]['ip_address'];?></td>
				</tr>
			    <?php } ?>		
			</tbody>	        			
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<?php $this->load->view('admin/admin-footer.php'); ?> 	
<script type="text/javascript">
		var responsiveHelper;
		var breakpointDefinition = {
		    tablet: 1024,
		    phone : 480
		};
		var tableContainer;
		
			jQuery(document).ready(function($)
			{

				jQuery('input:checkbox[class=is_enabled]').is(':checked');

				var $checks = jQuery(".is_enabled").change(function () {
           		var pop_id = jQuery(this).attr("id");
			  if ($checks.filter(":checked").length ==0)
			  {
			  	//alert(1)
			  	jQuery.post("<?php echo base_url();?>superadmin/admin/disableAds","pop_id="+pop_id,function(res){
            		})
			       $checks.not(":checked").prop("disabled", false);
			      
			    }
			    else
			    {
			    	//alert(2)
			    	jQuery.post("<?php echo base_url();?>superadmin/admin/enableAds","pop_id="+pop_id,function(res){
            		})
			    $checks.not(":checked").prop("disabled", true);;
			    }
			});

				tableContainer = $("#table-1");
				
				tableContainer.dataTable({
					"sPaginationType": "bootstrap",
					"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
					"bStateSave": true,
					
		
				    // Responsive Settings
				    bAutoWidth     : false,
				    fnPreDrawCallback: function () {
				        // Initialize the responsive datatables helper once.
				        if (!responsiveHelper) {
				            responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
				        }
				    },
				    fnRowCallback  : function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				        responsiveHelper.createExpandIcon(nRow);
				    },
				    fnDrawCallback : function (oSettings) {
				        responsiveHelper.respond();
				    }
				});
				
				$(".dataTables_wrapper select").select2({
					minimumResultsForSearch: -1
				});
			});

				function delete_ads(pop_id)
          {
            var x = window.confirm("Do you really want to delete this Ad?");
            if (x == true) {
                jQuery.post("<?php echo base_url();?>superadmin/admin/deleteAd","ad_id="+pop_id,function(res){
                window.location.href="<?php echo base_url();?>superadmin/admin/listAds"
           })
            } 
           
          }

           function sort_val(sort_value,artist_id)
          {
           // alert(sort_value);

            jQuery.post("<?php echo base_url();?>superadmin/admin/sortArtist","order="+sort_value+"&artist_id="+artist_id,function(res){
            })
          } 

           
		</script>

</body>
</html>
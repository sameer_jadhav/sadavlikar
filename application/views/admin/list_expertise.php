<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/admin-head.php'); ?> 
<body class="page-body  page-left-in" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<?php $this->load->view('admin/admin-sidebar.php'); ?> 

	<div class="main-content">
		<?php $this->load->view('admin/admin_top_nav.php'); ?> 		
		
		
		<hr />
		

		
		
		<div class="row">
			<div class="col-sm-12">
			<div class="panel-heading">
						<div class="panel-title">
							<?php echo $pagetitle;?>
						</div>

						
					</div>
					<?php
			          if($this->session->flashdata('error')!=''){  ?>
			        <div class="alert alert-danger"><?php echo $error; ?></div>
			        <?php }
			        if($this->session->flashdata('success')!=''){?>
			        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
			        <?php } ?>					
				<table class="table table-bordered datatable" id="table-1">
			<thead>
				<tr>
					<th>Expertise Name</th>
                    <th>Image</th>
                    <th>Sort Order</th>
                    <th>Actions</th>
				</tr>
			</thead>
			<tbody>
			<?php 
            	for($i=0;$i<count($category_data);$i++){ 
            ?>
				<tr class="odd gradeX">
					<td><?php echo $category_data[$i]['cat_name'];?></td>
                    
                    <td>    
            			 <img width="125" src="<?php echo base_url().'uploads/'.$category_data[$i]['cat_featured_image'];?>" alt="image" />         
                     </td>
					<td>
						<input type="text" class="form-control"  value="<?php echo $category_data[$i]['sort_order'];?>" name="sort_order"  id="sort_order"  onKeyup="sort_val(this.value,<?php echo $category_data[$i]['cat_id'];?>)">
					</td>
					
					<td>
						<a href="<?php echo base_url(); ?>superadmin/admin/editExpertise/<?php echo base64_encode($category_data[$i]['cat_id']); ?>" class="btn btn-default btn-sm btn-icon icon-left">
							<i class="entypo-pencil"></i>
							Edit
						</a>
						
						<a  href="javascript:void(0)" onclick="delete_expertise('<?php echo base64_encode($category_data[$i]['cat_id']); ?>')" class="btn btn-danger btn-sm btn-icon icon-left">
							<i class="entypo-cancel"></i>
							Delete
						</a>
					</td>	
				</tr>
				<?php } ?>
			</tbody>
		</table>
			</div>
		</div>
		
		
		
		
		
		<!-- Footer -->
		
	</div>

	
	
	
	
	

	
</div>

<?php $this->load->view('admin/admin-footer.php'); ?> 	
<script type="text/javascript">
		var responsiveHelper;
		var breakpointDefinition = {
		    tablet: 1024,
		    phone : 480
		};
		var tableContainer;
		
			jQuery(document).ready(function($)
			{
				tableContainer = $("#table-1");
				
				tableContainer.dataTable({
					"sPaginationType": "bootstrap",
					"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
					"bStateSave": true,
					
		
				    // Responsive Settings
				    bAutoWidth     : false,
				    fnPreDrawCallback: function () {
				        // Initialize the responsive datatables helper once.
				        if (!responsiveHelper) {
				            responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
				        }
				    },
				    fnRowCallback  : function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				        responsiveHelper.createExpandIcon(nRow);
				    },
				    fnDrawCallback : function (oSettings) {
				        responsiveHelper.respond();
				    }
				});
				
				$(".dataTables_wrapper select").select2({
					minimumResultsForSearch: -1
				});
			});

			function delete_expertise(cat_id)
          {
            var x = window.confirm("Do you really want to delete this Expertise?");
            if (x == true) {
                jQuery.post("<?php echo base_url();?>superadmin/admin/deleteCategory","cat_id="+cat_id,function(res){
                window.location.href="<?php echo base_url();?>superadmin/admin/listExpertise"
           })
            } 
           
          }

          function sort_val(sort_value,cat_id)
          {
           // alert(sort_value);

            jQuery.post("<?php echo base_url();?>superadmin/admin/sortExpertise","order="+sort_value+"&cat_id="+cat_id,function(res){
            })
          } 
		</script>

</body>
</html>
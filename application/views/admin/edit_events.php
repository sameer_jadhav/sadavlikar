<?php 
//print_r($media_data);exit;

?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/admin-head.php'); ?> 
<body class="page-body  page-left-in" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<?php $this->load->view('admin/admin-sidebar.php'); ?> 

	<div class="main-content">
		<?php $this->load->view('admin/admin_top_nav.php'); ?> 		
		
		
		<hr />
		

		
		
		<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title">
							<?php echo $pagetitle;?>
						</div>
						
						
					</div>
					
					<div class="panel-body">
					<?php
                          if($error!=''){  ?>
        <div class="alert alert-danger"><?php echo $error; ?></div>
        <?php }
        ///var_dump($this->session->flashdata('success'));
        if($this->session->flashdata('success')!=''){?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
        <?php } ?>
						
						<form role="form" novalidate='novalidate' method="post" id="validation-form" enctype="multipart/form-data" action="<?php echo base_url('superadmin/events/editEvent'); ?>" class="form-horizontal form-groups-bordered">
			
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Title*</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" data-rule-required="true" id="event_name" name="event_name" placeholder="Title" value="<?php echo $blog_data[0]['title'];?>">
								</div>
							</div>

							



							

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload Media Image</label>
								
								<div class="col-sm-5">
									<input type="file" class="form-control"  id="media_images" name="media_images[]" placeholder="Placeholder"  multiple="multiple">
									
									<label for="field-1" class="col-sm-10 control-label">Upload Image size 1200 X anything* and less than 2MB</label>
								</div>
							</div>

							<input type="hidden" value="<?php echo $blog_data[0]['id'];?>" class="form-control" id="event_id" name="event_id"  />
							

							
							
							
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-5">
									<button type="submit"  name="btn_submit" class="btn btn-success">Submit</button>
								</div>
							</div>
						</form>
						
					</div>
				
				</div>
			
			</div>
		</div>
		
		
		
		
		
		<!-- Footer -->
		
	</div>

	
	
	
	
	

	
</div>

<?php $this->load->view('admin/admin-footer.php'); ?> 
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>	
  <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
  <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<script>

jQuery("#validation-form").validate();


jQuery('.datepicker').datepicker({
    dateFormat: 'dd M yy'
 });

jQuery(document).ready(function() {

		var body = jQuery(document.body),
        filer_default_opts = {
            changeInput2: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><div class="jFiler-input-text"><h3>Drag&Drop files here</h3> <span style="display:inline-block; margin: 15px 0">or</span></div><a class="jFiler-input-choose-btn btn-custom blue-light">Browse Files</a></div></div>',
            templates: {
                box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
                item: '<li class="jFiler-item" style="width:49%">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-thumb-overlay">\
    										<div class="jFiler-item-info">\
    											<div style="display:table-cell;vertical-align: middle;">\
    												<span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
    												<span class="jFiler-item-others">{{fi-size2}}</span>\
    											</div>\
    										</div>\
    									</div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <li>{{fi-progressBar}}</li>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                            <li><a class="icon-jfi-trash jFiler-item-trash-action" id="" ></a></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                        </li>',
                itemAppend: '<li class="jFiler-item" style="width:49%">\
                                <div class="jFiler-item-container">\
                                    <div class="jFiler-item-inner">\
                                        <div class="jFiler-item-thumb">\
                                            <div class="jFiler-item-status"></div>\
                                            <div class="jFiler-item-thumb-overlay">\
        										<div class="jFiler-item-info">\
        											<div style="display:table-cell;vertical-align: middle;">\
        												<span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
        												<span class="jFiler-item-others">{{fi-size2}}</span>\
        											</div>\
        										</div>\
        									</div>\
                                            {{fi-image}}\
                                        </div>\
                                        <div class="jFiler-item-assets jFiler-row">\
                                            <ul class="list-inline pull-left">\
                                                <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                            </ul>\
                                            <ul class="list-inline pull-right">\
                                                <li><a class="icon-jfi-trash jFiler-item-trash-action" id="{{fi-name}}"></a></li>\
                                            </ul>\
                                        </div>\
                                    </div>\
                                </div>\
                            </li>',
                progressBar: '<div class="bar"></div>',
                itemAppendToEnd: false,
                removeConfirmation: true,
                _selectors: {
                    list: '.jFiler-items-list',
                    item: '.jFiler-item',
                    progressBar: '.bar',
                    remove: '.jFiler-item-trash-action'
                }
            }
            
        };
     


   

      jQuery('#media_images').filer({
     	limit: 10,
		maxSize: 10,
		extensions:["jpg", "png", "gif","jpeg"],
		showThumbs: true,
		addMore: true,
		templates: filer_default_opts.templates,
		files : [
					<?php 
						for($k=0;$k<count($media_data);$k++){ 
						$finfo = finfo_open(FILEINFO_MIME_TYPE);
						$mime = finfo_file($finfo, 'uploads/'.$media_data[$k]['file_name']);
						finfo_close($finfo);
						//$mime = "jpeg";
						?>
				    {
				        name: "<?php echo $media_data[$k]['id']; ?>",
				        size: <?php echo filesize('uploads/'.$media_data[$k]['file_name']);?>,
				        type: "<?php echo $mime;?>",
				        file: "<?php echo base_url();?>uploads/<?php echo $media_data[$k]['file_name'];?>",
				        url: "<?php echo base_url().'uploads/'.$media_data[$k]['file_name'];?>",
				        id : "<?php echo $media_data[$k]['id'];?>"
				    } ,
				    <?php } ?>
				    
				],
				onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
		        var file_name;
		        var filerKit = inputEl.prop("jFiler");

		        if (typeof filerKit.files_list[id].name === 'undefined') {
		        	console.log(filerKit.files_list[id].file.id)
		            file_name = filerKit.files_list[id].file.id;
		        }
		        else {

		            file_name = filerKit.files_list[id].id;
		        }
		        //alert(file_name);
		       // $.post('./php/ajax_remove_file.php', {file: file_name});
		       jQuery.post("<?php echo base_url();?>superadmin/admin/delete_gal_images/","investor_id="+file_name,function(data){

				})
		    }	

     }); 

       
     
      /*jQuery('.summernote').summernote({
      	 	height: 300,
            callbacks: {
            	onImageUpload: function(files, editor, welEditable) {
                sendFile(files[0], editor, welEditable);
            	}
            }
            
        });

       function sendFile(file, editor, welEditable) {
            data = new FormData();
            data.append("file", file);
            jQuery.ajax({
                data: data,
                type: "POST",
                url: "<?php echo base_url();?>superadmin/blog/uploadImage",
                cache: false,
                contentType: false,
                processData: false,
                success: function(url) {
                    //editor.insertImage(url);
                    jQuery('.summernote').summernote('insertImage', url);
                }
            });
        }*/
        //CKEDITOR.replace( 'editor1' );

   
      
});




	</script>  

</body>
</html>
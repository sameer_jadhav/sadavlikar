<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/admin-head.php'); ?> 
<body class="page-body  page-left-in" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<?php $this->load->view('admin/admin-sidebar.php'); ?> 

	<div class="main-content">
		<?php $this->load->view('admin/admin_top_nav.php'); ?> 		
		
		
		<hr />
		

		
		
		<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title">
							<?php echo $pagetitle;?>
						</div>
						
						
					</div>
					
					<div class="panel-body">
					<?php
                          if($error!=''){  ?>
        <div class="alert alert-danger"><?php echo $error; ?></div>
        <?php }
        ///var_dump($this->session->flashdata('success'));
        if($this->session->flashdata('success')!=''){?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
        <?php } ?>
						
						<form role="form" novalidate='novalidate' method="post" id="validation-form" enctype="multipart/form-data" action="<?php echo base_url('superadmin/media/editPhoto'); ?>" class="form-horizontal form-groups-bordered">
			
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Title*</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" data-rule-required="true" id="media_name" name="media_name" placeholder="Title" value="<?php echo $media_data[0]['title'];?>">
								</div>
							</div>


							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Category*</label>
								
								<div class="col-sm-5">
								<?php 
										   $press_coverage = "";
										   $img_gallery = "";
										   $media_updates = "";
										   if($media_data[0]['category']=="press-coverage"){
											$press_coverage = "selected=selected";
											}
											if($media_data[0]['category']=="photo-gallery"){
												$img_gallery = "selected=selected";
											}
											if($media_data[0]['category']=="media-updates"){
												$media_updates = "selected=selected";
											}
								?>		   
									<!-- <input type=""  data-rule-required="true"  placeholder="Icon Class"> -->
									<select class="form-control" id="media_category" name="media_category">
										  <!-- <option value="press-coverage" <?php echo $press_coverage;?> >Press coverage</option>
										  
										  <option value="media-updates" <?php echo $media_updates;?> >Media Updates</option> -->
										  <option value="photo-gallery" <?php echo $img_gallery;?> >Image Gallery</option>
										  
										</select>
								</div>
							</div>

							
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload Thumbnail Image</label>
								
								<div class="col-sm-5">
									<input type="file" class="form-control"  id="media_banner_img" name="media_banner_img" placeholder="Banner Image">
									
									<!-- <label for="field-1" class="col-sm-10 control-label">Upload Image size 768X500 * and less than 2MB</label> -->
								</div>
							</div>
							
							
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload JPG/JPEG Image</label>
								
								<div class="col-sm-5">
									<input type="file" class="form-control"  id="press_jpg_img" name="press_jpg_img" placeholder="JPG?JPEG Image">
									
									<!-- <label for="field-1" class="col-sm-10 control-label">Upload Image size 1920x1200 * and less than 2MB</label> -->
								</div>
							</div>


							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload TIFF Image</label>
								
								<div class="col-sm-5">
									<input type="file" class="form-control"   id="press_tiff_img" name="press_tiff_img" placeholder="TIFF Image">
									
									<!-- <label for="field-1" class="col-sm-10 control-label">Upload Image size 1920x1200* and less than 2MB</label> -->
								</div>
							</div>


							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload PDF</label>
								
								<div class="col-sm-5">
									<input type="file" class="form-control"   id="media_pdf" name="media_pdf" placeholder="PDF File">
									
									<label for="field-1" class="col-sm-10 control-label">Upload PDF less than 2MB</label>
								</div>
							</div>
							
							<input type="hidden" value="<?php echo $media_data[0]['id'];?>" class="form-control" id="media_id" name="media_id"  />
							
							
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-5">
									<button type="submit"  name="btn_submit" class="btn btn-success">Submit</button>
								</div>
							</div>
						</form>
						
					</div>
				
				</div>
			
			</div>
		</div>
		
		
		
		
		
		<!-- Footer -->
		
	</div>

	
	
	
	
	

	
</div>

<?php $this->load->view('admin/admin-footer.php'); ?> 
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>	
<script>

jQuery("#validation-form").validate();
filer_default_opts = {
            changeInput2: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><div class="jFiler-input-text"><h3>Drag&Drop files here</h3> <span style="display:inline-block; margin: 15px 0">or</span></div><a class="jFiler-input-choose-btn btn-custom blue-light">Browse Files</a></div></div>',
            templates: {
                box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
                item: '<li class="jFiler-item" style="width:49%">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-thumb-overlay">\
    										<div class="jFiler-item-info">\
    											<div style="display:table-cell;vertical-align: middle;">\
    												<span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
    												<span class="jFiler-item-others">{{fi-size2}}</span>\
    											</div>\
    										</div>\
    									</div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <li>{{fi-progressBar}}</li>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                            <li><a class="icon-jfi-trash jFiler-item-trash-action" id="" ></a></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                        </li>',
                itemAppend: '<li class="jFiler-item" style="width:49%">\
                                <div class="jFiler-item-container">\
                                    <div class="jFiler-item-inner">\
                                        <div class="jFiler-item-thumb">\
                                            <div class="jFiler-item-status"></div>\
                                            <div class="jFiler-item-thumb-overlay">\
        										<div class="jFiler-item-info">\
        											<div style="display:table-cell;vertical-align: middle;">\
        												<span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
        												<span class="jFiler-item-others">{{fi-size2}}</span>\
        											</div>\
        										</div>\
        									</div>\
                                            {{fi-image}}\
                                        </div>\
                                        <div class="jFiler-item-assets jFiler-row">\
                                            <ul class="list-inline pull-left">\
                                                <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                            </ul>\
                                            <ul class="list-inline pull-right">\
                                            </ul>\
                                        </div>\
                                    </div>\
                                </div>\
                            </li>',
                progressBar: '<div class="bar"></div>',
                itemAppendToEnd: false,
                removeConfirmation: true,
                _selectors: {
                    list: '.jFiler-items-list',
                    item: '.jFiler-item',
                    progressBar: '.bar',
                    remove: '.jFiler-item-trash-action'
                }
            }
            
        };

jQuery(document).ready(function() {
     
     

     jQuery('#media_pdf').filer({
     	limit: 1,
		maxSize: 1,
		extensions: ["pdf"],
		showThumbs: true,
		addMore: false,
		showThumbs: true,
		templates: filer_default_opts.templates,
		files : [
					<?php 
						$finfo = finfo_open(FILEINFO_MIME_TYPE);
						$mime = finfo_file($finfo, 'uploads/'.$media_data[0]['pdf_file_name']);
						finfo_close($finfo);
						//$mime = "jpeg";
						?>
				    {
				        name: "<?php echo $media_data[0]['id']; ?>",
				        size: <?php echo filesize('uploads/'.$media_data[0]['pdf_file_name']);?>,
				        type: "<?php echo $mime;?>",
				        file: "<?php echo base_url();?>uploads/<?php echo $media_data[0]['pdf_file_name'];?>",
				        url: "<?php echo base_url().'uploads/'.$media_data[0]['pdf_file_name'];?>"
				    } ,
				    
				]
     }); 


     jQuery('#press_jpg_img').filer({
     	limit: 1,
		maxSize: 1,
		extensions: ["jpg","jpeg"],
		showThumbs: true,
		addMore: false,
		showThumbs: true,
		templates: filer_default_opts.templates,
		files : [
					<?php 
						$finfo = finfo_open(FILEINFO_MIME_TYPE);
						$mime = finfo_file($finfo, 'uploads/'.$media_data[0]['banner_desktop_img']);
						finfo_close($finfo);
						//$mime = "jpeg";
						?>
				    {
				        name: "<?php echo $media_data[0]['id']; ?>",
				        size: <?php echo filesize('uploads/'.$media_data[0]['banner_desktop_img']);?>,
				        type: "<?php echo $mime;?>",
				        file: "<?php echo base_url();?>uploads/<?php echo $media_data[0]['banner_desktop_img'];?>",
				        url: "<?php echo base_url().'uploads/'.$media_data[0]['banner_desktop_img'];?>"
				    } ,
				    
				]
     }); 

     jQuery('#press_tiff_img').filer({
     	limit: 1,
		maxSize: 1,
		extensions: ["tif"],
		showThumbs: true,
		addMore: false,
		showThumbs: true,
		templates: filer_default_opts.templates,
		files : [
					<?php 
						$finfo = finfo_open(FILEINFO_MIME_TYPE);
						$mime = finfo_file($finfo, 'uploads/'.$media_data[0]['banner_mobile_img']);
						finfo_close($finfo);
						//$mime = "jpeg";
						?>
				    {
				        name: "<?php echo $media_data[0]['id']; ?>",
				        size: <?php echo filesize('uploads/'.$media_data[0]['banner_mobile_img']);?>,
				        type: "<?php echo $mime;?>",
				        file: "<?php echo base_url();?>uploads/<?php echo $media_data[0]['banner_mobile_img'];?>",
				        url: "<?php echo base_url().'uploads/'.$media_data[0]['banner_mobile_img'];?>"
				    } ,
				    
				]
     }); 


       jQuery('#media_banner_img').filer({
     	limit: 1,
		maxSize: 1,
		extensions: ["jpg", "png", "gif","jpeg"],
		showThumbs: true,
		addMore: false,
		showThumbs: true,
		templates: filer_default_opts.templates,
		files : [
					<?php 
						$finfo = finfo_open(FILEINFO_MIME_TYPE);
						$mime = finfo_file($finfo, 'uploads/'.$media_data[0]['banner_img']);
						finfo_close($finfo);
						//$mime = "jpeg";
						?>
				    {
				        name: "<?php echo $media_data[0]['id']; ?>",
				        size: <?php echo filesize('uploads/'.$media_data[0]['banner_img']);?>,
				        type: "<?php echo $mime;?>",
				        file: "<?php echo base_url();?>uploads/<?php echo $media_data[0]['banner_img'];?>",
				        url: "<?php echo base_url().'uploads/'.$media_data[0]['banner_img'];?>"
				    } ,
				    
				]
     }); 




     /*jQuery('#project_gal_img').filer({
     	limit: 1,
		maxSize: 5,
		extensions: ["pdf"],
		showThumbs: true,
		addMore: true,
		//templates: filer_default_opts.templates,
		files : [
					<?php 
					//print_r($project_gal_data);
					if($article_data[0]['pdf_file_name']!=""){ 
						$finfo = finfo_open(FILEINFO_MIME_TYPE);
						$mime = finfo_file($finfo, 'uploads/'.$spaces_data[0]['pdf_file_name']);
						finfo_close($finfo);
						//echo $mime;
						//$mime = "jpeg";
						?>
				    {

				        name: "<?php echo $spaces_data[0]['pdf_file_name'];?>",
				        size: <?php echo filesize('uploads/'.$spaces_data[0]['pdf_file_name']);?>,
				        type: "<?php echo $mime;?>",
				        file: "<?php echo base_url();?>uploads/<?php echo $spaces_data[0]['pdf_file_name'];?>",
				        url: "<?php echo base_url();?>uploads/<?php echo $spaces_data[0]['pdf_file_name'];?>",
				        id : "<?php echo $spaces_data[0]['id'];?>"
				    } ,
				    <?php } ?>
				],
		onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
		        var file_name;
		        var filerKit = inputEl.prop("jFiler");

		        if (typeof filerKit.files_list[id].name === 'undefined') {
		        	console.log(filerKit.files_list[id].file.id)
		            file_name = filerKit.files_list[id].file.id;
		        }
		        else {

		            file_name = filerKit.files_list[id].id;
		        }
		        //alert(file_name);
		       // $.post('./php/ajax_remove_file.php', {file: file_name});
		       jQuery.post("<?php echo base_url();?>superadmin/admin/delete_article_pdf/","article_id="+file_name,function(data){

				})
		    }		
     });  */
     



    /* jQuery(".icon-jfi-trash").click(function(){
     	
     	var filename = jQuery(this).attr('id')
     	//var filetype = jQuery(this).attr('type')
     	filename = filerKit.files_list[id].file.name;
     	alert(filename)
     	
     	if(filename!="")
     	{
     		jQuery.post("<?php echo base_url();?>superadmin/admin/delete_article_images/","filename="+filename,function(data){

			})
     	}
     }) */       
});

/*jQuery("#select_expertise").change(function(){
	var expertise = jQuery("#select_expertise option:selected").text();
	alert(expertise)
	jQuery("#tags").val(expertise)
})
*/






	</script>  

</body>
</html>
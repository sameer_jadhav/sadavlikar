<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/admin-head.php'); ?> 
<body class="page-body  page-left-in" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<?php $this->load->view('admin/admin-sidebar.php'); ?> 

	<div class="main-content">
		<?php $this->load->view('admin/admin_top_nav.php'); ?> 		
		
		
		<hr />
		

		
		
		<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title">
							<?php echo $pagetitle;?>
						</div>
						
						
					</div>
					
					<div class="panel-body">
					<?php
					//var_dump($error);
                          if($error!=''){  ?>
        <div class="alert alert-danger"><?php echo $error; ?></div>
        <?php }
        ///var_dump($this->session->flashdata('success'));
        if($this->session->flashdata('success')!=''){?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
        <?php } ?>
						
						<form role="form" novalidate='novalidate' method="post" id="validation-form" enctype="multipart/form-data" action="<?php echo base_url('superadmin/admin/editWork'); ?>" class="form-horizontal form-groups-bordered">
			
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Work Title *</label>
								
								<div class="col-sm-5">
									<input type="text" value="<?php echo $work_data[0]['work_title'];?>" class="form-control" data-rule-required="true" id="work_title" name="work_title" placeholder="Work Title">
								</div>
							</div>

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Work Details *</label>
								
								<div class="col-sm-5">
								<label for="field-1" class="col-sm-10 control-label">Please add semicolon for entering new line</label>
									<textarea class="form-control"  id="work_details" data-rule-required="true" name="work_details" placeholder="Work Description"><?php echo $work_data[0]['work_details'];?></textarea>

								</div>
							</div>

							<!-- <div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Director</label>
								
								<div class="col-sm-5">
									<input type="text" value="<?php //echo $work_data[0]['work_director'];?>"  class="form-control"  id="director_name" name="director_name" placeholder="Director">
								</div>
							</div>

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Producer</label>
								
								<div class="col-sm-5">
									<input type="text" value="<?php //echo $work_data[0]['work_shorttext'];?>" class="form-control" id="short_desc" name="short_desc" placeholder="Producer">
								</div>
							</div>

							<div class="form-group">
								<label for="field-1"   class="col-sm-3 control-label">Client *</label>
								
								<div class="col-sm-5">
									<input type="text" value="<?php //echo $work_data[0]['work_client'];?>"  class="form-control" id="client_name" data-rule-required="true" name="client_name" placeholder="Client">
								</div>
							</div> -->
							
							<div class="form-group">
								<label class="col-sm-3 control-label">Select Artist*</label>
								
								<div class="col-sm-5">
									<select class="form-control" id="select_artist" data-rule-required="true" name="select_artist[]" multiple>
										<?Php 
                         				$work_artist = explode(",",$work_data[0]['artist_id']);
			                          for($j=0;$j<count($artist_data);$j++){ 
			                             $sel ="";
			                            if(in_array($artist_data[$j]['artist_id'],$work_artist)){
			                              $sel = "selected='selected'";
			                            }
			                            ?>
                          					<option <?php echo $sel;?> value="<?Php echo $artist_data[$j]['artist_id'];?>"><?Php echo $artist_data[$j]['artist_name'];?></option>
                          				<?Php } ?>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Vimeo/Youtube Url *</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" data-rule-required="true" value="<?php echo $work_data[0]['work_url'];?>"  id="work_url" type="url" name="work_url"  placeholder="Vimeo/Youtube Url">
								</div>
							</div>

							<!-- <div class="form-group">
								<label for="field-1" class="col-sm-3 control-label"> Url</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" value="<?php //echo $work_data[0]['work_url_youtube'];?>"  id="work_url_youtube" type="url" name="work_url_youtube"  placeholder="Youtube Url">
								</div>
							</div> -->
							
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Upload Image*</label>
								
								<div class="col-sm-5">
									<input type="file" class="form-control"  id="work_image" name="work_image" placeholder="Placeholder">
								</div>
								<label for="field-1" class="col-sm-7 control-label">Upload Image size 300 X 300 * and less than 2MB</label>
							</div>
							<input type="hidden" value="<?php echo $work_data[0]['work_image'];?>" name="old_image" >
                    <input type="hidden" value="<?php echo $work_data[0]['work_id'];?>" name="work_id" >
							
							
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-5">
									<button type="submit"  name="btn_submit" class="btn btn-success">Submit</button>
								</div>
							</div>
						</form>
						
					</div>
				
				</div>
			
			</div>
		</div>
		
		
		
		
		
		<!-- Footer -->
		
	</div>

	
	
	
	
	

	
</div>

<?php $this->load->view('admin/admin-footer.php'); ?> 	
<script>
jQuery("#validation-form").validate({
  rules: {
    work_url: {
    	required:true,
      url: true
    }
  }
});
	</script>  

</body>
</html>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('admin/admin-head.php'); ?> 
<body class="page-body  page-left-in" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<?php $this->load->view('admin/admin-sidebar.php'); ?> 

	<div class="main-content">
		<?php $this->load->view('admin/admin_top_nav.php'); ?> 		
		
		
		<hr />
		

		
		
		<div class="row">
			<div class="col-md-12">
				
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title">
							<?php echo $pagetitle;?>
						</div>
						
						
					</div>
					
					<div class="panel-body">
					<?php
                          if($error!=''){  ?>
        <div class="alert alert-danger"><?php echo $error; ?></div>
        <?php }
        ///var_dump($this->session->flashdata('success'));
        if($this->session->flashdata('success')!=''){?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
        <?php } ?>
						
						<form role="form" novalidate='novalidate' method="post" id="validation-form" enctype="multipart/form-data" action="<?php echo base_url('superadmin/admin/editMembership'); ?>" class="form-horizontal form-groups-bordered">
			
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Title Big*</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" data-rule-required="true" id="title_big" name="title_big" placeholder="Title Big" value="<?php echo $spaces_data[0]['title_big'];?>">
								</div>
							</div>
							<?php 
								$class_title_small = "show";
								if($spaces_data[0]['type']!='individual'){
									$class_title_small = "hide";
								}
							?>

							<div class="form-group <?php echo $class_title_small;?>">
								<label for="field-1" class="col-sm-3 control-label">Title Small*</label>
								
								<div class="col-sm-5">
									<input type="text" class="form-control" data-rule-required="true" id="title_small" name="title_small" placeholder="Title Small" value="<?php echo $spaces_data[0]['title_small'];?>">
								</div>
							</div>

							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Description *</label>
								
								<div class="col-sm-5">
								
									<textarea class="form-control" rows="5" id="description" data-rule-required="true" name="description" placeholder="Description"><?php echo $spaces_data[0]['description'];?></textarea>
									
								</div>
							</div>


							<input type="hidden" value="<?php echo $spaces_data[0]['type'];?>" class="form-control" id="member_type" name="member_type"  />
							<input type="hidden" value="<?php echo $spaces_data[0]['id'];?>" class="form-control" id="plan_id" name="plan_id"  />
							
							
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-5">
									<button type="submit"  name="btn_submit" class="btn btn-success">Submit</button>
								</div>
							</div>
						</form>
						
					</div>
				
				</div>
			
			</div>
		</div>
		
		
		
		
		
		<!-- Footer -->
		
	</div>

	
	
	
	
	

	
</div>

<?php $this->load->view('admin/admin-footer.php'); ?> 
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>	
<script>

jQuery("#validation-form").validate({
	rules:{
		v_tour_link : {
			url: true,
			required:true
		}
	}
});
jQuery(document).ready(function() {
    

     jQuery('#project_banner_img').filer({
     	limit: 1,
		maxSize: 5,
		extensions: ["jpg", "png", "gif","jpeg"],
		showThumbs: true,
		addMore: true
     });    

     jQuery('#project_mobile_banner_img').filer({
     	limit: 1,
		maxSize: 5,
		extensions: ["jpg", "png", "gif","jpeg"],
		showThumbs: true,
		addMore: true
     });   
          
});


/*jQuery("#select_expertise").change(function(){
	var expertise = jQuery("#select_expertise option:selected").text();
	alert(expertise)
	jQuery("#tags").val(expertise)
})
*/


	</script>  

</body>
</html>
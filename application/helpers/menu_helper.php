<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

  function activate_menu($controller) {
    // Getting CI class instance.
    $CI = get_instance();
    // Getting router class to active.
    $class = $CI->router->fetch_class();
    
    return ($class == $controller) ? 'active' : '';
  }

  function get_expertise()
  {
  	 $CI = get_instance();
  	 $expertise_data =   $CI->master_model->getRecords('table_category',array(),'table_category.*',array('sort_order'=>'asc')); 
  	 return $expertise_data;
  }

  

?>
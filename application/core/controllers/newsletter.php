<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newsletter extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function subscribe()
	{
		$data['success']=$data['error']="";
		if(isset($_POST))
		 {
		 	$this->form_validation->set_rules('email_newsletter','','required|xss_clean');
		 	if($this->form_validation->run())
			{

				$email_id = $this->input->post('email_newsletter',true);
				$result=$this->master_model->getRecords('table_newsletter',array("email"=>$email_id),'table_newsletter.*');
				if(count($result)>0)
				{
					echo 'Your email address is already subscribed';
				}else
				{	
					$inputArray = array('email' => $email_id );
					if($user_info=$this->master_model->insertRecord('table_newsletter',$inputArray))
							{ 
								echo 'Newsletter subscribed Successfully!!';			
							}
							else
							{
								echo 'Something went wrong ,try again later';	
							}
				}
				
			}
		 }
	}

	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact_us extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		 
		 $data['pagetitle']='Contact Us';
	 	 $contact_data =   $this->master_model->getRecords('table_cms',array('cms_id'=>1),'table_cms.*'); 
	 	 //$artist_data =   $this->master_model->getRecords('table_artist',array(),'table_artist.*',array('artist_name'=>'asc')); 
	 	 $artist_data =   $this->master_model->getRecords('table_artist',array(),'table_artist.*',array('sort_order'=>'asc')); 
	 	 
	 	 $data['logo_url'] = $contact_data[0]['logo_img'];
	 	 $data['artist_data']  = $artist_data;
	 	 $data['contact_data'] = $contact_data;
	 	 $data['sodaism_status'] = $this->master_model->getRecords("table_menu",array('menu_id'=>1),"table_menu.menu_status");

	 	 
	 	 /*$directory = explode('/',ltrim($_SERVER['REQUEST_URI'],'/'));
	 	 //var_dump($directory);	
		// loop through each directory, check against the known directories, and add class   
		$directories = array("contact_us", "about_us","work"); // set home as 'index', but can be changed based of the home uri
			
		foreach ($directories as $folder){
		$active[$folder] = ($directory[1] == $folder)? "active":"noactive";
		}*/
		
	 	$search_data =   $this->master_model->getRecords('table_work',"",'table_work.work_title');
	 	$data['search_tag'] = $search_data;
	 	$this->load->view('contact',$data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Project_detail extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['pagetitle'] = 'SMC Infra';
		$project_name_slug=$this->uri->segment('2');	
		$project_data = $this->master_model->getRecords('table_project',array("project_name_slug"=>$project_name_slug),"project_id");
		$project_id = $project_data[0]['project_id'];
		$data['project_data'] = $this->master_model->getRecords('table_project as tp JOIN table_country as tc ON tc.country_id=tp.country_id JOIN table_category as tca on tca.cat_id=tp.cat_id',array('tp.project_id'=>$project_id),'tp.*,tc.country_name,tca.cat_name');
		$data['cat_data'] = $this->master_model->getRecords('table_category',array('cat_id'=>$data['project_data'][0]['cat_id']),'table_category.*');
		$data['gallery_data'] = $this->master_model->getRecords('table_gallery',array('project_id'=>$data['project_data'][0]['project_id']),"table_gallery.*");
		
		$this->load->view('project_detail',$data);
	}

	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
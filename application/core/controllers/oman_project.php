<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Oman_project extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['pagetitle'] = 'SMC Infra';
		$data['project_data'] = $this->master_model->getRecords('table_project as tp JOIN table_country as tc ON tc.country_id=tp.country_id JOIN table_category as tca ON tca.cat_id=tp.cat_id',array("tp.key_project"=>1,"tp.country_id"=>2),'tp.*,tc.country_name,tca.cat_name,tca.cat_slug',array('tp.date_added'=>'desc'),0,4);
		
		$this->load->view('oman-project',$data);
	}

	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Aboutus extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['pagetitle'] = 'Shreeji - About Us';
		$data['investor_data'] =   $this->master_model->getRecords('table_investor',array(),'table_investor.*',array('table_investor.id'=>"ASC")); 

		$data['article_data'] =  $this->master_model->getRecords('table_article',array(),'table_article.*',array('table_article.id'=>"ASC")); 
		//$pdf_data = $this->master_model->getRecords('table_pdf',array(),'table_pdf.*'); 
		$query =  "select ti.id,ti.name,ti.slug,tp.file_name from table_investor as ti JOIN table_pdf as tp ON tp.investor_id=ti.id";
		  
     	 $query = $this->db->query( $query );
      	$result    = $query->result();	
      	/*echo "<pre>";
      	print_r($result);exit;*/
      	$data['pdf_data'] = $result;

		
		$this->load->view('about-us',$data);
	}

	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
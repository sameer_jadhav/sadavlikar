<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Services extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
	 public function __construct()
    {
	   parent::__construct();
	  $this->load->helper('captcha');
	   //$this->load->model('email_sending');	
	   
	}

	public function index()
	{
		$data['pagetitle'] = 'Shreeji - Services';
		$data['investor_data'] =   $this->master_model->getRecords('table_investor',array(),'table_investor.*',array('table_investor.id'=>"ASC")); 
		//$pdf_data = $this->master_model->getRecords('table_pdf',array(),'table_pdf.*'); 
		$query =  "select ti.id,ti.name,ti.slug,tp.file_name from table_investor as ti JOIN table_pdf as tp ON tp.investor_id=ti.id";
		  
     	 $query = $this->db->query( $query );
      	$result    = $query->result();	
      	/*echo "<pre>";
      	print_r($result);exit;*/
      	$data['pdf_data'] = $result;
		
		$this->load->view('services',$data);
	}

	public function service_enquiry()
	{
		
		 $data['success']=$data['error']="";
	  	//$data['pagetitle']='SMC | Add Country';
	  	 if(isset($_POST['btn_submit']))
		 {
		 	$this->form_validation->set_rules('name','','required|xss_clean');
		 	$this->form_validation->set_rules('email','','required|xss_clean');
		 	$this->form_validation->set_rules('company','','required|xss_clean');
		 	$this->form_validation->set_rules('phone','','required|xss_clean');
		 	$this->form_validation->set_rules('subject','','required|xss_clean');
		 	$this->form_validation->set_rules('feedback','','required|xss_clean');
		 	if($this->form_validation->run())
			{
				$input_array = $_POST;
				unset($input_array['btn_submit']);
				//print_r($input_array);exit;
				//$input_array = unset($input_array);
				//print_r($post_array);
				if($user_info=$this->master_model->insertRecord('table_service',$input_array))
							{ 
							
								$this->session->set_flashdata('success','Enquiry sent Successfully');			
								redirect(base_url().'services');
								
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								redirect(base_url().'services');
								 
							}
			}
		 }
	}

	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
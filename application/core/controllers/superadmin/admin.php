<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();
	   $this->load->library('upload');  
	   //$this->load->model('email_sending');	
	   
	}

	public function login()
	{ 

	     $data['pagetitle']='Shrreji | Login';
	     //$data['error'] = "";
		 if(isset($_POST['btn_login']))
		 {
			$this->form_validation->set_rules('username','','required|xss_clean');
			$this->form_validation->set_rules('password','','required|xss_clean');
			if($this->form_validation->run())
			{
				$username=$this->input->post('username',true);
				$password=$this->input->post('password',true);
				$input_array=array('username'=>$username,'user_pass'=>md5($password));				
				$user_info=$this->master_model->getRecords('admin_user',$input_array);
				if(count($user_info)>0)
				{ 
				
					$mysqltime=date("H:i:s");
					$user_data=array('username'=>$user_info[0]['username'],
									 'admin_id'=>$user_info[0]['user_id'],
									 'timer'=>base64_encode($mysqltime));
					$this->session->set_userdata($user_data);
					redirect(base_url().'superadmin/admin/dashboard/');			
				}
				else
				{

					 $this->session->set_flashdata('error','Please enter valid username or password !');
					 redirect(base_url().'superadmin/admin/login'); 
				}
			}
		  }
	      $this->load->view('admin/login',$data);
	}
	
	public function forgotpassword()
	{
	  $data['pagetitle']='Lets learn India | Forgotpassword';
	  $data['error']=$data['success']='';	
	  if(isset($_POST['btn_recovery']))
	  {
	    $this->form_validation->set_rules('email','','required|valid_email');
		if($this->form_validation->run())
		{
		  $admin_email=$this->input->post('email',true);
		  $result=$this->master_model->getRecords('admin_login',array('admin_login.admin_email'=>$admin_email),'admin_login.*');
		  if(count($result)>0)
		  {
			 $whr=array('id'=>'1');
			 $info_mail=$this->master_model->getRecords('admin_login',$whr,'*');
			 $info_arr=array('from'=>$info_mail[0]['admin_email'],'to'=>$admin_email,'subject'=>'Password Recovery','view'=>'admin-forgot-password');
			 $other_info=array('name'=>$result[0]['admin_username'],'email_id'=>base64_encode($info_mail[0]['id']));
			 if($this->email_sending->sendmail($info_arr,$other_info))
			 {
				$change_password=array('password_status'=>'0');	
				$this->master_model->updateRecord('admin_login',$change_password,array('id'=>'1'));	 
				$this->session->set_flashdata('success','Mail send successfully.');
				redirect(base_url().'superadmin/admin/forgotpassword/');	
			 }
			 else
			 {
				$this->session->set_flashdata('error','While error for sending mail');
				redirect(base_url().'superadmin/admin/forgotpassword/');	
			 }
		  }
		  else
		  {
			 $this->session->set_flashdata('error','Your email was not found.');
			 redirect(base_url().'superadmin/admin/forgotpassword/'); 
		  }
		}
	  }
	  $this->load->view('admin/login',$data);
	}
	
	public function change_password()
	{
	  $data['pagetitle']='Shreeji | Change Password';
	  $data['error']=$data['success']=$data['error_alreay']='';
	  
	  
		  if(isset($_POST['btn_password']))
		  {
			 $this->form_validation->set_rules('cur_password','','required');
			 $this->form_validation->set_rules('re_new_password','','required');
			 if($this->form_validation->run())
			 {
				$password=$this->input->post('cur_password',true);
				$confirm_password=$this->input->post('re_new_password',true);
				$result=$this->master_model->getRecords('admin_user as u',array('u.user_id'=>1,"u.user_pass"=>md5($password)),'u.*'); 
				if(!empty($result))
				{
					$update_password=array('user_pass'=>md5($confirm_password));
					if($this->master_model->updateRecord('admin_user',$update_password,array('user_id'=>'1')))
					{
						$this->session->set_flashdata('success','Password Change successfully');
					   // $data['success']='Password Change successfully'; 
						redirect(base_url().'superadmin/admin/change_password');
					}	
				}else{
					$this->session->set_flashdata('error','Current Password does not match');
					redirect(base_url().'superadmin/admin/change_password');
					//$data['error']='Current Password does not match';
				}	
				
			 }
		  }
	  
	  $this->load->view('admin/change_password',$data);
	}
	
	public function logout()
	{
	  $this->session->unset_userdata('admin_id');
	  $this->session->unset_userdata('username');
	   $this->session->unset_userdata('timer');
	  redirect(base_url().'superadmin/admin/login');
	}
	
	public function dashboard()
	{
	  $data['success']=$data['error']='';	
	  $data['pagetitle']='Shreeji | Dashboard';
	  $data['middle_content']='dashboard';
	  $this->load->view('admin/dashboard',$data);
	}
	

	

	public function listNewsletter()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='Shreeji | List Newsletter Subscibers';
	  //$data['middle_content']='list_artist';
	  $query = "SELECT * FROM (`table_newsletter`) WHERE 1 = '1' ";

		//$data  = array();
    	$query = $this->db->query( $query );
    	$result    = $query->result();

	 // $result=$this->master_model->getRecords('table_country',array(1=>"1"),'*'); 
	  $data['country_data'] = $result;
	 
	  $this->load->view('admin/list_newsletter',$data);
	}

	public function listInvestor()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='Shreeji | List Investors';
	  //$data['middle_content']='list_artist';
	  $result=$this->master_model->getRecords('table_investor',array(),'table_investor.*'); 

	  //echo $this->db->last_query();
	  $data['investor_data'] = $result;
	  $this->load->view('admin/list_investor',$data);
	}

	public function listEnquiry()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='Shreeji | List Enquiry';
	  //$data['middle_content']='list_artist';
	  $result=$this->master_model->getRecords('table_service',array(),'table_service.*'); 
	  $data['country_data'] = $result;
	  $this->load->view('admin/list_enquiry',$data);
	}

	public function listArticle()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='Shreeji | List Articles';
	  //$data['middle_content']='list_artist';
	  $result=$this->master_model->getRecords('table_article',array(),'table_article.*'); 
	  $data['article_data'] = $result;
	  $this->load->view('admin/list_article',$data);
	}

	public function listProjectCountry()
	{
		$data['success']=$data['error']='';	
	  $data['pagetitle']='SMC | List Project';
	  //$data['middle_content']='list_artist';
	  
	  $result=$this->master_model->getRecords('table_country',array(),'table_country.*'); 
	  $data['country_data'] = $result;
	  $this->load->view('admin/project_list_country',$data);
	}

	public function listHome()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='SMC | List Home Images';
	  //$data['middle_content']='list_artist';
	  $result=$this->master_model->getRecords('table_home_images',array("image_name_desktop !="=> ""),'table_home_images.*'); 
	  //echo $this->db->last_query();
	  $data['image_data'] = $result;
	  $this->load->view('admin/list_home',$data);
	}

	
	
	public function listProject()
	{
		$country_id=$this->input->get('country_id',true);
		$country_id=base64_decode($this->uri->segment('4'));
	   
	  //$data['middle_content']='list_artist';
	  //$cond = array('ta.artist_id')
	  //$result=$this->master_model->getRecords('table_work as  tw JOIN table_artist as ta ON ta.artist_id=tw.artist_id',array(),'tw.*,ta.artist_name'); 
	  $query = "SELECT `tw`.*, `ta`.`country_name` FROM (`table_project` as tw JOIN  `table_country` as ta ON ta.country_id=tw.country_id) WHERE FIND_IN_SET( '$country_id' ,  `tw`.`country_id` ) ORDER BY `tw`.`sort_order` asc";		//echo $query;
	  $data  = array();
      $query = $this->db->query( $query );
      $result    = $query->result();
	  $data['project_data'] = $result;
	  $data['success']=$data['error']='';
	  $data['country_id'] = $country_id;	
	  	$data['country_id'] = base64_encode($country_id);
	   $data['pagetitle'] = 'SMC | List Project';
	  $this->load->view('admin/listProject',$data);
	}

	public function listWorkorder()
	{
		$artist_id=$this->input->get('artist_id',true);
		$artist_id=base64_decode($this->uri->segment('4'));
	   
	  //$data['middle_content']='list_artist';
	  //$cond = array('ta.artist_id')
	  //$result=$this->master_model->getRecords('table_work as  tw JOIN table_artist as ta ON ta.artist_id=tw.artist_id',array(),'tw.*,ta.artist_name'); 
	  $query = "SELECT `tw`.*, `ta`.`artist_name` FROM (`table_work` as tw JOIN  `table_artist` as ta ON ta.artist_id=tw.artist_id) WHERE FIND_IN_SET( '$artist_id' ,  `tw`.`artist_id` ) ORDER BY `tw`.`sort_order` asc";		//echo $query;
	  $data  = array();
      $query = $this->db->query( $query );
      $result    = $query->result();
	  $data['work_data'] = $result;
	  $data['success']=$data['error']='';	
	  	$data['artist_id'] = base64_encode($artist_id);
	   $data['pagetitle'] = 'SMC | List Work Order';
	  $this->load->view('admin/work_list_order',$data);
	}

	

	public function listCms()
	{
		 $data['success']=$data['error']='';	
	  $data['pagetitle']='SMC | CMS Pages';
	  //$data['middle_content']='list_artist';
	  $result=$this->master_model->getRecords('table_cms',array(),'table_cms.*'); 
	  $data['cms_data'] = $result;
	  $this->load->view('admin/list_cms',$data);
	}

	

	public function addCountry()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='SMC | Add Country';
		 if(isset($_POST['btn_submit']))
		 {
		 	//$slug = url_title($title, 'dash', true);

			$this->form_validation->set_rules('country_name','','required|xss_clean');
			//$this->form_validation->set_rules('password','','required|xss_clean');
			if($this->form_validation->run())
			{
				$country_name=$this->input->post('country_name',true);
				
				$country_name_slug = url_title($country_name, 'dash', true);

						  $input_array=array('country_name'=>$country_name);	
							
							if($user_info=$this->master_model->insertRecord('table_country',$input_array))
							{ 
							
								$this->session->set_flashdata('success','Country added Successfully');			
								redirect(base_url().'superadmin/admin/listCountry/');
								
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								redirect(base_url().'superadmin/admin/addCountry/');
								 
							}
				
			}
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
		  }		
	  
	  $this->load->view('admin/add_country',$data);
	}

	public function addHomeimage()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='SMC | Add Home Image';
		 if(isset($_POST['btn_submit']))
		 {
		 	

				$work_image=$this->input->post('work_image',true);
				
				$date = date('Y-m-d H:i:s', time());
				$config=array('upload_path'=>'uploads/',
					          'allowed_types'=>'jpg|jpeg|gif|png|svg|svgz',
					          'file_name'=>rand(1,9999),'max_size'=>0);
			    $this->upload->initialize($config);
			    if($_FILES['desktop_image']['name']!='')
					{ 
						if($this->upload->do_upload('desktop_image'))
						{
						  $dt=$this->upload->data();
						  $file_desktop=$dt['file_name'];
						  
						  $input_array=array('image_name_desktop'=>$file_desktop,'date_added'=>$date );	
							
							if($user_info=$this->master_model->insertRecord('table_home_images',$input_array))
							{ 
							
								$this->session->set_flashdata('success','Image added Successfully');			
								redirect(base_url().'superadmin/admin/listHome/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								redirect(base_url().'superadmin/admin/addHomeimage/');
								 
							}
						 // $input_array=array();
						}
						else
						{
							$file="";
							//$data['error']=$this->upload->display_errors();
							$this->session->set_flashdata('error',$this->upload->display_errors());
								redirect(base_url().'superadmin/admin/addHomeimage/');
						}
					}	
					
				
			
		  }		
	  
	  $this->load->view('admin/add_home_image',$data);
	}


	public function search_tag()
	{
		//var_dump($_GET);
		$term = $this->input->get('term',true);
		$result=$this->master_model->getRecords('table_tags',array("tag_name like "=> "%".$term."%"),'table_tags.tag_id,table_tags.tag_name'); 
		//echo $this->db->last_query();
		
		foreach ($result as $res)
		{
			$res['value'] = $res['tag_name'];
			$res['id'] = $res['tag_id'];
			$result_set[] = $res;
		}
		//var_dump($result);
		echo (json_encode($result_set));
	}


	public function addInvestor()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='Shreeji | Add Investor';
		 if(isset($_POST['btn_submit']))
		 {


			$this->form_validation->set_rules('project_name','','required|xss_clean');
			
			
			if($this->form_validation->run())
			{
				
				$investor_name=$this->input->post('project_name',true);
				$input_array = array('name' =>  $investor_name);
							if($user_info=$this->master_model->insertRecord('table_investor',$input_array))
							{ 
								$investor_id = $this->db->insert_id();
								$count_gal = count($_FILES['project_gal_img']['name']);
								$files = $_FILES;
								if($_FILES['project_gal_img']['name'][0]!=""){

									for($i=0; $i<$count_gal; $i++)
									{
										$config_rec=array('upload_path'=>'uploads/',
								          'allowed_types'=>'pdf',
								          //'file_name'=>md5(rand(1,9999)),
								          'file_name'=>$files['project_gal_img']['name'][$i],
								          'max_size'=>10000);
							    		$this->upload->initialize($config_rec);

							    		$_FILES['project_gal_img']['name'] = $files['project_gal_img']['name'][$i];
								        $_FILES['project_gal_img']['type']= $files['project_gal_img']['type'][$i];
								        $_FILES['project_gal_img']['tmp_name']= $files['project_gal_img']['tmp_name'][$i];
								        $_FILES['project_gal_img']['error']= $files['project_gal_img']['error'][$i];
								        $_FILES['project_gal_img']['size']= $files['project_gal_img']['size'][$i]; 
								        if($this->upload->do_upload('project_gal_img'))
										{
										  $dt_gallery=$this->upload->data();
										  $file_gallery[]=$dt_gallery['file_name'];

										  //$res = $this->master_model->createThumb($file,'uploads/profile/',200,200,FALSE);
										  
										}else
										{
											$this->session->set_flashdata('error',$this->upload->display_errors());
										}
									}
									for($m=0;$m<count($file_gallery);$m++)
											{
												$input_array_gal = array('investor_id'=>$investor_id,'file_name'=>$file_gallery[$m]);
												$this->master_model->insertRecord('table_pdf',$input_array_gal)	;

											}
								}
								$this->session->set_flashdata('success','Investor added Successfully');			
								redirect(base_url().'superadmin/admin/listInvestor/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_investor',$data);
	}


	public function addArticle()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='Shreeji | Add Article';
		 if(isset($_POST['btn_submit']))
		 {


			$this->form_validation->set_rules('project_name','','required|xss_clean');
			$this->form_validation->set_rules('project_short_desc','','required|xss_clean');
			$this->form_validation->set_rules('publish_date','','required|xss_clean');
			
			//$this->form_validation->set_rules('site_url','','required|xss_clean');

			
			
			if($this->form_validation->run())
			{
				
				$title=$this->input->post('project_name',true);
				$description =$this->input->post('project_short_desc',true);
				$publish_date =$this->input->post('publish_date',true);
				$site_url=$this->input->post('site_url',true);

				$input_array = array(
									'title' =>  $title,
									'description' =>  $description,
									'url' =>  $site_url,
									'publish_date'=>$publish_date
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->insertRecord('table_article',$input_array))
							{ 
								$article_id = $this->db->insert_id();
								
								$count_gal = count($_FILES['project_gal_img']['name']);
								
								$files = $_FILES;
								$config=array('upload_path'=>'uploads/',
					          'allowed_types'=>'jpg|jpeg|gif|png',
					          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);


								if($_FILES['project_banner_img']['name']!='')
								{
									if($this->upload->do_upload('project_banner_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
						
									
									$data_inserted=$this->master_model->updateRecord('table_article',$input_array,array('id'=>$article_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								//print_r($count_gal);exit;
								
								if($_FILES['project_gal_img']['name'][0]!=""){

									for($j=0; $j<$count_gal; $j++)
									{
										$config_pdf=array('upload_path'=>'uploads/',
								          'allowed_types'=>'pdf',
								          //'file_name'=>md5(rand(1,9999)),
								          'file_name'=>$files['project_gal_img']['name'][$j],
								          'max_size'=>10000);
							    		$this->upload->initialize($config_pdf);

							    		$_FILES['project_gal_img']['name'] = $files['project_gal_img']['name'][$j];
								        $_FILES['project_gal_img']['type']= $files['project_gal_img']['type'][$j];
								        $_FILES['project_gal_img']['tmp_name']= $files['project_gal_img']['tmp_name'][$j];
								        $_FILES['project_gal_img']['error']= $files['project_gal_img']['error'][$j];
								        $_FILES['project_gal_img']['size']= $files['project_gal_img']['size'][$j]; 
								        if($this->upload->do_upload('project_gal_img'))
										{
										  $dt_pdf=$this->upload->data();
										  $file_pdf[]=$dt_pdf['file_name'];

										  //$res = $this->master_model->createThumb($file,'uploads/profile/',200,200,FALSE);
										  
										}else
										{
											$this->session->set_flashdata('error',$this->upload->display_errors());
										}
									}
									
									for($n=0;$n<count($file_pdf);$n++)
											{
												$input_array_gal = array('pdf_file_name'=>$file_pdf[$n]);
												//$this->master_model->insertRecord('table_pdf',$input_array_gal)	;
												$data_inserted=$this->master_model->updateRecord('table_article',$input_array_gal,array('id'=>$article_id));


											}
								}


								$this->session->set_flashdata('success','Article added Successfully');			
								redirect(base_url().'superadmin/admin/listArticle/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_article',$data);
	}
	

	public function addProject()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='SMC | Add Project';
	  $country_data =   $this->master_model->getRecords('table_country',array(),'table_country.*');
	  $cat_data =   $this->master_model->getRecords('table_category',array(),'table_category.*'); 
	  $data['country_data'] = $country_data;
	  $data['cat_data'] = $cat_data;
		 if(isset($_POST['btn_submit']))
		 {


			$this->form_validation->set_rules('project_name','','required|xss_clean');
			$this->form_validation->set_rules('select_country','','required|xss_clean');
			$this->form_validation->set_rules('project_short_desc','','required|xss_clean');
			$this->form_validation->set_rules('status','','required|xss_clean');
			$this->form_validation->set_rules('key_project','','required|xss_clean');
			//$this->form_validation->set_rules('client_name','','required|xss_clean');
			
			//$this->form_validation->set_rules('work_url_youtube','Youtube Url','required|xss_clean');

			//$this->form_validation->set_rules('password','','required|xss_clean');
			
			if($this->form_validation->run())
			{
				
				$project_name=$this->input->post('project_name',true);
				$project_name_slug = url_title($project_name, 'dash', true);
				$project_short_desc=$this->input->post('project_short_desc',true);
				$project_des_left =$this->input->post('project_des_left',true);
				$project_des_right =$this->input->post('project_des_right',true);

				$project_heading1 =$this->input->post('project_heading1',true);
				$project_heading2 =$this->input->post('project_heading2',true);
				$status =$this->input->post('status',true);
				$key_project =$this->input->post('key_project',true);
				$banner_heading1 =$this->input->post('banner_heading1',true);
				$banner_heading2 =$this->input->post('banner_heading2',true);
				
				//$select_country=$this->input->post('select_country',true);
				
				$select_country=$this->input->post('select_country',true);
				$select_expertise=$this->input->post('select_expertise',true);
				
				//$select_country = implode(",",$select_country);
				
				$date = date('Y-m-d H:i:s', time());
				$config=array('upload_path'=>'uploads/',
					          'allowed_types'=>'jpg|jpeg|gif|png',
					          'file_name'=>rand(1,9999),'max_size'=>0);
			    $this->upload->initialize($config);
			    if($_FILES['project_featured_img']['name']!='')
					{
						if($this->upload->do_upload('project_featured_img'))
						{
						  $dt=$this->upload->data();
						  $file=$dt['file_name'];
						  $this->upload->do_upload('project_banner_img');
						  $dt_banner = $this->upload->data();
						  $banner_file=$dt_banner['file_name'];

						  $input_array=array('project_name'=>$project_name,'project_name_slug'=>$project_name_slug,'heading1'=>$project_heading1,'heading2'=>$project_heading2,'project_short_desc'=>$project_short_desc,'project_des'=>$project_des_left,'right_desc'=>$project_des_right,'country_id'=>$select_country,'project_featured_img'=>$file,'status'=>$status,"cat_id"=>$select_expertise,'banner_heading1'=>$banner_heading1,'banner_heading2'=>$banner_heading2,'key_project'=>$key_project,'project_banner_img'=>$banner_file);	
			
							if($user_info=$this->master_model->insertRecord('table_project',$input_array))
							{ 
								$project_id = $this->db->insert_id();
								$count_gal = count($_FILES['project_gal_img']['name']);
								$files = $_FILES;
								if($_FILES['project_gal_img']['name'][0]!=""){

									for($i=0; $i<$count_gal; $i++)
									{
										$config_rec=array('upload_path'=>'uploads/',
								          'allowed_types'=>'jpg|jpeg|gif|png',
								          'file_name'=>md5(rand(1,9999)),'max_size'=>10000);
							    		$this->upload->initialize($config_rec);

							    		$_FILES['project_gal_img']['name'] = $files['project_gal_img']['name'][$i];
								        $_FILES['project_gal_img']['type']= $files['project_gal_img']['type'][$i];
								        $_FILES['project_gal_img']['tmp_name']= $files['project_gal_img']['tmp_name'][$i];
								        $_FILES['project_gal_img']['error']= $files['project_gal_img']['error'][$i];
								        $_FILES['project_gal_img']['size']= $files['project_gal_img']['size'][$i]; 
								        if($this->upload->do_upload('project_gal_img'))
										{
										  $dt_gallery=$this->upload->data();
										  $file_gallery[]=$dt_gallery['file_name'];

										  //$res = $this->master_model->createThumb($file,'uploads/profile/',200,200,FALSE);
										  
										}else
										{
											$this->session->set_flashdata('error',$this->upload->display_errors());
										}
									}

									for($m=0;$m<count($file_gallery);$m++)
											{
												$input_array_gal = array('project_id'=>$project_id,'img_name'=>$file_gallery[$m]);
												$this->master_model->insertRecord('table_gallery',$input_array_gal)	;
											}
								}
								$this->session->set_flashdata('success','Project added Successfully');			
								redirect(base_url().'superadmin/admin/listProject/'.base64_encode($select_country));
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						}
						else
						{
							$file="";
							$this->session->set_flashdata('error',$this->upload->display_errors());
							$data['error']=$this->upload->display_errors();
						}
					}
					

				
			}else
			{
				$this->session->set_flashdata('error',$this->upload->display_errors());
				$data['error']=$this->form_validation->error_string();
			}
		  }		
	  
	  $this->load->view('admin/add_work',$data);
	}

	

	public function addExpertise()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='SMC | Add Expertise';
	  $artist_data =   $this->master_model->getRecords('table_category',array(),'table_category.*'); 
	  $data['category_data'] = $artist_data;
	  
		 if(isset($_POST['btn_submit']))
		 {
		 	
			$this->form_validation->set_rules('cat_name','','required|xss_clean');

			//$this->form_validation->set_rules('designation','','required|xss_clean');
			

			//$this->form_validation->set_rules('password','','required|xss_clean');
			if($this->form_validation->run())
			{
				$cat_name=$this->input->post('cat_name',true);
				$cat_name_slug = url_title($cat_name, 'dash', true);

				//$password=$this->input->post('password',true);
				$date = date('Y-m-d H:i:s', time());
				$config=array('upload_path'=>'uploads/',
					          'allowed_types'=>'jpg|jpeg|gif|png',
					          'file_name'=>rand(1,9999),'max_size'=>0);
			    $this->upload->initialize($config);
			    if($_FILES['cat_featured_img']['name']!='')
					{
						if($this->upload->do_upload('cat_featured_img'))
						{
						  $dt=$this->upload->data();
						  $cat_featured_img=$dt['file_name'];
						  $input_array=array('cat_name'=>$cat_name,'cat_slug'=>$cat_name_slug,'cat_featured_image'=>$cat_featured_img);	
			
							if($user_info=$this->master_model->insertRecord('table_category',$input_array))
							{ 
							
								$this->session->set_flashdata('success','Expertise added Successfully');			
								redirect(base_url().'superadmin/admin/listExpertise/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 //$data['error']='Something went wrong ,try again later';
							}
						}
						else
						{
							$file="";
							echo $this->upload->display_errors();
							$this->session->set_flashdata('error',$this->upload->display_errors());
							//$data['error']=$this->upload->display_errors();
						}
					}
					

				
			}
		  }		
	  
	  $this->load->view('admin/add_expertise',$data);
	}

	
	public function editInvestor(){
	  $front_id=base64_decode($this->uri->segment('4'));		
	  $data['success']=$data['error']="";
	  $data['pagetitle']='Shreeji | Edit Project';
	   $project_gal_data =   $this->master_model->getRecords('table_pdf',array('investor_id'=>$front_id),'table_pdf.*'); 
	   $project_data =   $this->master_model->getRecords('table_investor',array('id'=>$front_id),'table_investor.*'); 
	    $data['project_data'] = $project_data;
	  
	  $data['project_gal_data'] = $project_gal_data;
	  if(isset($_POST['btn_submit']))
		 {
		 	$this->form_validation->set_rules('project_name','','required|xss_clean');
		 	if($this->form_validation->run())
			{
				$investor_name=$this->input->post('project_name',true);
				$investor_id = $this->input->post('investor_id',true);
				$input_array_data=array('name'=>$investor_name);
				$data_inserted=$this->master_model->updateRecord('table_investor',$input_array_data,array('id'=>$investor_id));

				$count_gal = count($_FILES['project_gal_img']['name']);
								$files = $_FILES;
								if($_FILES['project_gal_img']['name'][0]!=""){

									for($i=0; $i<$count_gal; $i++)
									{
										$config_rec=array('upload_path'=>'uploads/',
								          'allowed_types'=>'pdf',
								          //'file_name'=>md5(rand(1,9999)),
								          'file_name'=>$files['project_gal_img']['name'][$i],
								          'max_size'=>10000);
							    		$this->upload->initialize($config_rec);

							    		$_FILES['project_gal_img']['name'] = $files['project_gal_img']['name'][$i];
								        $_FILES['project_gal_img']['type']= $files['project_gal_img']['type'][$i];
								        $_FILES['project_gal_img']['tmp_name']= $files['project_gal_img']['tmp_name'][$i];
								        $_FILES['project_gal_img']['error']= $files['project_gal_img']['error'][$i];
								        $_FILES['project_gal_img']['size']= $files['project_gal_img']['size'][$i]; 
								        if($this->upload->do_upload('project_gal_img'))
										{
										  $dt_gallery=$this->upload->data();
										  $file_gallery[]=$dt_gallery['file_name'];

										  //$res = $this->master_model->createThumb($file,'uploads/profile/',200,200,FALSE);
										  
										}else
										{
											$this->session->set_flashdata('error',$this->upload->display_errors());
										}
									}

									for($m=0;$m<count($file_gallery);$m++)
											{
												$input_array_gal = array('investor_id'=>$investor_id,'file_name'=>$file_gallery[$m]);
												$this->master_model->insertRecord('table_pdf',$input_array_gal)	;
											}
								}
					
				$this->session->set_flashdata('success','Project Updated Successfully');			
				redirect(base_url().'superadmin/admin/listInvestor/');

			}
			else
			{
				$this->session->set_flashdata('error',$this->upload->display_errors());
				$data['error']=$this->form_validation->error_string();
			}
		 }
	   $this->load->view('admin/edit_investor',$data);
	}


	public function editArticle(){
	  $front_id=base64_decode($this->uri->segment('4'));		
	  $data['success']=$data['error']="";
	  $data['pagetitle']='Shreeji | Edit Article';
	   //$project_gal_data =   $this->master_model->getRecords('table_pdf',array('investor_id'=>$front_id),'table_pdf.*'); 
	   $article_data =   $this->master_model->getRecords('table_article',array('id'=>$front_id),'table_article.*'); 
	   //print_r($article_data);exit;
	    $data['article_data'] = $article_data;
	  
	  //$data['project_gal_data'] = $project_gal_data;
	  if(isset($_POST['btn_submit']))
		 {
		 	$this->form_validation->set_rules('project_name','','required|xss_clean');
		 	$this->form_validation->set_rules('project_short_desc','','required|xss_clean');
		 	$this->form_validation->set_rules('publish_date','','required|xss_clean');
		 	if($this->form_validation->run())
			{
				$title=$this->input->post('project_name',true);
				$desc = $this->input->post('project_short_desc',true);
				$publish_date = $this->input->post('publish_date',true);
				$article_id = $this->input->post('article_id',true);
				$input_array_data=array(
										'title'=>$title,
										'description' => $desc ,
										'publish_date'=>$publish_date
										);
				$data_inserted=$this->master_model->updateRecord('table_article',$input_array_data,array('id'=>$article_id));

					
					if($_FILES['project_banner_img']['name']!='')
					{
						$config=array('upload_path'=>'uploads/',
					          'allowed_types'=>'jpg|jpeg|gif|png',
					          'file_name'=>rand(1,9999),'max_size'=>0);
			    		$this->upload->initialize($config);
						if($this->upload->do_upload('project_banner_img'))
						{
						  $dt=$this->upload->data();
						  $file=$dt['file_name'];
						  $banner_arr=array('img_name'=>$file);	
			
						$banner_img_updated=$this->master_model->updateRecord('table_article',$banner_arr,array('id'=>$article_id));
							
						}
						else
						{
							$file="";
							$this->session->set_flashdata('error',$this->upload->display_errors());
							$data['error']=$this->upload->display_errors();
						}
					}


				$count_gal = count($_FILES['project_gal_img']['name']);
								$files = $_FILES;
								if($_FILES['project_gal_img']['name'][0]!=""){

									for($i=0; $i<$count_gal; $i++)
									{
										$config_rec=array('upload_path'=>'uploads/',
								          'allowed_types'=>'pdf',
								          //'file_name'=>md5(rand(1,9999)),
								          'file_name'=>$files['project_gal_img']['name'][$i],
								          'max_size'=>10000);
							    		$this->upload->initialize($config_rec);

							    		$_FILES['project_gal_img']['name'] = $files['project_gal_img']['name'][$i];
								        $_FILES['project_gal_img']['type']= $files['project_gal_img']['type'][$i];
								        $_FILES['project_gal_img']['tmp_name']= $files['project_gal_img']['tmp_name'][$i];
								        $_FILES['project_gal_img']['error']= $files['project_gal_img']['error'][$i];
								        $_FILES['project_gal_img']['size']= $files['project_gal_img']['size'][$i]; 
								        if($this->upload->do_upload('project_gal_img'))
										{
										  $dt_gallery=$this->upload->data();
										  $file_gallery[]=$dt_gallery['file_name'];

										  //$res = $this->master_model->createThumb($file,'uploads/profile/',200,200,FALSE);
										  
										}else
										{
											$this->session->set_flashdata('error',$this->upload->display_errors());
										}
									}
									//print_r($file_gallery);exit;
									for($m=0;$m<count($file_gallery);$m++)
											{
												$input_array_gal = array('id'=>$article_id,'pdf_file_name'=>$file_gallery[$m]);
												//$this->master_model->insertRecord('table_article',$input_array_gal)	;
												$this->master_model->updateRecord('table_article',$input_array_gal,array('id'=>$article_id));
												//echo $this->db->last_query();exit;
											}
								}
					
				$this->session->set_flashdata('success','Article Updated Successfully');			
				redirect(base_url().'superadmin/admin/listArticle/');

			}
			else
			{
				$this->session->set_flashdata('error',$this->upload->display_errors());
				$data['error']=$this->form_validation->error_string();
			}
		 }
	   $this->load->view('admin/edit_article',$data);
	}
	

	public function editProject()
	{
	  $front_id=base64_decode($this->uri->segment('4'));		
	  $data['success']=$data['error']="";
	  $data['pagetitle']='SMC | Edit Project';
	  $country_data =   $this->master_model->getRecords('table_country',array(),'table_country.*'); 
	  $project_data =   $this->master_model->getRecords('table_project',array('project_id'=>$front_id),'table_project.*'); 
	  $project_gal_data =   $this->master_model->getRecords('table_gallery',array('project_id'=>$front_id),'table_gallery.*'); 
	  //var_dump($project_gal_data);
	  $cat_data =   $this->master_model->getRecords('table_category',array(),'table_category.*'); 
	  $data['country_data'] = $country_data;
	  $data['project_data'] = $project_data;
	   $data['cat_data'] = $cat_data;
	  $data['project_gal_data'] = $project_gal_data;
		 if(isset($_POST['btn_submit']))
		 {


			$this->form_validation->set_rules('project_name','','required|xss_clean');
			$this->form_validation->set_rules('select_country','','required|xss_clean');
			$this->form_validation->set_rules('project_short_desc','','required|xss_clean');
			$this->form_validation->set_rules('status','','required|xss_clean');
			$this->form_validation->set_rules('key_project','','required|xss_clean');
			//$this->form_validation->set_rules('client_name','','required|xss_clean');
			
			//$this->form_validation->set_rules('work_url_youtube','Youtube Url','required|xss_clean');

			//$this->form_validation->set_rules('password','','required|xss_clean');
			
			if($this->form_validation->run())
			{
				
				$project_name=$this->input->post('project_name',true);
				$project_name_slug = url_title($project_name, 'dash', true);
				$project_short_desc=$this->input->post('project_short_desc',true);
				$project_des_left =$this->input->post('project_des_left',true);
				$project_des_right =$this->input->post('project_des_right',true);

				$project_heading1 =$this->input->post('project_heading1',true);
				$project_heading2 =$this->input->post('project_heading2',true);
				$status =$this->input->post('status',true);
				$key_project =$this->input->post('key_project',true);
				$banner_heading1 =$this->input->post('banner_heading1',true);
				$banner_heading2 =$this->input->post('banner_heading2',true);

				$project_id = $this->input->post('project_id',true);
				//$select_country=$this->input->post('select_country',true);
				
				$select_country=$this->input->post('select_country',true);
				$select_expertise=$this->input->post('select_expertise',true);
				
				//$select_country = implode(",",$select_country);
				
				$date = date('Y-m-d H:i:s', time());
				$config=array('upload_path'=>'uploads/',
					          'allowed_types'=>'jpg|jpeg|gif|png',
					          'file_name'=>rand(1,9999),'max_size'=>0);
			    $this->upload->initialize($config);

			    $input_array_data=array('project_name'=>$project_name,'project_name_slug'=>$project_name_slug,'heading1'=>$project_heading1,'heading2'=>$project_heading2,'project_short_desc'=>$project_short_desc,'project_des'=>$project_des_left,'right_desc'=>$project_des_right,'country_id'=>$select_country,'status'=>$status,"cat_id"=>$select_expertise,'banner_heading1'=>$banner_heading1,'banner_heading2'=>$banner_heading2,'key_project'=>$key_project);
			    $data_inserted=$this->master_model->updateRecord('table_project',$input_array_data,array('project_id'=>$project_id));


			    if($_FILES['project_featured_img']['name']!='')
					{
						if($this->upload->do_upload('project_featured_img'))
						{
						  $dt=$this->upload->data();
						  $file=$dt['file_name'];
						  $input_array=array('project_featured_img'=>$file);	
			
						$featured_img_updated=$this->master_model->updateRecord('table_project',$input_array,array('project_id'=>$project_id));
							
						}
						else
						{
							$file="";
							$this->session->set_flashdata('error',$this->upload->display_errors());
							$data['error']=$this->upload->display_errors();
						}
					}

					 if($_FILES['project_banner_img']['name']!='')
					{
						if($this->upload->do_upload('project_banner_img'))
						{
						  $dt=$this->upload->data();
						  $file=$dt['file_name'];
						  $input_array=array('project_banner_img'=>$file);	
			
						$banner_img_updated=$this->master_model->updateRecord('table_project',$input_array,array('project_id'=>$project_id));
							
						}
						else
						{
							$file="";
							$this->session->set_flashdata('error',$this->upload->display_errors());
							$data['error']=$this->upload->display_errors();
						}
					}

					$count_gal = count($_FILES['project_gal_img']['name']);
								$files = $_FILES;
								if($_FILES['project_gal_img']['name'][0]!=""){

									for($i=0; $i<$count_gal; $i++)
									{
										$config_rec=array('upload_path'=>'uploads/',
								          'allowed_types'=>'jpg|jpeg|gif|png',
								          'file_name'=>md5(rand(1,9999)),'max_size'=>10000);
							    		$this->upload->initialize($config_rec);

							    		$_FILES['project_gal_img']['name'] = $files['project_gal_img']['name'][$i];
								        $_FILES['project_gal_img']['type']= $files['project_gal_img']['type'][$i];
								        $_FILES['project_gal_img']['tmp_name']= $files['project_gal_img']['tmp_name'][$i];
								        $_FILES['project_gal_img']['error']= $files['project_gal_img']['error'][$i];
								        $_FILES['project_gal_img']['size']= $files['project_gal_img']['size'][$i]; 
								        if($this->upload->do_upload('project_gal_img'))
										{
										  $dt_gallery=$this->upload->data();
										  $file_gallery[]=$dt_gallery['file_name'];

										  //$res = $this->master_model->createThumb($file,'uploads/profile/',200,200,FALSE);
										  
										}else
										{
											$this->session->set_flashdata('error',$this->upload->display_errors());
										}
									}

									for($m=0;$m<count($file_gallery);$m++)
											{
												$input_array_gal = array('project_id'=>$project_id,'img_name'=>$file_gallery[$m]);
												$this->master_model->insertRecord('table_gallery',$input_array_gal)	;
											}
								}
					
				$this->session->set_flashdata('success','Project Updated Successfully');			
				redirect(base_url().'superadmin/admin/listProject/'.base64_encode($select_country));				
				
			}else
			{
				$this->session->set_flashdata('error',$this->upload->display_errors());
				$data['error']=$this->form_validation->error_string();
			}
		  }		
	  
	  $this->load->view('admin/edit_project',$data);
	}
	

	public function editExpertise()
	{
	  $front_id=base64_decode($this->uri->segment('4'));		
	  $data['success']=$data['error']="";
	  $data['pagetitle']='SMC | Edit Expertise';
	  $category_data =   $this->master_model->getRecords('table_category',array('cat_id'=>$front_id),'table_category.*'); 
	  $data['category_data'] = $category_data;
	  
		 if(isset($_POST['btn_submit']))
		 {
			$this->form_validation->set_rules('cat_name','','required|xss_clean');
			$this->form_validation->set_rules('cat_id','','required|xss_clean');
			
			if($this->form_validation->run())
			{
				$cat_name=$this->input->post('cat_name',true);
				$cat_name_slug = url_title($cat_name, 'dash', true);
				$cat_id=$this->input->post('cat_id',true);
				$old_image=$this->input->post('old_img',true);

				//$password=$this->input->post('password',true);
				$date = date('Y-m-d H:i:s', time());
				$config=array('upload_path'=>'uploads/',
					          'allowed_types'=>'jpg|jpeg|gif|png',
					          'file_name'=>rand(1,9999),'max_size'=>0);
			    $this->upload->initialize($config);
			    if($_FILES['cat_featured_image']['name']!='')
					{
						if($this->upload->do_upload('cat_featured_image'))
						{
						  $dt=$this->upload->data();
						  $file=$dt['file_name'];

						}
						else
						{
							$file=$old_image;
							$data['error']=$this->upload->display_errors();
						}
					}else
					{	
						
						$file= $old_image;
					}
					
					 $input_array=array('cat_name'=>$cat_name,'cat_slug'=>$cat_name_slug,'cat_featured_image'=>$file);
			
							if($user_info=$this->master_model->updateRecord('table_category',$input_array,array('cat_id'=>$cat_id)))
							{ 
								if($_FILES['cat_featured_image']['name']!='')
								{
									@unlink('uploads/'.$old_image);
								}
								$this->session->set_flashdata('success','Expertise edited Successfully');			
								redirect(base_url().'superadmin/admin/listExpertise/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}					

			}
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
		  }		
	  
	  $this->load->view('admin/edit_expertise',$data);
	}

	public function editCountry()
	{
		$front_id=base64_decode($this->uri->segment('4'));
	  $data['success']=$data['error']="";
	  $data['pagetitle']='SMC | Edit Country';
	  $result=$this->master_model->getRecords('table_country',array('country_id'=>$front_id),'table_country.*'); 
	  $data['country_data'] = $result;
		 if(isset($_POST['btn_submit']))
		 {
			$this->form_validation->set_rules('country_name','','required|xss_clean');
			//$this->form_validation->set_rules('password','','required|xss_clean');
			if($this->form_validation->run())
			{
				$country_name=$this->input->post('country_name',true);
				$country_name_slug = url_title($artist_name, 'dash', true);
				$country_id=$this->input->post('country_id',true);
				$input_array = array('country_name'=>$country_name);
					
					if($this->master_model->updateRecord('table_country',$input_array,array('country_id'=>$country_id)))
					{ 
						
						$this->session->set_flashdata('success','Country edited Successfully');			
						redirect(base_url().'superadmin/admin/listCountry/');
					}
					else
					{	
						$this->session->set_flashdata('error','Something went wrong ,try again later');	
						 redirect(base_url().'superadmin/admin/editCountry/'.base64_encode($artist_id));
					}
				//$user_info=$this->master_model->insertRecord('table_artist',$input_array);
				
			}
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
		  }		
	  
	  $this->load->view('admin/edit_country',$data);
	}

	

	public function editCms()
	{
	  $data['pagetitle'] = "SMC | Edit CMS";
	  $front_id=base64_decode($this->uri->segment('4'));
	  $data['success']=$data['error']="";
	  //$data['pagetitle']=' | Edit CMS';
	  $result=$this->master_model->getRecords('table_cms',array('cms_id'=>$front_id),'table_cms.*'); 
	  $data['cms_data'] = $result;
		 if(isset($_POST['btn_submit']))
		 {
			
			//$this->form_validation->set_rules('password','','required|xss_clean');

				$cms_content=mysql_real_escape_string($this->input->post('content'));
				$cms_id=$this->input->post('cms_id',true);
				$old_image=$this->input->post('old_image',true);
				//var_dump($_POST);
				//$password=$this->input->post('password',true);
				
				$date = date('Y-m-d H:i:s', time());
				$config=array('upload_path'=>'uploads/',
					          'allowed_types'=>'jpg|jpeg|gif|png',
					          'file_name'=>rand(1,9999),'max_size'=>0);
			    $this->upload->initialize($config);
			    if($_FILES['work_image']['name']!='')
					{
						if($this->upload->do_upload('work_image'))
						{
						  $dt=$this->upload->data();
						  $file=$dt['file_name'];

						}
						else
						{
							$file=$old_image;
							$data['error']=$this->upload->display_errors();
						}
					}else
					{
						$file=$old_image;
					}
				$input_array=array('content'=>$cms_content,'logo_img'=>$file,'date_added'=>$date);	
				
				//$user_info=$this->master_model->insertRecord('table_artist',$input_array);
				if($this->master_model->updateRecord('table_cms',$input_array,array('cms_id'=>$cms_id)))
				{ 
					//echo $this->db->last_query();
					if($_FILES['file_upload']['name']!='')
								{
									@unlink('uploads/admin/'.$old_image);
								}
					$this->session->set_flashdata('success','Page edited Successfully');			
					redirect(base_url().'superadmin/admin/listCms/');
				}
				else
				{
					 $data['error']='Something went wrong ,try again later';
				}
			
		  }		
	  
	  $this->load->view('admin/edit_cms',$data);
	}


	public function editHomeImagetablet()
	{

	  
	  $data['success']=$data['error']="";
	  $front_id=base64_decode($this->uri->segment('4'));
	  $data['pagetitle']='SMC | Edit Home Page Image Tablet';
	  $result=$this->master_model->getRecords('table_home_images',array('image_id'=>$front_id),'table_home_images.*'); 
	  $data['artist_data'] = $result;
		 if(isset($_POST['btn_submit']))
		 {
			
			
				
				$old_image=$this->input->post('old_image',true);
				$image_id=$this->input->post('image_id',true);
				//$password=$this->input->post('password',true);
				$date = date('Y-m-d H:i:s', time());
				$config=array('upload_path'=>'uploads/',
					          'allowed_types'=>'svg|jpg|jpeg|gif|png',
					          'file_name'=>rand(1,9999),'max_size'=>0);
			    $this->upload->initialize($config);
			    if($_FILES['work_image']['name']!='')
					{
						if($this->upload->do_upload('work_image'))
						{
						  $dt=$this->upload->data();
						  $file=$dt['file_name'];

						}
						else
						{
							$file=$old_image;
							//$data['error']=$this->upload->display_errors();
							$this->session->set_flashdata('error',$this->upload->display_errors());	
						 redirect(base_url().'superadmin/admin/editHomeImagetablet/'.base64_encode($image_id));
						}
					}else
					{
						$file=$old_image;
					}
				$input_array=array('image_name_tablet'=>$file,'date_added'=>$date);	
					
					if($this->master_model->updateRecord('table_home_images',$input_array,array('image_id'=>$image_id)))
					{ 
						if($_FILES['file_upload']['name']!='')
								{
									@unlink('uploads/admin/'.$old_image);
								}
						$this->session->set_flashdata('success','Image edited Successfully');			
						redirect(base_url().'superadmin/admin/listHometablet/');
					}
					else
					{	
						$this->session->set_flashdata('error','Something went wrong ,try again later');	
						 redirect(base_url().'superadmin/admin/editHomeImagetablet/'.base64_encode($image_id));
						//redirect(base_url().'superadmin/admin/editHomeImagetablet/');
					}
				//$user_info=$this->master_model->insertRecord('table_artist',$input_array);
				
			}
			
		  	
	  
	  $this->load->view('admin/edit_HomeImagetablet',$data);
	}


	public function editHomeImagemobile()
	{

	  
	  $data['success']=$data['error']="";
	  $front_id=base64_decode($this->uri->segment('4'));
	  $data['pagetitle']='SMC | Edit Home Page Image mobile';
	  $result=$this->master_model->getRecords('table_home_images',array('image_id'=>$front_id),'table_home_images.*'); 
	  $data['artist_data'] = $result;
		 if(isset($_POST['btn_submit']))
		 {
			
			

				$old_image=$this->input->post('old_image',true);
				$image_id=$this->input->post('image_id',true);
				//$password=$this->input->post('password',true);
				$date = date('Y-m-d H:i:s', time());
				$config=array('upload_path'=>'uploads/',
					          'allowed_types'=>'svg|jpg|jpeg|gif|png',
					          'file_name'=>rand(1,9999),'max_size'=>0);
			    $this->upload->initialize($config);
			    if($_FILES['work_image']['name']!='')
					{
						if($this->upload->do_upload('work_image'))
						{
						  $dt=$this->upload->data();
						  $file=$dt['file_name'];

						}
						else
						{
							$file=$old_image;
							//$data['error']=$this->upload->display_errors();
							$this->session->set_flashdata('error',$this->upload->display_errors());	
						 redirect(base_url().'superadmin/admin/editHomeImagemobile/'.base64_encode($image_id));

						}
					}else
					{
						$file=$old_image;
					}
				$input_array=array('image_name_mobile'=>$file,'date_added'=>$date);	
					
					if($this->master_model->updateRecord('table_home_images',$input_array,array('image_id'=>$image_id)))
					{ 
						if($_FILES['file_upload']['name']!='')
								{
									@unlink('uploads/admin/'.$old_image);
								}
						$this->session->set_flashdata('success','Image edited Successfully');			
						redirect(base_url().'superadmin/admin/listHomemobile/');
					}
					else
					{	
						$this->session->set_flashdata('error','Something went wrong ,try again later');	
						 redirect(base_url().'superadmin/admin/editHomeImagemobile/'.base64_encode($image_id));
						//redirect(base_url().'superadmin/admin/editHomeImagetablet/');
					}
				//$user_info=$this->master_model->insertRecord('table_artist',$input_array);
				
			}
			
		  	
	  
	  $this->load->view('admin/edit_HomeImagemobile',$data);
	}

	public function deleteCountry()
	{
		 $country_id=$this->input->post('country_id',true);
		 $country_id=base64_decode($country_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_country','country_id',$country_id))
				{ 
					$this->session->set_flashdata('success','Country deleted successfully');
					//redirect(base_url().'superadmin/admin/listArtist');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		
	}

	public function deleteInvestor()
	{
		 $investor_id=$this->input->post('investor_id',true);
		 $investor_id=base64_decode($investor_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_investor','id',$investor_id))
				{ 
					$this->session->set_flashdata('success','Investor deleted successfully');
					//redirect(base_url().'superadmin/admin/listArtist');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		
	}

	public function deleteArticle()
	{
		 $article_id=$this->input->post('article_id',true);
		 $article_id=base64_decode($article_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_article','id',$article_id))
				{ 
					$this->session->set_flashdata('success','Article deleted successfully');
					//redirect(base_url().'superadmin/admin/listArtist');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		
	}

	public function deleteCategory()
	{
		
		 $cat_id=$this->input->post('cat_id',true);
		 $cat_id=base64_decode($cat_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_category','cat_id',$cat_id))
				{ 
					$this->session->set_flashdata('success','Expertise deleted successfully');
					//redirect(base_url().'superadmin/admin/listTeam');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		//redirect(base_url().'superadmin/admin/listTeam');
	}
	public function delete_gal_images()
	{
		
		 $investor_id=$this->input->post('investor_id',true);
		 //$image_id=base64_decode($image_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_pdf','id',$investor_id))
				{ 
					//unlink(base_url().'uploads/'.$image_id);
					//$this->session->set_flashdata('success','Image deleted successfully');
					//redirect(base_url().'superadmin/admin/listTeam');
					echo 1;
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		//redirect(base_url().'superadmin/admin/listTeam');
	}

	public function delete_article_images()
	{
		
		 $image_id=$this->input->post('filename',true);
		 //$image_id=base64_decode($image_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				/*if($this->master_model->deleteRecord('table_pdf','id',$image_id))
				{ 
					//unlink(base_url().'uploads/'.$image_id);
					//$this->session->set_flashdata('success','Image deleted successfully');
					//redirect(base_url().'superadmin/admin/listTeam');
					echo 1;
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	*/
		//redirect(base_url().'superadmin/admin/listTeam');
	}

	public function delete_article_pdf()
	{
		
		 $article_id=$this->input->post('article_id',true);
		 //$image_id=base64_decode($image_id);
		//echo $artist_id;
		 $input_array  = array('pdf_file_name' => "" );
		$this->master_model->updateRecord('table_article',$input_array,array('id'=>$article_id));
				if($this->master_model->updateRecord('table_article',$input_array,array('id'=>$article_id)))
				{ 
					//unlink(base_url().'uploads/'.$image_id);
					echo 'file deleted successfully';
					
				}else
				{
					echo 'Something went wrong ,try again later';
				}	
		//redirect(base_url().'superadmin/admin/listTeam');
	}
    
	public function deletesodaismImage()
	{
		
		 $image_id=$this->input->post('image_id',true);
		 $image_id=base64_decode($image_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_sodaism','image_id',$image_id))
				{ 
					$this->session->set_flashdata('success','Image deleted successfully');
					//redirect(base_url().'superadmin/admin/listTeam');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		//redirect(base_url().'superadmin/admin/listTeam');
	}
	
	public function deletework()
	{
		 $work_id=$this->input->post('work_id',true);
		 $work_id=base64_decode($work_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_project','project_id',$work_id))
				{ 
					$this->session->set_flashdata('success','Project deleted successfully');
					//redirect(base_url().'superadmin/admin/listWork');
				}else
				{
					$this->session->set_flashdata('error','Something went wrong ,try again later');
					//$data['error']='Something went wrong ,try again later';
				}	
		//redirect(base_url().'superadmin/admin/listWork');
	}
	public function accountsetting()
	{
	  $data['success']=$data['error']=$data['success1']=$data['error1']=$data['success2']=$data['error2']='';
	  $data['pagetitle']='Lets learn India | Account Setting';	
	  if(isset($_POST['btn_account']))
	  {
		  $this->form_validation->set_rules('username','','required|xss_clean');
		  $this->form_validation->set_rules('email','','required|xss_clean|valid_email');
		   $this->form_validation->set_rules('phone','','required|xss_clean');
		  if($this->form_validation->run())
		  {
			 	$username=$this->input->post('username',true);
				$email=$this->input->post('email',true);
				$phone=$this->input->post('phone',true);
				$fax=$this->input->post('fax',true);
				$address=$this->input->post('address',true);
				$old_image=$this->input->post('old_image',true);
				$config=array('upload_path'=>'uploads/admin/',
					          'allowed_types'=>'jpg|jpeg|gif|png',
					          'file_name'=>rand(1,9999),'max_size'=>0);
			    $this->upload->initialize($config);
				
					if($_FILES['file_upload']['name']!='')
					{
						if($this->upload->do_upload('file_upload'))
						{
						  $dt=$this->upload->data();
						  $file=$dt['file_name'];
						}
						else
						{
							$file=$old_image;
							$data['error']=$this->upload->display_errors();
						}
					}
					else
					{
						$file=$old_image;
					}
					$input_array=array('admin_username'=>$username,'admin_email'=>$email,'admin_img'=>$file,'admin_phone'=>$phone,'admin_fax'=>$fax,'admin_address'=>$address);
					$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
					if($_FILES['file_upload']['name']!='')
					{
						@unlink('uploads/admin/'.$old_image);
					}
					$user_data=array('admin_img'=>$file);
					$this->session->set_userdata($user_data);
					$data['success']='Record Updated Successfully.';
		  }
		  else
		  {
			$data['error']=$this->form_validation->error_string();
		  }
	  }
	  if(isset($_POST['btn_password']))
	  {
		   	$this->form_validation->set_rules('current_pass','','required|xss_clean');
		  	$this->form_validation->set_rules('new_pass','New Password','required|xss_clean');
			$this->form_validation->set_rules('confirm_pass','Confirm Password','required|xss_clean|matches[new_pass]|max_length[6]');
			  if($this->form_validation->run())
			  {
				  	$current_pass=$this->input->post('current_pass',true);
					$new_pass=$this->input->post('new_pass',true);
					$row=$this->master_model->getRecordCount('admin_login',array('admin_password'=>$current_pass));
					if($row==0)
					{
						$data['error1']="Current Password is Wrong.";
					}
					else
					{
						$input_array=array('admin_password'=>$new_pass);
						$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
						$data['success1']='Password Updated Successfully.'; 
					}
			  }
			  else
			  {
				  $data['error1']=$this->form_validation->error_string();
			  }
	  }
	  if(isset($_POST['btn_status']))
	  {
		$site_status=$this->input->post('site_status',true);
		$input_array1=array('site_status'=>$site_status);
		$this->master_model->updateRecord('tbl_site_status',$input_array1,array('site_id'=>'1'));
	    $data['success2']='Site Status Changed Successfully.'; 
	  }
	  $data['result']=$this->master_model->getRecords('admin_login',array('admin_login.id'=>1),'admin_login.*');
	  $data['status']=$this->master_model->getRecords('tbl_site_status',array('site_id'=>1),'*');
	  $data['middle_content']='accountsetting';
	  $this->load->view('admin/common-file',$data);
	}
	/*
	  Function   : sociallink
	  Developer  : Dhananjay
	  Routes File: 'academymaster/sociallink' used by 'academymaster/admin/sociallink'
	  Description: Admin can update social link.    
	*/
	public function sociallink()
	{
	  $data['success']=$data['error']='';
	  $data['pagetitle']='Lets learn India | Sociallink';	
	  $data['social_link']=$this->master_model->getRecords('tbl_social',array('tbl_social.social_id'=>1),'tbl_social.*');
	  if(isset($_POST['btn_social']))
	  {
		 $this->form_validation->set_rules('facebook_link','','required|xss_clean|valid_url');
		 $this->form_validation->set_rules('twitter_link','','required|xss_clean|valid_url'); 
		 $this->form_validation->set_rules('linkedin_link','','required|xss_clean|valid_url');
		  $this->form_validation->set_rules('pinterest_link','','required|xss_clean|valid_url');
		 if($this->form_validation->run())
		 {
			 $facebook_link=$this->input->post('facebook_link',true);
			 $twitter_link=$this->input->post('twitter_link',true);
			 $linkedin_link=$this->input->post('linkedin_link',true);
			  $pinterest_link=$this->input->post('pinterest_link',true);
			 $update_link=array('facebook'=>$facebook_link,'linkedin'=>$linkedin_link,'twitter'=>$twitter_link,'pinterest'=>$pinterest_link);
		     if($this->master_model->updateRecord('tbl_social',$update_link,array('social_id'=>'1')))
			 {
				$this->session->set_flashdata('success','Social links updated successfully.'); 
				redirect(base_url().'superadmin/admin/sociallink/');
			 }
			 else
			 {
				$this->session->set_flashdata('error','Error while updating social link.'); 
				redirect(base_url().'superadmin/admin/sociallink/'); 
			 }
		}
	  }
	  $data['middle_content']='sociallink';
	  $this->load->view('admin/common-file',$data);
	}

	public function sortWork(){
		 $work_id=$this->input->post('work_id',true);
		 $sort_order=$this->input->post('order',true);	
		 $update_order=array('sort_order'=>$sort_order);
		 $this->master_model->updateRecord('table_work',$update_order,array('work_id'=>$work_id));
	}

	public function sortWorkorder()
	{
		
		$data_arr = json_decode($_POST['positions']);
		//echo "<pre>";
		//print_r($data_arr);
		for($i=0;$i<count($data_arr);$i++)
		{
			$work_id = $data_arr[$i][0];
			$update_order=array('sort_order'=>$data_arr[$i][1]);
		 	
		 	$this->master_model->updateRecord('table_work',$update_order,array('work_id'=>$work_id));
		 	
			 //{
			 	//echo $this->db->last_query()."<br>";
				//$this->session->set_flashdata('success','Work Order list updated successfully.'); 
				//redirect(base_url().'superadmin/admin/listWorkorder/'.$_POST['artist_id']);
			 //}
		}
		$this->session->set_flashdata('success','Work Order list updated successfully.'); 
			redirect(base_url().'superadmin/admin/listWorkorder/'.$_POST['artist_id']);
	}

	public function sortExpertise(){
		 $cat_id=$this->input->post('cat_id',true);
		 $sort_order=$this->input->post('order',true);	
		 $update_order=array('sort_order'=>$sort_order);
		 $this->master_model->updateRecord('table_category',$update_order,array('cat_id'=>$cat_id));
	}

	public function sortImage(){
		 $image_id=$this->input->post('image_id',true);
		 $sort_order=$this->input->post('order',true);	
		 $update_order=array('sort_order'=>$sort_order);
		 $this->master_model->updateRecord('table_home_images',$update_order,array('image_id'=>$image_id));
	}

	public function sortsodaismImage(){
		 $image_id=$this->input->post('image_id',true);
		 $sort_order=$this->input->post('order',true);	
		 $update_order=array('sort_order'=>$sort_order);
		 $this->master_model->updateRecord('table_sodaism',$update_order,array('image_id'=>$image_id));
	}

	

	public function sortTeam(){
		 $member_id=$this->input->post('member_id',true);
		 $sort_order=$this->input->post('order',true);	
		 $update_order=array('sort_order'=>$sort_order);
		 $this->master_model->updateRecord('table_team',$update_order,array('member_id'=>$member_id));
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Project extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['pagetitle'] = 'SMC Infra';
		$slug=$this->uri->segment('2');	
		$cat_data =   $this->master_model->getRecords('table_category',array('cat_slug'=>$slug),'table_category.cat_id,table_category.cat_name'); 
		$data['country_data'] =   $this->master_model->getRecords('table_country',array(),'table_country.*'); 
		//var_dump($cat_id);
		$data['project_data'] = $this->master_model->getRecords('table_project as tp JOIN table_country as tc ON tc.country_id=tp.country_id',array('tp.cat_id'=>$cat_data[0]['cat_id']),'tp.*,tc.country_name',array('tp.sort_order'=>'asc'));

		
		$data['cat_data'] = $cat_data;

		//var_dump($project_data);
		$this->load->view('project-list',$data);
	}

	

	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
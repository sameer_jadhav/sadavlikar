<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Project_archive extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['pagetitle'] = 'SMC Infra';
		$data['country_data'] =   $this->master_model->getRecords('table_country',array(),'table_country.*'); 
		//var_dump($cat_id);
		$data['project_data'] = $this->master_model->getRecords('table_project as tp JOIN table_country as tc ON tc.country_id=tp.country_id JOIN table_category as tca ON tca.cat_id=tp.cat_id',array(),'tp.*,tc.country_name,tca.cat_name,tca.cat_slug',array('tp.sort_order'=>'asc'));

		
		

		
		$this->load->view('project_archives',$data);
	}

	public function project_archives_ajax()
	{
		$project_id = $_POST['project_id'];
		$data['gallery_data'] = $this->master_model->getRecords("table_gallery",array("project_id"=>$project_id),"table_gallery.*");
		$this->load->view('gallery_ajax',$data);	
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
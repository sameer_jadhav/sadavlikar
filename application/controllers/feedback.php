<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        /*
        $check_auth_client = $this->MyModel->check_auth_client();
		if($check_auth_client != true){
			die($this->output->get_output());
		}
		*/
    }

public function send(){
		$this->load->model('email_sending');
    	$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}else{

			$params = json_decode(file_get_contents('php://input'), TRUE);

			if (($params['social_id'] == "") || !isset($params['social_id']) )  {
				json_output(400,array('status' => 400,'message' => 'Bad request.'));
			}else{
				$rec_count = $this->master_model->getRecordCount('table_user ',array("facebook_id"=>$params['social_id'])); 
				if($rec_count>0){
					$img_name = $this->upload_img($params['img_name']);
					
					$data_array = array(
										"name"    	 => $params['name'],
										"address"    => $params['address'],
										"contact"	 => $params['contact'],
										"description"=> $params['description'],
										"img_name"	 => $img_name
										);
					$response['status'] = 400;
					
					$insert_user = $this->master_model->insertRecord("table_feedback",$data_array);
					//$update_user = $this->master_model->updateRecord("table_user",$data_array,array("facebook_id"=>$params['social_id']));
					if($insert_user){
						$response['status'] = 200;
						$email_id = 'sadavlisan@gmail.com';
						$info_arr=array('from'=>'sadavlisan@gmail.com','to'=>$email_id,'subject'=>'Compalint/Feedback from App','view'=>'contact-us');
						$other_info=$data_array;
						$path = $_SERVER["DOCUMENT_ROOT"]."/uploads/feedback/".$img_name;
						//echo $path;exit;
						$this->email_sending->sendmail_attach($info_arr,$other_info,$path);
		    	    			
						$output  = array(
		        					'status'=>$response['status'],
		        					'message'=>'success'
		        					);
						json_output($response['status'],$output);
					}
				}else{
					json_output(400,array('status' => 400,'message' => 'No Data'));
				}
			}
		}
    }


    private function upload_img($img_name)
    {
    	
    	$data = $img_name;
		if (preg_match('/^data:image\/(\w+);base64,/', $data, $type)) {

		    $data = substr($data, strpos($data, ',') + 1);
		    $type = strtolower($type[1]); // jpg, png, gif

		    if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ])) {
		        throw new \Exception('invalid image type');
		    }

		    $data = base64_decode($data);

		    if ($data === false) {
		        throw new \Exception('base64_decode failed');
		    }
		} else {
		    throw new \Exception('did not match data URI with image data');
		}
		
		$image_name = md5(uniqid(rand(), true));
		$filename = $image_name . '.' . $type;
		//rename file name with random number
		$path = "uploads/feedback/";
		//image uploading folder path
		file_put_contents($path . $filename, $data);
		return $filename;
	}

  }
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        /*
        $check_auth_client = $this->MyModel->check_auth_client();
		if($check_auth_client != true){
			die($this->output->get_output());
		}
		*/
    }

    public function index()
    {
    	$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}else{
					$page=  $this->input->get("page");
					$limit=  $this->input->get("limit");
					$start_from = ($page-1) * $limit;  
					$response['status'] = 200;
		        	//$resp = $this->my_model->event_listing($page,$start_from,$limit);
		        	$resp = $this->master_model->getRecords('table_payment ',array(),'table_payment.id,table_payment.title,table_payment.payment_url',array("table_payment.id"=>"asc")); 


		   

	
		    $output  = array(
		        					'data' => $resp,
		        					'status'=>$response['status'],
		        					'message'=>'success');
	    			json_output($response['status'],$output);    	
		}
    }


    

}
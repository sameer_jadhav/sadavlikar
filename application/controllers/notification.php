<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        /*
        $check_auth_client = $this->MyModel->check_auth_client();
		if($check_auth_client != true){
			die($this->output->get_output());
		}
		*/
    }

    public function index()
    {
    	$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}else{
			$page=  $this->input->get("page");
					$limit=  $this->input->get("limit");
					$start_from = ($page-1) * $limit;  
					$response['status'] = 200;
		        	//$resp = $this->my_model->event_listing($page,$start_from,$limit);
		        	$resp = $this->master_model->getRecords('table_notification ',array(),'table_notification.id,table_notification.title',array("table_notification.id"=>"desc"),0,10); 


		    for($i=0;$i<count($resp);$i++){
		    	if($resp[$i]['img_name']!=""){
		    			$resp[$i]['img_name'] = base_url().'uploads/'.$resp[$i]['img_name'];
		    	}
		    }

	
		    $output  = array(
		        					'data' => $resp,
		        					'status'=>$response['status'],
		        					'message'=>'success');
	    			json_output($response['status'],$output);    	
		}
    }

    public function detail($id)
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
					$response['status'] = 200;
		       
		        	$resp = $this->my_model->event_detail($id);

		        	$resp = $this->master_model->getRecords('table_notification ',array("id"=>$id),'table_notification.id,table_notification.title,table_notification.description',array("table_notification.id"=>"desc"),0,5); 
		        	//print_r($resp[0]);
		        			
		        	$output  = array(
		        					'data' => $resp,
		        					'status'=>$response['status'],
		        					'message'=>'success');
					json_output($response['status'],$output);
		        
			
		}
	}


    

}
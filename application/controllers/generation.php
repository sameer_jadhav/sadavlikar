<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Generation extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        /*
        $check_auth_client = $this->MyModel->check_auth_client();
		if($check_auth_client != true){
			die($this->output->get_output());
		}
		*/
    }

    public function history()
    {
    	$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}else{
			$page=  $this->input->get("page");
					$limit=  $this->input->get("limit");
					$start_from = ($page-1) * $limit;  
					$response['status'] = 200;
		        	//$resp = $this->my_model->event_listing($page,$start_from,$limit);
		        	$resp = $this->master_model->getRecords('table_gen_history ',array("id"=>1),'table_gen_history.id,table_gen_history.description',array("table_gen_history.id"=>"asc")); 


		    for($i=0;$i<count($resp);$i++){
		    	if($resp[$i]['img_name']!=""){
		    			$resp[$i]['img_name'] = base_url().'uploads/'.$resp[$i]['img_name'];
		    	}
		    }

	
		    $output  = array(
		        					'data' => $resp,
		        					'status'=>$response['status'],
		        					'message'=>'success');
	    			json_output($response['status'],$output);    	
		}


    }

    public function areas(){

    	$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}else{
			$page=  $this->input->get("page");
					$limit=  $this->input->get("limit");
					$start_from = ($page-1) * $limit;  
					$response['status'] = 200;
		        	//$resp = $this->my_model->event_listing($page,$start_from,$limit);
		        	$resp = $this->master_model->getRecords('table_area ',array(),'table_area.id,table_area.area_name,table_area.img_name',array("table_area.area_name"=>"asc")); 


		    for($i=0;$i<count($resp);$i++){
		    	if($resp[$i]['img_name']!=""){
		    			$resp[$i]['img_name'] = base_url().'uploads/'.$resp[$i]['img_name'];
		    	}
		    }

	
		    $output  = array(
		        					'data' => $resp,
		        					'status'=>$response['status'],
		        					'message'=>'success');
	    			json_output($response['status'],$output);    	
		}
    
    }

    public function family($id){

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			$response['status'] = 200;
		        	//$resp = $this->my_model->event_listing($page,$start_from,$limit);
		     $resp = $this->master_model->getRecords('table_generation as tg',array("tg.area_id"=>$id),'tg.id,tg.gen_name,tg.img_name',array("tg.gen_name"=>"asc")); 


		    for($i=0;$i<count($resp);$i++){
		    	if($resp[$i]['img_name']!=""){
		    			$resp[$i]['img_name'] = base_url().'uploads/'.$resp[$i]['img_name'];
		    	}
		    }

			if(empty($resp)){
				$response['status'] = 400;
				$output  = array(
		        					'data' => "No Data",
		        					'status'=>$response['status'],
		        					'message'=>'success');
			}else{
				$output  = array(
		        					'data' => $resp,
		        					'status'=>$response['status'],
		        					'message'=>'success');	
			}
		    
	    			json_output($response['status'],$output);
		}
	
    }
    

}
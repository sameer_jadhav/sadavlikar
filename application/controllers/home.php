<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        /*
        $check_auth_client = $this->MyModel->check_auth_client();
		if($check_auth_client != true){
			die($this->output->get_output());
		}
		*/
    }

	public function index()
	{

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			
			//$check_auth_client = $this->MyModel->check_auth_client();
			//if($check_auth_client == true){
		        //$response = $this->MyModel->auth();
		        ///if($response['status'] == 200){
					$response['status'] = 200;
		        	$resp = $this->my_model->spaces_all_data();
		        	//print_r($resp1);
		        	foreach ($resp as $key => $value) {
		        		//
		        		$value->project_banner_img = base_url().'uploads/'.$value->project_banner_img;
		        		$value->project_mobile_img = base_url().'uploads/'.$value->project_mobile_img;

		        	}
		        	
		        	$banner_resp = $this->my_model->banner_all_data();
		        	foreach ($banner_resp as $key => $value) {
		        		//
		        		$value->banner_large_img = base_url().'uploads/'.$value->banner_large_img;
		        		$value->banner_mobile_img = base_url().'uploads/'.$value->banner_mobile_img;

		        	}

		        	$our_spaces_content = $this->my_model->get_ourspaces_content();

		        	$testimonial_resp = $this->my_model->testimonial_all_data();
		        	$output  = array(
		        					'data' =>  array('spaces' => $resp,
		        									 'banner' => $banner_resp,
		        									 'testimonial'=>$testimonial_resp,
		        									 'our_spaces'=>$our_spaces_content ),
		        					'status'=>$response['status'],
		        					'message'=>'success');
	    			json_output($response['status'],$output);
		        }
		
		
	}


	public function banner()
	{

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			
			//$check_auth_client = $this->MyModel->check_auth_client();
			//if($check_auth_client == true){
		        //$response = $this->MyModel->auth();
		        ///if($response['status'] == 200){
					$response['status'] = 200;
		        	$resp = $this->master_model->getRecords('table_banner ',array(),'table_banner.id,table_banner.img_name',array("table_banner.id"=>"asc")); 
		        	//$banner_resp = $this->my_model->banner_all_data();.
		        	 for($i=0;$i<count($resp);$i++){
				    	if($resp[$i]['img_name']!=""){
				    			$resp[$i]['img_name'] = base_url().'uploads/'.$resp[$i]['img_name'];
				    	}
				    }
		        	$output  = array(
		        					'data' =>  $resp,
		        					'status'=>$response['status'],
		        					'message'=>'success');
	    			json_output($response['status'],$output);
		        }
		
		
	}

	public function spaces()
	{

		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			
			//$check_auth_client = $this->MyModel->check_auth_client();
			//if($check_auth_client == true){
		        //$response = $this->MyModel->auth();
		        ///if($response['status'] == 200){
					$response['status'] = 200;
		        	$resp = $this->my_model->spaces_limited_data();
		        	//$banner_resp = $this->my_model->banner_all_data();
		        	$output  = array(
		        					'data' =>  $resp,
		        					'status'=>$response['status'],
		        					'message'=>'success');
	    			json_output($response['status'],$output);
		        }
		
		
	}

	public function detail($id)
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->MyModel->check_auth_client();
			if($check_auth_client == true){
		        $response = $this->MyModel->auth();
		        if($response['status'] == 200){
		        	$resp = $this->MyModel->book_detail_data($id);
					json_output($response['status'],$resp);
		        }
			}
		}
	}

	public function create()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->MyModel->check_auth_client();
			if($check_auth_client == true){
		        $response = $this->MyModel->auth();
		        $respStatus = $response['status'];
		        if($response['status'] == 200){
					$params = json_decode(file_get_contents('php://input'), TRUE);
					if ($params['title'] == "" || $params['author'] == "") {
						$respStatus = 400;
						$resp = array('status' => 400,'message' =>  'Title & Author can\'t empty');
					} else {
		        		$resp = $this->MyModel->book_create_data($params);
					}
					json_output($respStatusa,$resp);
		        }
			}
		}
	}

	public function update($id)
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'PUT' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->MyModel->check_auth_client();
			if($check_auth_client == true){
		        $response = $this->MyModel->auth();
		        $respStatus = $response['status'];
		        if($response['status'] == 200){
					$params = json_decode(file_get_contents('php://input'), TRUE);
					$params['updated_at'] = date('Y-m-d H:i:s');
					if ($params['title'] == "" || $params['author'] == "") {
						$respStatus = 400;
						$resp = array('status' => 400,'message' =>  'Title & Author can\'t empty');
					} else {
		        		$resp = $this->MyModel->book_update_data($id,$params);
					}
					json_output($respStatus,$resp);
		        }
			}
		}
	}

	public function delete($id)
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'DELETE' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->MyModel->check_auth_client();
			if($check_auth_client == true){
		        $response = $this->MyModel->auth();
		        if($response['status'] == 200){
		        	$resp = $this->MyModel->book_delete_data($id);
					json_output($response['status'],$resp);
		        }
			}
		}
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mandal extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        /*
        $check_auth_client = $this->MyModel->check_auth_client();
		if($check_auth_client != true){
			die($this->output->get_output());
		}
		*/
    }

    public function area(){
    	$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}else{
			$page=  $this->input->get("page");
					$limit=  $this->input->get("limit");
					$start_from = ($page-1) * $limit;  
					$response['status'] = 200;
		        	//$resp = $this->my_model->event_listing($page,$start_from,$limit);
		        	$resp = $this->master_model->getRecords('table_area_master as tam left join table_org as tor on tor.loc_id=tam.id ',array(),'tam.id,tam.name,tor.id as org_id,tam.type as org_flag',array("tam.sort_order"=>"asc")); 


		    for($i=0;$i<count($resp);$i++){
		    	if($resp[$i]['img_name']!=""){
		    			$resp[$i]['img_name'] = base_url().'uploads/'.$resp[$i]['img_name'];
		    	}
		    }

	
		    $output  = array(
		        					'data' => $resp,
		        					'status'=>$response['status'],
		        					'message'=>'success');
	    			json_output($response['status'],$output);    	
		}
    }

    public function history($id){
    	$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}else{
			$page=  $this->input->get("page");
					$limit=  $this->input->get("limit");
					$start_from = ($page-1) * $limit;  
					$response['status'] = 200;
		        	//$resp = $this->my_model->event_listing($page,$start_from,$limit);
		        	$resp = $this->master_model->getRecords('table_org ',array("table_org.id"=>$id),'table_org.id,table_org.title,table_org.description',array("table_org.title"=>"asc")); 


		   

	
		    $output  = array(
		        					'data' => $resp,
		        					'status'=>$response['status'],
		        					'message'=>'success');
	    			json_output($response['status'],$output);    	
		}
    }

    public function member_info($id){
    	$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}else{
			$page=  $this->input->get("page");
					$limit=  $this->input->get("limit");
					$start_from = ($page-1) * $limit;  
					$response['status'] = 200;
					if($id == 5){
						$resp =  $this->master_model->getRecords('table_mandal_member ',array(),'table_mandal_member.id,table_mandal_member.name,table_mandal_member.contact,table_mandal_member.address',array("table_mandal_member.name"=>"asc"),"","","table_mandal_member.name"); 

					}else{
						$resp =  $this->master_model->getRecords('table_mandal_member ',array("table_mandal_member.org_id"=>$id),'table_mandal_member.id,table_mandal_member.name,table_mandal_member.contact,table_mandal_member.address',array("table_mandal_member.name"=>"asc")); 

					}
		        	//$resp = $this->my_model->event_listing($page,$start_from,$limit);
		        	//$resp = $this->master_model->getRecords('table_org ',array("table_org.id"=>$id),'table_org.id,table_org.member_info',array("table_org.title"=>"asc")); 
					
		    

	
		    $output  = array(
		        					'data' => $resp,
		        					'status'=>$response['status'],
		        					'message'=>'success');
	    			json_output($response['status'],$output);    	
		}
    }


     public function president($id){
    	$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}else{
			$page=  $this->input->get("page");
					$limit=  $this->input->get("limit");
					$start_from = ($page-1) * $limit;  
					$response['status'] = 200;
		        	//$resp = $this->my_model->event_listing($page,$start_from,$limit);
		        	$resp = $this->master_model->getRecords('table_member_master as tmm',array("tmm.master_type"=>"presi","master_id"=>$id),'tmm.*',array("tmm.sort_order"=>"asc")); 
		        	 for($i=0;$i<count($resp);$i++){
				    	if($resp[$i]['img_name']!=""){
				    			$resp[$i]['img_name'] = base_url().'uploads/'.$resp[$i]['img_name'];
				    	}
				    }

		    

	
		    $output  = array(
		        					'data' => $resp,
		        					'status'=>$response['status'],
		        					'message'=>'success');
	    			json_output($response['status'],$output);    	
		}
    }

    public function member($id){
    	$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}else{
			$page=  $this->input->get("page");
					$limit=  $this->input->get("limit");
					$start_from = ($page-1) * $limit;  
					$response['status'] = 200;
		        	//$resp = $this->my_model->event_listing($page,$start_from,$limit);
		        	$resp = $this->master_model->getRecords('table_member_master as tmm',array("tmm.master_type"=>"member","master_id"=>$id),'tmm.*',array("tmm.sort_order"=>"asc")); 
		        	 for($i=0;$i<count($resp);$i++){
				    	if($resp[$i]['img_name']!=""){
				    			$resp[$i]['img_name'] = base_url().'uploads/'.$resp[$i]['img_name'];
				    	}
				    }

		    

	
		    $output  = array(
		        					'data' => $resp,
		        					'status'=>$response['status'],
		        					'message'=>'success');
	    			json_output($response['status'],$output);    	
		}
    }

    




    public function index()
    {
    	$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}else{
			$page=  $this->input->get("page");
					$limit=  $this->input->get("limit");
					$start_from = ($page-1) * $limit;  
					$response['status'] = 200;
		        	//$resp = $this->my_model->event_listing($page,$start_from,$limit);
		        	$resp = $this->master_model->getRecords('table_org ',array(),'table_org.id,table_org.title,table_org.img_name,table_org.description',array("table_org.id"=>"asc")); 


		    for($i=0;$i<count($resp);$i++){
		    	if($resp[$i]['img_name']!=""){
		    			$resp[$i]['img_name'] = base_url().'uploads/'.$resp[$i]['img_name'];
		    	}
		    }

	
		    $output  = array(
		        					'data' => $resp,
		        					'status'=>$response['status'],
		        					'message'=>'success');
	    			json_output($response['status'],$output);    	
		}
    }


    

}
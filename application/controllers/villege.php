<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Villege extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        /*
        $check_auth_client = $this->MyModel->check_auth_client();
		if($check_auth_client != true){
			die($this->output->get_output());
		}
		*/
    }

    public function index()
    {
    	$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}else{
			$page=  $this->input->get("page");
					$limit=  $this->input->get("limit");
					$start_from = ($page-1) * $limit;  
					$response['status'] = 200;
		        	//$resp = $this->my_model->event_listing($page,$start_from,$limit);
		        	$resp = $this->master_model->getRecords('table_villege ',array(),'table_villege.id,table_villege.title,table_villege.img_name,table_villege.description',array("table_villege.id"=>"asc")); 


		    for($i=0;$i<count($resp);$i++){
		    	if($resp[$i]['img_name']!=""){
		    			$resp[$i]['img_name'] = base_url().'uploads/'.$resp[$i]['img_name'];
		    	}
		    }

	
		    $output  = array(
		        					'data' => $resp,
		        					'status'=>$response['status'],
		        					'message'=>'success');
	    			json_output($response['status'],$output);    	
		}
    }


    public function history()
    {
    	$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}else{
			$page=  $this->input->get("page");
					$limit=  $this->input->get("limit");
					$start_from = ($page-1) * $limit;  
					$response['status'] = 200;
		        	//$resp = $this->my_model->event_listing($page,$start_from,$limit);
		        	$resp = $this->master_model->getRecords('table_gen_history ',array("id"=>2),'table_gen_history.id,table_gen_history.description',array("table_gen_history.id"=>"asc")); 


		    for($i=0;$i<count($resp);$i++){
		    	if($resp[$i]['img_name']!=""){
		    			$resp[$i]['img_name'] = base_url().'uploads/'.$resp[$i]['img_name'];
		    	}
		    }

	
		    $output  = array(
		        					'data' => $resp,
		        					'status'=>$response['status'],
		        					'message'=>'success');
	    			json_output($response['status'],$output);    	
		}
    }

    public function sites()
    {
    	$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}else{
			$page=  $this->input->get("page");
					$limit=  $this->input->get("limit");
					$start_from = ($page-1) * $limit;  
					$response['status'] = 200;
		        	//$resp = $this->my_model->event_listing($page,$start_from,$limit);
		        	$resp = $this->master_model->getRecords('table_villege ',array("cat_name"=>"sites"),'table_villege.id,table_villege.title,table_villege.img_name,table_villege.description',array("table_villege.id"=>"asc")); 


		    for($i=0;$i<count($resp);$i++){
		    	if($resp[$i]['img_name']!=""){
		    			$resp[$i]['img_name'] = base_url().'uploads/'.$resp[$i]['img_name'];
		    	}
		    }

	
		    $output  = array(
		        					'data' => $resp,
		        					'status'=>$response['status'],
		        					'message'=>'success');
	    			json_output($response['status'],$output);    	
		}
    }

    public function school()
    {
    	$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}else{
			$page=  $this->input->get("page");
					$limit=  $this->input->get("limit");
					$start_from = ($page-1) * $limit;  
					$response['status'] = 200;
		        	//$resp = $this->my_model->event_listing($page,$start_from,$limit);
		        	$resp = $this->master_model->getRecords('table_villege ',array("cat_name"=>"school"),'table_villege.id,table_villege.title,table_villege.img_name,table_villege.description',array("table_villege.id"=>"asc")); 


		    for($i=0;$i<count($resp);$i++){
		    	if($resp[$i]['img_name']!=""){
		    			$resp[$i]['img_name'] = base_url().'uploads/'.$resp[$i]['img_name'];
		    	}
		    }

	
		    $output  = array(
		        					'data' => $resp,
		        					'status'=>$response['status'],
		        					'message'=>'success');
	    			json_output($response['status'],$output);    	
		}
    }

    public function festival()
    {
    	$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}else{
			$page=  $this->input->get("page");
					$limit=  $this->input->get("limit");
					$start_from = ($page-1) * $limit;  
					$response['status'] = 200;
		        	//$resp = $this->my_model->event_listing($page,$start_from,$limit);
		        	$resp = $this->master_model->getRecords('table_villege ',array("cat_name"=>"festival"),'table_villege.id,table_villege.title,table_villege.img_name,table_villege.description',array("table_villege.id"=>"asc")); 


		    for($i=0;$i<count($resp);$i++){
		    	if($resp[$i]['img_name']!=""){
		    			$resp[$i]['img_name'] = base_url().'uploads/'.$resp[$i]['img_name'];
		    	}
		    }

	
		    $output  = array(
		        					'data' => $resp,
		        					'status'=>$response['status'],
		        					'message'=>'success');
	    			json_output($response['status'],$output);    	
		}
    }

    public function temple()
    {
    	$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}else{
			$page=  $this->input->get("page");
					$limit=  $this->input->get("limit");
					$start_from = ($page-1) * $limit;  
					$response['status'] = 200;
		        	//$resp = $this->my_model->event_listing($page,$start_from,$limit);
		        	$resp = $this->master_model->getRecords('table_villege ',array("cat_name"=>"temple"),'table_villege.id,table_villege.title,table_villege.img_name,table_villege.description',array("table_villege.id"=>"asc")); 


		    for($i=0;$i<count($resp);$i++){
		    	if($resp[$i]['img_name']!=""){
		    			$resp[$i]['img_name'] = base_url().'uploads/'.$resp[$i]['img_name'];
		    	}
		    }

	
		    $output  = array(
		        					'data' => $resp,
		        					'status'=>$response['status'],
		        					'message'=>'success');
	    			json_output($response['status'],$output);    	
		}
    }




    

/*    public function detail($id)
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
					$response['status'] = 200;
		       
		        	$resp = $this->my_model->event_detail($id);

		        	$resp_media = $this->my_model->event_media($id);
		        	
		        	foreach ($resp as $key => $value) {
		        		//
		        		if($value->file_name!=""){
		        			$value->file_name = base_url().'uploads/'.$value->file_name;	
		        		}
		        		

		        		
		        	
		        	}
		        	foreach ($resp_media as $key => $value) {
		        		if($value->file_name!=""){
		        			$value->file_name = base_url().'uploads/'.$value->file_name;
		        		}
		        	}
		        	$resp[0]->media_img = $resp_media;
		        	//print_r($resp[0]);
		        			
		        	$output  = array(
		        					'data' => $resp,
		        					'status'=>$response['status'],
		        					'message'=>'success');
					json_output($response['status'],$output);
		        
			
		}
	}*/

}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Media extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();
	   $this->load->library('upload');  
	   //$this->load->model('email_sending');	
	   
	}

	public function listMedia()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='The A | List Media';
	  //$data['middle_content']='list_artist';
	  $result=$this->master_model->getRecords('table_media',array('category'=>'press-coverage'),'table_media.*'); 
	  $data['media_data'] = $result;
	 
	  $this->load->view('admin/list_media',$data);
	}


	public function listPhoto()
	{
	  $data['success']=$data['error']='';	
	  $data['pagetitle']='The A | List Image Gallery';
	  //$data['middle_content']='list_artist';
	  $result=$this->master_model->getRecords('table_media',array('category'=>'photo-gallery'),'table_media.*'); 
	  $data['media_data'] = $result;
	 
	  $this->load->view('admin/list_photo',$data);	
	}


	public function listMediaupdate()
	{
	  $data['success']=$data['error']='';	
	  $data['pagetitle']='The A | List Media Updates';
	  //$data['middle_content']='list_artist';
	  $result=$this->master_model->getRecords('table_media',array('category'=>'media-updates'),'table_media.*'); 
	  $data['media_data'] = $result;
	 
	  $this->load->view('admin/list_media_updates',$data);	
	}

	public function addMedia()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='The A | Add Media Listing';
		 if(isset($_POST['btn_submit']))
		 {


			$this->form_validation->set_rules('media_name','','required|xss_clean');
			$this->form_validation->set_rules('media_category','','required|xss_clean');
			$this->form_validation->set_rules('media_desc','','required|xss_clean');
			//$this->form_validation->set_rules('quote_by','','required|xss_clean');
			//$this->form_validation->set_rules('image_quote','','required|xss_clean');
			
			
			if($this->form_validation->run())
			{
				
				$media_name=$this->input->post('media_name',true);
				$media_category=$this->input->post('media_category',true);
				$newspaper=$this->input->post('newspaper',true);
				$publish_date =$this->input->post('publish_date',true);
				$media_desc =$this->input->post('media_desc',true);
				$publisher = $this->input->post('publisher',true);
				$short_desc = $this->input->post('short_desc',true);
				//print_r($_FILES);exit;

				$input_array = array(
									'title' =>  $media_name,
									'description' =>  $media_desc,
									'short_desc' =>  $short_desc,
									'newspaper' =>  $newspaper,
									'publisher'=>$publisher,
									'publish_date'=>$publish_date,
									'category'=>$media_category
									);
				//print_r($_FILES);
							if($user_info=$this->master_model->insertRecord('table_media',$input_array))
							{ 
								$media_id = $this->db->insert_id();
								
								//echo $space_id;exit;
								//print_r($_FILES['project_icon_img']);exit;
								//$files = $_FILES;
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				//echo "<pre>";
			    				//print_r($_FILES['project_icon_img']['name']);exit;
			    				//print_r($_FILES['media_banner_img']['name']);exit;
								
								if($_FILES['media_banner_img']['name']!='')
								{
									if($this->upload->do_upload('media_banner_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('banner_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_media',$input_array,array('id'=>$media_id));
										//echo $this->db->last_query();exit();
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}


								if($_FILES['press_banner_desktop_img']['name']!='')
								{
									if($this->upload->do_upload('press_banner_desktop_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('banner_desktop_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_media',$input_array,array('id'=>$media_id));
										//echo $this->db->last_query();exit();
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								if($_FILES['press_banner_mobile_img']['name']!='')
								{
									if($this->upload->do_upload('press_banner_mobile_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('banner_mobile_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_media',$input_array,array('id'=>$media_id));
										//echo $this->db->last_query();exit();
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								$config_pdf=array('upload_path'=>'uploads/',
									          'allowed_types'=>'pdf',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config_pdf);

								if($_FILES['media_pdf']['name']!='')
								{
									if($this->upload->do_upload('media_pdf'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('pdf_file_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_media',$input_array,array('id'=>$media_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}



								$this->session->set_flashdata('success','Media listing added Successfully');			
								redirect(base_url().'superadmin/media/listMedia/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_media',$data);
	}



	public function addMediaupdate()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='The A | Add Media Update';
		 if(isset($_POST['btn_submit']))
		 {


			$this->form_validation->set_rules('media_name','','required|xss_clean');
			$this->form_validation->set_rules('media_category','','required|xss_clean');
			$this->form_validation->set_rules('media_desc','','required|xss_clean');
			//$this->form_validation->set_rules('quote_by','','required|xss_clean');
			//$this->form_validation->set_rules('image_quote','','required|xss_clean');
			
			
			if($this->form_validation->run())
			{
				
				$media_name=$this->input->post('media_name',true);
				$media_category=$this->input->post('media_category',true);
				$newspaper=$this->input->post('newspaper',true);
				$publish_date =$this->input->post('publish_date',true);
				$media_desc =$this->input->post('media_desc',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									'title' =>  $media_name,
									'description' =>  $media_desc,
									'newspaper' =>  $newspaper,
									'publish_date'=>$publish_date,
									'category'=>$media_category
									);
				//print_r($_FILES);
							if($user_info=$this->master_model->insertRecord('table_media',$input_array))
							{ 
								$media_id = $this->db->insert_id();
								
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				//echo "<pre>";
			    				//print_r($_FILES['project_icon_img']['name']);exit;
			    				//print_r($_FILES['media_banner_img']['name']);exit;
								
								if($_FILES['media_banner_img']['name']!='')
								{
									if($this->upload->do_upload('media_banner_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('banner_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_media',$input_array,array('id'=>$media_id));
										//echo $this->db->last_query();exit();
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}


								$config_pdf=array('upload_path'=>'uploads/',
									          'allowed_types'=>'pdf',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config_pdf);

								if($_FILES['media_pdf']['name']!='')
								{
									if($this->upload->do_upload('media_pdf'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('pdf_file_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_media',$input_array,array('id'=>$media_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}



								$this->session->set_flashdata('success','Media updates added Successfully');			
								redirect(base_url().'superadmin/media/listMediaupdate/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_media_updates',$data);
	}
	


	public function addPhoto()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='The A | Add Photo Gallery';
		 if(isset($_POST['btn_submit']))
		 {


			$this->form_validation->set_rules('media_name','','required|xss_clean');
			$this->form_validation->set_rules('media_category','','required|xss_clean');
			//$this->form_validation->set_rules('media_desc','','required|xss_clean');
			//$this->form_validation->set_rules('quote_by','','required|xss_clean');
			//$this->form_validation->set_rules('image_quote','','required|xss_clean');
			
			
			if($this->form_validation->run())
			{
				
				$media_name=$this->input->post('media_name',true);
				$media_category=$this->input->post('media_category',true);
				
				
				//print_r($_FILES);exit;

				$input_array = array(
									'title' =>  $media_name,
									'category'=>$media_category
									);
				//print_r($_FILES);
							if($user_info=$this->master_model->insertRecord('table_media',$input_array))
							{ 
								$media_id = $this->db->insert_id();
								

								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				//echo "<pre>";
			    				//print_r($_FILES['project_icon_img']['name']);exit;
			    				//print_r($_FILES['media_banner_img']['name']);exit;
								
								if($_FILES['media_banner_img']['name']!='')
								{
									if($this->upload->do_upload('media_banner_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('banner_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_media',$input_array,array('id'=>$media_id));
										//echo $this->db->last_query();exit();
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}



								$config_pdf=array('upload_path'=>'uploads/',
									          'allowed_types'=>'pdf|jpg|jpeg|gif|png|tif',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config_pdf);

								if($_FILES['media_pdf']['name']!='')
								{
									if($this->upload->do_upload('media_pdf'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('pdf_file_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_media',$input_array,array('id'=>$media_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}


								if($_FILES['press_jpg_img']['name']!='')
								{
									if($this->upload->do_upload('press_jpg_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('banner_desktop_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_media',$input_array,array('id'=>$media_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								if($_FILES['press_tiff_img']['name']!='')
								{
									if($this->upload->do_upload('press_tiff_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('banner_mobile_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_media',$input_array,array('id'=>$media_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}



								$this->session->set_flashdata('success','Photo gallery added Successfully');			
								redirect(base_url().'superadmin/media/listPhoto/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_photo',$data);
	}


	public function editMedia()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='The A | Edit Media Listing';
	  $front_id=base64_decode($this->uri->segment('4'));
	  $media_data =   $this->master_model->getRecords('table_media',array('id'=>$front_id),'table_media.*');
	  $data['media_data'] = $media_data;
		 if(isset($_POST['btn_submit']))
		 {


			$this->form_validation->set_rules('media_name','','required|xss_clean');
			$this->form_validation->set_rules('media_category','','required|xss_clean');
			$this->form_validation->set_rules('media_desc','','required|xss_clean');
			//$this->form_validation->set_rules('quote_by','','required|xss_clean');
			//$this->form_validation->set_rules('image_quote','','required|xss_clean');
			
			
			if($this->form_validation->run())
			{
				$media_id=$this->input->post('media_id',true);
				$media_name=$this->input->post('media_name',true);
				$media_category=$this->input->post('media_category',true);
				$newspaper=$this->input->post('newspaper',true);
				$publish_date =$this->input->post('publish_date',true);
				$media_desc =$this->input->post('media_desc',true);
				$publisher = $this->input->post('publisher',true);
				$short_desc = $this->input->post('short_desc',true);
				
				//print_r($publisher);exit;

				$input_array = array(
									'title' =>  $media_name,
									'description' =>  $media_desc,
									'newspaper' =>  $newspaper,
									'short_desc'=>$short_desc,
									'publish_date'=>$publish_date,
									'category'=>$media_category,
									'publisher' => $publisher
									);
				//print_r($_FILES);
							if($user_info=$this->master_model->updateRecord('table_media',$input_array,array('id'=>$media_id)))
							{ 
								//$media_id = $this->db->insert_id();
								
								//echo $space_id;exit;
								//print_r($_FILES['project_icon_img']);exit;
								//$files = $_FILES;
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				//echo "<pre>";
			    				//print_r($_FILES['project_icon_img']['name']);exit;
			    				//print_r($_FILES['media_banner_img']['name']);exit;
								
								if($_FILES['media_banner_img']['name']!='')
								{
									if($this->upload->do_upload('media_banner_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('banner_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_media',$input_array,array('id'=>$media_id));
										//echo $this->db->last_query();exit();
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}


								if($_FILES['press_banner_desktop_img']['name']!='')
								{
									if($this->upload->do_upload('press_banner_desktop_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('banner_desktop_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_media',$input_array,array('id'=>$media_id));
										//echo $this->db->last_query();exit();
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								if($_FILES['press_banner_mobile_img']['name']!='')
								{
									if($this->upload->do_upload('press_banner_mobile_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('banner_mobile_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_media',$input_array,array('id'=>$media_id));
										//echo $this->db->last_query();exit();
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								$config_pdf=array('upload_path'=>'uploads/',
									          'allowed_types'=>'pdf',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config_pdf);

								if($_FILES['media_pdf']['name']!='')
								{
									if($this->upload->do_upload('media_pdf'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('pdf_file_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_media',$input_array,array('id'=>$media_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}



								$this->session->set_flashdata('success','Media listing edited Successfully');			
								redirect(base_url().'superadmin/media/listMedia/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/edit_media',$data);
	}



	public function editMediaupdate()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='The A | Edit Media Update Listing';
	  $front_id=base64_decode($this->uri->segment('4'));
	  $media_data =   $this->master_model->getRecords('table_media',array('id'=>$front_id),'table_media.*');
	  $data['media_data'] = $media_data;
		 if(isset($_POST['btn_submit']))
		 {


			$this->form_validation->set_rules('media_name','','required|xss_clean');
			$this->form_validation->set_rules('media_category','','required|xss_clean');
			$this->form_validation->set_rules('media_desc','','required|xss_clean');
			//$this->form_validation->set_rules('quote_by','','required|xss_clean');
			//$this->form_validation->set_rules('image_quote','','required|xss_clean');
			
			
			if($this->form_validation->run())
			{
				$media_id=$this->input->post('media_id',true);
				$media_name=$this->input->post('media_name',true);
				$media_category=$this->input->post('media_category',true);
				$newspaper=$this->input->post('newspaper',true);
				$publish_date =$this->input->post('publish_date',true);
				$media_desc =$this->input->post('media_desc',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									'title' =>  $media_name,
									'description' =>  $media_desc,
									'newspaper' =>  $newspaper,
									'publish_date'=>$publish_date,
									'category'=>$media_category
									);
				//print_r($_FILES);
							if($user_info=$this->master_model->updateRecord('table_media',$input_array,array('id'=>$media_id)))
							{ 
								//$media_id = $this->db->insert_id();
								
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				//echo "<pre>";
			    				//print_r($_FILES['project_icon_img']['name']);exit;
			    				//print_r($_FILES['media_banner_img']['name']);exit;
								
								if($_FILES['media_banner_img']['name']!='')
								{
									if($this->upload->do_upload('media_banner_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('banner_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_media',$input_array,array('id'=>$media_id));
										//echo $this->db->last_query();exit();
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								

								$config_pdf=array('upload_path'=>'uploads/',
									          'allowed_types'=>'pdf',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config_pdf);

								if($_FILES['media_pdf']['name']!='')
								{
									if($this->upload->do_upload('media_pdf'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('pdf_file_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_media',$input_array,array('id'=>$media_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}



								$this->session->set_flashdata('success','Media updates listing edited Successfully');			
								redirect(base_url().'superadmin/media/listMediaupdate/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/edit_media_updates',$data);
	}
	

	public function editPhoto()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='The A | Edit Photo Gallery';
	  $front_id=base64_decode($this->uri->segment('4'));
	  $media_data =   $this->master_model->getRecords('table_media',array('id'=>$front_id),'table_media.*');
	  $data['media_data'] = $media_data;
		 if(isset($_POST['btn_submit']))
		 {


			$this->form_validation->set_rules('media_name','','required|xss_clean');
			$this->form_validation->set_rules('media_category','','required|xss_clean');
			//$this->form_validation->set_rules('media_desc','','required|xss_clean');
			//$this->form_validation->set_rules('quote_by','','required|xss_clean');
			//$this->form_validation->set_rules('image_quote','','required|xss_clean');
			
			
			if($this->form_validation->run())
			{
				$media_id=$this->input->post('media_id',true);
				$media_name=$this->input->post('media_name',true);
				$media_category=$this->input->post('media_category',true);

				
				//print_r($_FILES);exit;

				$input_array = array(
									'title' =>  $media_name,
									'category'=>$media_category
									);
				//print_r($_FILES);
							if($user_info=$this->master_model->updateRecord('table_media',$input_array,array('id'=>$media_id)))
							{ 
								//$media_id = $this->db->insert_id();
								
								//echo $space_id;exit;
								//print_r($_FILES['project_icon_img']);exit;
								//$files = $_FILES;
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				//echo "<pre>";
			    				//print_r($_FILES['project_icon_img']['name']);exit;
			    				//print_r($_FILES['media_banner_img']['name']);exit;
								
								if($_FILES['media_banner_img']['name']!='')
								{
									if($this->upload->do_upload('media_banner_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('banner_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_media',$input_array,array('id'=>$media_id));
										//echo $this->db->last_query();exit();
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}


								$config_pdf=array('upload_path'=>'uploads/',
									          'allowed_types'=>'pdf|jpg|jpeg|gif|png|tif',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config_pdf);

								if($_FILES['media_pdf']['name']!='')
								{
									if($this->upload->do_upload('media_pdf'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('pdf_file_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_media',$input_array,array('id'=>$media_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								if($_FILES['press_jpg_img']['name']!='')
								{
									if($this->upload->do_upload('press_jpg_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('banner_desktop_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_media',$input_array,array('id'=>$media_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								if($_FILES['press_tiff_img']['name']!='')
								{
									if($this->upload->do_upload('press_tiff_img'))
									{
										//echo 123;exit;
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('banner_mobile_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_media',$input_array,array('id'=>$media_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}



								$this->session->set_flashdata('success','Photo gallery  edited Successfully');			
								redirect(base_url().'superadmin/media/listPhoto/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/edit_photo',$data);
	}


	public function deleteMedia()
	{
		 $media_id=$this->input->post('media_id',true);
		 $media_id=base64_decode($media_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_media','id',$media_id))
				{ 
					$this->session->set_flashdata('success','Media Listing deleted successfully');
					//redirect(base_url().'superadmin/admin/listArtist');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		
	}


	

}
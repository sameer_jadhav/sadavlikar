<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Payment extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();
	   $this->load->library('upload');  
	   //$this->load->model('email_sending');	
	   
	}

	public function listPayment()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='सडवली संघटन | पेमेंट यादी ';
	  //$data['middle_content']='list_artist';
	  $result = $this->master_model->getRecords('table_payment',array(),'table_payment.*'); 
	 // $result=$this->master_model->getRecords('table_country',array(1=>"1"),'*'); 
	  $data['villege'] = $result;
	 //echo 123;exit;
	  $this->load->view('admin/list_payment',$data);
	}


	public function addPayment()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='सडवली संघटन | पेमेंट add करा ';
		 if(isset($_POST['btn_submit']))
		 {

			$this->form_validation->set_rules('title','','required|xss_clean');
			
			if($this->form_validation->run())
			{
				$title =$this->input->post('title',true);
				$url =$this->input->post('url',true);


				$input_array = array(
									
									'title'=>$title,
									'payment_url' => $url
									);

							if($user_info=$this->master_model->insertRecord('table_payment',$input_array))
							{ 

								$this->session->set_flashdata('success','Info added Successfully');			
								redirect(base_url().'superadmin/payment/listPayment/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_payment',$data);
	}


	public function editPayment()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='सडवली संघटन | पेमेंट edit करा ';
	  $front_id=base64_decode($this->uri->segment('4'));
	  
	  $villege_data =   $this->master_model->getRecords('table_payment',array('id'=>$front_id),'table_payment.*'); 
	  $data['villege'] = $villege_data;
		 if(isset($_POST['btn_submit']))
		 {

			$this->form_validation->set_rules('title','','required|xss_clean');
			$this->form_validation->set_rules('villege_id','','required|xss_clean');
			
			if($this->form_validation->run())
			{

				$title =$this->input->post('title',true);
				$url =$this->input->post('url',true);
				$villege_id =$this->input->post('villege_id',true);
		
				$input_array = array(
									
									'title'=>$title,
									'payment_url' => $url
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->updateRecord('table_payment',$input_array,array("id"=>$villege_id)))
							{ 

								$this->session->set_flashdata('success','Info added Successfully');			
								redirect(base_url().'superadmin/payment/listPayment/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/edit_payment',$data);
	}

	public function deletePayment()
	{
		 $payment_id=$this->input->post('payment_id',true);
		 $payment_id=base64_decode($payment_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_payment','id',$payment_id))
				{ 
					$this->session->set_flashdata('success','Payment Listing deleted successfully');
					//redirect(base_url().'superadmin/admin/listArtist');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		
	}

}
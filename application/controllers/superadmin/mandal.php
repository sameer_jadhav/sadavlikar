<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mandal extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();
	   $this->load->library('upload');  
	   //$this->load->model('email_sending');	
	   
	}

	public function listMandal()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='सडवली संघटन | मंडळे यादी ';
	  //$data['middle_content']='list_artist';
	   $result = $this->master_model->getRecords('table_org as tor join table_area_master as ta on ta.id=tor.loc_id',array(),'tor.*,ta.name as loc_name'); 
	 // $result=$this->master_model->getRecords('table_country',array(1=>"1"),'*'); 
	  $data['villege'] = $result;
	 //echo 123;exit;
	  $this->load->view('admin/list_mandal',$data);
	}

	public function listPresi()
	{

	  $master_id=base64_decode($this->uri->segment('4'));
	  $data['success']=$data['error']='';	
	  $data['pagetitle']='सडवली संघटन | मंडळे अध्यक्ष  ';
	  //$data['middle_content']='list_artist';
	 
	  $result = $this->master_model->getRecords('table_member_master as tmm',array("tmm.master_type"=>"presi","master_id"=>$master_id),'tmm.id,tmm.name,tmm.designation,tmm.contact,tmm.address,tmm.img_name,tmm.sort_order'); 
	 // $result=$this->master_model->getRecords('table_country',array(1=>"1"),'*'); 
	  $data['master_id'] = $master_id;
	  $data['villege'] = $result;
	 //echo 123;exit;
	  $this->load->view('admin/list_member',$data);
	}

	public function listMember()
	{

	  $data['success']=$data['error']='';	
	  $master_id=base64_decode($this->uri->segment('4'));
	  $data['pagetitle']='सडवली संघटन | मंडळे कमिटी मेंबर्स  ';
	  //$data['middle_content']='list_artist';
	   $result = $this->master_model->getRecords('table_member_master as tmm',array("tmm.master_type"=>"member","master_id"=>$master_id),'tmm.id,tmm.name,tmm.designation,tmm.contact,tmm.address,tmm.img_name,tmm.sort_order'); 
	 // $result=$this->master_model->getRecords('table_country',array(1=>"1"),'*'); 
	  $data['villege'] = $result;
	   $data['master_id'] = $master_id;
	 //echo 123;exit;
	  $this->load->view('admin/list_member',$data);
	}


	public function addMandal()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='सडवली संघटन | मंडळ add करा ';

	  $loc_data =   $this->master_model->getRecords('table_area_master',array(),'table_area_master.*'); 
	  //print_r($villege_data);exit;
	  $data['loc_data'] = $loc_data;

		 if(isset($_POST['btn_submit']))
		 {

			$this->form_validation->set_rules('title','','required|xss_clean');
			$this->form_validation->set_rules('location','','required|xss_clean');
			
			
			if($this->form_validation->run())
			{
				
				//$img_name=$this->input->post('img_name',true);
				$title =$this->input->post('title',true);
				$desc =$this->input->post('description',true);
				$loc_id = $this->input->post('location',true);
				$member_info = $this->input->post('member_info',true);
				//$logo_url =$this->input->post('logo_url',true);
				//$type =$this->input->post('loc_type',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									
									'title'=>$title,
									'description' => $desc,
									'member_info'=> $member_info,
									'loc_id'=>$loc_id
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->insertRecord('table_org',$input_array))
							{ 
								$org_id = $this->db->insert_id();
								
								
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				
								

								if($_FILES['img_name']['name']!='')
								{
									if($this->upload->do_upload('img_name'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_org',$input_array,array('id'=>$org_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								$this->session->set_flashdata('success','Info added Successfully');			
								redirect(base_url().'superadmin/mandal/listMandal/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_mandal',$data);
	}


	public function editMandal()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='माझे गाव | ';
	  $front_id=base64_decode($this->uri->segment('4'));
	  $loc_data =   $this->master_model->getRecords('table_area_master',array(),'table_area_master.*'); 
	  //print_r($villege_data);exit;
	  $data['loc_data'] = $loc_data;
	  $villege_data =   $this->master_model->getRecords('table_org',array('id'=>$front_id),'table_org.*'); 
	  //print_r($villege_data);exit;
	  $data['villege'] = $villege_data;
		 if(isset($_POST['btn_submit']))
		 {

			$this->form_validation->set_rules('title','','required|xss_clean');
			$this->form_validation->set_rules('villege_id','','required|xss_clean');
			$this->form_validation->set_rules('location','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				
				//$img_name=$this->input->post('img_name',true);
				$title =$this->input->post('title',true);
				$desc =$this->input->post('description',true);
				$villege_id =$this->input->post('villege_id',true);
				$loc_id = $this->input->post('location',true);
				$member_info = $this->input->post('member_info',true);
				//$logo_url =$this->input->post('logo_url',true);
				//$type =$this->input->post('loc_type',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									
									'title'=>$title,
									'description' => $desc,
									'member_info'=> $member_info,
									'loc_id'=>$loc_id
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->updateRecord('table_org',$input_array,array("id"=>$villege_id)))
							{ 
								
								
								
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				
								

								if($_FILES['img_name']['name']!='')
								{
									if($this->upload->do_upload('img_name'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_org',$input_array,array('id'=>$villege_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								$this->session->set_flashdata('success','Info added Successfully');			
								redirect(base_url().'superadmin/mandal/listMandal/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/edit_mandal',$data);
	}



	public function addPresi()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='सडवली संघटन | मंडळ अध्यक्ष add करा ';
	  $master_id=$this->uri->segment('4');
	  $data['master_id'] = $master_id;
	  //print_r($master_id);exit;
	  

		 if(isset($_POST['btn_submit']))
		 {

			$this->form_validation->set_rules('name','','required|xss_clean');
			$this->form_validation->set_rules('contact','','required|xss_clean');
			$this->form_validation->set_rules('designation','','required|xss_clean');
			$this->form_validation->set_rules('master_id','','required|xss_clean');
			
			
			if($this->form_validation->run())
			{
				
				//$img_name=$this->input->post('img_name',true);
				$name =$this->input->post('name',true);
				$contact =$this->input->post('contact',true);
				$designation = $this->input->post('designation',true);
				$address = $this->input->post('address',true);
				$master_id = $this->input->post('master_id',true);
				//$logo_url =$this->input->post('logo_url',true);
				//$type =$this->input->post('loc_type',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									
									'name'=>$name,
									'contact' => $contact,
									'designation'=> $designation,
									'address'=>$address,
									'master_id' => $master_id,
									'master_type' => "presi"
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->insertRecord('table_member_master',$input_array))
							{ 
								$org_id = $this->db->insert_id();
								
								
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				
								

								if($_FILES['img_name']['name']!='')
								{
									if($this->upload->do_upload('img_name'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_member_master',$input_array,array('id'=>$org_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								$this->session->set_flashdata('success','Info added Successfully');			
								redirect(base_url().'superadmin/mandal/listPresi/'.base64_encode($master_id));
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_member',$data);
	}


	public function editPresi()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='सडवली संघटन | मंडळ अध्यक्ष edit करा ';
	  $master_id=base64_decode($this->uri->segment('4'));
	  $data['master_id'] = $master_id;

	  $front_id=base64_decode($this->uri->segment('5'));
	  //$data['master_id'] = $master_id;
	  //print_r($master_id);exit;
	  $result = $this->master_model->getRecords('table_member_master as tmm',array("tmm.id"=>$front_id),'tmm.id,tmm.name,tmm.designation,tmm.contact,tmm.address,tmm.img_name'); 
	  $data['member'] = $result;
 		 if(isset($_POST['btn_submit']))
		 {

			$this->form_validation->set_rules('name','','required|xss_clean');
			$this->form_validation->set_rules('contact','','required|xss_clean');
			$this->form_validation->set_rules('designation','','required|xss_clean');
			$this->form_validation->set_rules('master_id','','required|xss_clean');
			$this->form_validation->set_rules('mm_id','','required|xss_clean');
			
			
			if($this->form_validation->run())
			{
				
				//$img_name=$this->input->post('img_name',true);
				$name =$this->input->post('name',true);
				$contact =$this->input->post('contact',true);
				$designation = $this->input->post('designation',true);
				$address = $this->input->post('address',true);
				$master_id = $this->input->post('master_id',true);
				$mm_id = $this->input->post('mm_id',true);
				//$logo_url =$this->input->post('logo_url',true);
				//$type =$this->input->post('loc_type',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									
									'name'=>$name,
									'contact' => $contact,
									'designation'=> $designation,
									'address'=>$address,
									'master_id' => $master_id,
									'master_type' => "presi"
									);
				//print_r($input_array);exit;
				
							if($user_info=$this->master_model->updateRecord('table_member_master',$input_array,array("id"=>$mm_id)))
							{ 
								
								
								
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				
								

								if($_FILES['img_name']['name']!='')
								{
									if($this->upload->do_upload('img_name'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_member_master',$input_array,array('id'=>$mm_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								$this->session->set_flashdata('success','Info added Successfully');			
								redirect(base_url().'superadmin/mandal/listPresi/'.base64_encode($master_id));
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/edit_member',$data);
	}


	public function addMember()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='सडवली संघटन | मंडळ Comitte Member add करा ';
	  $master_id=$this->uri->segment('4');
	  $data['master_id'] = $master_id;
	  //print_r($master_id);exit;
	  

		 if(isset($_POST['btn_submit']))
		 {

			$this->form_validation->set_rules('name','','required|xss_clean');
			$this->form_validation->set_rules('contact','','required|xss_clean');
			$this->form_validation->set_rules('designation','','required|xss_clean');
			$this->form_validation->set_rules('master_id','','required|xss_clean');
			
			
			if($this->form_validation->run())
			{
				
				//$img_name=$this->input->post('img_name',true);
				$name =$this->input->post('name',true);
				$contact =$this->input->post('contact',true);
				$designation = $this->input->post('designation',true);
				$address = $this->input->post('address',true);
				$master_id = $this->input->post('master_id',true);
				//$logo_url =$this->input->post('logo_url',true);
				//$type =$this->input->post('loc_type',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									
									'name'=>$name,
									'contact' => $contact,
									'designation'=> $designation,
									'address'=>$address,
									'master_id' => $master_id,
									'master_type' => "member"
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->insertRecord('table_member_master',$input_array))
							{ 
								$org_id = $this->db->insert_id();
								
								
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				
								

								if($_FILES['img_name']['name']!='')
								{
									if($this->upload->do_upload('img_name'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_member_master',$input_array,array('id'=>$org_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								$this->session->set_flashdata('success','Info added Successfully');			
								redirect(base_url().'superadmin/mandal/listMember/'.base64_encode($master_id));
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_member',$data);
	}


	public function editMember()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='सडवली संघटन | मंडळ अध्यक्ष edit करा ';
	  $param_total = count($this->uri->segment_array());
	  if($param_total==4){
	  	$master_id=0;
	  	$data['master_id'] = $master_id;

	  	$front_id=base64_decode($this->uri->segment('4'));
	  }else{
	  	$master_id=base64_decode($this->uri->segment('4'));
	  	$data['master_id'] = $master_id;

	  	$front_id=base64_decode($this->uri->segment('5'));
	  }
	  
	  //$data['master_id'] = $master_id;
	  //print_r($master_id);exit;
	  $result = $this->master_model->getRecords('table_member_master as tmm',array("tmm.id"=>$front_id),'tmm.id,tmm.name,tmm.designation,tmm.contact,tmm.address,tmm.img_name'); 
	  $data['member'] = $result;
 		 if(isset($_POST['btn_submit']))
		 {

			$this->form_validation->set_rules('name','','required|xss_clean');
			$this->form_validation->set_rules('contact','','required|xss_clean');
			$this->form_validation->set_rules('designation','','required|xss_clean');
			$this->form_validation->set_rules('master_id','','required|xss_clean');
			$this->form_validation->set_rules('mm_id','','required|xss_clean');
			
			
			if($this->form_validation->run())
			{
				
				//$img_name=$this->input->post('img_name',true);
				$name =$this->input->post('name',true);
				$contact =$this->input->post('contact',true);
				$designation = $this->input->post('designation',true);
				$address = $this->input->post('address',true);
				$master_id = $this->input->post('master_id',true);
				$mm_id = $this->input->post('mm_id',true);
				//$logo_url =$this->input->post('logo_url',true);
				//$type =$this->input->post('loc_type',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									
									'name'=>$name,
									'contact' => $contact,
									'designation'=> $designation,
									'address'=>$address,
									'master_id' => $master_id,
									'master_type' => "member"
									);
				//print_r($input_array);exit;
				
							if($user_info=$this->master_model->updateRecord('table_member_master',$input_array,array("id"=>$mm_id)))
							{ 
								
								
								
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				
								

								if($_FILES['img_name']['name']!='')
								{
									if($this->upload->do_upload('img_name'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_member_master',$input_array,array('id'=>$mm_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								$this->session->set_flashdata('success','Info added Successfully');			
								redirect(base_url().'superadmin/mandal/listMember/'.base64_encode($master_id));
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/edit_member',$data);
	}

	public function deleteMandal()
	{
		 $org_id=$this->input->post('org_id',true);
		 $org_id=base64_decode($org_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_org','id',$org_id))
				{ 
					$this->session->set_flashdata('success','Event Listing deleted successfully');
					//redirect(base_url().'superadmin/admin/listArtist');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		
	}


	public function deleteMember()
	{
		 $id=$this->input->post('id',true);
		 $id=base64_decode($id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_member_master','id',$id))
				{ 
					$this->session->set_flashdata('success','Member Listing deleted successfully');
					//redirect(base_url().'superadmin/admin/listArtist');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		
	}


	public function deletePresi()
	{
		 $id=$this->input->post('id',true);
		 $id=base64_decode($id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_member_master','id',$id))
				{ 
					$this->session->set_flashdata('success','President Listing deleted successfully');
					//redirect(base_url().'superadmin/admin/listArtist');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		
	}

}
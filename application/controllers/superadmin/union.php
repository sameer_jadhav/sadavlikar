<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Union extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();
	   $this->load->library('upload');  
	   //$this->load->model('email_sending');	
	   
	}

	public function info()
	{


	  $data['success']=$data['error']="";
	  $data['pagetitle']='सडवली संघटन | इतिहास  ';
	  $front_id=base64_decode($this->uri->segment('4'));
	  
	  $villege_data =   $this->master_model->getRecords('table_gen_history',array('id'=>3),'table_gen_history.*'); 
	  //print_r($villege_data);exit;
	  $data['villege'] = $villege_data;
		 if(isset($_POST['btn_submit']))
		 {

			$this->form_validation->set_rules('description','','required|xss_clean');
			//$this->form_validation->set_rules('villege_id','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				
				
				$desc =$this->input->post('description',true);
				
				

				$input_array = array(
									
									
									'description' => $desc
									);
				
							if($user_info=$this->master_model->updateRecord('table_gen_history',$input_array,array("id"=>3)))
							{ 
								
								$this->session->set_flashdata('success','Info updated Successfully');			
								redirect(base_url().'superadmin/union/info/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/union_history',$data);
	 
	}

	public function listProgram()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='सडवली संघटन | कार्यक्रम  ';
	  //$data['middle_content']='list_artist';
	  $result = $this->master_model->getRecords('table_program',array(),'table_program.*'); 
	 // $result=$this->master_model->getRecords('table_country',array(1=>"1"),'*'); 
	  $data['villege'] = $result;
	  
	  $this->load->view('admin/list_program',$data);
	}

	public function addProgram()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='सडवली संघटन | कार्यक्रम add करा ';
		 if(isset($_POST['btn_submit']))
		 {

			$this->form_validation->set_rules('title','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				
				//$img_name=$this->input->post('img_name',true);
				$title =$this->input->post('title',true);
				$desc =$this->input->post('description',true);
				//$logo_url =$this->input->post('logo_url',true);
				//$type =$this->input->post('loc_type',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									
									'title'=>$title,
									'description' => $desc
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->insertRecord('table_program',$input_array))
							{ 
								
								$this->session->set_flashdata('success','Info added Successfully');			
								redirect(base_url().'superadmin/union/listProgram/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_program',$data);
	}


	public function editProgram()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='सडवली संघटन | कार्यक्रम edit करा ';
	  $front_id=base64_decode($this->uri->segment('4'));
	  
	  $villege_data =   $this->master_model->getRecords('table_program',array('id'=>$front_id),'table_program.*'); 
	  $data['villege'] = $villege_data;
		 if(isset($_POST['btn_submit']))
		 {

			$this->form_validation->set_rules('title','','required|xss_clean');
			$this->form_validation->set_rules('villege_id','','required|xss_clean');
			
			if($this->form_validation->run())
			{

				$title =$this->input->post('title',true);
				$desc =$this->input->post('description',true);
				$villege_id =$this->input->post('villege_id',true);
		
				$input_array = array(
									
									'title'=>$title,
									'description' => $desc
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->updateRecord('table_program',$input_array,array("id"=>$villege_id)))
							{ 
								
								$this->session->set_flashdata('success','Info added Successfully');			
								redirect(base_url().'superadmin/union/listProgram/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/edit_program',$data);
	}

	public function deleteProgram()
	{
		 $noti_id=$this->input->post('noti_id',true);
		 $noti_id=base64_decode($noti_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_program','id',$noti_id))
				{ 
					$this->session->set_flashdata('success','Program Listing deleted successfully');
					//redirect(base_url().'superadmin/admin/listArtist');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();
	   $this->load->library('upload');  
	   //$this->load->model('email_sending');	
	   
	}

	public function login()
	{ 

	     $data['pagetitle']='सडवली संघटन | Login';
	     //$data['error'] = "";
		 if(isset($_POST['btn_login']))
		 {
			$this->form_validation->set_rules('username','','required|xss_clean');
			$this->form_validation->set_rules('password','','required|xss_clean');
			if($this->form_validation->run())
			{
				$username=$this->input->post('username',true);
				$password=$this->input->post('password',true);
				$input_array=array('username'=>$username,'password'=>md5($password));				
				$user_info=$this->master_model->getRecords('admin_user',$input_array);
				if(count($user_info)>0)
				{ 
				
					$mysqltime=date("H:i:s");
					$user_data=array('username'=>$user_info[0]['username'],
									 'admin_id'=>$user_info[0]['id'],
									 'timer'=>base64_encode($mysqltime));
					$this->session->set_userdata($user_data);
					redirect(base_url().'superadmin/admin/dashboard/');			
				}
				else
				{

					 $this->session->set_flashdata('error','Please enter valid username or password !');
					 redirect(base_url().'superadmin/admin/login'); 
				}
			}
		  }
	      $this->load->view('admin/login',$data);
	}
	
	public function forgotpassword()
	{
	  $data['pagetitle']='Lets learn India | Forgotpassword';
	  $data['error']=$data['success']='';	
	  if(isset($_POST['btn_recovery']))
	  {
	    $this->form_validation->set_rules('email','','required|valid_email');
		if($this->form_validation->run())
		{
		  $admin_email=$this->input->post('email',true);
		  $result=$this->master_model->getRecords('admin_login',array('admin_login.admin_email'=>$admin_email),'admin_login.*');
		  if(count($result)>0)
		  {
			 $whr=array('id'=>'1');
			 $info_mail=$this->master_model->getRecords('admin_login',$whr,'*');
			 $info_arr=array('from'=>$info_mail[0]['admin_email'],'to'=>$admin_email,'subject'=>'Password Recovery','view'=>'admin-forgot-password');
			 $other_info=array('name'=>$result[0]['admin_username'],'email_id'=>base64_encode($info_mail[0]['id']));
			 if($this->email_sending->sendmail($info_arr,$other_info))
			 {
				$change_password=array('password_status'=>'0');	
				$this->master_model->updateRecord('admin_login',$change_password,array('id'=>'1'));	 
				$this->session->set_flashdata('success','Mail send successfully.');
				redirect(base_url().'superadmin/admin/forgotpassword/');	
			 }
			 else
			 {
				$this->session->set_flashdata('error','While error for sending mail');
				redirect(base_url().'superadmin/admin/forgotpassword/');	
			 }
		  }
		  else
		  {
			 $this->session->set_flashdata('error','Your email was not found.');
			 redirect(base_url().'superadmin/admin/forgotpassword/'); 
		  }
		}
	  }
	  $this->load->view('admin/login',$data);
	}
	
	public function change_password()
	{
	  $data['pagetitle']='The A | Change Password';
	  $data['error']=$data['success']=$data['error_alreay']='';
	  
	  
		  if(isset($_POST['btn_password']))
		  {
			 $this->form_validation->set_rules('cur_password','','required');
			 $this->form_validation->set_rules('re_new_password','','required');
			 if($this->form_validation->run())
			 {
				$password=$this->input->post('cur_password',true);
				$confirm_password=$this->input->post('re_new_password',true);
				$result=$this->master_model->getRecords('admin_user as u',array('u.user_id'=>1,"u.user_pass"=>md5($password)),'u.*'); 
				if(!empty($result))
				{
					$update_password=array('user_pass'=>md5($confirm_password));
					if($this->master_model->updateRecord('admin_user',$update_password,array('user_id'=>'1')))
					{
						$this->session->set_flashdata('success','Password Change successfully');
					   // $data['success']='Password Change successfully'; 
						redirect(base_url().'superadmin/admin/change_password');
					}	
				}else{
					$this->session->set_flashdata('error','Current Password does not match');
					redirect(base_url().'superadmin/admin/change_password');
					//$data['error']='Current Password does not match';
				}	
				
			 }
		  }
	  
	  $this->load->view('admin/change_password',$data);
	}
	
	public function logout()
	{
	  $this->session->unset_userdata('admin_id');
	  $this->session->unset_userdata('username');
	   $this->session->unset_userdata('timer');
	  redirect(base_url().'superadmin/admin/login');
	}
	
	public function dashboard()
	{
	  $data['success']=$data['error']='';	
	  $data['pagetitle']='सडवली संघटन  | Dashboard';
	  $data['middle_content']='dashboard';
	  $this->load->view('admin/dashboard',$data);
	}
	

	

	public function listNewsletter()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='The A | List Newsletter Subscibers';
	  //$data['middle_content']='list_artist';
	  $query = "SELECT * FROM (`table_newsletter`) WHERE 1 = '1' ";

		//$data  = array();
    	$query = $this->db->query( $query );
    	$result    = $query->result();

	 // $result=$this->master_model->getRecords('table_country',array(1=>"1"),'*'); 
	  $data['country_data'] = $result;
	 
	  $this->load->view('admin/list_newsletter',$data);
	}

	

	public function listEnquiry()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='The A | List Enquiry';
	  //$data['middle_content']='list_artist';
	  $result=$this->master_model->getRecords('table_service',array(),'table_service.*'); 
	  $data['country_data'] = $result;
	  $this->load->view('admin/list_enquiry',$data);
	}



	

	public function listSpaces()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='The A | List Spaces';
	  //$data['middle_content']='list_artist';
	  $result=$this->master_model->getRecords('table_spaces',array(),'table_spaces.*'); 
	  $data['spaces_data'] = $result;
	  $this->load->view('admin/list_spaces',$data);
	}

	public function listOurSpaces(){
		  $data['success']=$data['error']='';	
		  $data['pagetitle']='The A | List Our Spaces';
		  //$data['middle_content']='list_artist';
		  $result=$this->master_model->getRecords('table_content',array('type'=>'our_spaces'),'table_content.*'); 
		  //print_r($result);exit;
		  $data['spaces_data'] = $result;
		  $this->load->view('admin/list_ourspaces',$data);
	}

	public function listPartnerLogo()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='The A | List Partner Logo';
	  //$data['middle_content']='list_artist';
	  $result=$this->master_model->getRecords('table_partner_logo',array(),'table_partner_logo.*'); 
	  $data['spaces_data'] = $result;
	  $this->load->view('admin/list_partner_logo',$data);
	}


	public function listClub()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='The A | List Reciprocal Club';
	  //$data['middle_content']='list_artist';
	  $result=$this->master_model->getRecords('table_club',array(),'table_club.*'); 
	  $data['spaces_data'] = $result;
	  $this->load->view('admin/list_club',$data);
	}


	public function listPopups()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='The A | List Popups';
	  //$data['middle_content']='list_artist';
	  $result=$this->master_model->getRecords('table_popup',array(),'table_popup.*'); 
	  $data['spaces_data'] = $result;
	  $this->load->view('admin/list_popups',$data);
	}

	public function listAds()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='The A | List Ads';
	  //$data['middle_content']='list_artist';
	  $result=$this->master_model->getRecords('table_ads',array(),'table_ads.*'); 

	  for($i=0;$i<count($result);$i++){
	  	$total_views_hits = $this->master_model->getRecords('ad_total_analytics',array("ad_id"=>$result[$i]['id']),'ad_total_analytics.*'); 
	  	$result[$i]['total_views'] =  $total_views_hits[0]['total_view'];
	  	$result[$i]['total_hits'] =  $total_views_hits[0]['total_hits'];

	  	$unique_views = $this->master_model->getRecords('table_ad_unique_views',array("ad_id"=>$result[$i]['id']),'count(table_ad_unique_views.id) as unique_views');

	  	$unique_views_data = $this->master_model->getRecords('table_ad_unique_views',array("ad_id"=>$result[$i]['id']),'table_ad_unique_views.ip_address');

	  	$unique_hits = $this->master_model->getRecords('table_ad_unique_hits',array("ad_id"=>$result[$i]['id']),'count(table_ad_unique_hits.id) as unique_hits');

	  	$unique_hits_data = $this->master_model->getRecords('table_ad_unique_hits',array("ad_id"=>$result[$i]['id']),'table_ad_unique_hits.ip_address');

	  	$result[$i]['unique_views'] =  $unique_views[0]['unique_views'];
	  	$result[$i]['unique_hits'] =  $unique_hits[0]['unique_hits'];

	  }

	  $data['spaces_data'] = $result;
	  $data['unique_hits_data'] = $unique_hits_data;
	  $data['unique_views_data'] = $unique_views_data;
	  $this->load->view('admin/list_ads',$data);
	}

	public function listBanner()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='The A | List Banner';
	  //$data['middle_content']='list_artist';
	  $result=$this->master_model->getRecords('table_banner',array(),'table_banner.*'); 
	  $data['spaces_data'] = $result;
	  $this->load->view('admin/list_banner',$data);
	}

	public function listLocation()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='The A | List Location';
	  //$data['middle_content']='list_artist';
	  $result=$this->master_model->getRecords('table_location',array(),'table_location.*'); 
	  $data['spaces_data'] = $result;
	  $this->load->view('admin/list_location',$data);
	}


	public function listTestimonial()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='The A | List Testimonial';
	  //$data['middle_content']='list_artist';
	  $result=$this->master_model->getRecords('table_testimonial',array(),'table_testimonial.*'); 
	  $data['spaces_data'] = $result;
	  $this->load->view('admin/list_testimonial',$data);
	}


	public function listMembership()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='The A | List Membership';
	  //$data['middle_content']='list_artist';
	  $member_type=$this->uri->segment('4');
	   
	  $result=$this->master_model->getRecords('table_membership',array('type'=>$member_type),'table_membership.*'); 
	  $data['spaces_data'] = $result;
	  //$data['spaces_data']['member_type'] = $member_type;
	  $this->load->view('admin/list_membership',$data);
	}

	
	public function listHome()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='SMC | List Home Images';
	  //$data['middle_content']='list_artist';
	  $result=$this->master_model->getRecords('table_home_images',array("image_name_desktop !="=> ""),'table_home_images.*'); 
	  //echo $this->db->last_query();
	  $data['image_data'] = $result;
	  $this->load->view('admin/list_home',$data);
	}

	
	
	public function listProject()
	{
		$country_id=$this->input->get('country_id',true);
		$country_id=base64_decode($this->uri->segment('4'));
	   
	  //$data['middle_content']='list_artist';
	  //$cond = array('ta.artist_id')
	  //$result=$this->master_model->getRecords('table_work as  tw JOIN table_artist as ta ON ta.artist_id=tw.artist_id',array(),'tw.*,ta.artist_name'); 
	  $query = "SELECT `tw`.*, `ta`.`country_name` FROM (`table_project` as tw JOIN  `table_country` as ta ON ta.country_id=tw.country_id) WHERE FIND_IN_SET( '$country_id' ,  `tw`.`country_id` ) ORDER BY `tw`.`sort_order` asc";		//echo $query;
	  $data  = array();
      $query = $this->db->query( $query );
      $result    = $query->result();
	  $data['project_data'] = $result;
	  $data['success']=$data['error']='';
	  $data['country_id'] = $country_id;	
	  	$data['country_id'] = base64_encode($country_id);
	   $data['pagetitle'] = 'SMC | List Project';
	  $this->load->view('admin/listProject',$data);
	}

	
	

	public function listCms()
	{
		 $data['success']=$data['error']='';	
	  $data['pagetitle']='SMC | CMS Pages';
	  //$data['middle_content']='list_artist';
	  $result=$this->master_model->getRecords('table_cms',array(),'table_cms.*'); 
	  $data['cms_data'] = $result;
	  $this->load->view('admin/list_cms',$data);
	}


	public function listAboutVillege()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='सडवली संघटन | माझे गाव';
	  //$data['middle_content']='list_artist';
	  $result = $this->master_model->getRecords('table_villege',array(),'table_villege.*'); 
	 // $result=$this->master_model->getRecords('table_country',array(1=>"1"),'*'); 
	  $data['villege'] = $result;
	 //echo 123;exit;
	  $this->load->view('admin/list_aboutvillege',$data);
	}
	

	public function addCountry()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='SMC | Add Country';
		 if(isset($_POST['btn_submit']))
		 {
		 	//$slug = url_title($title, 'dash', true);

			$this->form_validation->set_rules('country_name','','required|xss_clean');
			//$this->form_validation->set_rules('password','','required|xss_clean');
			if($this->form_validation->run())
			{
				$country_name=$this->input->post('country_name',true);
				
				$country_name_slug = url_title($country_name, 'dash', true);

						  $input_array=array('country_name'=>$country_name);	
							
							if($user_info=$this->master_model->insertRecord('table_country',$input_array))
							{ 
							
								$this->session->set_flashdata('success','Country added Successfully');			
								redirect(base_url().'superadmin/admin/listCountry/');
								
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								redirect(base_url().'superadmin/admin/addCountry/');
								 
							}
				
			}
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
		  }		
	  
	  $this->load->view('admin/add_country',$data);
	}

	public function addHomeimage()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='SMC | Add Home Image';
		 if(isset($_POST['btn_submit']))
		 {
		 	

				$work_image=$this->input->post('work_image',true);
				
				$date = date('Y-m-d H:i:s', time());
				$config=array('upload_path'=>'uploads/',
					          'allowed_types'=>'jpg|jpeg|gif|png|svg|svgz',
					          'file_name'=>rand(1,9999),'max_size'=>0);
			    $this->upload->initialize($config);
			    if($_FILES['desktop_image']['name']!='')
					{ 
						if($this->upload->do_upload('desktop_image'))
						{
						  $dt=$this->upload->data();
						  $file_desktop=$dt['file_name'];
						  
						  $input_array=array('image_name_desktop'=>$file_desktop,'date_added'=>$date );	
							
							if($user_info=$this->master_model->insertRecord('table_home_images',$input_array))
							{ 
							
								$this->session->set_flashdata('success','Image added Successfully');			
								redirect(base_url().'superadmin/admin/listHome/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								redirect(base_url().'superadmin/admin/addHomeimage/');
								 
							}
						 // $input_array=array();
						}
						else
						{
							$file="";
							//$data['error']=$this->upload->display_errors();
							$this->session->set_flashdata('error',$this->upload->display_errors());
								redirect(base_url().'superadmin/admin/addHomeimage/');
						}
					}	
					
				
			
		  }		
	  
	  $this->load->view('admin/add_home_image',$data);
	}


	public function search_tag()
	{
		//var_dump($_GET);
		$term = $this->input->get('term',true);
		$result=$this->master_model->getRecords('table_tags',array("tag_name like "=> "%".$term."%"),'table_tags.tag_id,table_tags.tag_name'); 
		//echo $this->db->last_query();
		
		foreach ($result as $res)
		{
			$res['value'] = $res['tag_name'];
			$res['id'] = $res['tag_id'];
			$result_set[] = $res;
		}
		//var_dump($result);
		echo (json_encode($result_set));
	}


	public function addInvestor()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='The A | Add Investor';
		 if(isset($_POST['btn_submit']))
		 {


			$this->form_validation->set_rules('project_name','','required|xss_clean');
			
			
			if($this->form_validation->run())
			{
				
				$investor_name=$this->input->post('project_name',true);
				$slug = url_title($investor_name, 'dash', true);
				$input_array = array('name' =>  $investor_name,'slug'=>$slug);
							if($user_info=$this->master_model->insertRecord('table_investor',$input_array))
							{ 
								$investor_id = $this->db->insert_id();
								$count_gal = count($_FILES['project_gal_img']['name']);
								$files = $_FILES;
								if($_FILES['project_gal_img']['name'][0]!=""){

									for($i=0; $i<$count_gal; $i++)
									{
										$config_rec=array('upload_path'=>'uploads/',
								          'allowed_types'=>'pdf',
								          //'file_name'=>md5(rand(1,9999)),
								          'file_name'=>$files['project_gal_img']['name'][$i],
								          'max_size'=>10000);
							    		$this->upload->initialize($config_rec);

							    		$_FILES['project_gal_img']['name'] = $files['project_gal_img']['name'][$i];
								        $_FILES['project_gal_img']['type']= $files['project_gal_img']['type'][$i];
								        $_FILES['project_gal_img']['tmp_name']= $files['project_gal_img']['tmp_name'][$i];
								        $_FILES['project_gal_img']['error']= $files['project_gal_img']['error'][$i];
								        $_FILES['project_gal_img']['size']= $files['project_gal_img']['size'][$i]; 
								        if($this->upload->do_upload('project_gal_img'))
										{
										  $dt_gallery=$this->upload->data();
										  $file_gallery[]=$dt_gallery['file_name'];

										  //$res = $this->master_model->createThumb($file,'uploads/profile/',200,200,FALSE);
										  
										}else
										{
											$this->session->set_flashdata('error',$this->upload->display_errors());
										}
									}
									for($m=0;$m<count($file_gallery);$m++)
											{
												$input_array_gal = array('investor_id'=>$investor_id,'file_name'=>$file_gallery[$m]);
												$this->master_model->insertRecord('table_pdf',$input_array_gal)	;

											}
								}
								$this->session->set_flashdata('success','Investor added Successfully');			
								redirect(base_url().'superadmin/admin/listInvestor/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_investor',$data);
	}


	public function addArticle()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='The A | Add Article';
		 if(isset($_POST['btn_submit']))
		 {


			$this->form_validation->set_rules('project_name','','required|xss_clean');
			$this->form_validation->set_rules('project_short_desc','','required|xss_clean');
			$this->form_validation->set_rules('publish_date','','required|xss_clean');
			
			//$this->form_validation->set_rules('site_url','','required|xss_clean');

			
			
			if($this->form_validation->run())
			{
				
				$title=$this->input->post('project_name',true);
				$description =$this->input->post('project_short_desc',true);
				$publish_date =$this->input->post('publish_date',true);
				$site_url=$this->input->post('site_url',true);

				$input_array = array(
									'title' =>  $title,
									'description' =>  $description,
									'url' =>  $site_url,
									'publish_date'=>$publish_date
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->insertRecord('table_article',$input_array))
							{ 
								$article_id = $this->db->insert_id();
								
								$count_gal = count($_FILES['project_gal_img']['name']);
								
								$files = $_FILES;
								$config=array('upload_path'=>'uploads/',
					          'allowed_types'=>'jpg|jpeg|gif|png',
					          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);


								if($_FILES['project_banner_img']['name']!='')
								{
									if($this->upload->do_upload('project_banner_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
						
									
									$data_inserted=$this->master_model->updateRecord('table_article',$input_array,array('id'=>$article_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								//print_r($count_gal);exit;
								
								if($_FILES['project_gal_img']['name'][0]!=""){

									for($j=0; $j<$count_gal; $j++)
									{
										$config_pdf=array('upload_path'=>'uploads/',
								          'allowed_types'=>'pdf',
								          //'file_name'=>md5(rand(1,9999)),
								          'file_name'=>$files['project_gal_img']['name'][$j],
								          'max_size'=>10000);
							    		$this->upload->initialize($config_pdf);

							    		$_FILES['project_gal_img']['name'] = $files['project_gal_img']['name'][$j];
								        $_FILES['project_gal_img']['type']= $files['project_gal_img']['type'][$j];
								        $_FILES['project_gal_img']['tmp_name']= $files['project_gal_img']['tmp_name'][$j];
								        $_FILES['project_gal_img']['error']= $files['project_gal_img']['error'][$j];
								        $_FILES['project_gal_img']['size']= $files['project_gal_img']['size'][$j]; 
								        if($this->upload->do_upload('project_gal_img'))
										{
										  $dt_pdf=$this->upload->data();
										  $file_pdf[]=$dt_pdf['file_name'];

										  //$res = $this->master_model->createThumb($file,'uploads/profile/',200,200,FALSE);
										  
										}else
										{
											$this->session->set_flashdata('error',$this->upload->display_errors());
										}
									}
									
									for($n=0;$n<count($file_pdf);$n++)
											{
												$input_array_gal = array('pdf_file_name'=>$file_pdf[$n]);
												//$this->master_model->insertRecord('table_pdf',$input_array_gal)	;
												$data_inserted=$this->master_model->updateRecord('table_article',$input_array_gal,array('id'=>$article_id));


											}
								}


								$this->session->set_flashdata('success','Article added Successfully');			
								redirect(base_url().'superadmin/admin/listArticle/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_article',$data);
	}


	public function addSpaces()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='The A | Add Spaces';
		 if(isset($_POST['btn_submit']))
		 {


			$this->form_validation->set_rules('project_name','','required|xss_clean');
			$this->form_validation->set_rules('icon_class','','required|xss_clean');
			$this->form_validation->set_rules('project_quote','','required|xss_clean');
			$this->form_validation->set_rules('project_short_desc','','required|xss_clean');
			//$this->form_validation->set_rules('quote_by','','required|xss_clean');
			//$this->form_validation->set_rules('image_quote','','required|xss_clean');
			
			
			if($this->form_validation->run())
			{
				
				$title=$this->input->post('project_name',true);
				$icon_class=$this->input->post('icon_class',true);
				$quote_by=$this->input->post('quote_by',true);
				$quote =$this->input->post('project_quote',true);
				$project_short_desc =$this->input->post('project_short_desc',true);
				$img_quote = $this->input->post('image_quote',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									'project_title' =>  $title,
									'project_quote' =>  $quote,
									'project_short_desc' =>  $project_short_desc,
									'quote_by'=>$quote_by,
									'icon_class'=>$icon_class,
									'img_quote'=>$img_quote
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->insertRecord('table_spaces',$input_array))
							{ 
								$space_id = $this->db->insert_id();
								
								//echo $space_id;exit;
								//print_r($_FILES['project_icon_img']);exit;
								//$files = $_FILES;
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				//echo "<pre>";
			    				//print_r($_FILES['project_icon_img']['name']);exit;

								/*if($_FILES['project_icon_img']['name']!='')
								{
									//echo 123;exit;
									//var_dump($this->upload->do_upload('project_icon_img'));exit;
									if($this->upload->do_upload('project_icon_img'))
									{
										//echo 567; exit;
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('project_icon_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_spaces',$input_array,array('id'=>$space_id));
										//echo $this->db->last_query();exit;
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}
*/
								if($_FILES['project_banner_img']['name']!='')
								{
									if($this->upload->do_upload('project_banner_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('project_banner_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_spaces',$input_array,array('id'=>$space_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}


								if($_FILES['project_mobile_banner_img']['name']!='')
								{
									if($this->upload->do_upload('project_mobile_banner_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('project_mobile_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_spaces',$input_array,array('id'=>$space_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}



								$this->session->set_flashdata('success','Spaces added Successfully');			
								redirect(base_url().'superadmin/admin/listSpaces/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_spaces',$data);
	}
	

	public function addBanner()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='The A | Add Banner';
		 if(isset($_POST['btn_submit']))
		 {


			$this->form_validation->set_rules('title','','required|xss_clean');
			//$this->form_validation->set_rules('project_icon_img','','required|xss_clean');
			$this->form_validation->set_rules('v_tour_link','','required|xss_clean');
			$this->form_validation->set_rules('description','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				
				$title=$this->input->post('title',true);
				$v_tour_link =$this->input->post('v_tour_link',true);
				$description =$this->input->post('description',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									'title' =>  $title,
									'v_tour_link' =>  $v_tour_link,
									'description' =>  $description
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->insertRecord('table_banner',$input_array))
							{ 
								$space_id = $this->db->insert_id();
								
								//echo $space_id;exit;
								//print_r($_FILES['project_icon_img']);exit;
								//$files = $_FILES;
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				//echo "<pre>";
			    				//print_r($_FILES['project_icon_img']['name']);exit;

								

								if($_FILES['project_banner_img']['name']!='')
								{
									if($this->upload->do_upload('project_banner_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('banner_large_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_banner',$input_array,array('id'=>$space_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}


								if($_FILES['project_mobile_banner_img']['name']!='')
								{
									if($this->upload->do_upload('project_mobile_banner_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('banner_mobile_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_banner',$input_array,array('id'=>$space_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}



								$this->session->set_flashdata('success','Banner added Successfully');			
								redirect(base_url().'superadmin/admin/listBanner/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_banner',$data);
	}


	public function addLocation()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='The A | Add Location';
		 if(isset($_POST['btn_submit']))
		 {


			$this->form_validation->set_rules('title','','required|xss_clean');
			//$this->form_validation->set_rules('project_icon_img','','required|xss_clean');
			$this->form_validation->set_rules('distance','','required|xss_clean');
			$this->form_validation->set_rules('loc_type','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				
				$title=$this->input->post('title',true);
				$distance =$this->input->post('distance',true);
				$type =$this->input->post('loc_type',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									'title' =>  $title,
									'distance' =>  $distance,
									'type' =>  $type
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->insertRecord('table_location',$input_array))
							{ 
								$space_id = $this->db->insert_id();
								
								//echo $space_id;exit;
								//print_r($_FILES['project_icon_img']);exit;
								//$files = $_FILES;
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				//echo "<pre>";
			    				//print_r($_FILES['project_icon_img']['name']);exit;

								

								if($_FILES['project_banner_img']['name']!='')
								{
									if($this->upload->do_upload('project_banner_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_location',$input_array,array('id'=>$space_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}


								


								$this->session->set_flashdata('success','Location added Successfully');			
								redirect(base_url().'superadmin/admin/listLocation/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_location',$data);
	}


	public function addPartnerLogo()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='The A | Add partner Logo';
		 if(isset($_POST['btn_submit']))
		 {


			 //$this->form_validation->set_rules('logo_url','','required|xss_clean');
			//$this->form_validation->set_rules('project_icon_img','','required|xss_clean');
			$this->form_validation->set_rules('alt_text','','required|xss_clean');
			//$this->form_validation->set_rules('loc_type','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				
				$logo_url=$this->input->post('logo_url',true);
				$alt_text =$this->input->post('alt_text',true);
				//$type =$this->input->post('loc_type',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									'alt_text' =>  $alt_text,
									'title'=>$logo_url
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->insertRecord('table_partner_logo',$input_array))
							{ 
								$space_id = $this->db->insert_id();
								
								//echo $space_id;exit;
								//print_r($_FILES['project_icon_img']);exit;
								//$files = $_FILES;
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				//echo "<pre>";
			    				//print_r($_FILES['project_icon_img']['name']);exit;

								

								if($_FILES['project_banner_img']['name']!='')
								{
									if($this->upload->do_upload('project_banner_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_partner_logo',$input_array,array('id'=>$space_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}


								


								$this->session->set_flashdata('success','Logo added Successfully');			
								redirect(base_url().'superadmin/admin/listPartnerLogo/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_partner_logo',$data);
	}



	public function addClub()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='The A | Add Club ';
		 if(isset($_POST['btn_submit']))
		 {


			 //$this->form_validation->set_rules('logo_url','','required|xss_clean');
			//$this->form_validation->set_rules('project_icon_img','','required|xss_clean');
			$this->form_validation->set_rules('title','','required|xss_clean');
			//$this->form_validation->set_rules('loc_type','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				
				//$img_name=$this->input->post('img_name',true);
				$title =$this->input->post('title',true);
				$logo_url =$this->input->post('logo_url',true);
				//$type =$this->input->post('loc_type',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									
									'title'=>$title,
									'url' => $logo_url
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->insertRecord('table_club',$input_array))
							{ 
								$space_id = $this->db->insert_id();
								
								//echo $space_id;exit;
								//print_r($_FILES['project_icon_img']);exit;
								//$files = $_FILES;
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				//echo "<pre>";
			    				//print_r($_FILES['project_icon_img']['name']);exit;

								

								if($_FILES['project_banner_img']['name']!='')
								{
									if($this->upload->do_upload('project_banner_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_club',$input_array,array('id'=>$space_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}


								


								$this->session->set_flashdata('success','Club added Successfully');			
								redirect(base_url().'superadmin/admin/listClub/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_club',$data);
	}


	public function addPopup()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='The A | Add Popup';
		 if(isset($_POST['btn_submit']))
		 {


			 //$this->form_validation->set_rules('logo_url','','required|xss_clean');
			//$this->form_validation->set_rules('project_icon_img','','required|xss_clean');
			$this->form_validation->set_rules('logo_url','','required|xss_clean');
			//$this->form_validation->set_rules('loc_type','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				
				$logo_url=$this->input->post('logo_url',true);
				
				//$type =$this->input->post('loc_type',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									'url' =>  $logo_url
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->insertRecord('table_popup',$input_array))
							{ 
								$space_id = $this->db->insert_id();
								
								//echo $space_id;exit;
								//print_r($_FILES['project_icon_img']);exit;
								//$files = $_FILES;
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				//echo "<pre>";
			    				//print_r($_FILES['project_icon_img']['name']);exit;

								

								if($_FILES['project_banner_img']['name']!='')
								{
									if($this->upload->do_upload('project_banner_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_popup',$input_array,array('id'=>$space_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}


								


								$this->session->set_flashdata('success','Popup added Successfully');			
								redirect(base_url().'superadmin/admin/listPopups/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_popup',$data);
	}
	


	public function addAds()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='The A | Add Advertise';
		 if(isset($_POST['btn_submit']))
		 {


			 $this->form_validation->set_rules('redirect_url','','required|xss_clean');
			 $this->form_validation->set_rules('valid_days','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				
				$redirect_url=$this->input->post('redirect_url',true);
				$valid_days=$this->input->post('valid_days',true);
				//$type =$this->input->post('loc_type',true);
				$expiry_date = date('Y-m-d', strtotime( ' + '.$valid_days.' days'));
				
				//print_r($_FILES);exit;

				$input_array = array(
									'redirect_url' =>  $redirect_url,
									'valid_days' =>  $valid_days,
									'expiry_date'=> $expiry_date
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->insertRecord('table_ads',$input_array))
							{ 
								$space_id = $this->db->insert_id();
								
								//echo $space_id;exit;
								//print_r($_FILES['project_icon_img']);exit;
								//$files = $_FILES;
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				//echo "<pre>";
			    				//print_r($_FILES['project_icon_img']['name']);exit;

								

								if($_FILES['project_banner_img']['name']!='')
								{
									if($this->upload->do_upload('project_banner_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_ads',$input_array,array('id'=>$space_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}


								if($_FILES['project_mobile_banner_img']['name']!='')
								{
									if($this->upload->do_upload('project_mobile_banner_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('mobile_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_ads',$input_array,array('id'=>$space_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}


								


								$this->session->set_flashdata('success','Advertise added Successfully');			
								redirect(base_url().'superadmin/admin/listAds/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_ads',$data);
	}



	public function addMembership()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='The A | Add Membership';
	  $member_type=$this->uri->segment('4');
		 if(isset($_POST['btn_submit']))
		 {


			$this->form_validation->set_rules('title_big','','required|xss_clean');
			//$this->form_validation->set_rules('project_icon_img','','required|xss_clean');
			//$this->form_validation->set_rules('title_small','','required|xss_clean');
			$this->form_validation->set_rules('description','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				
				$title_big=$this->input->post('title_big',true);
				$title_small =$this->input->post('title_small',true);
				$description =$this->input->post('description',true);
				$member_type =$this->input->post('member_type',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									'title_big' =>  $title_big,
									'title_small' =>  $title_small,
									'description' =>  $description,
									'type'=>$member_type
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->insertRecord('table_membership',$input_array))
							{ 
								

								$this->session->set_flashdata('success','Membership Plan added Successfully');			
								redirect(base_url().'superadmin/admin/listMembership/'.$member_type);
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_membership',$data);
	}



	public function addTestimonial()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='The A | Add Testimonial';
		 if(isset($_POST['btn_submit']))
		 {


			$this->form_validation->set_rules('testimonial','','required|xss_clean');
			//$this->form_validation->set_rules('project_icon_img','','required|xss_clean');
			$this->form_validation->set_rules('testimonial_by','','required|xss_clean');
			
			if($this->form_validation->run())
			{
				
				$testimonial=$this->input->post('testimonial',true);
				$testimonial_by =$this->input->post('testimonial_by',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									'testimonial' =>  $testimonial,
									'testimonial_by' =>  $testimonial_by
									
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->insertRecord('table_testimonial',$input_array))
							{ 

								$this->session->set_flashdata('success','Testimonial added Successfully');			
								redirect(base_url().'superadmin/admin/listTestimonial/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_testimonial',$data);
	}



	public function addProject()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='SMC | Add Project';
	  $country_data =   $this->master_model->getRecords('table_country',array(),'table_country.*');
	  $cat_data =   $this->master_model->getRecords('table_category',array(),'table_category.*'); 
	  $data['country_data'] = $country_data;
	  $data['cat_data'] = $cat_data;
		 if(isset($_POST['btn_submit']))
		 {


			$this->form_validation->set_rules('project_name','','required|xss_clean');
			$this->form_validation->set_rules('select_country','','required|xss_clean');
			$this->form_validation->set_rules('project_short_desc','','required|xss_clean');
			$this->form_validation->set_rules('status','','required|xss_clean');
			$this->form_validation->set_rules('key_project','','required|xss_clean');
			//$this->form_validation->set_rules('client_name','','required|xss_clean');
			
			//$this->form_validation->set_rules('work_url_youtube','Youtube Url','required|xss_clean');

			//$this->form_validation->set_rules('password','','required|xss_clean');
			
			if($this->form_validation->run())
			{
				
				$project_name=$this->input->post('project_name',true);
				$project_name_slug = url_title($project_name, 'dash', true);
				$project_short_desc=$this->input->post('project_short_desc',true);
				$project_des_left =$this->input->post('project_des_left',true);
				$project_des_right =$this->input->post('project_des_right',true);

				$project_heading1 =$this->input->post('project_heading1',true);
				$project_heading2 =$this->input->post('project_heading2',true);
				$status =$this->input->post('status',true);
				$key_project =$this->input->post('key_project',true);
				$banner_heading1 =$this->input->post('banner_heading1',true);
				$banner_heading2 =$this->input->post('banner_heading2',true);
				
				//$select_country=$this->input->post('select_country',true);
				
				$select_country=$this->input->post('select_country',true);
				$select_expertise=$this->input->post('select_expertise',true);
				
				//$select_country = implode(",",$select_country);
				
				$date = date('Y-m-d H:i:s', time());
				$config=array('upload_path'=>'uploads/',
					          'allowed_types'=>'jpg|jpeg|gif|png',
					          'file_name'=>rand(1,9999),'max_size'=>0);
			    $this->upload->initialize($config);
			    if($_FILES['project_featured_img']['name']!='')
					{
						if($this->upload->do_upload('project_featured_img'))
						{
						  $dt=$this->upload->data();
						  $file=$dt['file_name'];
						  $this->upload->do_upload('project_banner_img');
						  $dt_banner = $this->upload->data();
						  $banner_file=$dt_banner['file_name'];

						  $input_array=array('project_name'=>$project_name,'project_name_slug'=>$project_name_slug,'heading1'=>$project_heading1,'heading2'=>$project_heading2,'project_short_desc'=>$project_short_desc,'project_des'=>$project_des_left,'right_desc'=>$project_des_right,'country_id'=>$select_country,'project_featured_img'=>$file,'status'=>$status,"cat_id"=>$select_expertise,'banner_heading1'=>$banner_heading1,'banner_heading2'=>$banner_heading2,'key_project'=>$key_project,'project_banner_img'=>$banner_file);	
			
							if($user_info=$this->master_model->insertRecord('table_project',$input_array))
							{ 
								$project_id = $this->db->insert_id();
								$count_gal = count($_FILES['project_gal_img']['name']);
								$files = $_FILES;
								if($_FILES['project_gal_img']['name'][0]!=""){

									for($i=0; $i<$count_gal; $i++)
									{
										$config_rec=array('upload_path'=>'uploads/',
								          'allowed_types'=>'jpg|jpeg|gif|png',
								          'file_name'=>md5(rand(1,9999)),'max_size'=>10000);
							    		$this->upload->initialize($config_rec);

							    		$_FILES['project_gal_img']['name'] = $files['project_gal_img']['name'][$i];
								        $_FILES['project_gal_img']['type']= $files['project_gal_img']['type'][$i];
								        $_FILES['project_gal_img']['tmp_name']= $files['project_gal_img']['tmp_name'][$i];
								        $_FILES['project_gal_img']['error']= $files['project_gal_img']['error'][$i];
								        $_FILES['project_gal_img']['size']= $files['project_gal_img']['size'][$i]; 
								        if($this->upload->do_upload('project_gal_img'))
										{
										  $dt_gallery=$this->upload->data();
										  $file_gallery[]=$dt_gallery['file_name'];

										  //$res = $this->master_model->createThumb($file,'uploads/profile/',200,200,FALSE);
										  
										}else
										{
											$this->session->set_flashdata('error',$this->upload->display_errors());
										}
									}

									for($m=0;$m<count($file_gallery);$m++)
											{
												$input_array_gal = array('project_id'=>$project_id,'img_name'=>$file_gallery[$m]);
												$this->master_model->insertRecord('table_gallery',$input_array_gal)	;
											}
								}
								$this->session->set_flashdata('success','Project added Successfully');			
								redirect(base_url().'superadmin/admin/listProject/'.base64_encode($select_country));
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						}
						else
						{
							$file="";
							$this->session->set_flashdata('error',$this->upload->display_errors());
							$data['error']=$this->upload->display_errors();
						}
					}
					

				
			}else
			{
				$this->session->set_flashdata('error',$this->upload->display_errors());
				$data['error']=$this->form_validation->error_string();
			}
		  }		
	  
	  $this->load->view('admin/add_work',$data);
	}

	

	public function addExpertise()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='SMC | Add Expertise';
	  $artist_data =   $this->master_model->getRecords('table_category',array(),'table_category.*'); 
	  $data['category_data'] = $artist_data;
	  
		 if(isset($_POST['btn_submit']))
		 {
		 	
			$this->form_validation->set_rules('cat_name','','required|xss_clean');

			//$this->form_validation->set_rules('designation','','required|xss_clean');
			

			//$this->form_validation->set_rules('password','','required|xss_clean');
			if($this->form_validation->run())
			{
				$cat_name=$this->input->post('cat_name',true);
				$cat_name_slug = url_title($cat_name, 'dash', true);

				//$password=$this->input->post('password',true);
				$date = date('Y-m-d H:i:s', time());
				$config=array('upload_path'=>'uploads/',
					          'allowed_types'=>'jpg|jpeg|gif|png',
					          'file_name'=>rand(1,9999),'max_size'=>0);
			    $this->upload->initialize($config);
			    if($_FILES['cat_featured_img']['name']!='')
					{
						if($this->upload->do_upload('cat_featured_img'))
						{
						  $dt=$this->upload->data();
						  $cat_featured_img=$dt['file_name'];
						  $input_array=array('cat_name'=>$cat_name,'cat_slug'=>$cat_name_slug,'cat_featured_image'=>$cat_featured_img);	
			
							if($user_info=$this->master_model->insertRecord('table_category',$input_array))
							{ 
							
								$this->session->set_flashdata('success','Expertise added Successfully');			
								redirect(base_url().'superadmin/admin/listExpertise/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 //$data['error']='Something went wrong ,try again later';
							}
						}
						else
						{
							$file="";
							echo $this->upload->display_errors();
							$this->session->set_flashdata('error',$this->upload->display_errors());
							//$data['error']=$this->upload->display_errors();
						}
					}
					

				
			}
		  }		
	  
	  $this->load->view('admin/add_expertise',$data);
	}

	
	public function addAboutVillege()
	{
	  $front_id=$this->uri->segment('4');
	  if($front_id=="sites") { $page_title="पर्यटन स्थळे" ; } 
	  if($front_id=="school") { $page_title="शाळा " ; } 
	  if($front_id=="temple") { $page_title="गावातील मंदिरे" ; } 
	  if($front_id=="festival") { $page_title="मुख्य सण" ; } 

	  $data['success']=$data['error']="";
	  $data['pagetitle']='माझे गाव | '.$page_title;
	  $data['cat_id'] = $front_id;
		 if(isset($_POST['btn_submit']))
		 {

			$this->form_validation->set_rules('title','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				
				//$img_name=$this->input->post('img_name',true);
				$title =$this->input->post('title',true);
				$desc =$this->input->post('description',true);
				$cat_name =$this->input->post('cat_name',true);
				//$logo_url =$this->input->post('logo_url',true);
				//$type =$this->input->post('loc_type',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									
									'title'=>$title,
									'description' => $desc,
									'cat_name' => $cat_name
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->insertRecord('table_villege',$input_array))
							{ 
								$villege_id = $this->db->insert_id();
								
								
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				
								

								if($_FILES['img_name']['name']!='')
								{
									if($this->upload->do_upload('img_name'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_villege',$input_array,array('id'=>$villege_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								$this->session->set_flashdata('success','Info added Successfully');			
								redirect(base_url().'superadmin/villege/list'.$cat_name);
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_villege',$data);
	}


	public function editAboutVillege()
	{
	  $front_id=$this->uri->segment('5');
	  /*if($front_id=="sites") { $page_title="पर्यटन स्थळे" ; } 
	  if($front_id=="listschool") { $page_title="शाळा " ; } 
	  if($front_id=="listtemple") { $page_title="गावातील मंदिरे" ; } 
	  if($front_id=="listfestival") { $page_title="मुख्य सण" ; } */
	   if($front_id=="sites") { $page_title="पर्यटन स्थळे" ; } 
	  if($front_id=="school") { $page_title="शाळा " ; } 
	  if($front_id=="temple") { $page_title="गावातील मंदिरे" ; } 
	  if($front_id=="festival") { $page_title="मुख्य सण" ; } 
	  //echo $front_id;
	  $data['cat_id'] = $front_id;
	  /*switch ($front_id) {
	  	case 'listsites':
	  			$data['cat_id'] = "sites";
	  		break;
	  	case 'listschool':
	  			$data['cat_id'] = "school";
	  		break;
	  	case 'listtemple':
	  			$data['cat_id'] = "temple";
	  		break;
	  	case 'listfestival':
	  			$data['cat_id'] = "festival";
	  		break;
	  	
	  	default:
	  		# code...
	  		break;
	  }*/
	  $data['success']=$data['error']="";
	  $data['pagetitle']='माझे गाव | '.$page_title;
	  $front_id=base64_decode($this->uri->segment('4'));
	  
	  $villege_data =   $this->master_model->getRecords('table_villege',array('id'=>$front_id),'table_villege.*'); 
	  //print_r($villege_data);exit;
	  $data['villege'] = $villege_data;
		 if(isset($_POST['btn_submit']))
		 {

			$this->form_validation->set_rules('title','','required|xss_clean');
			$this->form_validation->set_rules('villege_id','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				
				//$img_name=$this->input->post('img_name',true);
				$title =$this->input->post('title',true);
				$desc =$this->input->post('description',true);
				$villege_id =$this->input->post('villege_id',true);
				$cat_name =$this->input->post('cat_name',true);
				//$logo_url =$this->input->post('logo_url',true);
				//$type =$this->input->post('loc_type',true);
				
				//print_r($_FILES);exit;
				//echo $front_id;exit;

				$input_array = array(
									
									'title'=>$title,
									'description' => $desc,
									'cat_name' => $cat_name
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->updateRecord('table_villege',$input_array,array("id"=>$villege_id)))
							{ 
								
								
								
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				
								

								if($_FILES['img_name']['name']!='')
								{
									if($this->upload->do_upload('img_name'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_villege',$input_array,array('id'=>$villege_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								$this->session->set_flashdata('success','Info added Successfully');			
								redirect(base_url().'superadmin/villege/list'.$cat_name);
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/edit_villege',$data);
	}

	public function editSpaces()
	{
	  $data['success']=$data['error']="";
	  $front_id=base64_decode($this->uri->segment('4'));
	  $data['pagetitle']='The A | Edit Spaces';
	  $spaces_data =   $this->master_model->getRecords('table_spaces',array('id'=>$front_id),'table_spaces.*'); 
	   //print_r($article_data);exit;
	  $data['spaces_data'] = $spaces_data;
		 if(isset($_POST['btn_submit']))
		 {


			$this->form_validation->set_rules('project_name','','required|xss_clean');
			//$this->form_validation->set_rules('quote_by','','required|xss_clean');
			$this->form_validation->set_rules('project_quote','','required|xss_clean');
			$this->form_validation->set_rules('project_short_desc','','required|xss_clean');
			$this->form_validation->set_rules('icon_class','','required|xss_clean');
			//$this->form_validation->set_rules('img_quote','','required|xss_clean');
			
			
			if($this->form_validation->run())
			{
				
				$space_id=$this->input->post('space_id',true);
				$title=$this->input->post('project_name',true);
				$quote =$this->input->post('project_quote',true);
				$project_short_desc =$this->input->post('project_short_desc',true);
				$icon_class = $this->input->post('icon_class',true);
				$quote_by = $this->input->post('quote_by',true);
				$img_quote = $this->input->post('img_quote',true);
				//print_r($_FILES);exit;

				$input_array = array(
									'project_title' =>  $title,
									'project_quote' =>  $quote,
									'project_short_desc' =>  $project_short_desc,
									'quote_by'=>$quote_by,
									'icon_class'=>$icon_class,
									'img_quote'=> $img_quote
									);

				//print_r($input_array);exit;

							if($data_inserted=$this->master_model->updateRecord('table_spaces',$input_array,array('id'=>$space_id)))
							{ 
								//$space_id = $this->db->insert_id();
								
								//echo $space_id;exit;
								//print_r($_FILES['project_icon_img']);exit;
								//$files = $_FILES;
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				//echo "<pre>";
			    				//print_r($_FILES['project_icon_img']['name']);exit;

								/*if($_FILES['project_icon_img']['name']!='')
								{
									//echo 123;exit;
									//var_dump($this->upload->do_upload('project_icon_img'));exit;
									if($this->upload->do_upload('project_icon_img'))
									{
										//echo 567; exit;
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('project_icon_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_spaces',$input_array,array('id'=>$space_id));
										//echo $this->db->last_query();exit;
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}*/

								if($_FILES['project_banner_img']['name']!='')
								{
									if($this->upload->do_upload('project_banner_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('project_banner_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_spaces',$input_array,array('id'=>$space_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}


								if($_FILES['project_mobile_banner_img']['name']!='')
								{
									if($this->upload->do_upload('project_mobile_banner_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('project_mobile_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_spaces',$input_array,array('id'=>$space_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}



								$this->session->set_flashdata('success','Spaces edited Successfully');			
								redirect(base_url().'superadmin/admin/listSpaces/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/edit_spaces',$data);
	}


	public function editBanner()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='The A | Edit Banner';

	  $data['success']=$data['error']="";
	  $front_id=base64_decode($this->uri->segment('4'));
	  $data['pagetitle']='The A | Edit Banner';
	  $spaces_data =   $this->master_model->getRecords('table_banner',array('id'=>$front_id),'table_banner.*'); 
	   $data['spaces_data'] = $spaces_data;
		 if(isset($_POST['btn_submit']))
		 {


			$this->form_validation->set_rules('title','','required|xss_clean');
			//$this->form_validation->set_rules('project_icon_img','','required|xss_clean');
			$this->form_validation->set_rules('v_tour_link','','required|xss_clean');
			$this->form_validation->set_rules('description','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				
				$title=$this->input->post('title',true);
				$v_tour_link =$this->input->post('v_tour_link',true);
				$description =$this->input->post('description',true);
				$banner_id=$this->input->post('banner_id',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									'title' =>  $title,
									'v_tour_link' =>  $v_tour_link,
									'description' =>  $description
									);
				//print_r($input_array);exit;

							if($data_inserted=$this->master_model->updateRecord('table_banner',$input_array,array('id'=>$banner_id)))
							{ 
								//$space_id = $this->db->insert_id();
								
								//echo $space_id;exit;
								//print_r($_FILES['project_icon_img']);exit;
								//$files = $_FILES;
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				//echo "<pre>";
			    				//print_r($_FILES['project_icon_img']['name']);exit;

								

								if($_FILES['project_banner_img']['name']!='')
								{
									if($this->upload->do_upload('project_banner_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('banner_large_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_banner',$input_array,array('id'=>$banner_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}


								if($_FILES['project_mobile_banner_img']['name']!='')
								{
									if($this->upload->do_upload('project_mobile_banner_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('banner_mobile_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_banner',$input_array,array('id'=>$banner_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}



								$this->session->set_flashdata('success','Banner edited Successfully');			
								redirect(base_url().'superadmin/admin/listBanner/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/edit_banner',$data);
	}


	public function editLocation()
	{
	  
	  $data['success']=$data['error']="";
	  $front_id=base64_decode($this->uri->segment('4'));
	  $data['pagetitle']='The A | Edit Location';
	  $spaces_data =   $this->master_model->getRecords('table_location',array('id'=>$front_id),'table_location.*'); 
	   $data['spaces_data'] = $spaces_data;
		 if(isset($_POST['btn_submit']))
		 {


			$this->form_validation->set_rules('title','','required|xss_clean');
			//$this->form_validation->set_rules('project_icon_img','','required|xss_clean');
			$this->form_validation->set_rules('loc_type','','required|xss_clean');
			$this->form_validation->set_rules('distance','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				
				$title=$this->input->post('title',true);
				$type =$this->input->post('loc_type',true);
				$distance =$this->input->post('distance',true);
				$location_id=$this->input->post('location_id',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									'title' =>  $title,
									'type' =>  $type,
									'distance' =>  $distance
									);
				//print_r($input_array);exit;

							if($data_inserted=$this->master_model->updateRecord('table_location',$input_array,array('id'=>$location_id)))
							{ 
								//$space_id = $this->db->insert_id();
								
								//echo $space_id;exit;
								//print_r($_FILES['project_icon_img']);exit;
								//$files = $_FILES;
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				//echo "<pre>";
			    				//print_r($_FILES['project_icon_img']['name']);exit;

								

								if($_FILES['project_banner_img']['name']!='')
								{
									if($this->upload->do_upload('project_banner_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_location',$input_array,array('id'=>$location_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}


								


								$this->session->set_flashdata('success','Location edited Successfully');			
								redirect(base_url().'superadmin/admin/listLocation/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/edit_location',$data);
	}

	public function editPartnerLogo()
	{
		$data['success']=$data['error']="";
	  $front_id=base64_decode($this->uri->segment('4'));
	  $data['pagetitle']='The A | Edit Logo';
	  $spaces_data =   $this->master_model->getRecords('table_partner_logo',array('id'=>$front_id),'table_partner_logo.*'); 
	   $data['spaces_data'] = $spaces_data;
		 if(isset($_POST['btn_submit']))
		 {


			 //$this->form_validation->set_rules('logo_url','','required|xss_clean');
			//$this->form_validation->set_rules('project_icon_img','','required|xss_clean');
			$this->form_validation->set_rules('alt_text','','required|xss_clean');
			//$this->form_validation->set_rules('distance','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				
				$logo_url=$this->input->post('logo_url',true);
				$alt_text =$this->input->post('alt_text',true);
				//$distance =$this->input->post('distance',true);
				$logo_id=$this->input->post('logo_id',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									'title'=>$logo_url,
									'alt_text' =>  $alt_text
									);
				//print_r($input_array);exit;

							if($data_inserted=$this->master_model->updateRecord('table_partner_logo',$input_array,array('id'=>$logo_id)))
							{ 
								//$space_id = $this->db->insert_id();
								
								//echo $space_id;exit;
								//print_r($_FILES['project_icon_img']);exit;
								//$files = $_FILES;
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				//echo "<pre>";
			    				//print_r($_FILES['project_icon_img']['name']);exit;

								

								if($_FILES['project_banner_img']['name']!='')
								{
									if($this->upload->do_upload('project_banner_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_partner_logo',$input_array,array('id'=>$logo_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}


								


								$this->session->set_flashdata('success','Logo edited Successfully');			
								redirect(base_url().'superadmin/admin/listPartnerLogo/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/edit_partner_logo',$data);
	}



	public function editClub()
	{
		$data['success']=$data['error']="";
	  $front_id=base64_decode($this->uri->segment('4'));
	  $data['pagetitle']='The A | Edit Club';
	  $spaces_data =   $this->master_model->getRecords('table_club',array('id'=>$front_id),'table_club.*'); 
	   $data['spaces_data'] = $spaces_data;
		 if(isset($_POST['btn_submit']))
		 {


			 //$this->form_validation->set_rules('logo_url','','required|xss_clean');
			//$this->form_validation->set_rules('project_icon_img','','required|xss_clean');
			$this->form_validation->set_rules('title','','required|xss_clean');
			//$this->form_validation->set_rules('distance','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				
				$logo_url=$this->input->post('logo_url',true);
				$title =$this->input->post('title',true);
				//$distance =$this->input->post('distance',true);
				$club_id=$this->input->post('club_id',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									'url'=>$logo_url,
									'title' =>  $title
									);
				//print_r($input_array);exit;

							if($data_inserted=$this->master_model->updateRecord('table_club',$input_array,array('id'=>$club_id)))
							{ 
								//$space_id = $this->db->insert_id();
								
								//echo $space_id;exit;
								//print_r($_FILES['project_icon_img']);exit;
								//$files = $_FILES;
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				//echo "<pre>";
			    				//print_r($_FILES['project_icon_img']['name']);exit;

								

								if($_FILES['project_banner_img']['name']!='')
								{
									if($this->upload->do_upload('project_banner_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_club',$input_array,array('id'=>$club_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}


								


								$this->session->set_flashdata('success','Club edited Successfully');			
								redirect(base_url().'superadmin/admin/listClub/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/edit_club',$data);
	}


	public function editMembership()
	{
		$data['success']=$data['error']="";
	  $front_id=base64_decode($this->uri->segment('4'));
	  $data['pagetitle']='The A | Edit Membership';
	  $spaces_data =   $this->master_model->getRecords('table_membership',array('id'=>$front_id),'table_membership.*'); 
	   $data['spaces_data'] = $spaces_data;
		 if(isset($_POST['btn_submit']))
		 {


			$this->form_validation->set_rules('title_big','','required|xss_clean');
			 //$this->form_validation->set_rules('title_small','','required|xss_clean');
			$this->form_validation->set_rules('description','','required|xss_clean');
			//$this->form_validation->set_rules('distance','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				
				$title_big=$this->input->post('title_big',true);
				$title_small =$this->input->post('title_small',true);
				$description =$this->input->post('description',true);
				$member_type =$this->input->post('member_type',true);
				$plan_id=$this->input->post('plan_id',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									'title_big' =>  $title_big,
									'title_small' =>  $title_small,
									'description' => $description
									);
				//print_r($input_array);exit;

							if($data_inserted=$this->master_model->updateRecord('table_membership',$input_array,array('id'=>$plan_id)))
							{ 
		
								$this->session->set_flashdata('success','Plan  edited Successfully');			
								redirect(base_url().'superadmin/admin/listMembership/'.$member_type);
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/edit_membership',$data);
	}


	public function edit_mem_banner_section()
	{
	  $data['success']=$data['error']="";
	  $front_id=base64_decode($this->uri->segment('4'));
	  //echo $front_id;exit;
	  $data['pagetitle']='The A | Edit Banner';
	  $spaces_data =   $this->master_model->getRecords('table_mem_banner',array('id'=>$front_id),'table_mem_banner.*'); 
	  //print_r($spaces_data);exit;
	   $data['spaces_data'] = $spaces_data;
		 if(isset($_POST['btn_submit']))
		 {


			$this->form_validation->set_rules('title','','required|xss_clean');
			//$this->form_validation->set_rules('project_icon_img','','required|xss_clean');
			$this->form_validation->set_rules('description','','required|xss_clean');
			//$this->form_validation->set_rules('distance','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				
				$title=$this->input->post('title',true);
				$description =$this->input->post('description',true);
				//$distance =$this->input->post('distance',true);
				$banner_id=$this->input->post('banner_id',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									'title' =>  $title,
									'description' =>  $description
									);
				//print_r($input_array);exit;

							if($data_inserted=$this->master_model->updateRecord('table_mem_banner',$input_array,array('id'=>$banner_id)))
							{ 
								//$space_id = $this->db->insert_id();
								
								//echo $space_id;exit;
								//print_r($_FILES['project_icon_img']);exit;
								//$files = $_FILES;
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				//echo "<pre>";
			    				//print_r($_FILES['project_icon_img']['name']);exit;

								

								if($_FILES['project_banner_img']['name']!='')
								{
									if($this->upload->do_upload('project_banner_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_mem_banner',$input_array,array('id'=>$banner_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}


								


								$this->session->set_flashdata('success','Banner edited Successfully');			
								redirect(base_url().'superadmin/admin/dashboard/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/edit_mem_banner_section',$data);	
	}

	public function editmemImage()
	{
		$data['success']=$data['error']="";
	  $front_id=base64_decode($this->uri->segment('4'));
	  //echo $front_id;exit;
	  $data['pagetitle']='The A | Edit Images';
	  $spaces_data =   $this->master_model->getRecords('table_mem_image',array('id'=>$front_id),'table_mem_image.*'); 
	  //print_r($spaces_data);exit;
	   $data['spaces_data'] = $spaces_data;
		 if(isset($_POST['btn_submit']))
		 {

		 		$title=$this->input->post('title',true);
				$description =$this->input->post('description',true);
				//$distance =$this->input->post('distance',true);
				$banner_id=$this->input->post('banner_id',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									'title' =>  $title,
									'description' =>  $description
									);
				$data_inserted=$this->master_model->updateRecord('table_mem_image',$input_array,array('id'=>$banner_id));	
				
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				//echo "<pre>";
			    				//print_r($_FILES['project_icon_img']['name']);exit;

								

								
								if($_FILES['project_banner_img_1']['name']!='')
								{
									if($this->upload->do_upload('project_banner_img_1'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name_1'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_mem_image',$input_array,array('id'=>$banner_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}


								if($_FILES['project_banner_img_2']['name']!='')
								{
									if($this->upload->do_upload('project_banner_img_2'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name_2'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_mem_image',$input_array,array('id'=>$banner_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}


								if($_FILES['project_banner_img_3']['name']!='')
								{
									if($this->upload->do_upload('project_banner_img_3'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name_3'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_mem_image',$input_array,array('id'=>$banner_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}


								if($_FILES['project_banner_img_4']['name']!='')
								{
									if($this->upload->do_upload('project_banner_img_4'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name_4'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_mem_image',$input_array,array('id'=>$banner_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}


								if($_FILES['project_banner_img_5']['name']!='')
								{
									if($this->upload->do_upload('project_banner_img_5'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name_5'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_mem_image',$input_array,array('id'=>$banner_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}





								


								$this->session->set_flashdata('success','Image edited Successfully');			
								redirect(base_url().'superadmin/admin/dashboard/');
							
						
					
		  }		
	  
	  $this->load->view('admin/edit_mem_image',$data);	
	}



	public function editOurSpaces()
	{
		$data['success']=$data['error']="";
	  $front_id=base64_decode($this->uri->segment('4'));
	  //echo $front_id;exit;
	  $data['pagetitle']='The A | Edit Our Spaces';
	  $spaces_data =   $this->master_model->getRecords('table_content',array('id'=>$front_id),'table_content.*'); 
	  //print_r($spaces_data);exit;
	   $data['spaces_data'] = $spaces_data;
		 if(isset($_POST['btn_submit']))
		 {

		 		
				$description =$this->input->post('description',true);
				//$distance =$this->input->post('distance',true);
				$content_id=$this->input->post('content_id',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									
									'content' =>  $description
									);
				if($data_inserted=$this->master_model->updateRecord('table_content',$input_array,array('id'=>$content_id)))
							{ 

								$this->session->set_flashdata('success','Our Spaces content edited Successfully');			
								redirect(base_url().'superadmin/admin/listOurSpaces/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}	
				

					
		  }		
	  
	  $this->load->view('admin/edit_ourspaces',$data);	
	}


	public function editTestimonial()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='The A | Edit Testimonial';
	  $front_id=base64_decode($this->uri->segment('4'));
	   $spaces_data =   $this->master_model->getRecords('table_testimonial',array('id'=>$front_id),'table_testimonial.*'); 
	  //print_r($spaces_data);exit;
	   $data['spaces_data'] = $spaces_data;

		 if(isset($_POST['btn_submit']))
		 {


			$this->form_validation->set_rules('testimonial','','required|xss_clean');
			//$this->form_validation->set_rules('project_icon_img','','required|xss_clean');
			$this->form_validation->set_rules('testimonial_by','','required|xss_clean');
			
			if($this->form_validation->run())
			{
				
				$testimonial=$this->input->post('testimonial',true);
				$testimonial_by =$this->input->post('testimonial_by',true);
				$testimonial_id = $this->input->post('testimonial_id',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									'testimonial' =>  $testimonial,
									'testimonial_by' =>  $testimonial_by
									
									);
				//print_r($input_array);exit;
				

							if($data_inserted=$this->master_model->updateRecord('table_testimonial',$input_array,array('id'=>$testimonial_id)))
							{ 

								$this->session->set_flashdata('success','Testimonial edit Successfully');			
								redirect(base_url().'superadmin/admin/listTestimonial/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/edit_testimonial',$data);
	}

	public function editArticle(){
	  $front_id=base64_decode($this->uri->segment('4'));		
	  $data['success']=$data['error']="";
	  $data['pagetitle']='The A | Edit Article';
	   //$project_gal_data =   $this->master_model->getRecords('table_pdf',array('investor_id'=>$front_id),'table_pdf.*'); 
	   $article_data =   $this->master_model->getRecords('table_article',array('id'=>$front_id),'table_article.*'); 
	   //print_r($article_data);exit;
	    $data['article_data'] = $article_data;
	  
	  //$data['project_gal_data'] = $project_gal_data;
	  if(isset($_POST['btn_submit']))
		 {
		 	$this->form_validation->set_rules('project_name','','required|xss_clean');
		 	$this->form_validation->set_rules('project_short_desc','','required|xss_clean');
		 	$this->form_validation->set_rules('publish_date','','required|xss_clean');
		 	if($this->form_validation->run())
			{
				$title=$this->input->post('project_name',true);
				$desc = $this->input->post('project_short_desc',true);
				$publish_date = $this->input->post('publish_date',true);
				$article_id = $this->input->post('article_id',true);
				$input_array_data=array(
										'title'=>$title,
										'description' => $desc ,
										'publish_date'=>$publish_date
										);
				$data_inserted=$this->master_model->updateRecord('table_article',$input_array_data,array('id'=>$article_id));

					
					if($_FILES['project_banner_img']['name']!='')
					{
						$config=array('upload_path'=>'uploads/',
					          'allowed_types'=>'jpg|jpeg|gif|png',
					          'file_name'=>rand(1,9999),'max_size'=>0);
			    		$this->upload->initialize($config);
						if($this->upload->do_upload('project_banner_img'))
						{
						  $dt=$this->upload->data();
						  $file=$dt['file_name'];
						  $banner_arr=array('img_name'=>$file);	
			
						$banner_img_updated=$this->master_model->updateRecord('table_article',$banner_arr,array('id'=>$article_id));
							
						}
						else
						{
							$file="";
							$this->session->set_flashdata('error',$this->upload->display_errors());
							$data['error']=$this->upload->display_errors();
						}
					}


				$count_gal = count($_FILES['project_gal_img']['name']);
								$files = $_FILES;
								if($_FILES['project_gal_img']['name'][0]!=""){

									for($i=0; $i<$count_gal; $i++)
									{
										$config_rec=array('upload_path'=>'uploads/',
								          'allowed_types'=>'pdf',
								          //'file_name'=>md5(rand(1,9999)),
								          'file_name'=>$files['project_gal_img']['name'][$i],
								          'max_size'=>10000);
							    		$this->upload->initialize($config_rec);

							    		$_FILES['project_gal_img']['name'] = $files['project_gal_img']['name'][$i];
								        $_FILES['project_gal_img']['type']= $files['project_gal_img']['type'][$i];
								        $_FILES['project_gal_img']['tmp_name']= $files['project_gal_img']['tmp_name'][$i];
								        $_FILES['project_gal_img']['error']= $files['project_gal_img']['error'][$i];
								        $_FILES['project_gal_img']['size']= $files['project_gal_img']['size'][$i]; 
								        if($this->upload->do_upload('project_gal_img'))
										{
										  $dt_gallery=$this->upload->data();
										  $file_gallery[]=$dt_gallery['file_name'];

										  //$res = $this->master_model->createThumb($file,'uploads/profile/',200,200,FALSE);
										  
										}else
										{
											$this->session->set_flashdata('error',$this->upload->display_errors());
										}
									}
									//print_r($file_gallery);exit;
									for($m=0;$m<count($file_gallery);$m++)
											{
												$input_array_gal = array('id'=>$article_id,'pdf_file_name'=>$file_gallery[$m]);
												//$this->master_model->insertRecord('table_article',$input_array_gal)	;
												$this->master_model->updateRecord('table_article',$input_array_gal,array('id'=>$article_id));
												//echo $this->db->last_query();exit;
											}
								}
					
				$this->session->set_flashdata('success','Article Updated Successfully');			
				redirect(base_url().'superadmin/admin/listArticle/');

			}
			else
			{
				$this->session->set_flashdata('error',$this->upload->display_errors());
				$data['error']=$this->form_validation->error_string();
			}
		 }
	   $this->load->view('admin/edit_article',$data);
	}
	

	public function editProject()
	{
	  $front_id=base64_decode($this->uri->segment('4'));		
	  $data['success']=$data['error']="";
	  $data['pagetitle']='SMC | Edit Project';
	  $country_data =   $this->master_model->getRecords('table_country',array(),'table_country.*'); 
	  $project_data =   $this->master_model->getRecords('table_project',array('project_id'=>$front_id),'table_project.*'); 
	  $project_gal_data =   $this->master_model->getRecords('table_gallery',array('project_id'=>$front_id),'table_gallery.*'); 
	  //var_dump($project_gal_data);
	  $cat_data =   $this->master_model->getRecords('table_category',array(),'table_category.*'); 
	  $data['country_data'] = $country_data;
	  $data['project_data'] = $project_data;
	   $data['cat_data'] = $cat_data;
	  $data['project_gal_data'] = $project_gal_data;
		 if(isset($_POST['btn_submit']))
		 {


			$this->form_validation->set_rules('project_name','','required|xss_clean');
			$this->form_validation->set_rules('select_country','','required|xss_clean');
			$this->form_validation->set_rules('project_short_desc','','required|xss_clean');
			$this->form_validation->set_rules('status','','required|xss_clean');
			$this->form_validation->set_rules('key_project','','required|xss_clean');
			//$this->form_validation->set_rules('client_name','','required|xss_clean');
			
			//$this->form_validation->set_rules('work_url_youtube','Youtube Url','required|xss_clean');

			//$this->form_validation->set_rules('password','','required|xss_clean');
			
			if($this->form_validation->run())
			{
				
				$project_name=$this->input->post('project_name',true);
				$project_name_slug = url_title($project_name, 'dash', true);
				$project_short_desc=$this->input->post('project_short_desc',true);
				$project_des_left =$this->input->post('project_des_left',true);
				$project_des_right =$this->input->post('project_des_right',true);

				$project_heading1 =$this->input->post('project_heading1',true);
				$project_heading2 =$this->input->post('project_heading2',true);
				$status =$this->input->post('status',true);
				$key_project =$this->input->post('key_project',true);
				$banner_heading1 =$this->input->post('banner_heading1',true);
				$banner_heading2 =$this->input->post('banner_heading2',true);

				$project_id = $this->input->post('project_id',true);
				//$select_country=$this->input->post('select_country',true);
				
				$select_country=$this->input->post('select_country',true);
				$select_expertise=$this->input->post('select_expertise',true);
				
				//$select_country = implode(",",$select_country);
				
				$date = date('Y-m-d H:i:s', time());
				$config=array('upload_path'=>'uploads/',
					          'allowed_types'=>'jpg|jpeg|gif|png',
					          'file_name'=>rand(1,9999),'max_size'=>0);
			    $this->upload->initialize($config);

			    $input_array_data=array('project_name'=>$project_name,'project_name_slug'=>$project_name_slug,'heading1'=>$project_heading1,'heading2'=>$project_heading2,'project_short_desc'=>$project_short_desc,'project_des'=>$project_des_left,'right_desc'=>$project_des_right,'country_id'=>$select_country,'status'=>$status,"cat_id"=>$select_expertise,'banner_heading1'=>$banner_heading1,'banner_heading2'=>$banner_heading2,'key_project'=>$key_project);
			    $data_inserted=$this->master_model->updateRecord('table_project',$input_array_data,array('project_id'=>$project_id));


			    if($_FILES['project_featured_img']['name']!='')
					{
						if($this->upload->do_upload('project_featured_img'))
						{
						  $dt=$this->upload->data();
						  $file=$dt['file_name'];
						  $input_array=array('project_featured_img'=>$file);	
			
						$featured_img_updated=$this->master_model->updateRecord('table_project',$input_array,array('project_id'=>$project_id));
							
						}
						else
						{
							$file="";
							$this->session->set_flashdata('error',$this->upload->display_errors());
							$data['error']=$this->upload->display_errors();
						}
					}

					 if($_FILES['project_banner_img']['name']!='')
					{
						if($this->upload->do_upload('project_banner_img'))
						{
						  $dt=$this->upload->data();
						  $file=$dt['file_name'];
						  $input_array=array('project_banner_img'=>$file);	
			
						$banner_img_updated=$this->master_model->updateRecord('table_project',$input_array,array('project_id'=>$project_id));
							
						}
						else
						{
							$file="";
							$this->session->set_flashdata('error',$this->upload->display_errors());
							$data['error']=$this->upload->display_errors();
						}
					}

					$count_gal = count($_FILES['project_gal_img']['name']);
								$files = $_FILES;
								if($_FILES['project_gal_img']['name'][0]!=""){

									for($i=0; $i<$count_gal; $i++)
									{
										$config_rec=array('upload_path'=>'uploads/',
								          'allowed_types'=>'jpg|jpeg|gif|png',
								          'file_name'=>md5(rand(1,9999)),'max_size'=>10000);
							    		$this->upload->initialize($config_rec);

							    		$_FILES['project_gal_img']['name'] = $files['project_gal_img']['name'][$i];
								        $_FILES['project_gal_img']['type']= $files['project_gal_img']['type'][$i];
								        $_FILES['project_gal_img']['tmp_name']= $files['project_gal_img']['tmp_name'][$i];
								        $_FILES['project_gal_img']['error']= $files['project_gal_img']['error'][$i];
								        $_FILES['project_gal_img']['size']= $files['project_gal_img']['size'][$i]; 
								        if($this->upload->do_upload('project_gal_img'))
										{
										  $dt_gallery=$this->upload->data();
										  $file_gallery[]=$dt_gallery['file_name'];

										  //$res = $this->master_model->createThumb($file,'uploads/profile/',200,200,FALSE);
										  
										}else
										{
											$this->session->set_flashdata('error',$this->upload->display_errors());
										}
									}

									for($m=0;$m<count($file_gallery);$m++)
											{
												$input_array_gal = array('project_id'=>$project_id,'img_name'=>$file_gallery[$m]);
												$this->master_model->insertRecord('table_gallery',$input_array_gal)	;
											}
								}
					
				$this->session->set_flashdata('success','Project Updated Successfully');			
				redirect(base_url().'superadmin/admin/listProject/'.base64_encode($select_country));				
				
			}else
			{
				$this->session->set_flashdata('error',$this->upload->display_errors());
				$data['error']=$this->form_validation->error_string();
			}
		  }		
	  
	  $this->load->view('admin/edit_project',$data);
	}
	

	public function editExpertise()
	{
	  $front_id=base64_decode($this->uri->segment('4'));		
	  $data['success']=$data['error']="";
	  $data['pagetitle']='SMC | Edit Expertise';
	  $category_data =   $this->master_model->getRecords('table_category',array('cat_id'=>$front_id),'table_category.*'); 
	  $data['category_data'] = $category_data;
	  
		 if(isset($_POST['btn_submit']))
		 {
			$this->form_validation->set_rules('cat_name','','required|xss_clean');
			$this->form_validation->set_rules('cat_id','','required|xss_clean');
			
			if($this->form_validation->run())
			{
				$cat_name=$this->input->post('cat_name',true);
				$cat_name_slug = url_title($cat_name, 'dash', true);
				$cat_id=$this->input->post('cat_id',true);
				$old_image=$this->input->post('old_img',true);

				//$password=$this->input->post('password',true);
				$date = date('Y-m-d H:i:s', time());
				$config=array('upload_path'=>'uploads/',
					          'allowed_types'=>'jpg|jpeg|gif|png',
					          'file_name'=>rand(1,9999),'max_size'=>0);
			    $this->upload->initialize($config);
			    if($_FILES['cat_featured_image']['name']!='')
					{
						if($this->upload->do_upload('cat_featured_image'))
						{
						  $dt=$this->upload->data();
						  $file=$dt['file_name'];

						}
						else
						{
							$file=$old_image;
							$data['error']=$this->upload->display_errors();
						}
					}else
					{	
						
						$file= $old_image;
					}
					
					 $input_array=array('cat_name'=>$cat_name,'cat_slug'=>$cat_name_slug,'cat_featured_image'=>$file);
			
							if($user_info=$this->master_model->updateRecord('table_category',$input_array,array('cat_id'=>$cat_id)))
							{ 
								if($_FILES['cat_featured_image']['name']!='')
								{
									@unlink('uploads/'.$old_image);
								}
								$this->session->set_flashdata('success','Expertise edited Successfully');			
								redirect(base_url().'superadmin/admin/listExpertise/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}					

			}
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
		  }		
	  
	  $this->load->view('admin/edit_expertise',$data);
	}

	public function editCountry()
	{
		$front_id=base64_decode($this->uri->segment('4'));
	  $data['success']=$data['error']="";
	  $data['pagetitle']='SMC | Edit Country';
	  $result=$this->master_model->getRecords('table_country',array('country_id'=>$front_id),'table_country.*'); 
	  $data['country_data'] = $result;
		 if(isset($_POST['btn_submit']))
		 {
			$this->form_validation->set_rules('country_name','','required|xss_clean');
			//$this->form_validation->set_rules('password','','required|xss_clean');
			if($this->form_validation->run())
			{
				$country_name=$this->input->post('country_name',true);
				$country_name_slug = url_title($artist_name, 'dash', true);
				$country_id=$this->input->post('country_id',true);
				$input_array = array('country_name'=>$country_name);
					
					if($this->master_model->updateRecord('table_country',$input_array,array('country_id'=>$country_id)))
					{ 
						
						$this->session->set_flashdata('success','Country edited Successfully');			
						redirect(base_url().'superadmin/admin/listCountry/');
					}
					else
					{	
						$this->session->set_flashdata('error','Something went wrong ,try again later');	
						 redirect(base_url().'superadmin/admin/editCountry/'.base64_encode($artist_id));
					}
				//$user_info=$this->master_model->insertRecord('table_artist',$input_array);
				
			}
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
		  }		
	  
	  $this->load->view('admin/edit_country',$data);
	}

	

	public function editCms()
	{
	  $data['pagetitle'] = "SMC | Edit CMS";
	  $front_id=base64_decode($this->uri->segment('4'));
	  $data['success']=$data['error']="";
	  //$data['pagetitle']=' | Edit CMS';
	  $result=$this->master_model->getRecords('table_cms',array('cms_id'=>$front_id),'table_cms.*'); 
	  $data['cms_data'] = $result;
		 if(isset($_POST['btn_submit']))
		 {
			
			//$this->form_validation->set_rules('password','','required|xss_clean');

				$cms_content=mysql_real_escape_string($this->input->post('content'));
				$cms_id=$this->input->post('cms_id',true);
				$old_image=$this->input->post('old_image',true);
				//var_dump($_POST);
				//$password=$this->input->post('password',true);
				
				$date = date('Y-m-d H:i:s', time());
				$config=array('upload_path'=>'uploads/',
					          'allowed_types'=>'jpg|jpeg|gif|png',
					          'file_name'=>rand(1,9999),'max_size'=>0);
			    $this->upload->initialize($config);
			    if($_FILES['work_image']['name']!='')
					{
						if($this->upload->do_upload('work_image'))
						{
						  $dt=$this->upload->data();
						  $file=$dt['file_name'];

						}
						else
						{
							$file=$old_image;
							$data['error']=$this->upload->display_errors();
						}
					}else
					{
						$file=$old_image;
					}
				$input_array=array('content'=>$cms_content,'logo_img'=>$file,'date_added'=>$date);	
				
				//$user_info=$this->master_model->insertRecord('table_artist',$input_array);
				if($this->master_model->updateRecord('table_cms',$input_array,array('cms_id'=>$cms_id)))
				{ 
					//echo $this->db->last_query();
					if($_FILES['file_upload']['name']!='')
								{
									@unlink('uploads/admin/'.$old_image);
								}
					$this->session->set_flashdata('success','Page edited Successfully');			
					redirect(base_url().'superadmin/admin/listCms/');
				}
				else
				{
					 $data['error']='Something went wrong ,try again later';
				}
			
		  }		
	  
	  $this->load->view('admin/edit_cms',$data);
	}


	public function editHomeImagetablet()
	{

	  
	  $data['success']=$data['error']="";
	  $front_id=base64_decode($this->uri->segment('4'));
	  $data['pagetitle']='SMC | Edit Home Page Image Tablet';
	  $result=$this->master_model->getRecords('table_home_images',array('image_id'=>$front_id),'table_home_images.*'); 
	  $data['artist_data'] = $result;
		 if(isset($_POST['btn_submit']))
		 {
			
			
				
				$old_image=$this->input->post('old_image',true);
				$image_id=$this->input->post('image_id',true);
				//$password=$this->input->post('password',true);
				$date = date('Y-m-d H:i:s', time());
				$config=array('upload_path'=>'uploads/',
					          'allowed_types'=>'svg|jpg|jpeg|gif|png',
					          'file_name'=>rand(1,9999),'max_size'=>0);
			    $this->upload->initialize($config);
			    if($_FILES['work_image']['name']!='')
					{
						if($this->upload->do_upload('work_image'))
						{
						  $dt=$this->upload->data();
						  $file=$dt['file_name'];

						}
						else
						{
							$file=$old_image;
							//$data['error']=$this->upload->display_errors();
							$this->session->set_flashdata('error',$this->upload->display_errors());	
						 redirect(base_url().'superadmin/admin/editHomeImagetablet/'.base64_encode($image_id));
						}
					}else
					{
						$file=$old_image;
					}
				$input_array=array('image_name_tablet'=>$file,'date_added'=>$date);	
					
					if($this->master_model->updateRecord('table_home_images',$input_array,array('image_id'=>$image_id)))
					{ 
						if($_FILES['file_upload']['name']!='')
								{
									@unlink('uploads/admin/'.$old_image);
								}
						$this->session->set_flashdata('success','Image edited Successfully');			
						redirect(base_url().'superadmin/admin/listHometablet/');
					}
					else
					{	
						$this->session->set_flashdata('error','Something went wrong ,try again later');	
						 redirect(base_url().'superadmin/admin/editHomeImagetablet/'.base64_encode($image_id));
						//redirect(base_url().'superadmin/admin/editHomeImagetablet/');
					}
				//$user_info=$this->master_model->insertRecord('table_artist',$input_array);
				
			}
			
		  	
	  
	  $this->load->view('admin/edit_HomeImagetablet',$data);
	}


	public function editHomeImagemobile()
	{

	  
	  $data['success']=$data['error']="";
	  $front_id=base64_decode($this->uri->segment('4'));
	  $data['pagetitle']='SMC | Edit Home Page Image mobile';
	  $result=$this->master_model->getRecords('table_home_images',array('image_id'=>$front_id),'table_home_images.*'); 
	  $data['artist_data'] = $result;
		 if(isset($_POST['btn_submit']))
		 {
			
			

				$old_image=$this->input->post('old_image',true);
				$image_id=$this->input->post('image_id',true);
				//$password=$this->input->post('password',true);
				$date = date('Y-m-d H:i:s', time());
				$config=array('upload_path'=>'uploads/',
					          'allowed_types'=>'svg|jpg|jpeg|gif|png',
					          'file_name'=>rand(1,9999),'max_size'=>0);
			    $this->upload->initialize($config);
			    if($_FILES['work_image']['name']!='')
					{
						if($this->upload->do_upload('work_image'))
						{
						  $dt=$this->upload->data();
						  $file=$dt['file_name'];

						}
						else
						{
							$file=$old_image;
							//$data['error']=$this->upload->display_errors();
							$this->session->set_flashdata('error',$this->upload->display_errors());	
						 redirect(base_url().'superadmin/admin/editHomeImagemobile/'.base64_encode($image_id));

						}
					}else
					{
						$file=$old_image;
					}
				$input_array=array('image_name_mobile'=>$file,'date_added'=>$date);	
					
					if($this->master_model->updateRecord('table_home_images',$input_array,array('image_id'=>$image_id)))
					{ 
						if($_FILES['file_upload']['name']!='')
								{
									@unlink('uploads/admin/'.$old_image);
								}
						$this->session->set_flashdata('success','Image edited Successfully');			
						redirect(base_url().'superadmin/admin/listHomemobile/');
					}
					else
					{	
						$this->session->set_flashdata('error','Something went wrong ,try again later');	
						 redirect(base_url().'superadmin/admin/editHomeImagemobile/'.base64_encode($image_id));
						//redirect(base_url().'superadmin/admin/editHomeImagetablet/');
					}
				//$user_info=$this->master_model->insertRecord('table_artist',$input_array);
				
			}
			
		  	
	  
	  $this->load->view('admin/edit_HomeImagemobile',$data);
	}

	

	public function deleteSpace()
	{
		 $space_id=$this->input->post('space_id',true);
		 $space_id=base64_decode($space_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_spaces','id',$space_id))
				{ 
					$this->session->set_flashdata('success','Space deleted successfully');
					//redirect(base_url().'superadmin/admin/listArtist');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		
	}

	public function deletePopup()
	{
		 $pop_id=$this->input->post('pop_id',true);
		 $pop_id=base64_decode($pop_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_popup','id',$pop_id))
				{ 
					$this->session->set_flashdata('success','Popup deleted successfully');
					//redirect(base_url().'superadmin/admin/listArtist');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		
	}

	public function deleteAd()
	{
		 $ad_id=$this->input->post('ad_id',true);
		 $ad_id=base64_decode($ad_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_ads','id',$ad_id))
				{ 
					$this->session->set_flashdata('success','Ad deleted successfully');
					//redirect(base_url().'superadmin/admin/listArtist');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		
	}

	public function enablePopup()
	{
		$pop_id=$this->input->post('pop_id',true);
		$update_order=array('is_enabled'=>1);
		 $this->master_model->updateRecord('table_popup',$update_order,array('id'=>$pop_id));

	}

	public function disablePopup()
	{
		$pop_id=$this->input->post('pop_id',true);
		$update_order=array('is_enabled'=>0);
		 $this->master_model->updateRecord('table_popup',$update_order,array('id'=>$pop_id));

	}


	public function enableAds()
	{
		$pop_id=$this->input->post('pop_id',true);
		$update_order=array('is_active'=>1);
		 $this->master_model->updateRecord('table_ads',$update_order,array('id'=>$pop_id));

	}

	public function disableAds()
	{
		$pop_id=$this->input->post('pop_id',true);
		$update_order=array('is_active'=>0);
		 $this->master_model->updateRecord('table_ads',$update_order,array('id'=>$pop_id));

	}


	public function deleteEnquiry()
	{
		 $enquiry_id=$this->input->post('enquiry_id',true);
		 $enquiry_id=base64_decode($enquiry_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_service','id',$enquiry_id))
				{ 
					$this->session->set_flashdata('success','Enquiry deleted successfully');
					//redirect(base_url().'superadmin/admin/listArtist');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		
	}


	public function deleteClub()
	{
		 $enquiry_id=$this->input->post('club_id',true);
		 $enquiry_id=base64_decode($enquiry_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_club','id',$enquiry_id))
				{ 
					$this->session->set_flashdata('success','Club deleted successfully');
					//redirect(base_url().'superadmin/admin/listArtist');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		
	}
	


	public function deleteTestimonial()
	{
		 $test_id=$this->input->post('test_id',true);
		 $test_id=base64_decode($test_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_testimonial','id',$test_id))
				{ 
					$this->session->set_flashdata('success','Testimonial deleted successfully');
					//redirect(base_url().'superadmin/admin/listArtist');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		
	}


	public function deleteBanner()
	{
		 $banner_id=$this->input->post('banner_id',true);
		 $banner_id=base64_decode($banner_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_banner','id',$banner_id))
				{ 
					$this->session->set_flashdata('success','Banner deleted successfully');
					//redirect(base_url().'superadmin/admin/listArtist');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		
	}

	public function deleteMembership()
	{
		 $member_id=$this->input->post('member_id',true);
		 //$member_type=$this->input->post('member_tyoe',true);
		 $member_id=base64_decode($member_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_membership','id',$member_id))
				{ 
					$this->session->set_flashdata('success','Membership Plan deleted successfully');
					//redirect(base_url().'superadmin/admin/listArtist');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		
	}
	

	public function deleteLocation()
	{
		 $location_id=$this->input->post('location_id',true);
		 $location_id=base64_decode($location_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_location','id',$location_id))
				{ 
					$this->session->set_flashdata('success','Location deleted successfully');
					//redirect(base_url().'superadmin/admin/listArtist');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		
	}

	public function deleteLogo()
	{
		 $logo_id=$this->input->post('logo_id',true);
		 $logo_id=base64_decode($logo_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_partner_logo','id',$logo_id))
				{ 
					$this->session->set_flashdata('success','Logo deleted successfully');
					//redirect(base_url().'superadmin/admin/listArtist');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
	}
	

	public function deleteCategory()
	{
		
		 $cat_id=$this->input->post('cat_id',true);
		 $cat_id=base64_decode($cat_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_category','cat_id',$cat_id))
				{ 
					$this->session->set_flashdata('success','Expertise deleted successfully');
					//redirect(base_url().'superadmin/admin/listTeam');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		//redirect(base_url().'superadmin/admin/listTeam');
	}
	public function delete_gal_images()
	{
		
		 $investor_id=$this->input->post('investor_id',true);
		 //$image_id=base64_decode($image_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_files','id',$investor_id))
				{ 
					//unlink(base_url().'uploads/'.$image_id);
					//$this->session->set_flashdata('success','Image deleted successfully');
					//redirect(base_url().'superadmin/admin/listTeam');
					echo 1;
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		//redirect(base_url().'superadmin/admin/listTeam');
	}

	public function delete_article_images()
	{
		
		 $image_id=$this->input->post('filename',true);
		 //$image_id=base64_decode($image_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				/*if($this->master_model->deleteRecord('table_pdf','id',$image_id))
				{ 
					//unlink(base_url().'uploads/'.$image_id);
					//$this->session->set_flashdata('success','Image deleted successfully');
					//redirect(base_url().'superadmin/admin/listTeam');
					echo 1;
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	*/
		//redirect(base_url().'superadmin/admin/listTeam');
	}

	public function delete_article_pdf()
	{
		
		 $article_id=$this->input->post('article_id',true);
		 //$image_id=base64_decode($image_id);
		//echo $artist_id;
		 $input_array  = array('pdf_file_name' => "" );
		$this->master_model->updateRecord('table_article',$input_array,array('id'=>$article_id));
				if($this->master_model->updateRecord('table_article',$input_array,array('id'=>$article_id)))
				{ 
					//unlink(base_url().'uploads/'.$image_id);
					echo 'file deleted successfully';
					
				}else
				{
					echo 'Something went wrong ,try again later';
				}	
		//redirect(base_url().'superadmin/admin/listTeam');
	}
    
	public function deletesodaismImage()
	{
		
		 $image_id=$this->input->post('image_id',true);
		 $image_id=base64_decode($image_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_sodaism','image_id',$image_id))
				{ 
					$this->session->set_flashdata('success','Image deleted successfully');
					//redirect(base_url().'superadmin/admin/listTeam');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		//redirect(base_url().'superadmin/admin/listTeam');
	}
	
	public function deletework()
	{
		 $work_id=$this->input->post('work_id',true);
		 $work_id=base64_decode($work_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_project','project_id',$work_id))
				{ 
					$this->session->set_flashdata('success','Project deleted successfully');
					//redirect(base_url().'superadmin/admin/listWork');
				}else
				{
					$this->session->set_flashdata('error','Something went wrong ,try again later');
					//$data['error']='Something went wrong ,try again later';
				}	
		//redirect(base_url().'superadmin/admin/listWork');
	}


	public function deleteVillege()
	{
		 $villege_id=$this->input->post('villege_id',true);
		 $villege_id=base64_decode($villege_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_villege','id',$villege_id))
				{ 
					$this->session->set_flashdata('success','Info deleted successfully');
					//redirect(base_url().'superadmin/admin/listWork');
				}else
				{
					$this->session->set_flashdata('error','Something went wrong ,try again later');
					//$data['error']='Something went wrong ,try again later';
				}	
		//redirect(base_url().'superadmin/admin/listWork');
	}

	public function accountsetting()
	{
	  $data['success']=$data['error']=$data['success1']=$data['error1']=$data['success2']=$data['error2']='';
	  $data['pagetitle']='Lets learn India | Account Setting';	
	  if(isset($_POST['btn_account']))
	  {
		  $this->form_validation->set_rules('username','','required|xss_clean');
		  $this->form_validation->set_rules('email','','required|xss_clean|valid_email');
		   $this->form_validation->set_rules('phone','','required|xss_clean');
		  if($this->form_validation->run())
		  {
			 	$username=$this->input->post('username',true);
				$email=$this->input->post('email',true);
				$phone=$this->input->post('phone',true);
				$fax=$this->input->post('fax',true);
				$address=$this->input->post('address',true);
				$old_image=$this->input->post('old_image',true);
				$config=array('upload_path'=>'uploads/admin/',
					          'allowed_types'=>'jpg|jpeg|gif|png',
					          'file_name'=>rand(1,9999),'max_size'=>0);
			    $this->upload->initialize($config);
				
					if($_FILES['file_upload']['name']!='')
					{
						if($this->upload->do_upload('file_upload'))
						{
						  $dt=$this->upload->data();
						  $file=$dt['file_name'];
						}
						else
						{
							$file=$old_image;
							$data['error']=$this->upload->display_errors();
						}
					}
					else
					{
						$file=$old_image;
					}
					$input_array=array('admin_username'=>$username,'admin_email'=>$email,'admin_img'=>$file,'admin_phone'=>$phone,'admin_fax'=>$fax,'admin_address'=>$address);
					$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
					if($_FILES['file_upload']['name']!='')
					{
						@unlink('uploads/admin/'.$old_image);
					}
					$user_data=array('admin_img'=>$file);
					$this->session->set_userdata($user_data);
					$data['success']='Record Updated Successfully.';
		  }
		  else
		  {
			$data['error']=$this->form_validation->error_string();
		  }
	  }
	  if(isset($_POST['btn_password']))
	  {
		   	$this->form_validation->set_rules('current_pass','','required|xss_clean');
		  	$this->form_validation->set_rules('new_pass','New Password','required|xss_clean');
			$this->form_validation->set_rules('confirm_pass','Confirm Password','required|xss_clean|matches[new_pass]|max_length[6]');
			  if($this->form_validation->run())
			  {
				  	$current_pass=$this->input->post('current_pass',true);
					$new_pass=$this->input->post('new_pass',true);
					$row=$this->master_model->getRecordCount('admin_login',array('admin_password'=>$current_pass));
					if($row==0)
					{
						$data['error1']="Current Password is Wrong.";
					}
					else
					{
						$input_array=array('admin_password'=>$new_pass);
						$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
						$data['success1']='Password Updated Successfully.'; 
					}
			  }
			  else
			  {
				  $data['error1']=$this->form_validation->error_string();
			  }
	  }
	  if(isset($_POST['btn_status']))
	  {
		$site_status=$this->input->post('site_status',true);
		$input_array1=array('site_status'=>$site_status);
		$this->master_model->updateRecord('tbl_site_status',$input_array1,array('site_id'=>'1'));
	    $data['success2']='Site Status Changed Successfully.'; 
	  }
	  $data['result']=$this->master_model->getRecords('admin_login',array('admin_login.id'=>1),'admin_login.*');
	  $data['status']=$this->master_model->getRecords('tbl_site_status',array('site_id'=>1),'*');
	  $data['middle_content']='accountsetting';
	  $this->load->view('admin/common-file',$data);
	}
	/*
	  Function   : sociallink
	  Developer  : Dhananjay
	  Routes File: 'academymaster/sociallink' used by 'academymaster/admin/sociallink'
	  Description: Admin can update social link.    
	*/
	public function sociallink()
	{
	  $data['success']=$data['error']='';
	  $data['pagetitle']='Lets learn India | Sociallink';	
	  $data['social_link']=$this->master_model->getRecords('tbl_social',array('tbl_social.social_id'=>1),'tbl_social.*');
	  if(isset($_POST['btn_social']))
	  {
		 $this->form_validation->set_rules('facebook_link','','required|xss_clean|valid_url');
		 $this->form_validation->set_rules('twitter_link','','required|xss_clean|valid_url'); 
		 $this->form_validation->set_rules('linkedin_link','','required|xss_clean|valid_url');
		  $this->form_validation->set_rules('pinterest_link','','required|xss_clean|valid_url');
		 if($this->form_validation->run())
		 {
			 $facebook_link=$this->input->post('facebook_link',true);
			 $twitter_link=$this->input->post('twitter_link',true);
			 $linkedin_link=$this->input->post('linkedin_link',true);
			  $pinterest_link=$this->input->post('pinterest_link',true);
			 $update_link=array('facebook'=>$facebook_link,'linkedin'=>$linkedin_link,'twitter'=>$twitter_link,'pinterest'=>$pinterest_link);
		     if($this->master_model->updateRecord('tbl_social',$update_link,array('social_id'=>'1')))
			 {
				$this->session->set_flashdata('success','Social links updated successfully.'); 
				redirect(base_url().'superadmin/admin/sociallink/');
			 }
			 else
			 {
				$this->session->set_flashdata('error','Error while updating social link.'); 
				redirect(base_url().'superadmin/admin/sociallink/'); 
			 }
		}
	  }
	  $data['middle_content']='sociallink';
	  $this->load->view('admin/common-file',$data);
	}

	public function sortWork(){
		 $work_id=$this->input->post('work_id',true);
		 $sort_order=$this->input->post('order',true);	
		 $update_order=array('sort_order'=>$sort_order);
		 $this->master_model->updateRecord('table_work',$update_order,array('work_id'=>$work_id));
	}

	public function sortWorkorder()
	{
		
		$data_arr = json_decode($_POST['positions']);
		//echo "<pre>";
		//print_r($data_arr);
		for($i=0;$i<count($data_arr);$i++)
		{
			$work_id = $data_arr[$i][0];
			$update_order=array('sort_order'=>$data_arr[$i][1]);
		 	
		 	$this->master_model->updateRecord('table_work',$update_order,array('work_id'=>$work_id));
		 	
			 //{
			 	//echo $this->db->last_query()."<br>";
				//$this->session->set_flashdata('success','Work Order list updated successfully.'); 
				//redirect(base_url().'superadmin/admin/listWorkorder/'.$_POST['artist_id']);
			 //}
		}
		$this->session->set_flashdata('success','Work Order list updated successfully.'); 
			redirect(base_url().'superadmin/admin/listWorkorder/'.$_POST['artist_id']);
	}

	public function sortExpertise(){
		 $cat_id=$this->input->post('cat_id',true);
		 $sort_order=$this->input->post('order',true);	
		 $update_order=array('sort_order'=>$sort_order);
		 $this->master_model->updateRecord('table_category',$update_order,array('cat_id'=>$cat_id));
	}

	public function sortMember(){
		 $member_id=$this->input->post('member_id',true);
		 $sort_order=$this->input->post('order',true);	
		 $update_order=array('sort_order'=>$sort_order);
		 $this->master_model->updateRecord('table_member_master',$update_order,array('id'=>$member_id));
	}

	public function sortImage(){
		 $image_id=$this->input->post('image_id',true);
		 $sort_order=$this->input->post('order',true);	
		 $update_order=array('sort_order'=>$sort_order);
		 $this->master_model->updateRecord('table_home_images',$update_order,array('image_id'=>$image_id));
	}

	public function sortsodaismImage(){
		 $image_id=$this->input->post('image_id',true);
		 $sort_order=$this->input->post('order',true);	
		 $update_order=array('sort_order'=>$sort_order);
		 $this->master_model->updateRecord('table_sodaism',$update_order,array('image_id'=>$image_id));
	}

	

	public function sortTeam(){
		 $member_id=$this->input->post('member_id',true);
		 $sort_order=$this->input->post('order',true);	
		 $update_order=array('sort_order'=>$sort_order);
		 $this->master_model->updateRecord('table_team',$update_order,array('member_id'=>$member_id));
	}

	public function sortMembership(){
		 $cat_id=$this->input->post('member_id',true);
		 $sort_order=$this->input->post('order',true);	
		 $update_order=array('plan_order'=>$sort_order);
		 $this->master_model->updateRecord('table_membership',$update_order,array('id'=>$cat_id));
	}
}
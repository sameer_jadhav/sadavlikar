<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Generation extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();
	   $this->load->library('upload');  
	   //$this->load->model('email_sending');	
	   
	}

	public function listArea(){
		 $data['success']=$data['error']='';	
	  $data['pagetitle']='सडवली संघटन | वाडी यादी ';
	  //$data['middle_content']='list_artist';
	  $result = $this->master_model->getRecords('table_area',array(),'table_area.*'); 
	 // $result=$this->master_model->getRecords('table_country',array(1=>"1"),'*'); 
	  $data['villege'] = $result;
	 //echo 123;exit;
	  $this->load->view('admin/list_area',$data);
	}

	public function listFamily(){
		 $data['success']=$data['error']='';	
	  $data['pagetitle']='सडवली संघटन | वाडी यादी ';
	  //$data['middle_content']='list_artist';
	  $result = $this->master_model->getRecords('table_generation tg LEFT JOIN table_area as ta ON ta.id=tg.area_id',array(),'tg.*,ta.area_name'); 
	 // $result=$this->master_model->getRecords('table_country',array(1=>"1"),'*'); 
	  $data['villege'] = $result;
	 //echo 123;exit;
	  $this->load->view('admin/list_family',$data);
	}


	public function addFamily(){

		$data['success']=$data['error']="";
	  	$data['pagetitle']='सडवली संघटन | भावकी add करा ';
	  	 $data['area_res'] = $this->master_model->getRecords('table_area',array(),'table_area.*');
	  	 if(isset($_POST['btn_submit']))
		 {

			$this->form_validation->set_rules('title','','required|xss_clean');
			$this->form_validation->set_rules('area_id','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				//$img_name=$this->input->post('img_name',true);
				$title =$this->input->post('title',true);
				$area =$this->input->post('area_id',true);
				

				$input_array = array(
									
									'gen_name'=>$title,
									'area_id'=>$area
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->insertRecord('table_generation',$input_array))
							{ 
								$org_id = $this->db->insert_id();
								
								
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'pdf',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				
								

								if($_FILES['img_name']['name']!='')
								{
									if($this->upload->do_upload('img_name'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_generation',$input_array,array('id'=>$org_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								$this->session->set_flashdata('success','Info added Successfully');			
								redirect(base_url().'superadmin/generation/listFamily/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_family',$data);
	
	}

	public function addArea(){
		$data['success']=$data['error']="";
	  	$data['pagetitle']='सडवली संघटन | वाडी add करा ';

	  	 if(isset($_POST['btn_submit']))
		 {

			$this->form_validation->set_rules('title','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				//$img_name=$this->input->post('img_name',true);
				$title =$this->input->post('title',true);
				

				$input_array = array(
									
									'area_name'=>$title
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->insertRecord('table_area',$input_array))
							{ 
								$org_id = $this->db->insert_id();
								
								
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				
								

								if($_FILES['img_name']['name']!='')
								{
									if($this->upload->do_upload('img_name'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_area',$input_array,array('id'=>$org_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								$this->session->set_flashdata('success','Info added Successfully');			
								redirect(base_url().'superadmin/generation/listArea/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_area',$data);
	}


	public function editFamily(){

	  $data['success']=$data['error']="";
	  $data['pagetitle']='सडवली संघटन | भावकी edit करा ';
	  $data['area_res'] = $this->master_model->getRecords('table_area',array(),'table_area.*');
	  $front_id=base64_decode($this->uri->segment('4'));
	  
	  $villege_data =   $this->master_model->getRecords('table_generation',array('id'=>$front_id),'table_generation.*'); 
	  //print_r($villege_data);exit;
	  $data['villege'] = $villege_data;
		 if(isset($_POST['btn_submit']))
		 {

			$this->form_validation->set_rules('title','','required|xss_clean');
			$this->form_validation->set_rules('villege_id','','required|xss_clean');
			$this->form_validation->set_rules('area_id','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				
				//$img_name=$this->input->post('img_name',true);
				$title =$this->input->post('title',true);
				$area =$this->input->post('area_id',true);
				$villege_id =$this->input->post('villege_id',true);
				//$logo_url =$this->input->post('logo_url',true);
				//$type =$this->input->post('loc_type',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									
									'gen_name'=>$title,
									'area_id' => $area
									
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->updateRecord('table_generation',$input_array,array("id"=>$villege_id)))
							{ 
								
								
								
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'pdf',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				
								

								if($_FILES['img_name']['name']!='')
								{
									if($this->upload->do_upload('img_name'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_generation',$input_array,array('id'=>$villege_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								$this->session->set_flashdata('success','Info added Successfully');			
								redirect(base_url().'superadmin/generation/listFamily/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/edit_family',$data);
	
	}

	public function editArea(){

	  $data['success']=$data['error']="";
	  $data['pagetitle']='माझे गाव | ';
	  $front_id=base64_decode($this->uri->segment('4'));
	  
	  $villege_data =   $this->master_model->getRecords('table_area',array('id'=>$front_id),'table_area.*'); 
	  //print_r($villege_data);exit;
	  $data['villege'] = $villege_data;
		 if(isset($_POST['btn_submit']))
		 {

			$this->form_validation->set_rules('title','','required|xss_clean');
			$this->form_validation->set_rules('villege_id','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				
				//$img_name=$this->input->post('img_name',true);
				$title =$this->input->post('title',true);
				
				$villege_id =$this->input->post('villege_id',true);
				//$logo_url =$this->input->post('logo_url',true);
				//$type =$this->input->post('loc_type',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									
									'area_name'=>$title,
									
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->updateRecord('table_area',$input_array,array("id"=>$villege_id)))
							{ 
								
								
								
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				
								

								if($_FILES['img_name']['name']!='')
								{
									if($this->upload->do_upload('img_name'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_area',$input_array,array('id'=>$villege_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								$this->session->set_flashdata('success','Info added Successfully');			
								redirect(base_url().'superadmin/generation/listArea/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/edit_area',$data);
	
	}

	public function history()
	{


	  $data['success']=$data['error']="";
	  $data['pagetitle']='सडवली संघटन | इतिहास  ';
	  $front_id=base64_decode($this->uri->segment('4'));
	  
	  $villege_data =   $this->master_model->getRecords('table_gen_history',array('id'=>1),'table_gen_history.*'); 
	  //print_r($villege_data);exit;
	  $data['villege'] = $villege_data;
		 if(isset($_POST['btn_submit']))
		 {

			$this->form_validation->set_rules('description','','required|xss_clean');
			//$this->form_validation->set_rules('villege_id','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				
				
				$desc =$this->input->post('description',true);
				
				

				$input_array = array(
									
									
									'description' => $desc
									);
				
							if($user_info=$this->master_model->updateRecord('table_gen_history',$input_array,array("id"=>1)))
							{ 
								
								$this->session->set_flashdata('success','Info updated Successfully');			
								redirect(base_url().'superadmin/generation/history/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/gen_history',$data);
	

	  
	}

	public function deleteArea()
	{
		 $org_id=$this->input->post('org_id',true);
		 $org_id=base64_decode($org_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_area','id',$org_id))
				{ 
					$this->session->set_flashdata('success','Area Listing deleted successfully');
					//redirect(base_url().'superadmin/admin/listArtist');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		
	}

	public function deleteFamily()
	{
		 $org_id=$this->input->post('org_id',true);
		 $org_id=base64_decode($org_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_generation','id',$org_id))
				{ 
					$this->session->set_flashdata('success','Family Listing deleted successfully');
					//redirect(base_url().'superadmin/admin/listArtist');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		
	}


}
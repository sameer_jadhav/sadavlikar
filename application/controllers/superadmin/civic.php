<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Civic extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();
	   $this->load->library('upload');  
	   //$this->load->model('email_sending');	
	   
	}


	public function history()
	{


	  $data['success']=$data['error']="";
	  $data['pagetitle']='ग्रामपंचायत | इतिहास  ';
	  $front_id=base64_decode($this->uri->segment('4'));
	  
	  $villege_data =   $this->master_model->getRecords('table_gen_history',array('id'=>4),'table_gen_history.*'); 
	  //print_r($villege_data);exit;
	  $data['villege'] = $villege_data;
		 if(isset($_POST['btn_submit']))
		 {

			$this->form_validation->set_rules('description','','required|xss_clean');
			//$this->form_validation->set_rules('villege_id','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				
				
				$desc =$this->input->post('description',true);
				
				

				$input_array = array(
									
									
									'description' => $desc
									);
				
							if($user_info=$this->master_model->updateRecord('table_gen_history',$input_array,array("id"=>4)))
							{ 
								
								$this->session->set_flashdata('success','Info updated Successfully');			
								redirect(base_url().'superadmin/civic/history/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/civic_history',$data);
	 
	}


	public function listHead()
	{

	  $master_id=base64_decode($this->uri->segment('4'));
	  $data['success']=$data['error']='';	
	  $data['pagetitle']='ग्रामपंचायत | सरपंच ';
	  //$data['middle_content']='list_artist';
	 
	  $result = $this->master_model->getRecords('table_member_master as tmm',array("tmm.master_type"=>"head"),'tmm.id,tmm.name,tmm.designation,tmm.contact,tmm.address,tmm.img_name,tmm.sort_order'); 
	 // $result=$this->master_model->getRecords('table_country',array(1=>"1"),'*'); 
	  $data['master_id'] = $master_id;
	  $data['villege'] = $result;
	 //echo 123;exit;
	  $this->load->view('admin/list_member',$data);
	}

	public function listEmployee()
	{

	  $master_id=base64_decode($this->uri->segment('4'));
	  $data['success']=$data['error']='';	
	  $data['pagetitle']='ग्रामपंचायत | सरपंच ';
	  //$data['middle_content']='list_artist';
	 
	  $result = $this->master_model->getRecords('table_member_master as tmm',array("tmm.master_type"=>"employee"),'tmm.id,tmm.name,tmm.designation,tmm.contact,tmm.address,tmm.img_name,tmm.sort_order'); 
	 // $result=$this->master_model->getRecords('table_country',array(1=>"1"),'*'); 
	  $data['master_id'] = $master_id;
	  $data['villege'] = $result;
	 //echo 123;exit;
	  $this->load->view('admin/list_member',$data);
	}


	public function listPlan()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='ग्रामपंचायत |योजना   ';
	  //$data['middle_content']='list_artist';
	  $result = $this->master_model->getRecords('table_plan',array(),'table_plan.*'); 
	 // $result=$this->master_model->getRecords('table_country',array(1=>"1"),'*'); 
	  $data['villege'] = $result;
	  
	  $this->load->view('admin/list_plan',$data);
	}


	public function addHead()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='ग्रामपंचायत   | सरपंच  add करा ';
	  $master_id=$this->uri->segment('4');
	  $data['master_id'] = $master_id;
	  //print_r($master_id);exit;
	  

		 if(isset($_POST['btn_submit']))
		 {

			$this->form_validation->set_rules('name','','required|xss_clean');
			$this->form_validation->set_rules('contact','','required|xss_clean');
			$this->form_validation->set_rules('designation','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				
				//$img_name=$this->input->post('img_name',true);
				$name =$this->input->post('name',true);
				$contact =$this->input->post('contact',true);
				$designation = $this->input->post('designation',true);
				$address = $this->input->post('address',true);
				$master_id = $this->input->post('master_id',true);
				//$logo_url =$this->input->post('logo_url',true);
				//$type =$this->input->post('loc_type',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									
									'name'=>$name,
									'contact' => $contact,
									'designation'=> $designation,
									'address'=>$address,
									'master_id' => $master_id,
									'master_type' => "head"
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->insertRecord('table_member_master',$input_array))
							{ 
								$org_id = $this->db->insert_id();
								
								
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				
								

								if($_FILES['img_name']['name']!='')
								{
									if($this->upload->do_upload('img_name'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_member_master',$input_array,array('id'=>$org_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								$this->session->set_flashdata('success','Info added Successfully');			
								redirect(base_url().'superadmin/civic/listHead/'.base64_encode($master_id));
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_member',$data);
	}


	public function addEmployee()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='ग्रामपंचायत | कर्मचारी add करा ';
	  $master_id=$this->uri->segment('4');
	  $data['master_id'] = $master_id;
	  //print_r($master_id);exit;
	  

		 if(isset($_POST['btn_submit']))
		 {

			$this->form_validation->set_rules('name','','required|xss_clean');
			$this->form_validation->set_rules('contact','','required|xss_clean');
			$this->form_validation->set_rules('designation','','required|xss_clean');
			//$this->form_validation->set_rules('master_id','','required|xss_clean');
			
			
			if($this->form_validation->run())
			{
				
				//$img_name=$this->input->post('img_name',true);
				$name =$this->input->post('name',true);
				$contact =$this->input->post('contact',true);
				$designation = $this->input->post('designation',true);
				$address = $this->input->post('address',true);
				$master_id = $this->input->post('master_id',true);
				//$logo_url =$this->input->post('logo_url',true);
				//$type =$this->input->post('loc_type',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									
									'name'=>$name,
									'contact' => $contact,
									'designation'=> $designation,
									'address'=>$address,
									'master_id' => $master_id,
									'master_type' => "employee"
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->insertRecord('table_member_master',$input_array))
							{ 
								$org_id = $this->db->insert_id();
								
								
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				
								

								if($_FILES['img_name']['name']!='')
								{
									if($this->upload->do_upload('img_name'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_member_master',$input_array,array('id'=>$org_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								$this->session->set_flashdata('success','Info added Successfully');			
								redirect(base_url().'superadmin/civic/listEmployee/'.base64_encode($master_id));
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_member',$data);
	}


	public function addPlan()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='ग्रामपंचायत | योजना  add करा ';
		 if(isset($_POST['btn_submit']))
		 {

			$this->form_validation->set_rules('title','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				
				//$img_name=$this->input->post('img_name',true);
				$title =$this->input->post('title',true);
				$desc =$this->input->post('description',true);
				//$logo_url =$this->input->post('logo_url',true);
				//$type =$this->input->post('loc_type',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									
									'title'=>$title,
									'description' => $desc
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->insertRecord('table_plan',$input_array))
							{ 
								$noti_id = $this->db->insert_id();
								
								
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				
								

								if($_FILES['img_name']['name']!='')
								{
									if($this->upload->do_upload('img_name'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_plan',$input_array,array('id'=>$noti_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								$this->session->set_flashdata('success','Info added Successfully');			
								redirect(base_url().'superadmin/civic/listPlan/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_plan',$data);
	}


	public function editPlan()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='ग्रामपंचायत | योजना edit करा ';
	  $front_id=base64_decode($this->uri->segment('4'));
	  
	  $villege_data =   $this->master_model->getRecords('table_plan',array('id'=>$front_id),'table_plan.*'); 
	  $data['villege'] = $villege_data;
		 if(isset($_POST['btn_submit']))
		 {

			$this->form_validation->set_rules('title','','required|xss_clean');
			$this->form_validation->set_rules('villege_id','','required|xss_clean');
			
			if($this->form_validation->run())
			{

				$title =$this->input->post('title',true);
				$desc =$this->input->post('description',true);
				$villege_id =$this->input->post('villege_id',true);
		
				$input_array = array(
									
									'title'=>$title,
									'description' => $desc
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->updateRecord('table_plan',$input_array,array("id"=>$villege_id)))
							{ 
								
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);

								if($_FILES['img_name']['name']!='')
								{
									if($this->upload->do_upload('img_name'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_plan',$input_array,array('id'=>$villege_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								$this->session->set_flashdata('success','Info added Successfully');			
								redirect(base_url().'superadmin/civic/listPlan/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/edit_plan',$data);
	}

	public function deletePlan()
	{
		 $noti_id=$this->input->post('noti_id',true);
		 $noti_id=base64_decode($noti_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_plan','id',$noti_id))
				{ 
					$this->session->set_flashdata('success','Plan Listing deleted successfully');
					//redirect(base_url().'superadmin/admin/listArtist');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		
	}

}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Events extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();
	   $this->load->library('upload');  
	   //$this->load->model('email_sending');	
	   
	}

	public function listEvents()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='सडवली संघटन | कार्यक्रम यादी ';
	  //$data['middle_content']='list_artist';
	  //$custom_state = "GROUP BY (e.id)";
	  $result=$this->master_model->getRecords('table_event e JOIN table_files f ON f.ref_id = e.id',array(),'e.*,f.file_name',array("e.id"=>"asc"),"","","e.id"); 
	  
	  $data['media_data'] = $result;
	 
	  $this->load->view('admin/list_events',$data);
	}

	public function addEvent()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='सडवली संघटन | कार्यक्रम add करा  ';
		 if(isset($_POST['btn_submit']))
		 {


			$this->form_validation->set_rules('event_name','','required|xss_clean');
			
			if($this->form_validation->run())
			{
				
				$media_name=$this->input->post('event_name',true);
				
				
				//print_r($_FILES);exit;

				$input_array = array(
									'title' =>  $media_name
									
									);
				//print_r($_FILES);
							if($user_info=$this->master_model->insertRecord('table_event',$input_array))
							{ 
								$media_id = $this->db->insert_id();
								
								//echo $space_id;exit;
								//print_r($_FILES['project_icon_img']);exit;
								//$files = $_FILES;
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				
								
								$count_gal = count($_FILES['media_images']['name']);
								$files = $_FILES;
								if($_FILES['media_images']['name'][0]!=""){

									for($i=0; $i<$count_gal; $i++)
									{
										$config_rec=array('upload_path'=>'uploads/',
								          'allowed_types'=>'jpg|jpeg|gif|png',
								          'file_name'=>md5(rand(1,9999)),
								          //'file_name'=>$files['media_images']['name'][$i],
								          'max_size'=>10000);
							    		$this->upload->initialize($config_rec);

							    		$_FILES['media_images']['name'] = $files['media_images']['name'][$i];
								        $_FILES['media_images']['type']= $files['media_images']['type'][$i];
								        $_FILES['media_images']['tmp_name']= $files['media_images']['tmp_name'][$i];
								        $_FILES['media_images']['error']= $files['media_images']['error'][$i];
								        $_FILES['media_images']['size']= $files['media_images']['size'][$i]; 
								        if($this->upload->do_upload('media_images'))
										{
										  $dt_gallery=$this->upload->data();
										  $file_gallery[]=$dt_gallery['file_name'];

										  //$res = $this->master_model->createThumb($file,'uploads/profile/',200,200,FALSE);
										  
										}else
										{
											$this->session->set_flashdata('error',$this->upload->display_errors());
										}
									}
									//print_r($file_gallery);exit;
									for($m=0;$m<count($file_gallery);$m++)
											{
												$input_array_gal = array('ref_id'=>$media_id,'file_name'=>$file_gallery[$m]);
												$this->master_model->insertRecord('table_files',$input_array_gal)	;

											}
									
								}

								$this->session->set_flashdata('success','Event listing added Successfully');			
								redirect(base_url().'superadmin/events/listEvents/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_event',$data);
	}



	public function editEvent()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='सडवली संघटन | कार्यक्रम edit करा  ';
	  $front_id=base64_decode($this->uri->segment('4'));
	  $blog_data =   $this->master_model->getRecords('table_event',array('id'=>$front_id),'table_event.*');
	  
	  $media_data = $this->master_model->getRecords('table_files',array('ref_id'=>$front_id),'table_files.id,table_files.file_name');
	  $data['blog_data'] = $blog_data;
	  $data['media_data'] = $media_data;

		 if(isset($_POST['btn_submit']))
		 {

		 	
			$this->form_validation->set_rules('event_name','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				$event_id=$this->input->post('event_id',true);	
				$media_name=$this->input->post('event_name',true);
				

				$input_array = array(
									'title' =>  $media_name
									);
				//print_r($_FILES);
							if($user_info=$this->master_model->updateRecord('table_event',$input_array,array('id'=>$event_id)))
							{ 
								
								
								//echo $space_id;exit;
								//print_r($_FILES['project_icon_img']);exit;
								//$files = $_FILES;
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				



								$count_gal = count($_FILES['media_images']['name']);
								$files = $_FILES;
								if($_FILES['media_images']['name'][0]!=""){

									for($i=0; $i<$count_gal; $i++)
									{
										$config_rec=array('upload_path'=>'uploads/',
								          'allowed_types'=>'jpg|jpeg|gif|png',
								          'file_name'=>md5(rand(1,9999)),
								          //'file_name'=>$files['media_images']['name'][$i],
								          'max_size'=>10000);
							    		$this->upload->initialize($config_rec);

							    		$_FILES['media_images']['name'] = $files['media_images']['name'][$i];
								        $_FILES['media_images']['type']= $files['media_images']['type'][$i];
								        $_FILES['media_images']['tmp_name']= $files['media_images']['tmp_name'][$i];
								        $_FILES['media_images']['error']= $files['media_images']['error'][$i];
								        $_FILES['media_images']['size']= $files['media_images']['size'][$i]; 
								        if($this->upload->do_upload('media_images'))
										{
										  $dt_gallery=$this->upload->data();
										  $file_gallery[]=$dt_gallery['file_name'];

										  //$res = $this->master_model->createThumb($file,'uploads/profile/',200,200,FALSE);
										  
										}else
										{
											$this->session->set_flashdata('error',$this->upload->display_errors());
										}
									}
									
									for($m=0;$m<count($file_gallery);$m++)
											{
												$input_array_gal = array('ref_id'=>$event_id,'file_name'=>$file_gallery[$m]);
												$this->master_model->insertRecord('table_files',$input_array_gal)	;

											}
								}



								$this->session->set_flashdata('success','Event listing edited Successfully');			
								redirect(base_url().'superadmin/events/listEvents/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/edit_events',$data);
	}

	public function uploadImage()
	{
		$config=array('upload_path'=>'uploads/',
					  'allowed_types'=>'jpg|jpeg|gif|png',
					   'file_name'=>rand(1,9999),'max_size'=>0);
		$this->upload->initialize($config);
								
		if($_FILES['file']['name']!='')
			{
				if($this->upload->do_upload('file'))
				{
					$dt=$this->upload->data();
					$file=$dt['file_name'];
					echo base_url()."uploads/".$file;
				}else{
					
					$error=$this->upload->display_errors();
					echo $error;
					}
			}
	}


	public function deleteEvent()
	{
		 $event_id=$this->input->post('event_id',true);
		 $event_id=base64_decode($event_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_event','id',$event_id))
				{ 
					$this->session->set_flashdata('success','Event Listing deleted successfully');
					//redirect(base_url().'superadmin/admin/listArtist');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		
	}

	public function sortBlog(){
		 $blog_id=$this->input->post('blog_id',true);
		 $sort_order=$this->input->post('order',true);	
		 $update_order=array('sort_order'=>$sort_order);
		 $this->master_model->updateRecord('table_blog',$update_order,array('id'=>$blog_id));
		 //echo $this->db->last_query();

	}

}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();
	   $this->load->library('upload');  
	   //$this->load->model('email_sending');	
	   
	}

	public function listBanner()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='सडवली संघटन | List Banner';
	  //$data['middle_content']='list_artist';
	  //$custom_state = "GROUP BY (e.id)";
	  $result=$this->master_model->getRecords('table_banner',array(),'table_banner.id,table_banner.img_name',array("table_banner.id"=>"asc")); 
	  
	  $data['media_data'] = $result;
	 
	  $this->load->view('admin/list_banner',$data);

	}


	public function addBanner()
	{ 
	 

		$data['success']=$data['error']="";
	  	$data['pagetitle']='सडवली संघटन | Add Banner';
	  	 
				  	 if(isset($_POST['btn_submit']))
					 {
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				
								

								if($_FILES['img_name']['name']!='')
								{
									if($this->upload->do_upload('img_name'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->insertRecord('table_banner',$input_array);
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								$this->session->set_flashdata('success','Info added Successfully');			
								redirect(base_url().'superadmin/home/listBanner/');
							}
							
						
					
					
		  
	  
	  $this->load->view('admin/add_banner',$data);
	
	
	}



	public function editBanner()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='सडवली संघटन | Edit Banner ';
	  $front_id=base64_decode($this->uri->segment('4'));
	 
	  
	  $banner_data =   $this->master_model->getRecords('table_banner',array('id'=>$front_id),'table_banner.*'); 
	  //print_r($villege_data);exit;
	  $data['banner'] = $banner_data;

		 if(isset($_POST['btn_submit']))
		 {

								
								
								$banner_id =$this->input->post('banner_id',true);
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				
								

								if($_FILES['img_name']['name']!='')
								{
									if($this->upload->do_upload('img_name'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_banner',$input_array,array('id'=>$banner_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								$this->session->set_flashdata('success','Info added Successfully');			
								redirect(base_url().'superadmin/home/listBanner/');
							
		 }		
	  
	  $this->load->view('admin/edit_banner',$data);
	}

	public function uploadImage()
	{
		$config=array('upload_path'=>'uploads/',
					  'allowed_types'=>'jpg|jpeg|gif|png',
					   'file_name'=>rand(1,9999),'max_size'=>0);
		$this->upload->initialize($config);
								
		if($_FILES['file']['name']!='')
			{
				if($this->upload->do_upload('file'))
				{
					$dt=$this->upload->data();
					$file=$dt['file_name'];
					echo base_url()."uploads/".$file;
				}else{
					
					$error=$this->upload->display_errors();
					echo $error;
					}
			}
	}


	public function deleteBanner()
	{
		 $banner_id=$this->input->post('banner_id',true);
		 $banner_id=base64_decode($banner_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_banner','id',$banner_id))
				{ 
					$this->session->set_flashdata('success','Banner Listing deleted successfully');
					//redirect(base_url().'superadmin/admin/listArtist');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		
	}

	public function sortBlog(){
		 $blog_id=$this->input->post('blog_id',true);
		 $sort_order=$this->input->post('order',true);	
		 $update_order=array('sort_order'=>$sort_order);
		 $this->master_model->updateRecord('table_blog',$update_order,array('id'=>$blog_id));
		 //echo $this->db->last_query();

	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Blog extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();
	   $this->load->library('upload');  
	   //$this->load->model('email_sending');	
	   
	}

	public function listBlog()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='The A | List Blog';
	  //$data['middle_content']='list_artist';
	  $result=$this->master_model->getRecords('table_blog',array(),'table_blog.*'); 
	  $data['media_data'] = $result;
	 
	  $this->load->view('admin/list_blog',$data);
	}

	public function addBlog()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='The A | Add Blog';
		 if(isset($_POST['btn_submit']))
		 {


			$this->form_validation->set_rules('blog_name','','required|xss_clean');
			$this->form_validation->set_rules('blog_category','','required|xss_clean');
			$this->form_validation->set_rules('blog_desc','','required');
			$this->form_validation->set_rules('blog_slug','','required');
			//$this->form_validation->set_rules('quote_by','','required|xss_clean');
			//$this->form_validation->set_rules('image_quote','','required|xss_clean');
			
			
			if($this->form_validation->run())
			{
				
				$media_name=$this->input->post('blog_name',true);
				$media_category=$this->input->post('blog_category',true);
				$sub_category=$this->input->post('sub_category',true);
				$author=$this->input->post('author',true);
				$publish_date =$this->input->post('publish_date',true);
				$media_desc =$this->input->post('blog_desc');
				$short_desc = $this->input->post('short_desc',true);
				$blog_slug=$this->input->post('blog_slug',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									'title' =>  $media_name,
									'title' =>  $media_name,
									'description' =>  $media_desc,
									'author' =>  $author,
									'short_desc'=>$short_desc,
									'publish_date'=>$publish_date,
									'category'=>$media_category,
									'slug'=> $blog_slug,
									'sub_category'=>$sub_category
									);
				//print_r($_FILES);
							if($user_info=$this->master_model->insertRecord('table_blog',$input_array))
							{ 
								$media_id = $this->db->insert_id();
								
								//echo $space_id;exit;
								//print_r($_FILES['project_icon_img']);exit;
								//$files = $_FILES;
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				//echo "<pre>";
			    				//print_r($_FILES['project_icon_img']['name']);exit;
			    				//print_r($_FILES['media_banner_img']['name']);exit;
								
								if($_FILES['media_banner_img']['name']!='')
								{
									if($this->upload->do_upload('media_banner_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('thumbnail_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_blog',$input_array,array('id'=>$media_id));
										//echo $this->db->last_query();exit();
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}


								if($_FILES['press_banner_desktop_img']['name']!='')
								{
									if($this->upload->do_upload('press_banner_desktop_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('banner_desktop_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_blog',$input_array,array('id'=>$media_id));
										//echo $this->db->last_query();exit();
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								if($_FILES['press_banner_mobile_img']['name']!='')
								{
									if($this->upload->do_upload('press_banner_mobile_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('banner_mobile_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_blog',$input_array,array('id'=>$media_id));
										//echo $this->db->last_query();exit();
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}


								$this->session->set_flashdata('success','Blog listing added Successfully');			
								redirect(base_url().'superadmin/blog/listBlog/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_blog',$data);
	}



	public function editBlog()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='The A | Edit Blog';
	  $front_id=base64_decode($this->uri->segment('4'));
	  $blog_data =   $this->master_model->getRecords('table_blog',array('id'=>$front_id),'table_blog.*');
	  $data['blog_data'] = $blog_data;
		 if(isset($_POST['btn_submit']))
		 {


			$this->form_validation->set_rules('blog_name','','required|xss_clean');
			$this->form_validation->set_rules('blog_category','','required|xss_clean');
			$this->form_validation->set_rules('blog_desc','','required');
			$this->form_validation->set_rules('blog_slug','','required');
			//$this->form_validation->set_rules('quote_by','','required|xss_clean');
			//$this->form_validation->set_rules('image_quote','','required|xss_clean');
			
			
			if($this->form_validation->run())
			{
				$blog_id=$this->input->post('blog_id',true);	
				$media_name=$this->input->post('blog_name',true);
				$media_category=$this->input->post('blog_category',true);
				$sub_category=$this->input->post('sub_category',true);
				$author=$this->input->post('author',true);
				$publish_date =$this->input->post('publish_date',true);
				$media_desc =$this->input->post('blog_desc');
				$short_desc = $this->input->post('short_desc',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									'title' =>  $media_name,
									'description' =>  $media_desc,
									'author' =>  $author,
									'publish_date'=>$publish_date,
									'category'=>$media_category,
									'short_desc'=>$short_desc,
									'sub_category'=>$sub_category
									);
				//print_r($_FILES);
							if($user_info=$this->master_model->updateRecord('table_blog',$input_array,array('id'=>$blog_id)))
							{ 
								
								
								//echo $space_id;exit;
								//print_r($_FILES['project_icon_img']);exit;
								//$files = $_FILES;
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				//echo "<pre>";
			    				//print_r($_FILES['project_icon_img']['name']);exit;
			    				//print_r($_FILES['media_banner_img']['name']);exit;
								
								if($_FILES['media_banner_img']['name']!='')
								{
									if($this->upload->do_upload('media_banner_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('thumbnail_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_blog',$input_array,array('id'=>$blog_id));
										//echo $this->db->last_query();exit();
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}


								if($_FILES['press_banner_desktop_img']['name']!='')
								{
									if($this->upload->do_upload('press_banner_desktop_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('banner_desktop_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_blog',$input_array,array('id'=>$blog_id));
										//echo $this->db->last_query();exit();
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								if($_FILES['press_banner_mobile_img']['name']!='')
								{
									if($this->upload->do_upload('press_banner_mobile_img'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('banner_mobile_img'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_blog',$input_array,array('id'=>$blog_id));
										//echo $this->db->last_query();exit();
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}


								$this->session->set_flashdata('success','Blog listing edited Successfully');			
								redirect(base_url().'superadmin/blog/listBlog/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/edit_blog',$data);
	}

	public function uploadImage()
	{
		$config=array('upload_path'=>'uploads/',
					  'allowed_types'=>'jpg|jpeg|gif|png',
					   'file_name'=>rand(1,9999),'max_size'=>0);
		$this->upload->initialize($config);
								
		if($_FILES['file']['name']!='')
			{
				if($this->upload->do_upload('file'))
				{
					$dt=$this->upload->data();
					$file=$dt['file_name'];
					echo base_url()."uploads/".$file;
				}else{
					
					$error=$this->upload->display_errors();
					echo $error;
					}
			}
	}


	public function deleteBlog()
	{
		 $blog_id=$this->input->post('blog_id',true);
		 $blog_id=base64_decode($blog_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_blog','id',$blog_id))
				{ 
					$this->session->set_flashdata('success','Blog Listing deleted successfully');
					//redirect(base_url().'superadmin/admin/listArtist');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		
	}

	public function sortBlog(){
		 $blog_id=$this->input->post('blog_id',true);
		 $sort_order=$this->input->post('order',true);	
		 $update_order=array('sort_order'=>$sort_order);
		 $this->master_model->updateRecord('table_blog',$update_order,array('id'=>$blog_id));
		 //echo $this->db->last_query();

	}

}

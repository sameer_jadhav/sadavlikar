<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Notification extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();
	   $this->load->library('upload');  
	   //$this->load->model('email_sending');	
	   
	}

	public function listNotification()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='सडवली संघटन | सूचना यादी ';
	  //$data['middle_content']='list_artist';
	  $result = $this->master_model->getRecords('table_notification',array(),'table_notification.*'); 
	 // $result=$this->master_model->getRecords('table_country',array(1=>"1"),'*'); 
	  $data['villege'] = $result;
	 //echo 123;exit;
	  $this->load->view('admin/list_notification',$data);
	}


	public function addNotification()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='सडवली संघटन | सूचना add करा ';
		 if(isset($_POST['btn_submit']))
		 {

			$this->form_validation->set_rules('title','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				
				//$img_name=$this->input->post('img_name',true);
				$title =$this->input->post('title',true);
				$desc =$this->input->post('description',true);
				//$logo_url =$this->input->post('logo_url',true);
				//$type =$this->input->post('loc_type',true);
				
				//print_r($_FILES);exit;

				$input_array = array(
									
									'title'=>$title,
									'description' => $desc
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->insertRecord('table_notification',$input_array))
							{ 
								
								$noti_id = $this->db->insert_id();
								$this->sendNotification($noti_id);
								
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);
			    				
								

								if($_FILES['img_name']['name']!='')
								{
									if($this->upload->do_upload('img_name'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_notification',$input_array,array('id'=>$noti_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								$this->session->set_flashdata('success','Info added Successfully');			
								redirect(base_url().'superadmin/notification/listNotification/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/add_notification',$data);
	}


	public function sendNotification($noti_id){
		
	$noti_data =   $this->master_model->getRecords('table_notification',array('id'=>$noti_id),'table_notification.*'); 
	$sql_user = "select device_id from table_user where device_id !='' ";
	$query = $this->db->query($sql_user);
	$user_data = $query->result_array();
	
   define('API_ACCESS_KEY','AAAACCpdrKo:APA91bEPX6vCy1jMq2WHdsIZE3bn_lHYuDnHK-MAbtfj5-pvJdQuObyWdn0DmHHt_OzykI7ZTgfDPL33Zn3hXZvcGZzl2tsmzjRz1Ad1SBSP8dIUhW91ocxTXNg59ju_MC1HD0rdf1IY');
   $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
   $tokenList = array();
   foreach ($user_data as $key => $value) {
   	# code...
   	 	$tokenList[]=$value['device_id'];

   }
    $notification = [
            'title' =>$noti_data[0]['title'],
            'body' => strip_tags($noti_data[0]['description']),
            'click_action' => "GHADAMODI"
            
        ];
        //print_r($notification);exit;
        $extraNotificationData = ["message" => $notification,"moredata" =>'dd'];

        $fcmNotification = [
            'registration_ids' => $tokenList, //multple token array
            //'to'        => $token, //single token
            'notification' => $notification,
            'data' => $extraNotificationData
        ];

        $headers = [
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
        //print_r($result);exit;
    
	}

	public function editNotification()
	{
	  $data['success']=$data['error']="";
	  $data['pagetitle']='सडवली संघटन | सूचना edit करा ';
	  $front_id=base64_decode($this->uri->segment('4'));
	  
	  $villege_data =   $this->master_model->getRecords('table_notification',array('id'=>$front_id),'table_notification.*'); 
	  $data['villege'] = $villege_data;
		 if(isset($_POST['btn_submit']))
		 {

			$this->form_validation->set_rules('title','','required|xss_clean');
			$this->form_validation->set_rules('villege_id','','required|xss_clean');
			
			if($this->form_validation->run())
			{

				$title =$this->input->post('title',true);
				$desc =$this->input->post('description',true);
				$villege_id =$this->input->post('villege_id',true);
		
				$input_array = array(
									
									'title'=>$title,
									'description' => $desc
									);
				//print_r($input_array);exit;
							if($user_info=$this->master_model->updateRecord('table_notification',$input_array,array("id"=>$villege_id)))
							{ 
								
								$config=array('upload_path'=>'uploads/',
									          'allowed_types'=>'jpg|jpeg|gif|png',
									          'file_name'=>rand(1,9999),'max_size'=>0);
			    				$this->upload->initialize($config);

								if($_FILES['img_name']['name']!='')
								{
									if($this->upload->do_upload('img_name'))
									{
									  $dt=$this->upload->data();
									  $file=$dt['file_name'];
									  $input_array=array('img_name'=>$file);	
									$data_inserted=$this->master_model->updateRecord('table_notification',$input_array,array('id'=>$villege_id));
										
									}
									else
									{
										$file="";
										$this->session->set_flashdata('error',$this->upload->display_errors());
										$data['error']=$this->upload->display_errors();
									}
								}

								$this->session->set_flashdata('success','Info added Successfully');			
								redirect(base_url().'superadmin/notification/listNotification/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/edit_notification',$data);
	}

	public function deleteNotification()
	{
		 $noti_id=$this->input->post('noti_id',true);
		 $noti_id=base64_decode($noti_id);
		//echo $artist_id;
		//$this->master_model->updateRecord('admin_login',$input_array,array('id'=>'1'));
				if($this->master_model->deleteRecord('table_notification','id',$noti_id))
				{ 
					$this->session->set_flashdata('success','Notification Listing deleted successfully');
					//redirect(base_url().'superadmin/admin/listArtist');
				}else
				{
					$data['error']='Something went wrong ,try again later';
				}	
		
	}

}
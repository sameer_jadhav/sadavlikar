<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Villege extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();
	   $this->load->library('upload');  
	   //$this->load->model('email_sending');	
	   
	}

public function history()
	{


	  $data['success']=$data['error']="";
	  $data['pagetitle']='सडवली संघटन | माझे गाव | इतिहास  ';
	  $front_id=base64_decode($this->uri->segment('4'));
	  
	  $villege_data =   $this->master_model->getRecords('table_gen_history',array('id'=>2),'table_gen_history.*'); 
	  //print_r($villege_data);exit;
	  $data['villege'] = $villege_data;
		 if(isset($_POST['btn_submit']))
		 {

			$this->form_validation->set_rules('description','','required|xss_clean');
			//$this->form_validation->set_rules('villege_id','','required|xss_clean');
			
			
			
			if($this->form_validation->run())
			{
				
				
				$desc =$this->input->post('description',true);
				
				

				$input_array = array(
									
									
									'description' => $desc
									);
				
							if($user_info=$this->master_model->updateRecord('table_gen_history',$input_array,array("id"=>2)))
							{ 
								
								$this->session->set_flashdata('success','Info updated Successfully');			
								redirect(base_url().'superadmin/villege/history/');
							}
							else
							{
								$this->session->set_flashdata('error','Something went wrong ,try again later');
								 $data['error']='Something went wrong ,try again later';
							}
						
					}
					else
					{
						$this->session->set_flashdata('error',$this->upload->display_errors());
						$data['error']=$this->form_validation->error_string();
					}
		  }		
	  
	  $this->load->view('admin/villege_history',$data);
	 
	}


	public function listSites()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='सडवली संघटन | माझे गाव | पर्यटन स्थळे ';
	  //$data['middle_content']='list_artist';
	  $result = $this->master_model->getRecords('table_villege',array("cat_name"=>"sites"),'table_villege.*'); 
	 // $result=$this->master_model->getRecords('table_country',array(1=>"1"),'*'); 
	  $data['villege'] = $result;
	  $front_id=$this->uri->segment('3');
	  if($front_id=="listSites") { $front_id="sites" ; } 
	  $data['cat_id'] = $front_id;
	  $this->load->view('admin/list_aboutvillege',$data);
	}

	public function listSchool()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='सडवली संघटन | माझे गाव | गावची शाळा ';
	  //$data['middle_content']='list_artist';
	  $result = $this->master_model->getRecords('table_villege',array("cat_name"=>"school"),'table_villege.*'); 
	 // $result=$this->master_model->getRecords('table_country',array(1=>"1"),'*'); 
	  $data['villege'] = $result;
	  $front_id=$this->uri->segment('3');
	  if($front_id=="listSchool") { $front_id="school" ; } 
	  $data['cat_id'] = $front_id;
	  $this->load->view('admin/list_aboutvillege',$data);
	}

	public function listTemple()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='सडवली संघटन | माझे गाव | गावची मंदिरे ';
	  //$data['middle_content']='list_artist';
	  $result = $this->master_model->getRecords('table_villege',array("cat_name"=>"temple"),'table_villege.*'); 
	 // $result=$this->master_model->getRecords('table_country',array(1=>"1"),'*'); 
	  $data['villege'] = $result;
	  $front_id=$this->uri->segment('3');
	  if($front_id=="listTemple") { $front_id="temple" ; } 
	  $data['cat_id'] = $front_id;
	  $this->load->view('admin/list_aboutvillege',$data);
	}

	public function listFestival()
	{

	  $data['success']=$data['error']='';	
	  $data['pagetitle']='सडवली संघटन | माझे गाव | मुख्य सण ';
	  //$data['middle_content']='list_artist';
	  $result = $this->master_model->getRecords('table_villege',array("cat_name"=>"festival"),'table_villege.*'); 
	 // $result=$this->master_model->getRecords('table_country',array(1=>"1"),'*'); 
	  $data['villege'] = $result;
	  $front_id=$this->uri->segment('3');
	  if($front_id=="listFestival") { $front_id="festival" ; } 
	  $data['cat_id'] = $front_id;
	  $this->load->view('admin/list_aboutvillege',$data);
	}

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Civic extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        /*
        $check_auth_client = $this->MyModel->check_auth_client();
		if($check_auth_client != true){
			die($this->output->get_output());
		}
		*/
    }

    public function history()
    {
    	$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}else{
			$page=  $this->input->get("page");
					$limit=  $this->input->get("limit");
					$start_from = ($page-1) * $limit;  
					$response['status'] = 200;
		        	//$resp = $this->my_model->event_listing($page,$start_from,$limit);
		        	$resp = $this->master_model->getRecords('table_gen_history ',array("id"=>4),'table_gen_history.id,table_gen_history.description',array("table_gen_history.id"=>"asc")); 


		    for($i=0;$i<count($resp);$i++){
		    	if($resp[$i]['img_name']!=""){
		    			$resp[$i]['img_name'] = base_url().'uploads/'.$resp[$i]['img_name'];
		    	}
		    }

	
		    $output  = array(
		        					'data' => $resp,
		        					'status'=>$response['status'],
		        					'message'=>'success');
	    			json_output($response['status'],$output);    	
		}
    }

    public function listPlan()
    {
    	$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}else{
			$page=  $this->input->get("page");
					$limit=  $this->input->get("limit");
					$start_from = ($page-1) * $limit;  
					$response['status'] = 200;
		        	//$resp = $this->my_model->event_listing($page,$start_from,$limit);
		        	$resp = $this->master_model->getRecords('table_plan ',array(),'table_plan.*',array("table_plan.id"=>"desc")); 


		    for($i=0;$i<count($resp);$i++){
		    	if($resp[$i]['img_name']!=""){
		    			$resp[$i]['img_name'] = base_url().'uploads/'.$resp[$i]['img_name'];
		    	}
		    }

	
		    $output  = array(
		        					'data' => $resp,
		        					'status'=>$response['status'],
		        					'message'=>'success');
	    			json_output($response['status'],$output);    	
		}
    }


    public function listHead()
    {
    	$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}else{
			$page=  $this->input->get("page");
					$limit=  $this->input->get("limit");
					$start_from = ($page-1) * $limit;  
					$response['status'] = 200;
		        	//$resp = $this->my_model->event_listing($page,$start_from,$limit);
		        	$resp = $this->master_model->getRecords('table_member_master ',array('master_type' => "head"),'table_member_master.*',array("table_member_master.sort_order"=>"asc")); 


		    for($i=0;$i<count($resp);$i++){
		    	if($resp[$i]['img_name']!=""){
		    			$resp[$i]['img_name'] = base_url().'uploads/'.$resp[$i]['img_name'];
		    	}
		    }

	
		    $output  = array(
		        					'data' => $resp,
		        					'status'=>$response['status'],
		        					'message'=>'success');
	    			json_output($response['status'],$output);    	
		}
    }


    public function listEmployee()
    {
    	$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}else{
			$page=  $this->input->get("page");
					$limit=  $this->input->get("limit");
					$start_from = ($page-1) * $limit;  
					$response['status'] = 200;
		        	//$resp = $this->my_model->event_listing($page,$start_from,$limit);
		        	$resp = $this->master_model->getRecords('table_member_master ',array('master_type' => "employee"),'table_member_master.*',array("table_member_master.sort_order"=>"asc")); 


		    for($i=0;$i<count($resp);$i++){
		    	if($resp[$i]['img_name']!=""){
		    			$resp[$i]['img_name'] = base_url().'uploads/'.$resp[$i]['img_name'];
		    	}
		    }

	
		    $output  = array(
		        					'data' => $resp,
		        					'status'=>$response['status'],
		        					'message'=>'success');
	    			json_output($response['status'],$output);    	
		}
    }
}
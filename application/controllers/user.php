<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        /*
        $check_auth_client = $this->MyModel->check_auth_client();
		if($check_auth_client != true){
			die($this->output->get_output());
		}
		*/
    }

    public function authUser(){
    	$method = $_SERVER['REQUEST_METHOD'];
    	if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}else{
			$params = json_decode(file_get_contents('php://input'), TRUE);
			if (($params['social_id'] == "") || !isset($params['social_id']) || ($params['email_id'] == "") || !isset($params['email_id']))  {
				json_output(400,array('status' => 400,'message' => 'Bad request.'));
			}else{
				$rec_count = $this->master_model->getRecordCount('table_user ',array("facebook_id"=>$params['social_id'],"email_id"=>$params['email_id'])); 
				//print_r($this->db->last_query());
				if($rec_count>0){
					$user_data = $this->master_model->getRecords('table_user ',array("facebook_id"=>$params['social_id'],"email_id"=>$params['email_id']),'table_user.*'); 
					$respStatus = 200;
					$response['status'] = 200;
					if(filter_var($user_data[0]['img_url'], FILTER_VALIDATE_URL))
							{
								$user_data[0]['img_url'] = $user_data[0]['img_url'];
							}else{

		        				$user_data[0]['img_url'] = base_url().'uploads/user/'.$user_data[0]['img_url'];
							}
							$data_array = array(
										
										"device_id"	 => $params['device_id']
										);
							$update_user = $this->master_model->updateRecord("table_user",$data_array,array("id"=>$user_data[0]['id']));
					$output  = array(
		        					'data' => $user_data,
		        					'status'=>$response['status'],
		        					'message'=>'success');
					
				}else{
					$data_array = array(
										"facebook_id"=>$params['social_id'],
										"name"		 => $params['name'],
										"email_id"	 => $params['email_id'],
										"img_url"	 => $params['profile_img'],
										"device_id"	 => $params['device_id']
										);


					$insert_user = $this->master_model->insertRecord("table_user",$data_array);
					$response['status'] = 400;
					if($insert_user){
						$response['status'] = 201;
						$output  = array(
		        					'status'=>$response['status'],
		        					'message'=>'success');
					}
				}
				json_output($response['status'],$output);
			}
		}
    }

    public function getUser(){
    	$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}else{
			//print_r($this->uri->segment(3) );
			$id = $this->uri->segment(3);
			$rec_count = $this->master_model->getRecordCount('table_user ',array("facebook_id"=>$id)); 
			if($rec_count>0){
				$user_data = $this->master_model->getRecords('table_user ',array("facebook_id"=>$id),'table_user.*'); 

				for($i=0;$i<count($user_data);$i++){
			    	if($user_data[$i]['img_url']!=""){
			    			$user_data[$i]['img_url'] = base_url().'uploads/user/'.$user_data[$i]['img_url'];
			    	}
			     }
					$respStatus = 200;
					$response['status'] = 200;
					$output  = array(
		        					'data' => $user_data,
		        					'status'=>$response['status'],
		        					'message'=>'success');
					json_output($response['status'],$output);
				}else{
					json_output(400,array('status' => 400,'message' => 'No Data'));
				}
			
					
		}
    }

    public function updateUser(){
    	$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}else{
			$params = json_decode(file_get_contents('php://input'), TRUE);
			//echo 1222222;
			//print_r($params);exit;
			if (($params['social_id'] == "") || !isset($params['social_id']) || ($params['email_id'] == "") || !isset($params['email_id']))  {
				json_output(400,array('status' => 400,'message' => 'Bad request.'));
			}else{
				$rec_count = $this->master_model->getRecordCount('table_user ',array("facebook_id"=>$params['social_id'],"email_id"=>$params['email_id'])); 
				if($rec_count>0){
					$user_data = $this->master_model->getRecords('table_user ',array("facebook_id"=>$params['social_id'],"email_id"=>$params['email_id']),'table_user.*'); 
					$img_name = $user_data[0]['img_url'];
					$sql_query = "";
					if($params['profile_img']!=""){

						
						if (filter_var($params['profile_img'], FILTER_VALIDATE_URL) === FALSE) {
							
							$img_name = $this->upload_img($params['profile_img']);
							$sql_query = " ,img_url='".$img_name."'";
						   	
						}else{
							$img_name = $params['profile_img'];
						}	
					}
					//var_dump($img_name);exit;
					if($img_name==false){

						$err_res['status'] = 400;
						$output  = array(
		        					'data' => "",
		        					'status'=>$err_res['status'],
		        					'message'=>'not a valid base64 img'
		        					);
						json_output($err_res['status'],$output);
						
					}else{

						
					$data_array = array(
										"address"    => $params['address'],
										"contact"    => $params['contact'],
										"dob"    	 => $params['dob'],
										"anniversary"=> $params['anniversary'],
										"name"		 => $params['name'],
										"img_url"	 => $img_name
										);

					$response['status'] = 400;
					$query = "UPDATE table_user SET address= '".$params['address']."',contact= '".$params['contact']."',dob= '".$params['dob']."',anniversary= '".$params['anniversary']."',name= '".$params['name']."' ".$sql_query."  where facebook_id='".$params['social_id']."'  ";
					//echo $query;exit;
					$update_user = $this->db->query($query);

					//$update_user = $this->master_model->updateRecord("table_user",$data_array,array("facebook_id"=>$params['social_id']));
					if($update_user){
						$response['status'] = 200;
						$user_data = $this->master_model->getRecords('table_user ',array("facebook_id"=>$params['social_id'],"email_id"=>$params['email_id']),'table_user.*'); 
						
						if(filter_var($user_data[0]['img_url'], FILTER_VALIDATE_URL))
							{
								$user_data[0]['img_url'] = $user_data[0]['img_url'];
							}else{

		        				$user_data[0]['img_url'] = base_url().'uploads/user/'.$user_data[0]['img_url'];
							}
						$output  = array(
		        					'data' => $user_data,
		        					'status'=>$response['status'],
		        					'message'=>'success'
		        					);
						json_output($response['status'],$output);
					}
					}
				}else{
					json_output(400,array('status' => 400,'message' => 'No Data'));
				}
			}
		}
    }


    public function listUser(){
    	$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}else{
			$page = $this->uri->segment(3);
			$limit = 2;
			$start_from = ($page-1) * $limit;  
			$user_data = $this->master_model->getRecords('table_user ',array(),'table_user.*',array("table_user.name"=>"asc"),$start_from,$limit); 
			$response['status'] = 200;
			$output  = array(
		        					'data' => $user_data,
		        					'status'=>$response['status'],
		        					'message'=>'success'
		        					);
						json_output($response['status'],$output);
		}
    }

    public function searchUser(){
    	$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'GET' || $this->input->get("q") == ''  )
		{
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		}else{
			$query=  $this->input->get("q");
			$result=$this->master_model->getRecords('table_user',array("name like "=> "%".$query."%"),'table_user.name,table_user.contact,table_user.email_id,table_user.address,table_user.img_url'); 
			//if(count($result)> 0){

			for($i=0;$i<count($result);$i++){
		    	if($result[$i]['img_url']!="" && filter_var($result[$i]['img_url'], FILTER_VALIDATE_URL)){
		    		$result[$i]['img_url'] = $result[$i]['img_url'];	
		    	}else if($result[$i]['img_url']!="" && !filter_var($result[$i]['img_url'], FILTER_VALIDATE_URL)){
		    		$result[$i]['img_url'] = base_url().'uploads/user/'.$result[$i]['img_url'];
		    	}else{
		    		$result[$i]['img_url'] = base_url().'uploads/user/default_user.png';
		    	}

		    	//$result[$i]['img_url'] = base_url().'uploads/user/'.$result[$i]['img_url'];
		    }
				$response['status'] = 200;
				$output  = array(
		        					'data' => $result,
		        					'status'=>$response['status'],
		        					'message'=>'success'
		        					);
				
			/*}else{
				$response['status'] = 200;
				$output  = array(
		        					'data' => $result,
		        					'status'=>$response['status'],
		        					'message'=>'success'
		        					);
			}*/

			


			json_output($response['status'],$output);
		}
    }


    private function upload_img($img_name)
    {
    	
    	$data = $img_name;
		if (preg_match('/^data:image\/(\w+);base64,/', $data, $type)) {

		    $data = substr($data, strpos($data, ',') + 1);
		    $type = strtolower($type[1]); // jpg, png, gif

		    if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ])) {
		        //throw new \Exception('invalid image type');
		        return false;
		    }

		    $data = base64_decode($data);

		    if ($data === false) {
		        //throw new \Exception('base64_decode failed');
		        return false;
		    }
		} else {
			$response['status'] = 500;
			$output  = array(
		        					
		        					'status'=>$response['status'],
		        					'message'=>'not a valid base64 url
		        					');
			return false;
		   
		}
		
		$image_name = md5(uniqid(rand(), true));
		$filename = $image_name . '.' . $type;
		//rename file name with random number
		$path = "uploads/user/";
		//image uploading folder path
		file_put_contents($path . $filename, $data);
		return $filename;
	}

    

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contactus extends CI_Controller {

	public function index(){
		$this->load->model('email_sending');
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			
			
		        
					$params = json_decode(file_get_contents('php://input'), TRUE);
			//print_r($params);
			if ($params['email'] == "") {
						$respStatus = 400;
						$resp = array('status' => 400,'message' =>  'Email ID can\'t empty');

					} else if($params['g-recaptcha-response']==""){
						$respStatus = 400;
						$resp = array('status' => 400,'message' =>  'Please click on the reCAPTCHA box.');
					}else {
							$secret = '6Le7t0YUAAAAAFmxnm5LjCF_9aKOG-CYRuXFdh4q';
							$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$params['g-recaptcha-response']);
							$responseData = json_decode($verifyResponse);
							
							if($responseData->success){
							
								$respStatus = 200;
			unset($params['g-recaptcha-response']);
								$email_id = 'connect@thea.club,deepti@thea.club,';
								//$email_id="sammeer.jadhav@gmail.com,pulkit.rangwani@gmail.com";
								$info_arr=array('from'=>'noreply@thea.club','to'=>$email_id,'subject'=>'Enquiry from The A Club','view'=>'contact-us');
								$other_info=$params;
								$this->email_sending->sendmail($info_arr,$other_info);
		    	    			$resp = $this->my_model->enquiry($params);
			}else{
								$respStatus = 400;
								$resp = array('status' => 400,'message' =>  'Captcha Not Valid');
							}
		        			        		
					}
					json_output($respStatus,$resp);
		        
			
		}
	}

	public function location(){
		$method = $_SERVER['REQUEST_METHOD'];
			if($method != 'GET'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			
			
		        	$response['status'] = 200;
		        	$resp = $this->my_model->location_all_data();

		        	foreach ($resp as $key => $value) {
		        		//
		        		//print_r($value);
		        		$value->img_name = base_url().'uploads/'.$value->img_name;
		        	}
		        	
		        	$output  = array(
		        					'data' =>  $resp,
		        					'status'=>$response['status'],
		        					'message'=>'success');
	    			json_output($response['status'],$output);
			
		}
	}

	public function enquiry(){
		//echo 123;exit;
		$method = $_SERVER['REQUEST_METHOD'];

		$this->load->model('email_sending');
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			
			
		        
					$params = json_decode(file_get_contents('php://input'), TRUE);
					if ($params['email'] == "") {
						$respStatus = 400;
						$resp = array('status' => 400,'message' =>  'Email ID can\'t empty');

					} else if($params['g-recaptcha-response']==""){
						$respStatus = 400;
						$resp = array('status' => 400,'message' =>  'Please click on the reCAPTCHA box.');
					}else {
							$secret = '6Le7t0YUAAAAAFmxnm5LjCF_9aKOG-CYRuXFdh4q';
							$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$params['g-recaptcha-response']);
							$responseData = json_decode($verifyResponse);
							if($responseData->success){
								$respStatus = 200;
								unset($params['g-recaptcha-response']);
								//$email_id = 'connect@thea.club,deepti@thea.club';
								$email_id="sammeer.jadhav@gmail.com";
		    	    			$info_arr=array('from'=>'admin@thea.club','to'=>$email_id,'subject'=>'Enquiry from The A Club','view'=>'contact-us');
			 					$other_info=$params;
								$this->email_sending->sendmail($info_arr,$other_info);
		    	    			$resp = $this->my_model->enquiry($params);
		    	    			
							}else{
								$respStatus = 400;
								$resp = array('status' => 400,'message' =>  'Captcha Not Valid');
							}
		        			        		
					}
					json_output($respStatus,$resp);
		        
			
		}
	}



}